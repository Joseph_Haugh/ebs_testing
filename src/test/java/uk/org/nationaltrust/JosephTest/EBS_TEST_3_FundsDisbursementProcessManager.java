package uk.org.nationaltrust.JosephTest;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class EBS_TEST_3_FundsDisbursementProcessManager {

	public final By userName = By.id("unamebean");

	public final By password = By.id("pwdbean");

	public final By submitButton = By.id("SubmitButton");

	String webUrl = "/html/body/form/span[2]/div/div[3]/div[1]/div[2]/table[2]/tbody/tr/td/div/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/ul/li";


	public final By fundsDisbursement = By.xpath("" + webUrl + "[3]/a");

	public final By ntCashManSup = By.xpath("" + webUrl + "[4]/a");

	@Test
	public void fundsDisbursementProcessManager() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Projects\\join-frontend-tests\\browserdrivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		String loginPage = "http://str2test.dev.crm.local:8000/OA_HTML/AppsLogin";
		String currentUsername = "JHAUGH";
		String currentPassword = "Ru3by#123";

		driver.navigate().to(loginPage);
		driver.findElement(userName).sendKeys(currentUsername);
		driver.findElement(password).click();
		driver.findElement(password).clear();
		driver.findElement(password).sendKeys(currentPassword);
		driver.findElement(submitButton).click();

		driver.manage().timeouts().implicitlyWait(450, TimeUnit.MILLISECONDS);
		int i = 1;
		driver.findElement(fundsDisbursement).click();
		Thread.sleep(450);
			if (driver.findElement(fundsDisbursement).getText().equals("Funds Disbursement Process Manager"))
				;
			{
				System.out.println("======================================================================================");
				System.out.println("		'" + driver.findElement(fundsDisbursement).getText() + "' Found (" + i + "/24)");
				i++;
				driver.findElement(ntCashManSup).click();
			}
		}
	}
