package uk.org.nationaltrust.JosephTest;

import org.testng.Assert;
import org.testng.annotations.Test;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.pages.PageBase;

public class HeaderAndFooterTests extends PageBase {

	//these tests check that the HEaders and footers are pulled into the apps from the Central location via an esi

	@Test(groups = "Headers")

	public void checkDonateVueFormEsiHeadersAppear() {

		driver.navigate().to(EnvironmentConfiguration.getText("donateMapPinURL"));
		Assert.assertTrue(headersAndFooters().doesESIHeaderAppear(), "ESI Header ID not appearing when expected to appear");
		Assert.assertTrue(headersAndFooters().doesESIFooterAppear(), "ESI Footer ID not appearing when expected to appear");

	}

	@Test(groups = "Headers")
	public void checkDonatePersonalDetailsFormEsiHeadersAppear() {

		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("individualUrl"));
		Assert.assertTrue(headersAndFooters().doesESIHeaderAppear(), "ESI Header ID not appearing when expected to appear");
		Assert.assertTrue(headersAndFooters().doesESIFooterAppear(), "ESI Footer ID not appearing when expected to appear");

	}

	@Test(groups = "Headers")
	public void checkJoinEsiHeadersAppear() {

		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("individualUrl"));
		Assert.assertTrue(headersAndFooters().doesESIHeaderAppear(), "ESI Header ID not appearing when expected to appear");
		Assert.assertTrue(headersAndFooters().doesESIFooterAppear(), "ESI Footer ID not appearing when expected to appear");

	}

	@Test(groups = "Headers")
	public void checkMYNTEsiHeadersAppear() throws Exception {

		signInPage().login(EnvironmentConfiguration.getText("ContactPrefUpdateUser"), (EnvironmentConfiguration.getText("password")));
		Helpers.waitForPageLoad(driver);
		Assert.assertTrue(headersAndFooters().doesESIHeaderAppear(), "ESI Header ID not appearing when expected to appear");
		Assert.assertTrue(headersAndFooters().doesESIFooterAppear(), "ESI Footer ID not appearing when expected to appear");

	}

	@Test(groups = "Headers")
	public void checkCPCEsiHeadersAppear() {
		driver.navigate().to(CPCDMUrl);
		Assert.assertTrue(headersAndFooters().doesESIHeaderAppear(), "ESI Header ID not appearing when expected to appear");
		Assert.assertTrue(headersAndFooters().doesESIFooterAppear(), "ESI Footer ID not appearing when expected to appear");
	}

	@Test(groups = "Headers")
	public void checkRenewEsiHeadersAppear() {
		driver.get(EnvironmentConfiguration.getRenewBaseURL());
		Assert.assertTrue(headersAndFooters().doesESIHeaderAppear(), "ESI Header ID not appearing when expected to appear");
		Assert.assertTrue(headersAndFooters().doesESIFooterAppear(), "ESI Footer ID not appearing when expected to appear");
	}
}