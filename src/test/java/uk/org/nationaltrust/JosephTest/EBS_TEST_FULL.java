package uk.org.nationaltrust.JosephTest;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.sql.Time;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.lang.String;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.pages.PageBase;


public class EBS_TEST_FULL {

	public final By userName = By.id("unamebean");
	public final By password = By.id("pwdbean");
	public final By submitButton = By.id("SubmitButton");
	String webUrl = "/html/body/form/span[2]/div/div[3]/div[1]/div[2]/table[2]/tbody/tr/td/div/table/tbody/tr/td[1]/table/tbody/tr/td/div/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td/ul/li";
	public final By applicationDiagnostics = By.xpath(""+webUrl+"[1]/a");
	public final By applicationDiagnosticsDiag = By.xpath(""+webUrl+"[1]/ul/li[1]/a");
	public final By applicationDiagnosticsView = By.xpath(""+webUrl+"[1]/ul/li[2]/a");
	public final By applicationDiagnosticsConfig = By.xpath(""+webUrl+"[1]/ul/li[3]/a");
	public final By fundsCapture = By.xpath(""+webUrl+"[2]/a");
	public final By fundsCaptureHome = By.xpath(""+webUrl+"[2]/ul/li[1]/a");
	public final By fundsCaptureMaintain = By.xpath(""+webUrl+"[2]/ul/li[2]/a");
	public final By fundsCaptureVerify = By.xpath(""+webUrl+"[2]/ul/li[3]/a");
	public final By fundsDisbursement = By.xpath(""+webUrl+"[3]/a");
	public final By ntCashManSup = By.xpath(""+webUrl+"[4]/a");
	public final By ntCashManSupBankStatement = By.xpath(""+webUrl+"[4]/ul/li[1]/a");
	public final By ntCashManSupBankStatementInterface = By.xpath(""+webUrl+"[4]/ul/li[1]/ul/li[1]/a");
	public final By ntCashManSupBankStatementReconciliation = By.xpath(""+webUrl+"[4]/ul/li[1]/ul/li[2]/a");
	public final By ntCashManSupBankMaintenance = By.xpath(""+webUrl+"[4]/ul/li[2]/a");
	public final By ntCashManSupBankMaintenanceBanks = By.xpath(""+webUrl+"[4]/ul/li[2]/ul/li/a");
	public final By ntCashManSupBankMaintenanceBanksBanks = By.xpath(""+webUrl+"[4]/ul/li[2]/ul/li/ul/li[1]/a");
	public final By ntCashManSupBankMaintenanceBanksBankAccounts = By.xpath(""+webUrl+"[4]/ul/li[2]/ul/li/ul/li[2]/a");
	public final By ntCashManSupBankMaintenanceBanksBankTransactionCodes = By.xpath(""+webUrl+"[4]/ul/li[2]/ul/li/ul/li[3]/a");
	public final By ntCashManSupBankView = By.xpath(""+webUrl+"[4]/ul/li[3]/a");
	public final By ntCashManSupBankViewStatements = By.xpath(""+webUrl+"[4]/ul/li[3]/ul/li[1]/a");
	public final By ntCashManSupBankViewTransactions = By.xpath(""+webUrl+"[4]/ul/li[3]/ul/li[2]/a");
	public final By ntCashManSupBankViewPayments = By.xpath(""+webUrl+"[4]/ul/li[3]/ul/li[3]/a");
	public final By ntCashManSupBankViewReciepts = By.xpath(""+webUrl+"[4]/ul/li[3]/ul/li[4]/a");
	public final By ntCashManSupBankViewBanks = By.xpath(""+webUrl+"[4]/ul/li[3]/ul/li[5]/a");
	public final By ntCashManSupBankViewBankAccounts = By.xpath(""+webUrl+"[4]/ul/li[3]/ul/li[6]/a");
	public final By ntCashManUsr = By.xpath(""+webUrl+"[5]/a");
	public final By ntCashManUsrBankStatements = By.xpath(""+webUrl+"[5]/ul/li[1]/a");
	public final By ntCashManUsrBankStatementsInterface = By.xpath(""+webUrl+"[5]/ul/li[1]/ul/li[1]/a");
	public final By ntCashManUsrBankStatementsReconciliation = By.xpath(""+webUrl+"[5]/ul/li[1]/ul/li[2]/a");
	public final By ntCashManUsrView = By.xpath(""+webUrl+"[5]/ul/li[2]/a");
	public final By ntCashManUsrViewStatements = By.xpath(""+webUrl+"[5]/ul/li[2]/ul/li[1]/a");
	public final By ntCashManUsrViewTransactions = By.xpath(""+webUrl+"[5]/ul/li[2]/ul/li[2]/a");
	public final By ntCashManUsrViewPayments = By.xpath(""+webUrl+"[5]/ul/li[2]/ul/li[3]/a");
	public final By ntCashManUsrViewReciepts = By.xpath(""+webUrl+"[5]/ul/li[2]/ul/li[4]/a");
	public final By ntCashManUsrViewBanks = By.xpath(""+webUrl+"[5]/ul/li[2]/ul/li[5]/a");
	public final By ntCashManUsrViewBankAccounts = By.xpath(""+webUrl+"[5]/ul/li[2]/ul/li[6]/a");
	public final By ntCustomRep = By.xpath(""+webUrl+"[6]/a");
	public final By ntCustomRep0235 = By.xpath(""+webUrl+"[6]/ul/li[1]/a");
	public final By ntCustomRep0236 = By.xpath(""+webUrl+"[6]/ul/li[2]/a");
	public final By ntCustomRep0237 = By.xpath(""+webUrl+"[6]/ul/li[3]/a");
	public final By ntCustomRep0238 = By.xpath(""+webUrl+"[6]/ul/li[4]/a");
	public final By ntCustomRep0239 = By.xpath(""+webUrl+"[6]/ul/li[5]/a");
	public final By ntCustomRep0240 = By.xpath(""+webUrl+"[6]/ul/li[6]/a");
	public final By ntCustomRep0241 = By.xpath(""+webUrl+"[6]/ul/li[7]/a");
	public final By ntCustomRep0242 = By.xpath(""+webUrl+"[6]/ul/li[8]/a");
	public final By ntCustomRep0243 = By.xpath(""+webUrl+"[6]/ul/li[9]/a");
	public final By ntCustomRep0244 = By.xpath(""+webUrl+"[6]/ul/li[10]/a");
	public final By ntCustomRep0245 = By.xpath(""+webUrl+"[6]/ul/li[11]/a");
	public final By ntCustomRep0246 = By.xpath(""+webUrl+"[6]/ul/li[12]/a");
	public final By ntCustomRep0247 = By.xpath(""+webUrl+"[6]/ul/li[12]/a");
	public final By ntCustomRep0248 = By.xpath(""+webUrl+"[6]/ul/li[14]/a");
	public final By ntCustomRep0249 = By.xpath(""+webUrl+"[6]/ul/li[15]/a");
	public final By ntCustomRep0250 = By.xpath(""+webUrl+"[6]/ul/li[16]/a");
	public final By ntCustomRep0251 = By.xpath(""+webUrl+"[6]/ul/li[17]/a");
	public final By ntCustomRep0252 = By.xpath(""+webUrl+"[6]/ul/li[18]/a");
	public final By ntCustomRep0253 = By.xpath(""+webUrl+"[6]/ul/li[19]/a");
	public final By ntCustomRep0254 = By.xpath(""+webUrl+"[6]/ul/li[20]/a");
	public final By ntCustomRep0255 = By.xpath(""+webUrl+"[6]/ul/li[21]/a");
	public final By ntCustomRep0256 = By.xpath(""+webUrl+"[6]/ul/li[22]/a");
	public final By ntCustomRep0257 = By.xpath(""+webUrl+"[6]/ul/li[23]/a");
	public final By ntCustomRep0294 = By.xpath(""+webUrl+"[6]/ul/li[24]/a");
	public final By ntCustomRep0284 = By.xpath(""+webUrl+"[6]/ul/li[25]/a");
	public final By ntCustomRepViewReq = By.xpath(""+webUrl+"[6]/ul/li[26]/a");
	public final By ntCustomRepSchedReq = By.xpath(""+webUrl+"[6]/ul/li[27]/a");
	public final By ntCustomerCare = By.xpath(""+webUrl+"[7]/a");
	public final By ntCustomerCareUniversalWorkQueue = By.xpath(""+webUrl+"[7]/ul/li[1]/a");
	public final By ntCustomerCareEBS = By.xpath(""+webUrl+"[7]/ul/li[2]/a");
	public final By ntCustomerCareUniversalSearch = By.xpath(""+webUrl+"[7]/ul/li[3]/a");
	public final By ntCustomerCareServReq = By.xpath(""+webUrl+"[7]/ul/li[4]/a");
	public final By ntCustomerCareTaskMgr = By.xpath(""+webUrl+"[7]/ul/li[5]/a");
	public final By ntCustomerCareSalesOrders = By.xpath(""+webUrl+"[7]/ul/li[6]/a");
	public final By ntCustomerCareModifiers = By.xpath(""+webUrl+"[7]/ul/li[7]/a");
	public final By ntCustomerCarePriceList = By.xpath(""+webUrl+"[7]/ul/li[8]/a");
	public final By ntCustomerCarePriceListSetup = By.xpath(""+webUrl+"[7]/ul/li[8]/ul/li[1]/a");
	public final By ntCustomerCarePriceListAdd = By.xpath(""+webUrl+"[7]/ul/li[8]/ul/li[2]/a");
	public final By ntGLSup = By.xpath(""+webUrl+"[8]/a");
	public final By ntGLSupJournals = By.xpath(""+webUrl+"[8]/ul/li[1]/a");
	public final By ntGLSupJournalsEnter = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[1]/a");
	public final By ntGLSupJournalsPost = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[2]/a");
	public final By ntGLSupJournalsEncumbrance = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[3]/a");
	public final By ntGLSupJournalsLaunchJournalWizard = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[4]/a");
	public final By ntGLSupJournalsImport = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[5]/a");
	public final By ntGLSupJournalsImportRun = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[5]/ul/li[1]/a");
	public final By ntGLSupJournalsImportCorrect = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[5]/ul/li[2]/a");
	public final By ntGLSupJournalsDefine = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[6]/a");
	public final By ntGLSupJournalsDefineAllocation = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[6]/ul/li[1]/a");
	public final By ntGLSupJournalsDefineRecurring = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[6]/ul/li[2]/a");
	public final By ntGLSupJournalsGenerate = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[7]/a");
	public final By ntGLSupJournalsGenerateReversal = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[7]/ul/li[1]/a");
	public final By ntGLSupJournalsGenerateAllocation = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[7]/ul/li[2]/a");
	public final By ntGLSupJournalsGenerateRecurring = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[7]/ul/li[3]/a");
	public final By ntGLSupJournalsGenerateCarryForward = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[7]/ul/li[4]/a");
	public final By ntGLSupJournalsSchedule = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[8]/a");
	public final By ntGLSupJournalsScheduleAllocation = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[8]/ul/li[1]/a");
	public final By ntGLSupJournalsScheduleRecurring = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[8]/ul/li[2]/a");
	public final By ntGLSupJournalsAutoAllocation = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[9]/a");
	public final By ntGLSupJournalsAutoAllocationWorkbench = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[9]/ul/li[1]/a");
	public final By ntGLSupJournalsAutoAllocationViewStatus = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[9]/ul/li[2]/a");
	public final By ntGLSupJournalsReconciliation = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[10]/a");
	public final By ntGLSupJournalsReconciliationReconcile = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[10]/ul/li[1]/a");
	public final By ntGLSupJournalsReconciliationReverse = By.xpath(""+webUrl+"[8]/ul/li[1]/ul/li[10]/ul/li[2]/a");
	public final By ntGLSupBudgets = By.xpath(""+webUrl+"[8]/ul/li[2]/a");
	public final By ntGLSupBudgetsFreeze = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[1]/a");
	public final By ntGLSupBudgetsLaunch = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[2]/a");
	public final By ntGLSupBudgetsEnter = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[3]/a");
	public final By ntGLSupBudgetsEnterAmounts = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[3]/ul/li[1]/a");
	public final By ntGLSupBudgetsEnterJournals = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[3]/ul/li[2]/a");
	public final By ntGLSupBudgetsEnterTransfer = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[3]/ul/li[3]/a");
	public final By ntGLSupBudgetsEnterUpload = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[3]/ul/li[4]/a");
	public final By ntGLSupBudgetsDefine = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[4]/a");
	public final By ntGLSupBudgetsDefineBudget = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[4]/ul/li[1]/a");
	public final By ntGLSupBudgetsDefineOrg = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[4]/ul/li[2]/a");
	public final By ntGLSupBudgetsDefineMassBudget = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[4]/ul/li[3]/a");
	public final By ntGLSupBudgetsDefineFormula = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[4]/ul/li[4]/a");
	public final By ntGLSupBudgetsDefineControls = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[4]/ul/li[5]/a");
	public final By ntGLSupBudgetsGenerate = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[5]/a");
	public final By ntGLSupBudgetsGenerateMassBudget = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[5]/ul/li[1]/a");
	public final By ntGLSupBudgetsGenerateFormula = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[5]/ul/li[2]/a");
	public final By ntGLSupBudgetsSchedule = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[6]/a");
	public final By ntGLSupBudgetsScheduleMassBudget = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[6]/ul/li[1]/a");
	public final By ntGLSupBudgetsScheduleFormula = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[6]/ul/li[2]/a");
	public final By ntGLSupInquiry = By.xpath(""+webUrl+"[8]/ul/li[3]/a");
	public final By ntGLSupInquiryAccount = By.xpath(""+webUrl+"[8]/ul/li[3]/ul/li[1]/a");
	public final By ntGLSupInquiryAvg = By.xpath(""+webUrl+"[8]/ul/li[3]/ul/li[2]/a");
	public final By ntGLSupInquiryBudget = By.xpath(""+webUrl+"[8]/ul/li[3]/ul/li[3]/a");
	public final By ntGLSupInquiryJournal = By.xpath(""+webUrl+"[8]/ul/li[3]/ul/li[4]/a");
	public final By ntGLSupInquiryFunds = By.xpath(""+webUrl+"[8]/ul/li[3]/ul/li[5]/a");
	public final By ntGLSupInquiryAnalysis = By.xpath(""+webUrl+"[8]/ul/li[3]/ul/li[6]/a");
	public final By ntGLSupCurrency = By.xpath(""+webUrl+"[8]/ul/li[4]/a");
	public final By ntGLSupCurrencyRevaluation = By.xpath(""+webUrl+"[8]/ul/li[4]/ul/li[1]/a");
	public final By ntGLSupCurrencyTranslation = By.xpath(""+webUrl+"[8]/ul/li[4]/ul/li[2]/a");
	public final By ntGLSupConsolidation = By.xpath(""+webUrl+"[8]/ul/li[5]/a");
	public final By ntGLSupConsolidationWorkbench = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[1]/a");
	public final By ntGLSupConsolidationPurge = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[2]/a");
	public final By ntGLSupConsolidationDefine = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[3]/a");
	public final By ntGLSupConsolidationDefineCon = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[3]/ul/li[1]/a");
	public final By ntGLSupConsolidationDefineConSet = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[3]/ul/li[2]/a");
	public final By ntGLSupConsolidationTransfer = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[4]/a");
	public final By ntGLSupConsolidationTransferData = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[4]/ul/li[1]/a");
	public final By ntGLSupConsolidationTransferDataSet = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[4]/ul/li[2]/a");
	public final By ntGLSupConsolidationElimination = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[5]/a");
	public final By ntGLSupConsolidationEliminationDefine = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[5]/ul/li[1]/a");
	public final By ntGLSupConsolidationEliminationGenerate = By.xpath(""+webUrl+"[8]/ul/li[5]/ul/li[5]/ul/li[2]/a");
	public final By ntGLSupReports = By.xpath(""+webUrl+"[8]/ul/li[6]/a");
	public final By ntGLSupReportsAutoCopy = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[1]/a");
	public final By ntGLSupReportsRequest = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[2]/a");
	public final By ntGLSupReportsRequestFinancial = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[2]/ul/li[1]/a");
	public final By ntGLSupReportsRequestStandard = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[2]/ul/li[2]/a");
	public final By ntGLSupReportsDefine = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/a");
	public final By ntGLSupReportsDefineGenerate = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/ul/li[1]/a");
	public final By ntGLSupReportsDefineColumn = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/ul/li[2]/a");
	public final By ntGLSupReportsDefineContent = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/ul/li[3]/a");
	public final By ntGLSupReportsDefineOrder = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/ul/li[4]/a");
	public final By ntGLSupReportsDefineReport = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/ul/li[5]/a");
	public final By ntGLSupReportsDefineReportSet = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/ul/li[6]/a");
	public final By ntGLSupReportsDefineRow = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/ul/li[7]");
	public final By ntGLSupReportsDefineDisplay = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/ul/li[8]/a");
	public final By ntGLSupReportsDefineDisplaySet = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/ul/li[8]/ul/li[1]/a");
	public final By ntGLSupReportsDefineDisplayGroup = By.xpath(""+webUrl+"[8]/ul/li[6]/ul/li[3]/ul/li[8]/ul/li[2]/a");
	public final By ntGLSupGLMaintenance = By.xpath(""+webUrl+"[8]/ul/li[7]/a");
	public final By ntGLSupGLMaintenanceCOA = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[1]/a");
	public final By ntGLSupGLMaintenanceAlias = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[2]/a");
	public final By ntGLSupGLMaintenanceValueset = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[3]/a");
	public final By ntGLSupGLMaintenanceCalendar = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[4]/a");
	public final By ntGLSupGLMaintenanceCodeCombi = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[5]/a");
	public final By ntGLSupGLMaintenanceGLPeriod = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[6]/a");
	public final By ntGLSupGLMaintenancePurchasing = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[7]/a");
	public final By ntGLSupGLMaintenanceCurrency = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[8]/a");
	public final By ntGLSupGLMaintenanceCurrencyDefine = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[8]/ul/li[1]/a");
	public final By ntGLSupGLMaintenanceCurrencyManager= By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[8]/ul/li[2]/a");
	public final By ntGLSupGLMaintenanceCurrencyManagerDaily = By.xpath(""+webUrl+"[8]/ul/li[2]/ul/li[1]/a");
	public final By ntGLSupGLMaintenanceCurrencyManagerHistorical = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[8]/ul/li[2]/ul/li[2]/a");
	public final By ntGLSupGLMaintenanceCurrencyManagerTypes = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[8]/ul/li[2]/ul/li[3]/a");
	public final By ntGLSupGLMaintenanceCurrencyRates = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[8]/ul/li[3]/a");
	public final By ntGLSupGLMaintenanceCurrencyRatesDaily = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[8]/ul/li[3]/ul/li[1]");
	public final By ntGLSupGLMaintenanceCurrencyRatesHistorical = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[8]/ul/li[3]/ul/li[2]");
	public final By ntGLSupGLMaintenanceCurrencyRatesTypes = By.xpath(""+webUrl+"[8]/ul/li[7]/ul/li[8]/ul/li[3]/ul/li[3]");

	public final By ntInvSup = By.xpath(""+webUrl+"[9]/a");
	public final By ntInvSupRequests = By.xpath(""+webUrl+"[9]/ul/li[1]/a");
	public final By ntInvSupChangeOrg = By.xpath(""+webUrl+"[9]/ul/li[2]/a");
	public final By ntInvSupNotificationSummary = By.xpath(""+webUrl+"[9]/ul/li[3]/a");
	public final By ntInvSupWorkflowMonitor = By.xpath(""+webUrl+"[9]/ul/li[4]/a");
	public final By ntInvSupNotificationList = By.xpath(""+webUrl+"[9]/ul/li[5]/a");
	public final By ntInvSupWorkflowBackgroundEngine = By.xpath(""+webUrl+"[9]/ul/li[6]/a");
	public final By ntInvSupTransactions = By.xpath(""+webUrl+"[9]/ul/li[7]/a");
	public final By ntInvSupTransactionsSubinventoryTransfer = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[1]/a");
	public final By ntInvSupTransactionsInterOrgTransfer = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[2]/a");
	public final By ntInvSupTransactionsMiscTransaction = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[3]/a");
	public final By ntInvSupTransactionsMovementStats = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[4]/a");
	public final By ntInvSupTransactionsPurge = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[5]/a");
	public final By ntInvSupTransactionsMaterialTransactions = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[6]/a");
	public final By ntInvSupTransactionsTransactionSummaries = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[7]/a");
	public final By ntInvSupTransactionsMaterialDistribition = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[8]/a");
	public final By ntInvSupTransactionsPendingTransactions = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[9]/a");
	public final By ntInvSupTransactionsTransactionopenInterface = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[10]/a");
	public final By ntInvSupTransactionsBorrowPaybackTransactions = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[11]/a");
	public final By ntInvSupTransactionsPlanningTransfer = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[12]/a");
	public final By ntInvSupTransactionsViewLabelRequests = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[13]/a");
	public final By ntInvSupTransactionsConsignedTransactions = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[14]/a");
	public final By ntInvSupTransactionsReceiving = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[15]/a");
	public final By ntInvSupTransactionsReceivingReceipts = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[15]/ul/li[1]/a");
	public final By ntInvSupTransactionsReceivingReturns = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[15]/ul/li[2]/a");
	public final By ntInvSupTransactionsReceivingReceivingTransactions = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[15]/ul/li[3]/a");
	public final By ntInvSupTransactionsReceivingCorrections = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[15]/ul/li[4]/a");
	public final By ntInvSupTransactionsReceivingManageShipments = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[15]/ul/li[5]/a");
	public final By ntInvSupTransactionsReceivingViewRecievingTransactions = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[15]/ul/li[6]/a");
	public final By ntInvSupTransactionsReceivingTransactionsStatusSummary = By.xpath(""+webUrl+"[9]/ul/li[7]/ul/li[15]/ul/li[7]/a");
	public final By ntInvSupMoveOrders = By.xpath(""+webUrl+"[9]/ul/li[8]/a");
	public final By ntInvSupMoveOrdersMove = By.xpath(""+webUrl+"[9]/ul/li[8]/a");
	public final By ntInvSupMoveOrdersTransact = By.xpath(""+webUrl+"[9]/ul/li[8]/a");
	public final By ntInvSupOnHandAvailability = By.xpath(""+webUrl+"[9]/ul/li[9]/a");
	public final By ntInvSupOnHandAvailabilityOnHandQuantity = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[1]/a");
	public final By ntInvSupOnHandAvailabilityMultiOrgQuantity = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[2]/a");
	public final By ntInvSupOnHandAvailabilityResourceSupply = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[3]/a");
	public final By ntInvSupOnHandAvailabilityItemSupply = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[4]/a");
	public final By ntInvSupOnHandAvailabilityPotentialShortage = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[5]/a");
	public final By ntInvSupOnHandAvailabilityReservations = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[6]/a");
	public final By ntInvSupOnHandAvailabilityReservationsInt = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[7]/a");
	public final By ntInvSupOnHandAvailabilityReservationsIntMgr = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[8]/a");
	public final By ntInvSupOnHandAvailabilityLots = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[9]/a");
	public final By ntInvSupOnHandAvailabilitySerialNo = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[10]/a");
	public final By ntInvSupOnHandAvailabilityGenerateSerialNo = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[11]/a");
	public final By ntInvSupOnHandAvailabilityInventoryPos = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[12]/a");
	public final By ntInvSupOnHandAvailabilityInventoryPosBuild = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[12]/ul/li[1]/a");
	public final By ntInvSupOnHandAvailabilityInventoryPosDisplay = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[12]/ul/li[2]/a");
	public final By ntInvSupOnHandAvailabilityInventoryPosRequests = By.xpath(""+webUrl+"[9]/ul/li[9]/ul/li[12]/ul/li[3]/a");

	public final By ntInvSupItems = By.xpath(""+webUrl+"[9]/ul/li[10]/a");
	public final By ntInvSupItemsMaster = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsOrg = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsCrossRef = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsItemRelationships = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsItemInfo = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsItemSearch = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsPendingStatus = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsDocuments = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsDeleteItems = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsCopyAttributes = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsPartNumbers = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsCustomerItems = By.xpath(""+webUrl+"");
	public final By ntInvSupItemsImport = By.xpath(""+webUrl+"");

	public final By ntInvSupCosts = By.xpath(""+webUrl+"[9]/ul/li[11]/a");
	public final By ntInvSupCounting = By.xpath(""+webUrl+"[9]/ul/li[12]/a");
	public final By ntInvSupKanban = By.xpath(""+webUrl+"[9]/ul/li[13]/a");
	public final By ntInvSupPlanning = By.xpath(""+webUrl+"[9]/ul/li[14]/a");
	public final By ntInvSupABCCodes = By.xpath(""+webUrl+"[9]/ul/li[15]/a");
	public final By ntInvSupAccountingCloseCycle = By.xpath(""+webUrl+"[9]/ul/li[16]/a");
	public final By ntInvSupReports = By.xpath(""+webUrl+"[9]/ul/li[17]/a");




	public final By ntInvUsr = By.xpath(""+webUrl+"[10]/a");

	public final By ntOdrMgnSup = By.xpath(""+webUrl+"[11]/a");

	public final By ntPayUsr = By.xpath(""+webUrl+"[12]/a");

	public final By ntQEPSup = By.xpath(""+webUrl+"[13]/a");

	public final By ntQEPUsr = By.xpath(""+webUrl+"[14]/a");

	public final By ntRecSupUsr = By.xpath(""+webUrl+"[15]/a");

	public final By ntRecSupvNT = By.xpath(""+webUrl+"[16]/a");

	public final By ntRecSupvVTX = By.xpath(""+webUrl+"[17]/a");

	public final By ntRecUsr = By.xpath(""+webUrl+"[18]/a");

	public final By ntRegDon = By.xpath(""+webUrl+"[19]/a");

	public final By ntRenAdm = By.xpath(""+webUrl+"[20]/a");

	public final By orcCusDataLib = By.xpath(""+webUrl+"[21]/a");

	public final By odrMgrSupUsr = By.xpath(""+webUrl+"[22]/a");

	public final By recMgr = By.xpath(""+webUrl+"[23]/a");

	public final By sysAdm = By.xpath(""+webUrl+"[24]/a");

	@Test
	public void tryLogin() {

		System.setProperty("webdriver.chrome.driver", "C:\\Projects\\join-frontend-tests\\browserdrivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		String loginPage = "http://str2test.dev.crm.local:8000/OA_HTML/AppsLogin";
		String currentUsername = "JHAUGH";
		String currentPassword = "Ru3by#123";

		driver.navigate().to(loginPage);
		//driver.findElement(userName).click();
		driver.findElement(userName).sendKeys(currentUsername);
		driver.findElement(password).click();
		driver.findElement(password).clear();
		driver.findElement(password).sendKeys(currentPassword);
		driver.findElement(submitButton).click();

	}

	@Test
	public void clickAllDropdowns() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Projects\\join-frontend-tests\\browserdrivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		String loginPage = "http://str2test.dev.crm.local:8000/OA_HTML/AppsLogin";
		String currentUsername = "JHAUGH";
		String currentPassword = "Ru3by#123";

		driver.navigate().to(loginPage);
		//driver.findElement(userName).click();
		driver.findElement(userName).sendKeys(currentUsername);
		driver.findElement(password).click();
		driver.findElement(password).clear();
		driver.findElement(password).sendKeys(currentPassword);
		driver.findElement(submitButton).click();

		driver.manage().timeouts().implicitlyWait(450, TimeUnit.MILLISECONDS);
		int i = 1;
		int x = 1;
		int y =1;
		int z = 1;
		int a = 1;
		driver.findElement(applicationDiagnostics).click();
		Thread.sleep(450);
		if (driver.findElement(applicationDiagnostics).getText().equals("Application Diagnostics"))
			; {
			System.out.println(" ");
			System.out.println("======================================================================================");
			System.out.println("		'"+driver.findElement(applicationDiagnostics).getText() + "' Found ("+i+"/24)");
			x=1;
			System.out.println("				'"+driver.findElement(applicationDiagnosticsDiag).getText() + "' Found ("+x+"/3)");
			x++;
			System.out.println("				'"+driver.findElement(applicationDiagnosticsView).getText() + "' Found ("+x+"/3)");
			x++;
			System.out.println("				'"+driver.findElement(applicationDiagnosticsConfig).getText() + "' Found ("+x+"/3)");
			i++;
			driver.findElement(fundsCapture).click();
			Thread.sleep(450);


			if (driver.findElement(fundsCapture).getText().equals("Funds Capture Process Manager"))
				; {
				System.out.println("======================================================================================");
				System.out.println("		'"+driver.findElement(fundsCapture).getText() + "' Found ("+i+"/24)");
				x = 1;
				System.out.println("				'"+driver.findElement(fundsCaptureHome).getText() + "' Found ("+x+"/3)");
				x++;
				System.out.println("				'"+driver.findElement(fundsCaptureMaintain).getText() + "' Found ("+x+"/3)");
				x++;
				System.out.println("				'"+driver.findElement(fundsCaptureVerify).getText() + "' Found ("+x+"/3)");
				i++;
				driver.findElement(fundsDisbursement);


				if (driver.findElement(fundsDisbursement).getText().equals("Funds Disbursement Process Manager"))
					; {
					System.out.println("======================================================================================");
					System.out.println("		'"+driver.findElement(fundsDisbursement).getText() + "' Found ("+i+"/24)");
					i++;
					driver.findElement(ntCashManSup).click();

					if (driver.findElement(ntCashManSup).getText().equals("NT Cash Management Supervisor"))
						; {
						System.out.println("======================================================================================");
						System.out.println("		'"+driver.findElement(ntCashManSup).getText() + "' Found ("+i+"/24)");
						driver.findElement(ntCashManSupBankStatement).click();
						x=1;
						System.out.println("				'"+driver.findElement(ntCashManSupBankStatement).getText()+"' Found ("+x+"/3)");
						Thread.sleep(300);
						System.out.println("						'"+driver.findElement(ntCashManSupBankStatementInterface).getText()+"' Found ("+y+"/2)");
						y++;
						System.out.println("						'"+driver.findElement(ntCashManSupBankStatementReconciliation).getText()+"' Found ("+y+"/2)");
						x++;
						System.out.println("				'"+driver.findElement(ntCashManSupBankMaintenance).getText()+"' Found ("+x+"/3)");
						driver.findElement(ntCashManSupBankMaintenance).click();
						driver.findElement(ntCashManSupBankMaintenanceBanks).click();
						y=1;
						Thread.sleep(450);
						System.out.println("						'"+driver.findElement(ntCashManSupBankMaintenanceBanks).getText()+"' Found ("+y+"/1)");
						z=1;
						System.out.println("								'"+driver.findElement(ntCashManSupBankMaintenanceBanksBanks).getText()+"' Found ("+z+"/3)");
						z++;
						System.out.println("								'"+driver.findElement(ntCashManSupBankMaintenanceBanksBankAccounts).getText()+"' Found ("+z+"/3)");
						z++;
						System.out.println("								'"+driver.findElement(ntCashManSupBankMaintenanceBanksBankTransactionCodes).getText()+"' Found ("+z+"/3)");
						x++;
						System.out.println("				'"+driver.findElement(ntCashManSupBankView).getText()+"' Found ("+x+"/3)");
						driver.findElement(ntCashManSupBankView).click();
						z=1;
						Thread.sleep(450);
						System.out.println("						'"+driver.findElement(ntCashManSupBankViewStatements).getText()+"' Found ("+z+"/6)");
						z++;
						System.out.println("						'"+driver.findElement(ntCashManSupBankViewTransactions).getText()+"' Found ("+z+"/6)");
						z++;
						System.out.println("						'"+driver.findElement(ntCashManSupBankViewPayments).getText()+"' Found ("+z+"/6)");
						z++;
						System.out.println("						'"+driver.findElement(ntCashManSupBankViewReciepts).getText()+"' Found ("+z+"/6)");
						z++;
						System.out.println("						'"+driver.findElement(ntCashManSupBankViewBanks).getText()+"' Found ("+z+"/6)");
						z++;
						System.out.println("						'"+driver.findElement(ntCashManSupBankViewBankAccounts).getText()+"' Found ("+z+"/6)");

						i++;
						Thread.sleep(450);
						driver.findElement(ntCashManUsr).click();


						if (driver.findElement(ntCashManUsr).getText().equals("NT Cash Management User"))
							; {
							System.out.println("======================================================================================");
							System.out.println("		'"+driver.findElement(ntCashManUsr).getText() + "' Found ("+i+"/24)");
							x=1;
							System.out.println("				'"+driver.findElement(ntCashManUsrBankStatements).getText() + "' Found ("+x+"/2)");
							driver.findElement(ntCashManUsrBankStatements).click();
							Thread.sleep(450);
							y=1;
							System.out.println("						'"+driver.findElement(ntCashManUsrBankStatementsInterface).getText() + "' Found ("+y+"/2)");
							y++;
							System.out.println("						'"+driver.findElement(ntCashManUsrBankStatementsReconciliation).getText() + "' Found ("+y+"/2)");
							x++;
							System.out.println("				'"+driver.findElement(ntCashManUsrView).getText() + "' Found ("+x+"/2)");
							driver.findElement(ntCashManUsrView).click();
							Thread.sleep(450);
							y=1;
							System.out.println("						'"+driver.findElement(ntCashManUsrViewStatements).getText() + "' Found ("+y+"/6)");
							y++;
							System.out.println("						'"+driver.findElement(ntCashManUsrViewTransactions).getText() + "' Found ("+y+"/6)");
							y++;
							System.out.println("						'"+driver.findElement(ntCashManUsrViewPayments).getText() + "' Found ("+y+"/6)");
							y++;
							System.out.println("						'"+driver.findElement(ntCashManUsrViewReciepts).getText() + "' Found ("+y+"/6)");
							y++;
							System.out.println("						'"+driver.findElement(ntCashManUsrViewBanks).getText() + "' Found ("+y+"/6)");
							y++;
							System.out.println("						'"+driver.findElement(ntCashManUsrViewBankAccounts).getText() + "' Found ("+y+"/6)");

							i++;

							driver.findElement(ntCustomRep).click();
							Thread.sleep(450);
							if (driver.findElement(ntCustomRep).getText().equals("NT Custom Reports"))

								; {
								Thread.sleep(450);
								System.out.println("======================================================================================");
								System.out.println("		'"+driver.findElement(ntCustomRep).getText() + "' Found ("+i+"/24)");
								x=1;
								System.out.println("				'"+driver.findElement(ntCustomRep0235).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0236).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0237).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0238).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0239).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0240).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0241).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0242).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0243).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0244).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0245).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0246).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0247).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0248).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0249).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0250).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0251).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0252).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0253).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0254).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0255).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0256).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0257).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0294).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRep0284).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRepViewReq).getText() + "' Found ("+x+"/27)");
								x++;
								System.out.println("				'"+driver.findElement(ntCustomRepSchedReq).getText() + "' Found ("+x+"/27)");
								x++;
								i++;
								Thread.sleep(450);
								driver.findElement(ntCustomerCare).click();

								if (driver.findElement(ntCustomerCare).getText().equals("NT Customer Care"))
									; {
									System.out.println("======================================================================================");
									System.out.println("		'"+driver.findElement(ntCustomerCare).getText() + "' Found ("+i+"/24)");
									x=1;
									Thread.sleep(450);
									Thread.sleep(450);
									System.out.println("				'"+driver.findElement(ntCustomerCareUniversalWorkQueue).getText() + "' Found ("+x+"/8)");
									x++;
									System.out.println("				'"+driver.findElement(ntCustomerCareEBS).getText() + "' Found ("+x+"/8)");
									x++;
									System.out.println("				'"+driver.findElement(ntCustomerCareUniversalSearch).getText() + "' Found ("+x+"/8)");
									x++;
									System.out.println("				'"+driver.findElement(ntCustomerCareServReq).getText() + "' Found ("+x+"/8)");
									x++;
									System.out.println("				'"+driver.findElement(ntCustomerCareTaskMgr).getText() + "' Found ("+x+"/8)");
									x++;
									System.out.println("				'"+driver.findElement(ntCustomerCareSalesOrders).getText() + "' Found ("+x+"/8)");
									x++;
									System.out.println("				'"+driver.findElement(ntCustomerCareModifiers).getText() + "' Found ("+x+"/8)");
									x++;
									System.out.println("				'"+driver.findElement(ntCustomerCarePriceList).getText() + "' Found ("+x+"/8)");
									x++;
									driver.findElement(ntCustomerCarePriceList).click();
									Thread.sleep(450);
									y=1;
									System.out.println("						'"+driver.findElement(ntCustomerCarePriceListSetup).getText() + "' Found ("+y+"/2)");
									y++;
									System.out.println("						'"+driver.findElement(ntCustomerCarePriceListAdd).getText() + "' Found ("+y+"/2)");
									y++;
									i++;

									driver.findElement(ntGLSup).click();
									Thread.sleep(450);
									if (driver.findElement(ntGLSup).getText().equals("NT GL Supervisor"))
										; {
										System.out.println("======================================================================================");
										System.out.println("		'"+driver.findElement(ntGLSup).getText() + "' Found ("+i+"/24)");
										x=1;
										System.out.println("				'"+driver.findElement(ntGLSupJournals).getText() + "' Found ("+x+"/7)");
										driver.findElement(ntGLSupJournals).click();
										Thread.sleep(450);
										x++;
										y=1;
										System.out.println("						'"+driver.findElement(ntGLSupJournalsEnter).getText() + "' Found ("+y+"/10)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupJournalsPost).getText() + "' Found ("+y+"/10)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupJournalsEncumbrance).getText() + "' Found ("+y+"/10)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupJournalsLaunchJournalWizard).getText() + "' Found ("+y+"/10)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupJournalsImport).getText() + "' Found ("+y+"/10)");
										y++;
										driver.findElement(ntGLSupJournalsImport).click();
										Thread.sleep(450);
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsImportRun).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsImportCorrect).getText() + "' Found ("+z+"/2)");
										System.out.println("						'"+driver.findElement(ntGLSupJournalsDefine).getText() + "' Found ("+y+"/10)");
										y++;
										driver.findElement(ntGLSupJournalsDefine).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsDefineAllocation).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsDefineRecurring).getText() + "' Found ("+z+"/2)");
										System.out.println("						'"+driver.findElement(ntGLSupJournalsGenerate).getText() + "' Found ("+y+"/10)");
										y++;
										driver.findElement(ntGLSupJournalsGenerate).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsGenerateReversal).getText() + "' Found ("+z+"/4)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsGenerateAllocation).getText() + "' Found ("+z+"/4)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsGenerateRecurring).getText() + "' Found ("+z+"/4)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsGenerateCarryForward).getText() + "' Found ("+z+"/4)");
										System.out.println("						'"+driver.findElement(ntGLSupJournalsSchedule).getText() + "' Found ("+y+"/10)");
										y++;
										driver.findElement(ntGLSupJournalsSchedule).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsScheduleAllocation).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsScheduleRecurring).getText() + "' Found ("+z+"/2)");
										System.out.println("						'"+driver.findElement(ntGLSupJournalsAutoAllocation).getText() + "' Found ("+y+"/10)");
										y++;
										driver.findElement(ntGLSupJournalsAutoAllocation).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsAutoAllocationViewStatus).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsAutoAllocationViewStatus).getText() + "' Found ("+z+"/2)");

										System.out.println("						'"+driver.findElement(ntGLSupJournalsReconciliation).getText() + "' Found ("+y+"/10)");

										driver.findElement(ntGLSupJournalsReconciliation).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsReconciliationReconcile).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupJournalsReconciliationReverse).getText() + "' Found ("+z+"/2)");

										System.out.println("				'"+driver.findElement(ntGLSupBudgets).getText() + "' Found ("+x+"/7)");
										driver.findElement(ntGLSupBudgets).click();
										Thread.sleep(450);
										x++;
										y=1;
										System.out.println("						'"+driver.findElement(ntGLSupBudgetsFreeze).getText() + "' Found ("+y+"/6)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupBudgetsLaunch).getText() + "' Found ("+y+"/6)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupBudgetsEnter).getText() + "' Found ("+y+"/6)");
										y++;
										driver.findElement(ntGLSupBudgetsEnter).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsEnterAmounts).getText() + "' Found ("+z+"/4)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsEnterJournals).getText() + "' Found ("+z+"/4)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsEnterTransfer).getText() + "' Found ("+z+"/4)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsEnterUpload).getText() + "' Found ("+z+"/4)");

										System.out.println("						'"+driver.findElement(ntGLSupBudgetsDefine).getText() + "' Found ("+y+"/6)");
										driver.findElement(ntGLSupBudgetsDefine).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsDefineBudget).getText() + "' Found ("+z+"/5)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsDefineOrg).getText() + "' Found ("+z+"/5)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsDefineMassBudget).getText() + "' Found ("+z+"/5)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsDefineFormula).getText() + "' Found ("+z+"/5)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsDefineControls).getText() + "' Found ("+z+"/5)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupBudgetsGenerate).getText() + "' Found ("+y+"/6)");
										driver.findElement(ntGLSupBudgetsGenerate).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsGenerateMassBudget).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsGenerateFormula).getText() + "' Found ("+z+"/2)");
										z++;

										y++;
										System.out.println("						'"+driver.findElement(ntGLSupBudgetsSchedule).getText() + "' Found ("+y+"/6)");
										driver.findElement(ntGLSupBudgetsSchedule).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsScheduleMassBudget).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupBudgetsScheduleFormula).getText() + "' Found ("+z+"/2)");
										z++;

										System.out.println("				'"+driver.findElement(ntGLSupInquiry).getText() + "' Found ("+x+"/7)");
										driver.findElement(ntGLSupInquiry).click();
										Thread.sleep(450);
										x++;
										y=1;
										System.out.println("						'"+driver.findElement(ntGLSupInquiryAccount).getText() + "' Found ("+y+"/6)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupInquiryAvg).getText() + "' Found ("+y+"/6)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupInquiryBudget).getText() + "' Found ("+y+"/6)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupInquiryJournal).getText() + "' Found ("+y+"/6)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupInquiryFunds).getText() + "' Found ("+y+"/6)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupInquiryAnalysis).getText() + "' Found ("+y+"/6)");
										y++;

										System.out.println("				'"+driver.findElement(ntGLSupCurrency).getText() + "' Found ("+x+"/7)");
										driver.findElement(ntGLSupCurrency).click();
										Thread.sleep(450);
										x++;
										y=1;
										System.out.println("						'"+driver.findElement(ntGLSupCurrencyRevaluation).getText() + "' Found ("+y+"/2)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupCurrencyTranslation).getText() + "' Found ("+y+"/2)");

										System.out.println("				'"+driver.findElement(ntGLSupConsolidation).getText() + "' Found ("+x+"/7)");
										driver.findElement(ntGLSupConsolidation).click();
										Thread.sleep(450);
										x++;
										y=1;
										System.out.println("						'"+driver.findElement(ntGLSupConsolidationWorkbench).getText() + "' Found ("+y+"/6)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupConsolidationPurge).getText() + "' Found ("+y+"/6)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupConsolidationDefine).getText() + "' Found ("+y+"/6)");
										driver.findElement(ntGLSupConsolidationDefine).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupConsolidationDefineCon).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupConsolidationDefineConSet).getText() + "' Found ("+z+"/2)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupConsolidationTransfer).getText() + "' Found ("+y+"/6)");
										driver.findElement(ntGLSupConsolidationTransfer).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupConsolidationTransferData).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupConsolidationTransferDataSet).getText() + "' Found ("+z+"/2)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupConsolidationElimination).getText() + "' Found ("+y+"/6)");
										driver.findElement(ntGLSupConsolidationElimination).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupConsolidationEliminationDefine).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupConsolidationEliminationGenerate).getText() + "' Found ("+z+"/2)");
										y++;

										System.out.println("				'"+driver.findElement(ntGLSupReports).getText() + "' Found ("+x+"/7)");
										driver.findElement(ntGLSupReports).click();
										Thread.sleep(450);
										x++;
										y=1;
										System.out.println("						'"+driver.findElement(ntGLSupReportsAutoCopy).getText() + "' Found ("+y+"/3)");
										y++;
										z=1;
										System.out.println("						'"+driver.findElement(ntGLSupReportsRequest).getText() + "' Found ("+y+"/3)");
										driver.findElement(ntGLSupReportsRequest).click();
										Thread.sleep(450);
										y++;
										System.out.println("								'"+driver.findElement(ntGLSupReportsRequestFinancial).getText() + "' Found ("+z+"/2)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupReportsRequestStandard).getText() + "' Found ("+z+"/2)");
										z=1;
										System.out.println("						'"+driver.findElement(ntGLSupReportsDefine).getText() + "' Found ("+y+"/3)");
										driver.findElement(ntGLSupReportsDefine).click();
										Thread.sleep(450);
										y++;
										System.out.println("								'"+driver.findElement(ntGLSupReportsDefineGenerate).getText() + "' Found ("+z+"/8)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupReportsDefineColumn).getText() + "' Found ("+z+"/8)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupReportsDefineContent).getText() + "' Found ("+z+"/8)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupReportsDefineOrder).getText() + "' Found ("+z+"/8)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupReportsDefineReport).getText() + "' Found ("+z+"/8)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupReportsDefineReportSet).getText() + "' Found ("+z+"/8)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupReportsDefineRow).getText() + "' Found ("+z+"/8)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupReportsDefineDisplay).getText() + "' Found ("+z+"/8)");
										driver.findElement(ntGLSupReportsDefineDisplay).click();
										Thread.sleep(450);
										z++;
										System.out.println("										'"+driver.findElement(ntGLSupReportsDefineDisplaySet).getText() + "' Found ("+a+"/2)");
										a++;
										System.out.println("										'"+driver.findElement(ntGLSupReportsDefineDisplayGroup).getText() + "' Found ("+a+"/2)");
										a++;




										System.out.println("				'"+driver.findElement(ntGLSupGLMaintenance).getText() + "' Found ("+x+"/7)");
										driver.findElement(ntGLSupGLMaintenance).click();
										Thread.sleep(450);
										x++;
										y=1;
										System.out.println("						'"+driver.findElement(ntGLSupGLMaintenanceCOA).getText() + "' Found ("+y+"/8)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupGLMaintenanceAlias).getText() + "' Found ("+y+"/8)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupGLMaintenanceValueset).getText() + "' Found ("+y+"/8)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupGLMaintenanceCalendar).getText() + "' Found ("+y+"/8)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupGLMaintenanceCodeCombi).getText() + "' Found ("+y+"/8)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupGLMaintenanceGLPeriod).getText() + "' Found ("+y+"/8)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupGLMaintenancePurchasing).getText() + "' Found ("+y+"/8)");
										y++;
										System.out.println("						'"+driver.findElement(ntGLSupGLMaintenanceCurrency).getText() + "' Found ("+y+"/8)");
										y++;
										driver.findElement(ntGLSupGLMaintenanceCurrency).click();
										Thread.sleep(450);
										z=1;
										System.out.println("								'"+driver.findElement(ntGLSupGLMaintenanceCurrencyDefine).getText() + "' Found ("+z+"/3)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupGLMaintenanceCurrencyManager).getText() + "' Found ("+z+"/3)");
										driver.findElement(ntGLSupGLMaintenanceCurrencyManager).click();
										Thread.sleep(450);
										a=1;
										System.out.println("										'"+driver.findElement(ntGLSupGLMaintenanceCurrencyManagerDaily).getText() + "' Found ("+a+"/3)");
										a++;
										System.out.println("										'"+driver.findElement(ntGLSupGLMaintenanceCurrencyManagerHistorical).getText() + "' Found ("+a+"/3)");
										a++;
										System.out.println("										'"+driver.findElement(ntGLSupGLMaintenanceCurrencyManagerTypes).getText() + "' Found ("+a+"/3)");
										z++;
										System.out.println("								'"+driver.findElement(ntGLSupGLMaintenanceCurrencyRates).getText() + "' Found ("+z+"/3)");
										driver.findElement(ntGLSupGLMaintenanceCurrencyRates).click();
										Thread.sleep(450);
										a=1;
										System.out.println("										'"+driver.findElement(ntGLSupGLMaintenanceCurrencyRatesDaily).getText() + "' Found ("+a+"/3)");
										a++;
										System.out.println("										'"+driver.findElement(ntGLSupGLMaintenanceCurrencyRatesHistorical).getText() + "' Found ("+a+"/3)");
										a++;
										System.out.println("										'"+driver.findElement(ntGLSupGLMaintenanceCurrencyRatesTypes).getText() + "' Found ("+a+"/3)");


										i++;
										Thread.sleep(450);
										driver.findElement(ntInvSup).click();

										if (driver.findElement(ntInvSup).getText().equals("NT Inventory Supervisor"))
											; {
											System.out.println("======================================================================================");
											System.out.println("		'"+driver.findElement(ntInvSup).getText() + "' Found ("+i+"/24)");
											i++;
											Thread.sleep(450);
											x=1;
											System.out.println("				'"+driver.findElement(ntInvSupRequests).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupChangeOrg).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupNotificationSummary).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupWorkflowMonitor).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupNotificationList).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupWorkflowBackgroundEngine).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupTransactions).getText() + "' Found ("+x+"/17)");
											driver.findElement(ntInvSupTransactions).click();
											Thread.sleep(450);
											x++;
											z=1;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsSubinventoryTransfer).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsInterOrgTransfer).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsMiscTransaction).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsMovementStats).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsPurge).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsMaterialTransactions).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsTransactionSummaries).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsMaterialDistribition).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsPendingTransactions).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsTransactionopenInterface).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsBorrowPaybackTransactions).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsPlanningTransfer).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsViewLabelRequests).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsConsignedTransactions).getText() + "' Found ("+z+"/15)");
											z++;
											System.out.println("						'"+driver.findElement(ntInvSupTransactionsReceiving).getText() + "' Found ("+z+"/15)");
											driver.findElement(ntInvSupTransactionsReceiving).click();
											y=1;
											Thread.sleep(450);
											System.out.println("								'"+driver.findElement(ntInvSupTransactionsReceivingReceipts).getText() + "' Found ("+y+"/7)");
											y++;
											System.out.println("								'"+driver.findElement(ntInvSupTransactionsReceivingReturns).getText() + "' Found ("+y+"/7)");
											y++;
											System.out.println("								'"+driver.findElement(ntInvSupTransactionsReceivingReceivingTransactions).getText() + "' Found ("+y+"/7)");
											y++;
											System.out.println("								'"+driver.findElement(ntInvSupTransactionsReceivingCorrections).getText() + "' Found ("+y+"/7)");
											y++;
											System.out.println("								'"+driver.findElement(ntInvSupTransactionsReceivingManageShipments).getText() + "' Found ("+y+"/7)");
											y++;
											System.out.println("								'"+driver.findElement(ntInvSupTransactionsReceivingViewRecievingTransactions).getText() + "' Found ("+y+"/7)");
											y++;
											System.out.println("								'"+driver.findElement(ntInvSupTransactionsReceivingTransactionsStatusSummary).getText() + "' Found ("+y+"/7)");

											System.out.println("				'"+driver.findElement(ntInvSupMoveOrders).getText() + "' Found ("+x+"/17)");
											driver.findElement(ntInvSupMoveOrders).click();
											x++;
											y=1;
											System.out.println("						'"+driver.findElement(ntInvSupMoveOrdersMove).getText() + "' Found ("+y+"/2)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupMoveOrdersTransact).getText() + "' Found ("+y+"/2)");

											System.out.println("				'"+driver.findElement(ntInvSupOnHandAvailability).getText() + "' Found ("+x+"/17)");
											driver.findElement(ntInvSupOnHandAvailability).click();
											Thread.sleep(450);
											x++;
											y=1;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityOnHandQuantity).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityMultiOrgQuantity).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityResourceSupply).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityItemSupply).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityPotentialShortage).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityReservations).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityReservationsInt).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityReservationsIntMgr).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityLots).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilitySerialNo).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityGenerateSerialNo).getText() + "' Found ("+y+"/12)");
											y++;
											System.out.println("						'"+driver.findElement(ntInvSupOnHandAvailabilityInventoryPos).getText() + "' Found ("+y+"/12)");
											driver.findElement(ntInvSupOnHandAvailabilityInventoryPos).click();
											Thread.sleep(450);
											y++;
											z=1;
											System.out.println("								'"+driver.findElement(ntInvSupOnHandAvailabilityInventoryPosBuild).getText() + "' Found ("+z+"/3)");
											y++;
											System.out.println("								'"+driver.findElement(ntInvSupOnHandAvailabilityInventoryPosDisplay).getText() + "' Found ("+z+"/3)");
											y++;
											System.out.println("								'"+driver.findElement(ntInvSupOnHandAvailabilityInventoryPosRequests).getText() + "' Found ("+z+"/3)");
											y++;

											System.out.println("				'"+driver.findElement(ntInvSupItems).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupCosts).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupCounting).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupKanban).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupPlanning).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupABCCodes).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupAccountingCloseCycle).getText() + "' Found ("+x+"/17)");
											x++;
											System.out.println("				'"+driver.findElement(ntInvSupReports).getText() + "' Found ("+x+"/17)");





											Thread.sleep(450);
											driver.findElement(ntInvUsr).click();

											if (driver.findElement(ntInvUsr).getText().equals("NT Inventory User"))
												; {
												System.out.println("======================================================================================");
												System.out.println("		'"+driver.findElement(ntInvUsr).getText() + "' Found ("+i+"/24)");
												i++;
												Thread.sleep(450);
												driver.findElement(ntOdrMgnSup).click();

												if (driver.findElement(ntOdrMgnSup).getText().equals("NT Order Management Supervisor"))
													; {
													System.out.println("======================================================================================");
													System.out.println("		'"+driver.findElement(ntOdrMgnSup).getText() + "' Found ("+i+"/24)");
													i++;
													Thread.sleep(450);
													driver.findElement(ntPayUsr).click();

													if (driver.findElement(ntPayUsr).getText().equals("NT Payables User"))
														; {
														System.out.println("======================================================================================");
														System.out.println("		'"+driver.findElement(ntPayUsr).getText() + "' Found ("+i+"/24)");
														i++;
														Thread.sleep(450);
														driver.findElement(ntQEPSup).click();

														if (driver.findElement(ntQEPSup).getText().equals("NT QEP Supervisor"))
															; {
															System.out.println("======================================================================================");
															System.out.println("		'"+driver.findElement(ntQEPSup).getText() + "' Found ("+i+"/24)");
															i++;
															Thread.sleep(450);
															driver.findElement(ntQEPUsr).click();

															if (driver.findElement(ntQEPUsr).getText().equals("NT QEP User"))
																; {
																System.out.println("======================================================================================");
																System.out.println("		'"+driver.findElement(ntQEPUsr).getText() + "' Found ("+i+"/24)");
																i++;
																Thread.sleep(450);
																driver.findElement(ntRecSupUsr).click();

																if (driver.findElement(ntRecSupUsr).getText().equals("NT Receivables Super User"))
																	; {
																	System.out.println("======================================================================================");
																	System.out.println("		'"+driver.findElement(ntRecSupUsr).getText() + "' Found ("+i+"/24)");
																	i++;
																	Thread.sleep(450);
																	driver.findElement(ntRecSupvNT).click();

																	if (driver.findElement(ntRecSupvNT).getText().equals("NT Receivables Supervisor NT"))
																		; {
																		System.out.println("======================================================================================");
																		System.out.println("		'"+driver.findElement(ntRecSupvNT).getText() + "' Found ("+i+"/24)");
																		i++;
																		Thread.sleep(450);
																		driver.findElement(ntRecSupvVTX).click();

																		if (driver.findElement(ntRecSupvVTX).getText().equals("NT Receivables Supervisor (Vertex)"))
																			; {
																			System.out.println("======================================================================================");
																			System.out.println("		'"+driver.findElement(ntRecSupvVTX).getText() + "' Found ("+i+"/24)");
																			i++;
																			Thread.sleep(450);
																			driver.findElement(ntRecUsr).click();

																			if (driver.findElement(ntRecUsr).getText().equals("NT Receivables User"))
																				; {
																				System.out.println("======================================================================================");
																				System.out.println("		'"+driver.findElement(ntRecUsr).getText() + "' Found ("+i+"/24)");
																				i++;
																				Thread.sleep(450);
																				driver.findElement(ntRegDon).click();

																				if (driver.findElement(ntRegDon).getText().equals("NT Regular Donation Processing"))
																					; {
																					System.out.println("======================================================================================");
																					System.out.println("		'"+driver.findElement(ntRegDon).getText() + "' Found ("+i+"/24)");
																					i++;
																					Thread.sleep(450);
																					driver.findElement(ntRenAdm).click();

																					if (driver.findElement(ntRenAdm).getText().equals("NT Renewals Administrator"))
																						; {
																						System.out.println("======================================================================================");
																						System.out.println("		'"+driver.findElement(ntRenAdm).getText() + "' Found ("+i+"/24)");
																						i++;
																						Thread.sleep(450);
																						driver.findElement(orcCusDataLib);

																						if (driver.findElement(orcCusDataLib).getText().equals("Oracle Customer Data Librarian"))
																							; {
																							System.out.println("======================================================================================");
																							System.out.println("		'"+driver.findElement(orcCusDataLib).getText() + "' Found ("+i+"/24)");
																							i++;
																							Thread.sleep(450);
																							driver.findElement(odrMgrSupUsr).click();

																							if (driver.findElement(odrMgrSupUsr).getText().equals("Order Management Super User"))
																								; {
																								System.out.println("======================================================================================");
																								System.out.println("		'"+driver.findElement(ntOdrMgnSup).getText() + "' Found ("+i+"/24)");
																								i++;
																								Thread.sleep(450);
																								driver.findElement(recMgr).click();

																								if (driver.findElement(recMgr).getText().equals("Receivables Manager"))
																									; {
																									System.out.println("======================================================================================");
																									System.out.println("		'"+driver.findElement(recMgr).getText() + "' Found ("+i+"/24)");
																									i++;
																									Thread.sleep(450);

																									if (driver.findElement(sysAdm).getText().equals("System Administrator"))
																										; {
																										System.out.println("======================================================================================");
																										System.out.println("		'"+driver.findElement(sysAdm).getText() + "' Found ("+i+"/24)");
																										i++;
																										driver.findElement(sysAdm).click();
																										Thread.sleep(450);

																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		driver.quit();
	}
}


