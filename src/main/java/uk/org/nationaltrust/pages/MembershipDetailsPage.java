package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * Created by nick.thompson on 31/03/2016.
 */
public class MembershipDetailsPage extends PageBase {

	private static final By LEAD_MEMBERSHIP_SUPPORTER_NUMBER = By.cssSelector("#myDetailsSupporterNumber");

	private static final By LEAD_MEMBERSHIP_PAYMENT_TYPE = By.className("tst-my-details-next-payment-method");

	private static final By LEAD_MEMBERSHIP_RENEWAL_DATE = By.cssSelector("#myDetailsRenewalDate");

	private static final By FIRST_JOINT_MEMBERSHIP_NAME = By.cssSelector("#myDetailsName");

	private static final By SECOND_JOINT_MEMBER_NAME = By.cssSelector("#otherMember1Name");

	private static final By THIRD_JOINT_MEMBER_NAME = By.cssSelector("#otherMember2Name");

	private static final By FORTH_JOINT_MEMBER_NAME = By.cssSelector("#otherMember3Name");

	private static final By FIFTH_JOINT_MEMBER_NAME = By.cssSelector("#otherMember4Name");

	private By LEAD_MEMBERSHIP_TYPE = By.cssSelector("#myDetailsMembershipType");

	private By LEAD_MEMBER_NAME = By.cssSelector("#myDetailsName");

	public static String MEMBERSHIP_DETAIL_NAME = "//*[@id='main-content']/div/div/main/div[2]/div/div[2]/div[1]/dl/dd[1]/ul/li";

	public static String MEMBERSHIP_DETAIL_TYPE_ = "//*[@id='main-content']/div/div/main/div[2]/div/div[2]/div[1]/dl/dd[2]/ul/li";

	public static String MEMBERSHIP_DETAIL_MEM_NUMBER_ = "//*[@id='main-content']/div/div/main/div[2]/div/div[2]/div[1]/dl/dd[3]/ul/li";

	public static String MEMBERSHIP_DETAIL_RENEWAL_DATE = "//*[@id='main-content']/div/div/main/div[2]/div/div[2]/div[1]/dl/dd[4]/ul/li";

	public static String MEMBERSHIP_DETAIL_PAYMENT_METHOD = "//*[@id='main-content']/div/div/main/div[2]/div/div[2]/div[2]/dl/dd/ul/li";

	//The additional below are for the family group check linked to leadmember1@bt.com
	public static String MEMBERSHIP_DETAIL_NAME_ADDITIONAL1 = "//*[@id='main-content']/div/div/main/div[3]/div/div[2]/div/dl/dd/ul/li";

	public static String MEMBERSHIP_DETAIL_NAME_ADDITIONAL2 = "//*[@id='main-content']/div/div/main/div[3]/div/div[3]/div/dl/dd/ul/li";

	public static String MEMBERSHIP_DETAIL_NAME_ADDITIONAL3 = "//*[@id='main-content']/div/div/main/div[3]/div/div[4]/div/dl/dd/ul/li";

	public static String MEMBERSHIP_DETAIL_NAME_ADDITIONAL4 = "//*[@id='main-content']/div/div/main/div[3]/div/div[5]/div/dl/dd/ul/li";

	public static String MEMBERSHIP_DETAIL_NAME_ADDITIONAL5 = "//*[@id='main-content']/div/div/main/div[3]/div/div[6]/div/dl/dd/ul/li";

	private By MEMBERSHIP_DETAILS_HEADER_LABLE = By.cssSelector("div.contentPane>div:nth-of-type(2)>div:nth-of-type(1)>dl>dt");

	private By MEMBERSHIP_DETAILS_MY_MEMBERSHIP_HEADER_LABLE = By.cssSelector(".dashboard-card-heading>h3");

	private By MEMBERSHIP_DETAILS_ADDITIONAL_LABLE = By.cssSelector("div.small-12.columns>div:nth-of-type(3)>div>div.dashboard-card-heading>h3");

	private String MEMBERSHIP_DETAILS_MY_DETAILS_LABEL = "//*[@id='main-content']/div/div/main/div[2]/div/div[2]/div[1]/h4";

	private By MEMBERSHIP_DETAILS_PAYMENT_DETAILS_LABEL = By.cssSelector("div.contentPane>div:nth-of-type(2)>div:nth-of-type(2)>dl>dt:nth-of-type(3)");

	private String MEMBERSHIP_NAME_HEADER_LABLE = "//*[@id='main-content']/div/div/main/div[2]/div/div[2]/div[1]/dl/dt[contains( . , 'Name')]";

	public By MY_MEMBERSHIP_DETAILS_HEADER_LABLE = By.cssSelector("#tab-my-memberships > div > section > h1");

	public By MEMBERSHIP_DETAILS_BACK_BUTTON = By.cssSelector("#main-content > section > div > main > div.small-12.columns.button-container > a");

	private By MEMBERSHIP_NUMBER_HEADER_LABLE = By.cssSelector(".sub-details>span:nth-of-type(1)");

	private By MEMBERSHIP_PAYMENT_METHOD_HEADER_LABLE = By.cssSelector("#dd-management-form > div.tst-membership-303503022.tst-dd-membership > ul > li > div:nth-child(4) > div:nth-child(1) > span.nt-list__details-item-catagory.tst-my-details-payment-method-label");

	private By MEMBERSHIP_RENEWAL_DATE_HEADER_LABLE = By.cssSelector("#membership > div.nt-pod__item-content > div.c-dashboard-card__inner > div > div:nth-child(3) > ul > li > span:nth-child(1)");

	private String ADDITIONAL_MEMBER_NAME_HEADER_LABLE = "//*[@id='main-content']/div/div/main/div[3]/div/div[2]/div/dl/dt";

	private By ADDITIONAL_JOINT_MEMBER_HEADER_LABLE = By.cssSelector("div.small-12.columns>div:nth-of-type(3)>div>div>h4");

	public By VIEW_PAYMENT_DETAILS_LINK = By.cssSelector ("#tst-membership-304838623viewPaymentDetails");

	public By SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT = By.cssSelector ("#tab-my-memberships > div > div.alert-box.info > div");

	private String leadMembershipType;

	private String firstJointMembershipName;

	private String secondJointMembershipName;

	private String thirdJointMembershipName;

	private String fifthJointMembershipName;

	public By RENEWAL_BUTTON = By.className("tst-renew-btn0");

	public MembershipDetailsPage(WebDriver dr) {
		this.driver = dr;
	}

	public String getMemberShipHeaderLableText() {
		return Helpers.getText(driver, MEMBERSHIP_DETAILS_HEADER_LABLE);
	}

	public String getMyMemberShipHeaderLableText() {
		setLogs("Getting the my membership details header");
		return Helpers.getText(driver, MY_MEMBERSHIP_DETAILS_HEADER_LABLE);
	}

	public String getAdditionalMemberHeaderLableText() {
		return Helpers.getText(driver, MEMBERSHIP_DETAILS_ADDITIONAL_LABLE);
	}

	public String getMyDetailsHeaderLableText() {
		return Helpers.getText(driver, By.xpath(MEMBERSHIP_DETAILS_MY_DETAILS_LABEL));
	}

	public String getMemberShipPaymentDetailsLableText() {
		return Helpers.getText(driver, MEMBERSHIP_DETAILS_PAYMENT_DETAILS_LABEL);
	}

	public String getMemberShipNameLableText() {
		return Helpers.getText(driver, By.xpath(MEMBERSHIP_NAME_HEADER_LABLE));
	}

	public String getMemberShipTypeLableText() {
		return Helpers.getText(driver, MY_MEMBERSHIP_DETAILS_HEADER_LABLE);
	}

	public String getMemberShipNumberLableText() {
		return Helpers.getText(driver, MEMBERSHIP_NUMBER_HEADER_LABLE);
	}

	public String getMemberShipPaymentMethodLableText() {
		String memberShipPaymentMethodLable = Helpers.getText(driver, MEMBERSHIP_PAYMENT_METHOD_HEADER_LABLE);
		setLogs("Getting the membership payment method lable -- " + memberShipPaymentMethodLable);
		return memberShipPaymentMethodLable;
	}

	public String getMemberShipRenewalDateLableText() {
		String memberShipRenewalDateLable = Helpers.getText(driver, MEMBERSHIP_RENEWAL_DATE_HEADER_LABLE);
		setLogs("Getting the membership renewal date lable  -- " + memberShipRenewalDateLable);
		return memberShipRenewalDateLable;
	}

	public String getAdditionalMemberHeaders(int i) {
		Helpers.waitForElementToAppear(driver, ADDITIONAL_JOINT_MEMBER_HEADER_LABLE, 10, "Joint member header lable not displayed");
		List<WebElement> additionalMemberHeaderLable = driver.findElements(ADDITIONAL_JOINT_MEMBER_HEADER_LABLE);
		String additionalMemberHeaderText = additionalMemberHeaderLable.get(i).getText();
		setLogs("Getting additional member lable text -- " + additionalMemberHeaderText);
		return additionalMemberHeaderText;
	}

	public void clickMembershipPodMoreDetailsButton() {

		Helpers.waitForElementToAppear(driver, userPage().MEMBERSHIP_POD_MORE_DETAILS_BUTTON, 10, "more details button in membership pod not displayed");
		setLogs("Clicking more details button");
		driver.findElement(userPage().MEMBERSHIP_POD_MORE_DETAILS_BUTTON).click();
	}

	public String getLeadSupporterName() {
		Helpers.waitForElementToAppear(driver, LEAD_MEMBER_NAME, 10, "Joint member header lable not displayed");
		String leadSupporterName = driver.findElement(LEAD_MEMBER_NAME).getText();
		return leadSupporterName;

	}

	public String getLeadMembershipType() {
		Helpers.waitForElementToAppear(driver, LEAD_MEMBERSHIP_TYPE, 10, "Lead membership type wrong or not displayed");
		String leadSupporterMemberShipType = driver.findElement(LEAD_MEMBERSHIP_TYPE).getText();
		return leadSupporterMemberShipType;
	}

	public String getLeadMembershipSuppororterNumber() {
		Helpers.waitForElementToAppear(driver, LEAD_MEMBERSHIP_SUPPORTER_NUMBER, 10, "Lead supporter number is not displayed");
		String leadMembershipSupporterNumber = driver.findElement(LEAD_MEMBERSHIP_SUPPORTER_NUMBER).getText();
		return leadMembershipSupporterNumber;
	}

	public String getLeadMembershipPaymentType() {
		Helpers.waitForElementToAppear(driver, LEAD_MEMBERSHIP_PAYMENT_TYPE, 10, "Lead member header wrong or not displayed");
		String leadMembershipPaymentType = driver.findElement(LEAD_MEMBERSHIP_PAYMENT_TYPE).getText();
		return leadMembershipPaymentType;
	}

	public String getLeadMembershipRenewalDate() {
		Helpers.waitForElementToAppear(driver, LEAD_MEMBERSHIP_RENEWAL_DATE, 10, "Lead member renewal date wrong or not displayed");
		String leadMembershipRenewalDate = driver.findElement(LEAD_MEMBERSHIP_RENEWAL_DATE).getText();
		return leadMembershipRenewalDate;
	}

	public String getFirstJointMembershipName() {
		Helpers.waitForElementToAppear(driver, FIRST_JOINT_MEMBERSHIP_NAME, 10, "Lead member name wrong or not displayed");
		String firstJointMembershipName = driver.findElement(FIRST_JOINT_MEMBERSHIP_NAME).getText();
		return firstJointMembershipName;
	}

	public String getSecondJointMembershipName() {
		Helpers.waitForElementToAppear(driver, SECOND_JOINT_MEMBER_NAME, 10, "Joint member second name wrong or not displayed");
		String secondJointMembershipName = driver.findElement(SECOND_JOINT_MEMBER_NAME).getText();
		return secondJointMembershipName;
	}

	public String getThirdJointMembershipName() {
		Helpers.waitForElementToAppear(driver, THIRD_JOINT_MEMBER_NAME, 10, "Joint member header lable not displayed");
		String thirdJointMembershipName = driver.findElement(THIRD_JOINT_MEMBER_NAME).getText();
		return thirdJointMembershipName;
	}

	public String getForthJointMembershipName() {
		Helpers.waitForElementToAppear(driver, FORTH_JOINT_MEMBER_NAME, 10, "Joint member header lable not displayed");
		String forthJointMembershipName = driver.findElement(FORTH_JOINT_MEMBER_NAME).getText();
		return forthJointMembershipName;
	}

	public String getFifthJointMembershipName() {
		Helpers.waitForElementToAppear(driver, FIFTH_JOINT_MEMBER_NAME, 10, "Joint member header lable not displayed");
		String fifthJointMembershipName = driver.findElement(FIFTH_JOINT_MEMBER_NAME).getText();
		return fifthJointMembershipName;
	}

	public void clickRenewMembershipsButton() {
		driver.findElement(RENEWAL_BUTTON).click ();
	}

}

