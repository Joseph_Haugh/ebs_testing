package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * Created by Pramod.Reguri on 01/09/2016.
 */
public class AccountSettingsPage extends PageBase {

	private static final By ACCOUNT_SETTINGS_LABEL = By.cssSelector("#tab-name-and-email > form > fieldset > legend > h1");

	public By ACCOUNT_SETTINGS_BUTTON = By.id("accountSettingsButton");

	public By CHANGE_YOUR_USER_NAME_LABEL = By.cssSelector("#tab-name-and-email > form > fieldset > h2:nth-child(3)");

	private static final By CHANGE_YOUR_PASSWORD_LABEL = By.cssSelector("#tab-name-and-email > form > fieldset > h2:nth-child(6)");

	private static final By NEW_USER_NAME_LABEL = By.id("newUsernameLabel");

	private static final By CURRENT_PASSWORD_LABEL = By.cssSelector("#currentPasswordLabel");

	private static final By NEW_PASSWORD_LABEL = By.cssSelector("#newPasswordLabel");

	public By NEW_USER_NAME_TEXT_FIELD = By.cssSelector("#newUsername");


	public By CURRENT_PASSWORD_TEXT_FIELD = By.cssSelector("#currentPassword");

	public By NEW_PASSWORD_TEXT_FIELD = By.cssSelector("#newPassword");

	public By UPDATE_ACCOUNT_BUTTON = By.id("submitAccountSettings");

	public By SIGNOUT_BUTTON = By.cssSelector("#MyNTSignOut");

	public By CANCEL_AND_GO_BACK_LINK = By.cssSelector(".button.link");

	public By ACCOUNT_SETTINGS_HAMBURGER =By.cssSelector("#nt-tst-esi-header > div > div > header > a.menu-icon");

	private static final By SHOW_OR_HIDE_PASSWORD = By.cssSelector(".toggle-password > .text-hidden");
	public By ACCOUNT_SETTING_MENU = By.cssSelector ("#accountSettingsButton");
	public By ACCOUNT_SETTINGS_LINK = By.id("accountSettingsLink");
	public By PERSONAL_DETAILS_LINK = By.cssSelector ("#mainContent > section > div > main > div > form > fieldset:nth-child(1) > p > a");

	protected WebDriver driver;

	public AccountSettingsPage(WebDriver dr) {
		this.driver = dr;
	}

	public void navigateToAccountSettingPage() {
		setLogs("Navigating to account setting page");
		driver.get(baseUrlMYNT + "account#tab-name-and-email");
		Helpers.waitForElementToAppear(driver, ACCOUNT_SETTINGS_LABEL, 30, "Account setting page not loaded after 30 seconds");
	}

	public String getAccountSettingHeadingLabelText() {
//		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, ACCOUNT_SETTINGS_LABEL, 10);
		List<WebElement> accountSettingHeader = driver.findElements(ACCOUNT_SETTINGS_LABEL);
		if (accountSettingHeader.size() != 0) {
			setLogs("Getting the account setting label displayed");
			return driver.findElement(ACCOUNT_SETTINGS_LABEL).getText();
		}
		return null;
	}

	public String selectSignOutButton (){
		Helpers.waitForIsDisplayed(driver, SIGNOUT_BUTTON, 10);
		List <WebElement> signOutButton = driver.findElements(SIGNOUT_BUTTON);
		if(signOutButton.size()!=0){
			setLogs ("Getting the account Sign out Button");
			driver.findElement(SIGNOUT_BUTTON).click();
		}
		return null;
	}
	public String selectAccountSettingsButton (){
		Helpers.waitForIsDisplayed(driver, ACCOUNT_SETTINGS_BUTTON, 50);
		List <WebElement> accountSettingButton = driver.findElements(ACCOUNT_SETTINGS_BUTTON);
		if(accountSettingButton.size()!=0){
			setLogs ("Getting the account Sign out Button");
			driver.findElement(ACCOUNT_SETTINGS_BUTTON).click();
		}
		return null;
	}
	public void selectAccountSettingsHamburgerOrPawn (){
//		Helpers.waitForIsDisplayed(driver, ACCOUNT_SETTINGS_BUTTON, 50);
		List <WebElement> accountSettingButton = driver.findElements(ACCOUNT_SETTINGS_HAMBURGER);
		if(accountSettingButton.stream().filter(webElement -> webElement.isDisplayed()).count() == 1){
			setLogs ("Getting the account Sign out Button");
			driver.findElement(ACCOUNT_SETTINGS_HAMBURGER).click();
		}
		else driver.findElement(ACCOUNT_SETTINGS_BUTTON).click();
	}




	public String getAccountSettingChangeYourUserNameLabelText() {
//		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, CHANGE_YOUR_USER_NAME_LABEL, 10);
		List<WebElement> changeUserNameLabel = driver.findElements(CHANGE_YOUR_USER_NAME_LABEL);
		if (changeUserNameLabel.size() != 0) {
			setLogs("Getting the current user name label displayed");
			return driver.findElement(CHANGE_YOUR_USER_NAME_LABEL).getText();
		}
		return null;
	}

	public String getAccountSettingChangeYourPasswordLabelText() {
//		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, CHANGE_YOUR_PASSWORD_LABEL, 10);
		List<WebElement> changePasswordLabel = driver.findElements(CHANGE_YOUR_PASSWORD_LABEL);
		if (changePasswordLabel.size() != 0) {
			setLogs("Getting the change your password label displayed");
			return driver.findElement(CHANGE_YOUR_PASSWORD_LABEL).getText();
		}
		return null;
	}

	public String getNewUserNameLabelText() {
//		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, NEW_USER_NAME_LABEL, 10);
		List<WebElement> newUserNameLabel = driver.findElements(NEW_USER_NAME_LABEL);
		if (newUserNameLabel.size() != 0) {
			setLogs("Getting the new user name label displayed");
			return driver.findElement(NEW_USER_NAME_LABEL).getText();
		}
		return null;
	}

	public String getCurrentPasswordLabelText() {
//		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, CURRENT_PASSWORD_LABEL, 10);
		List<WebElement> currentPasswordLabel = driver.findElements(CURRENT_PASSWORD_LABEL);
		if (currentPasswordLabel.size() != 0) {
			setLogs("Getting the current password label displayed");
			return driver.findElement(CURRENT_PASSWORD_LABEL).getText();
		}
		return null;
	}

	public String getNewPasswordLabelText() {
//		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, NEW_PASSWORD_LABEL, 10);
		List<WebElement> newPasswordLabel = driver.findElements(NEW_PASSWORD_LABEL);
		if (newPasswordLabel.size() != 0) {
			setLogs("Getting the new password label displayed");
			return driver.findElement(NEW_PASSWORD_LABEL).getText();
		}
		return null;
	}

	public String getCurrentUserEmailIdFromNewUserNameField() {
//		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, NEW_USER_NAME_TEXT_FIELD, 10);
		List<WebElement> newUserNameTextField = driver.findElements(NEW_USER_NAME_TEXT_FIELD);
		if (newUserNameTextField.size() != 0) {
			setLogs("Getting the new password label displayed");
			return driver.findElement(NEW_USER_NAME_TEXT_FIELD).getAttribute("placeholder");
		}
		return null;
	}

	public String getCurrentUserNameFromNewUserNameField() {

		Helpers.waitForIsDisplayed(driver, NEW_USER_NAME_TEXT_FIELD, 10);
		List<WebElement> newUserNameTextField = driver.findElements(NEW_USER_NAME_TEXT_FIELD);
		if (newUserNameTextField.size() != 0) {
			setLogs("Getting the new password label displayed");
			return driver.findElement(NEW_USER_NAME_TEXT_FIELD).getAttribute("placeholder");
		}
		return null;
	}

	public String getTextFromCurrentPasswordField() {
//		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, CURRENT_PASSWORD_TEXT_FIELD, 10);
		List<WebElement> currentPasswordTextField = driver.findElements(CURRENT_PASSWORD_TEXT_FIELD);
		if (currentPasswordTextField.size() != 0) {
			setLogs("Getting the new password label displayed");
			return driver.findElement(CURRENT_PASSWORD_TEXT_FIELD).getAttribute("placeholder");
		}
		return null;
	}

	public String getTextFromNewPasswordField() {
//		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, NEW_PASSWORD_TEXT_FIELD, 10);
		List<WebElement> newPasswordTextField = driver.findElements(NEW_PASSWORD_TEXT_FIELD);
		if (newPasswordTextField.size() != 0) {
			setLogs("Getting the new password label displayed");
			return driver.findElement(NEW_PASSWORD_TEXT_FIELD).getAttribute("placeholder");
		}
		return null;
	}

	public String getTextOfUpdateAccountButton() {
//		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, UPDATE_ACCOUNT_BUTTON, 10);
		List<WebElement> updateAccountButton = driver.findElements(UPDATE_ACCOUNT_BUTTON);
		if (updateAccountButton.size() != 0) {
			setLogs("Getting the update account button text");
			return driver.findElement(UPDATE_ACCOUNT_BUTTON).getText();
		}
		return null;
	}

	public String getTextOfCancelAndGoBackLink() {
//		navigateToAccountSettingPage();
//		accountSettingsPage().selectAccountSettingsHamburgerOrPawn();
		Helpers.waitForIsDisplayed(driver, CANCEL_AND_GO_BACK_LINK, 10);
		List<WebElement> updateAccountButton = driver.findElements(CANCEL_AND_GO_BACK_LINK);
		if (updateAccountButton.size() != 0) {
			setLogs("Getting the update account button text");
			return driver.findElement(CANCEL_AND_GO_BACK_LINK).getText();
		}
		return null;
	}

	public String getHrefOfCancelAndGoBackLink() {
//		navigateToAccountSettingPage();
//		accountSettingsPage().selectAccountSettingsHamburgerOrPawn();
		Helpers.waitForIsDisplayed(driver, CANCEL_AND_GO_BACK_LINK, 10);
		List<WebElement> updateAccountButton = driver.findElements(CANCEL_AND_GO_BACK_LINK);
		if (updateAccountButton.size() != 0) {
			setLogs("Getting the cancel and go back href");
			return driver.findElement(CANCEL_AND_GO_BACK_LINK).getAttribute("href");
		}
		return null;
	}

	public String getShowOrHidePasswordText() {
		navigateToAccountSettingPage();
		Helpers.waitForIsDisplayed(driver, SHOW_OR_HIDE_PASSWORD, 10);
		List<WebElement> updateAccountButton = driver.findElements(SHOW_OR_HIDE_PASSWORD);
		if (updateAccountButton.size() != 0) {
			setLogs("Getting the show or hide text");
			String newPasswordTypeAtt = driver.findElement(NEW_PASSWORD_TEXT_FIELD).getAttribute("type");
					if(newPasswordTypeAtt == "text"){
						return  driver.findElement(SHOW_OR_HIDE_PASSWORD).getText().substring(5, 18);
					}
			return driver.findElement(SHOW_OR_HIDE_PASSWORD).getText().substring(5, 18);
		}
		return null;
	}

	public void changeUserNameInAccountSettingsPage(String userName){
//		navigateToAccountSettingPage();
		setLogs("Changing the user email");
		driver.findElement (accountSettingsPage ().NEW_USER_NAME_TEXT_FIELD).sendKeys (userName);
		setLogs("Clicking the update account button");
		driver.findElement (accountSettingsPage ().UPDATE_ACCOUNT_BUTTON).click ();
	}

	public void changePasswordInAccountSettingsPage(String currentPassword, String newPassword) {
//		navigateToAccountSettingPage();
		Helpers.waitForElementToAppear(driver, accountSettingsPage().CHANGE_YOUR_USER_NAME_LABEL, 10, "Change username heading not displayed");
		setLogs("Entering the current password in current password field");
		driver.findElement (accountSettingsPage().CURRENT_PASSWORD_TEXT_FIELD).sendKeys (currentPassword);
		setLogs("Entering the new password in the new password field");
		driver.findElement (accountSettingsPage().NEW_PASSWORD_TEXT_FIELD).sendKeys (newPassword);
		setLogs("Clicking the update account details button");
		driver.findElement (accountSettingsPage().UPDATE_ACCOUNT_BUTTON).click ();
	}
}
