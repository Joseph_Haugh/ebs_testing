package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by nick.thompson on 02/03/2017.
 */
public class DonateConfirmationPage extends PageBase {

	protected WebDriver driver;

public static final By CONFIRMATION_TEXT = By.cssSelector("#thank-you");


	public String getConfirmationTextDisplayed (){
		List<WebElement> confirmationText = driver.findElements(CONFIRMATION_TEXT);
		if (confirmationText.size() !=0){
			String confirmationTextDisplayed = driver.findElement(CONFIRMATION_TEXT).getText();
			return confirmationTextDisplayed;
		}
		return null;

	}

	public DonateConfirmationPage(WebDriver dr) {
		this.driver = dr;

}}
