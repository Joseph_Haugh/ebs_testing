package uk.org.nationaltrust.pages;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pramod.Reguri on 16/02/2016.
 */
public class SignInPage extends PageBase {

	//	public static final By SIGNIN_TAB = By.cssSelector(".tab-link>a[href*='sign-in']");
	public static final By SIGNIN_TAB = By.cssSelector("#mainContent > section > div.row.content-group.nt-palette-primary > div.medium-6.columns.right > ul > li.tab-link.active > a");

	public static final By REGISTRATION_TAB = By.cssSelector(".tab-link>a[href*='register']");

	public static final By USER_EMAIL_TEXT_FILED = By.cssSelector("#loginEmail");

	public static final By USER_PASSWORD_TEXT_FILED = By.cssSelector("input[id=loginPassword]");

	//	public static final String SIGN_BUTTON = "//*[@id='loginSubmit']";

	public By SIGN_BUTTON = By.cssSelector("#loginSubmit");

	public static final By LOGIN_HEADER = By.cssSelector("#content > section > div.row > div.small-12.columns > div > h2");

	public static final By PASSWORD_RESET_SUCCESSFUL_MESSAGE = By.cssSelector("#passwordResetInfoMsg > p");

	private static final By USE_OF_PUBLIC_COMPUTER_MESSAGE = By.cssSelector("div[id=tab2]>.panel.panel__no-margin.panel__readonly");

	private static final By HAMBURGER_BUTTON = By.cssSelector(".menu-icon > span");

	private static final By REGISTER_SIGNIN_BENIFIT_LABEL = By.cssSelector(".medium-6.columns>h3");

	private static final By REGISTER_AND_SIGN_IN_BENIFITS_MESSAGE = By.cssSelector("ul.nt-benefit-list>li");

	private static final By JOIN_US_TEXT = By.cssSelector(".medium-6.columns>p:nth-of-type(2)");

	private static final By JOIN_US_LINK = By.cssSelector(".medium-6.columns>p >a");

	public static final By ACCOUNT_SETTINGS_BUTTON = By.cssSelector("#accountSettingsButton > span.icon.icon--profile");

	public static final By ACCOUNT_SETTINGS_LINK = By.cssSelector("#accountSettingsLink");

	private static final By ACCOUNT_SETTINGS_WINDOW = By.cssSelector("#c-profile-menu");

	public final By SIGNOUT_BUTTON = By.id("MyNTSignOut");

	public static final By PERSONAL_DETAILS_TEXT = By.cssSelector("#personalDetails>.dashboard-card-heading>h3");

	public static final By SIGNIN_ERROR_MESSAGES = By.cssSelector("#tab2 > form > fieldset > div.alert-box.icon-block.danger.icon-danger > p");

	public static final By REQUEST_NEW_PASSWORD_LINK = By.cssSelector("#requestNewPassword");

	public static final By SIGN_IN_TAB = By.cssSelector("#mainContent > section > div.row.content-group.nt-palette-primary > div.medium-6.columns.right > ul > li.tab-link.active > a");


	public By SIGN_PAGE_MESSAGES = By.cssSelector("#tab2 > form > fieldset > div.alert-box.icon-block.danger.icon-danger > p");

	public By LOGOUT_TEXT = By.cssSelector(".alert-box.icon-block.info.icon-info>p");

	public By LOGOUT_AFTER_INACTIVITY_TEXT = By.cssSelector("#tab2 > form > fieldset > div.alert-box.icon-block.warning.icon-warning > p");

	public By SIGNIN_TAB_MESSAGE = By.cssSelector("");

	public By SIGN_IN_PAGE_SHOW_PASSOWRD_BUTTON_TEXT = By.cssSelector("label[for*=loginPasswordShowPasswordCheckbox]");

	public By SHOW_PASSOWRD_CHECK_BOX = By.cssSelector("#regPasswordShowPasswordCheckbox");

	public By SUCCESSFULLY_SIGNED_OUT_MESSAGE = By.cssSelector(".c-notice.c-notice__info");

    public By SIDE_NAV_ARROW = By.cssSelector(".side-nav__button");

	protected WebDriver driver;

	public SignInPage(WebDriver dr) {
		this.driver = dr;
	}
    public void clickSIDE_NAV_ARROW(){
        driver.findElement(SIDE_NAV_ARROW).click();
    }

	public void navigateToSingInPage() {
		setLogs("Navigating to signIn  page");
//		if (driver.findElements(getSignoutButton()).size() != 0) {
//			clickSignOutButton();
//			Helpers.waitForIsDisplayed(driver, SUCCESSFULLY_SIGNED_OUT_MESSAGE, 10);
//		}
		driver.navigate().to(baseUrlMYNT + "dashboard");
		Helpers.waitForIsDisplayed(driver, SIGNIN_TAB, 10);
	}

	public void login(String username, String password) throws Exception {
//		timeCheck();
		navigateToSingInPage();
		setLogs("Entering the email id -- " + username);
//		driver.get("https://secure.tst.nationaltrust.org.uk/beta/mynationaltrust/");
		Helpers.clearAndSetText(driver, USER_EMAIL_TEXT_FILED, username);
		setLogs("Entering the passowrd");
		Helpers.clearAndSetText(driver, USER_PASSWORD_TEXT_FILED, password);
		setLogs("Clicking the singIn button");
		//		driver.findElement(By.xpath(SIGN_BUTTON)).click();
		driver.findElement(SIGN_BUTTON).click();

	}
// the reason for this method is due to the Network causing a bad health on the hour every hour at 35 minutes past the hour. So this add a delay if the test tries to run between 34 and 37 minutes past
	public void timeCheck() throws Exception {
		String[] timeNow = Helpers.getTimeNow().split(":");
		if (timeNow[1].contains("33") || timeNow[1].contains("34") || timeNow[1].contains("35") || timeNow[1].contains("36")) {
			while (!Helpers.getTimeNow().contains(":37")) {
				Thread.sleep(2000);
			}
		}
	}

	private void clickSignOutButton() {
		if (driver.findElements(SIGNOUT_BUTTON).size() != 0)
			driver.findElement(SIGNOUT_BUTTON).click();
	}

	public By loggout (){
		boolean hamburgerIcon = hamburgerIconDisplay();
		if (hamburgerIcon == true) {
			clickHamBurgerIFDisplayed();
			driver.findElement(SIGNOUT_BUTTON).click();
			return SIGNOUT_BUTTON;
		} else if (hamburgerIcon == false) {
			clickAccountSettingButton();
			driver.findElement(SIGNOUT_BUTTON).click();
			return SIGNOUT_BUTTON;
		}
		return null;

	}

	public By getSignoutButton() {
		boolean hamburgerIcon = hamburgerIconDisplay();
		if (hamburgerIcon == true) {
			clickHamBurgerIFDisplayed();
			return SIGNOUT_BUTTON;
		} else if (hamburgerIcon == false) {
			clickAccountSettingButton();
			return SIGNOUT_BUTTON;
		}
		return null;
	}

	public void clickAccountSettingButton() {
		List<WebElement> accountSettingsButton = driver.findElements(ACCOUNT_SETTINGS_BUTTON);
		if (accountSettingsButton.size() != 0) {
			driver.findElement(ACCOUNT_SETTINGS_BUTTON).click();
		}
	}

	public String getShowPasswordLabelText() {
		List<WebElement> showPasswordText = driver.findElements(SIGN_IN_PAGE_SHOW_PASSOWRD_BUTTON_TEXT);
		if (showPasswordText.size() != 0) {
			return driver.findElement(SIGN_IN_PAGE_SHOW_PASSOWRD_BUTTON_TEXT).getText();
		}
		return null;
	}

	public String useOfPublicComputerMessage() {
		Helpers.waitForElementToAppear(driver, USER_EMAIL_TEXT_FILED, 10, "username field not displayed after 10 second wait");
		return driver.findElement(USE_OF_PUBLIC_COMPUTER_MESSAGE).getText();
	}

	public void clickHamBurgerIFDisplayed() {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		String dispalyed = js.executeScript("return $('.menu-icon').css('display')").toString();
		if (dispalyed.equals("block")) {
			setLogs("Clicking the hamburger icon");
			driver.findElement(HAMBURGER_BUTTON).click();
		}
	}

	public boolean hamburgerIconDisplay() {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		String dispalyed = js.executeScript("return $('.menu-icon').css('display')").toString();
		if (dispalyed.equals("block")) {
			setLogs("Hamburger icon displayed");
			return true;
		} else {
			setLogs("Hamburger icon not displayed");
			return false;
		}
	}

	public String getRegisterSingInLabelText() {
		Helpers.waitForIsDisplayed(driver, USER_EMAIL_TEXT_FILED, 10);
		String registerAndSignInBenifitHeader = driver.findElement(REGISTER_SIGNIN_BENIFIT_LABEL).getText();
		return registerAndSignInBenifitHeader;
	}

	public String getRegisterAndSignInBenifitsMessage() {
		Helpers.waitForIsDisplayed(driver, USER_EMAIL_TEXT_FILED, 10);
		List<String> message = new ArrayList<String>();
		List<WebElement> registerAndmSignInBenefitsMessage = driver.findElements(REGISTER_AND_SIGN_IN_BENIFITS_MESSAGE);
		for (int i = 0; i < registerAndmSignInBenefitsMessage.size(); i++) {
			message.add(registerAndmSignInBenefitsMessage.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

	public String getExpectedRegisterAndSignInMessage() {
		String expectedString = EnvironmentConfiguration.getMessageText("register.benefits.listHtml").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getJoinUsMessage() {
		Helpers.waitForIsDisplayed(driver, JOIN_US_TEXT, 10);
		String joinUsText = driver.findElement(JOIN_US_TEXT).getText();
		return joinUsText;
	}

	public String getJoinusLink() {
		String joinUsLink = driver.findElement(JOIN_US_LINK).getAttribute("href");
		return joinUsLink;
	}

	public String getSignInTabText (){
		Helpers.waitForElementToAppear(driver, SIGN_IN_TAB, 10, "Sign in tab not appearing");
		List<WebElement> signInTabText = driver.findElements(SIGN_IN_TAB);
		if (signInTabText.size() != 0) {
			String signInTabTextDisplayed = driver.findElement(SIGN_IN_TAB).getText();
			return signInTabTextDisplayed;
		}
		return null;
	}


	public boolean visibilityOfShowPasswordRadioButton() {
		List<WebElement> showPasswordRadioButton = driver.findElements(SHOW_PASSOWRD_CHECK_BOX);
		if (showPasswordRadioButton.size() != 0) {
			return true;
		} else {
			return false;
		}


}}
