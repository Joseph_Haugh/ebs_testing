package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * Created by nick.thompson on 21/02/2017.
 */
public class AppealDonatePage extends PageBase {

	protected WebDriver driver;

	public By APPEAL_DONATE_PAGE_HEADER = By.cssSelector("#main-content > div.nt-header-module-houses-buildings.nt-palette-primary > div.row.nt-anim-header-fadein > div > div > h1");

	public By APPEAL_MONTHLY_DONATION_TYPE_TAB = By.cssSelector("#nt-appeal-donate__tabs__regular");


	public By APPEAL_BASKET_HEADER = By.cssSelector("#main-content > div.nt-appeal-donate.nt-palette-primary > div:nth-child(2) > h4");

	public By APPEAL_BASKET_DONATE_BUTTON_TEXT = By.cssSelector("#main-content > div.nt-appeal-donate.nt-palette-primary > div.nt-appeal-donate__custom > form > button");

	public By APPEAL_CUSTOM_FIELD_HEADER = By.cssSelector("#main-content > div.nt-appeal-donate.nt-palette-primary > div.nt-appeal-donate__custom > span");



	public By APPEAL_BASKET_AMOUNT_ONE = By.xpath("//*[@id=\"donation-tab__one-off\"]/div[1]/div/div/div/span");

	public By APPEAL_BASKET_ONEOFF_AMOUNT_ONE = By.xpath("//*[@id=\"main-content\"]/div[6]/div[3]/div/div/div/span[1]");


	public By APPEAL_BASKET_AMOUNT_TWO = By.xpath("//*[@id=\"donation-tab__one-off\"]/div[2]/div/div/div/span");

	public By APPEAL_BASKET_ONEOFF_AMOUNT_TWO = By.xpath("//*[@id=\"main-content\"]/div[6]/div[4]/div/div/div/span[1]");

	public By APPEAL_BASKET_AMOUNT_THREE = By.xpath("//*[@id=\"donation-tab__one-off\"]/div[3]/div/div/div/span");

	public By APPEAL_BASKET_ONEOFF_AMOUNT_THREE = By.xpath("//*[@id=\"main-content\"]/div[6]/div[5]/div/div/div/span[1]");

	public By APPEAL_MONTHLY_BASKET_AMOUNT_ONE =By.xpath("//*[@id=\'donation-tab__regular\']/div[1]/div/div/div/span");

	public By APPEAL_MONTHLY_BASKET_AMOUNT_TWO = By.cssSelector("#donation-tab__regular > div:nth-child(2) > div > div > div > span");

	public By APPEAL_MONTHLY_BASKET_AMOUNT_THREE = By.cssSelector("#donation-tab__regular > div:nth-child(3) > div > div > div > span");

	public By DONATE_BUTTON = By.cssSelector("#main-content > div.nt-appeal-donate.nt-palette-primary > div.nt-appeal-donate__custom > form > button");

	public By APPEAL_CUSTOM_FIELD = By.cssSelector("#main-content > div.nt-appeal-donate.nt-palette-primary > div.nt-appeal-donate__custom > form > div > input");

	public static By CUSTOM_AMOUNT_FIELD_LOW_LIMIT_WARNING_TEXT = By.cssSelector("#main-content > div.nt-appeal-donate.nt-palette-primary > div.nt-appeal-donate__custom > form > div > span");

	public static By CUSTOM_FIELD_ERROR_TEXT = By.cssSelector("");

	public String getBasketButtonOneAmount() {
		Helpers.waitForElementToAppear(driver, APPEAL_BASKET_AMOUNT_ONE, 10, "Basket button one not displayed");
		List<WebElement> basketButton1Text = driver.findElements(APPEAL_BASKET_AMOUNT_ONE);
		if (basketButton1Text.size() != 0) {
			String basketOneAmountText = driver.findElement(APPEAL_BASKET_AMOUNT_ONE).getText();
			return basketOneAmountText;
		}
		return null;
	}

	public String getBasketOneOffButtonOneAmount() {
		Helpers.waitForElementToAppear(driver, APPEAL_BASKET_ONEOFF_AMOUNT_ONE, 10, "Basket button one not displayed");
		List<WebElement> basketButton1Text = driver.findElements(APPEAL_BASKET_ONEOFF_AMOUNT_ONE);
		if (basketButton1Text.size() != 0) {
			String basketOneAmountText = driver.findElement(APPEAL_BASKET_ONEOFF_AMOUNT_ONE).getText();
			return basketOneAmountText;
		}
		return null;
	}

	public String getBasketButtonTwoAmount() {
		Helpers.waitForElementToAppear(driver, APPEAL_BASKET_AMOUNT_TWO, 10, "Basket button one not displayed");
		List<WebElement> basketButton2Text = driver.findElements(APPEAL_BASKET_AMOUNT_TWO);
		if (basketButton2Text.size() != 0) {
		String basketTwoAmountText = driver.findElement(APPEAL_BASKET_AMOUNT_TWO).getText();
		return basketTwoAmountText;
	}

	return null;
}

	public String getBasketOneOffButtonTwoAmount() {
		Helpers.waitForElementToAppear(driver, APPEAL_BASKET_ONEOFF_AMOUNT_TWO, 10, "Basket button one not displayed");
		List<WebElement> basketButton1Text = driver.findElements(APPEAL_BASKET_ONEOFF_AMOUNT_TWO);
		if (basketButton1Text.size() != 0) {
			String basketOneAmountText = driver.findElement(APPEAL_BASKET_ONEOFF_AMOUNT_TWO).getText();
			return basketOneAmountText;
		}
		return null;
	}

	public String getBasketButtonThreeAmount() {
		Helpers.waitForElementToAppear(driver, APPEAL_BASKET_AMOUNT_THREE, 10, "Basket button one not displayed");
		List<WebElement> basketButton3Text = driver.findElements(APPEAL_BASKET_AMOUNT_THREE);
		if (basketButton3Text.size() != 0) {
			String basketThreeAmountText = driver.findElement(APPEAL_BASKET_AMOUNT_THREE).getText();
			return basketThreeAmountText;
		}
		return null;
	}

	public String getBasketOneOffButtonThreeAmount() {
		Helpers.waitForElementToAppear(driver, APPEAL_BASKET_ONEOFF_AMOUNT_THREE, 10, "Basket button one not displayed");
		List<WebElement> basketButton1Text = driver.findElements(APPEAL_BASKET_ONEOFF_AMOUNT_THREE);
		if (basketButton1Text.size() != 0) {
			String basketOneAmountText = driver.findElement(APPEAL_BASKET_ONEOFF_AMOUNT_THREE).getText();
			return basketOneAmountText;
		}
		return null;
	}

	public void navigateToDonateFormFromAppealPageLink (){
		driver.get(EnvironmentConfiguration.getBaseURL());
		appealPagePage().getAppealLink();

	}

	public String getCustomFIeldErrorText() {
		Helpers.waitForElementToAppear(driver, CUSTOM_FIELD_ERROR_TEXT, 10, "Custom field error wrong or not displayed");
		List<WebElement> customFieldError = driver.findElements(CUSTOM_FIELD_ERROR_TEXT);
		if (customFieldError.size() != 0) {
			String customFieldErrorText = driver.findElement(CUSTOM_FIELD_ERROR_TEXT).getText();
			return customFieldErrorText;
		}
		return null;
	}

	public String getBasketHeaderText() {
		Helpers.waitForElementToAppear(driver, APPEAL_BASKET_HEADER, 10, "Basket Header not displayed or incorrect");
		List<WebElement> basketHeaderText = driver.findElements(APPEAL_BASKET_HEADER);
		if (basketHeaderText.size() != 0) {
			String basketHeaderTextDisplayed = driver.findElement(APPEAL_BASKET_HEADER).getText();
			return basketHeaderTextDisplayed;
		}
		return null;
	}
	public String getCustomFieldHeaderText() {
		Helpers.waitForElementToAppear(driver, APPEAL_CUSTOM_FIELD_HEADER, 10, "Basket Header not displayed or incorrect");
		List<WebElement> customFieldHeader = driver.findElements(APPEAL_CUSTOM_FIELD_HEADER);
		if (customFieldHeader.size() != 0) {
			String customFieldHeaderTextDisplayed = driver.findElement(APPEAL_CUSTOM_FIELD_HEADER).getText();
			return customFieldHeaderTextDisplayed;
		}
		return null;
	}

	public String getDonateButtonText() {
		Helpers.waitForElementToAppear(driver, APPEAL_BASKET_DONATE_BUTTON_TEXT, 10, "Basket Header not displayed or incorrect");
		List<WebElement> donateButton = driver.findElements(APPEAL_BASKET_DONATE_BUTTON_TEXT);
		if (donateButton.size() != 0) {
			String donateButtonTextDisplayed = driver.findElement(APPEAL_BASKET_DONATE_BUTTON_TEXT).getText();
			return donateButtonTextDisplayed;
		}
		return null;
	}

	public String getCustomFieldLowLimitWarningText() {
		Helpers.waitForElementToAppear(driver, CUSTOM_AMOUNT_FIELD_LOW_LIMIT_WARNING_TEXT, 10, "Low amount warning not displayed");
		List<WebElement> customFieldWarning = driver.findElements(CUSTOM_AMOUNT_FIELD_LOW_LIMIT_WARNING_TEXT);
		if (customFieldWarning.size() != 0) {
			String customFieldWarningText = driver.findElement(CUSTOM_AMOUNT_FIELD_LOW_LIMIT_WARNING_TEXT).getText();
			return customFieldWarningText;
		}
		return null;
	}

	public void setCustomFieldAmountText(String amount){

		driver.findElement(APPEAL_CUSTOM_FIELD).sendKeys(amount);
	}
//	public String selectAmountOneButtonLink (){
//		List<WebElement> appealButtonOneLink = driver.findElements(APPEAL_BASKET_AMOUNT_ONE);
//		if(appealButtonOneLink.size()!=0){
//			driver.findElement(APPEAL_BASKET_AMOUNT_ONE).click();
//		}
//		return null;
//	}


	public void setRandomDonationAmount (String amount){
		driver.findElement(APPEAL_CUSTOM_FIELD).sendKeys(amount);
	}




	public AppealDonatePage(WebDriver dr) {
		this.driver = dr;
	}

	public String setMonthlyDonation() {
		Helpers.waitForIsClickable(driver, APPEAL_MONTHLY_DONATION_TYPE_TAB, 20);
		List<WebElement> monthlyDonationTab = driver.findElements(APPEAL_MONTHLY_DONATION_TYPE_TAB);
		if (monthlyDonationTab.size() != 0) {
			driver.findElement(APPEAL_MONTHLY_DONATION_TYPE_TAB).click();

		}
		return null;
	}

	public String setBasketOneAmount() {
		Helpers.waitForIsClickable(driver, APPEAL_BASKET_AMOUNT_ONE, 20);
		List<WebElement> basketOneAmount = driver.findElements(APPEAL_BASKET_AMOUNT_ONE);
		if (basketOneAmount.size() != 0) {
			driver.findElement(APPEAL_BASKET_AMOUNT_ONE).click();

		}
		return null;
	}

	public String setBasketTwoAmount() {
		Helpers.waitForIsClickable(driver, APPEAL_BASKET_AMOUNT_TWO, 20);
		List<WebElement> basketTwoAmount = driver.findElements(APPEAL_BASKET_AMOUNT_TWO);
		if (basketTwoAmount.size() != 0) {
			driver.findElement(APPEAL_BASKET_AMOUNT_TWO).click();

		}
		return null;
	}

	public String setBasketThreeAmount() {
		Helpers.waitForIsClickable(driver, APPEAL_BASKET_AMOUNT_THREE, 20);
		List<WebElement> basketThreeAmount = driver.findElements(APPEAL_BASKET_AMOUNT_THREE);
		if (basketThreeAmount.size() != 0) {
			driver.findElement(APPEAL_BASKET_AMOUNT_THREE).click();

		}
		return null;
	}

	public String selectDonateButton() {
		Helpers.waitForIsClickable(driver, DONATE_BUTTON, 20);
		List<WebElement> monthlyDonationTab = driver.findElements(DONATE_BUTTON);
		if (monthlyDonationTab.size() != 0) {
			driver.findElement(DONATE_BUTTON).click();

		}
		return null;
	}

}
