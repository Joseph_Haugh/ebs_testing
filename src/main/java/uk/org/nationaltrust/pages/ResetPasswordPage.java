package uk.org.nationaltrust.pages;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.framework.SupporterServiceDBConnection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pramod.Reguri on 17/02/2016.
 */
public class ResetPasswordPage extends PageBase {

	public static final By RESET_PASSWORD_PAGE_RESUBMIT_REQUEST_MESSAGE = By.cssSelector(".alert-box.icon-block.warning.icon-warning>p");

	public static final By PASSWORD_RESET_REQUEST_SENT_HEADER = By.cssSelector ("#mainContent > section > div > main > div > div.row > header > h1");

	public static final By USER_EMAIL_LABLE = By.cssSelector("#emailLabel");

	public static final By EMAIL_EXIST_INFORMATION = By.cssSelector("#content > section > div > main > div > div.medium-12 > p");

	private static final By EMAIL_EXISTS_INFORMATION_BULLETS = By.cssSelector("ul.c-styled-list>li");

	public By REQUEST_NEW_PASSWORD_THANK_YOU_MESSAGE = By.cssSelector(".small-12.columns>h1");

	protected WebDriver driver;

	SupporterServiceDBConnection dbConnection = new SupporterServiceDBConnection();

	public ResetPasswordPage(WebDriver dr) {
		this.driver = dr;
	}

	public String getTheAccountId(String email, String columnLable) {
		String accountId = dbConnection.getData("select * from " + EnvironmentConfiguration.getText("account") + " where account_email=\'" + email + "\'", columnLable);
		return accountId;
	}

	public String getToken(String id, String columnLable, String dateAndTime) {
		String sqlQuery = "SELECT * FROM " + EnvironmentConfiguration.getText("activationToken") + " WHERE ACCOUNT_ID=" + id + "ORDER BY CREATEDDATE DESC";
		String token = dbConnection.getData(sqlQuery, columnLable);
		return token;
	}

	public void navigateToPasswordResetUrl(String accountRef, String tokenId) {
		setLogs("Navigating to the password reset url");
		driver.navigate().to(EnvironmentConfiguration.getPasswordResetUrl() + accountRef + "/" + tokenId);

	}

	public String getResubmitPasswordRequestMessage() {
		return driver.findElement(resetPasswordPage().RESET_PASSWORD_PAGE_RESUBMIT_REQUEST_MESSAGE).getText();
	}

	public String getEmailExistsOrNotInfoMessage() {
		return driver.findElement(EMAIL_EXIST_INFORMATION).getText();
	}
	public String getEmailExistsInformationBullets() {
		String expectedString = EnvironmentConfiguration.getMessageText("password.reset.page.listHtml").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getPasswordEmailExistsBulletedMessage() {
		Helpers.waitForIsDisplayed(driver, PASSWORD_RESET_REQUEST_SENT_HEADER, 10);
		List<String> message = new ArrayList<String>();
		List<WebElement> emailExistsBulletedMessage = driver.findElements(EMAIL_EXISTS_INFORMATION_BULLETS);
		for (int i = 0; i < emailExistsBulletedMessage.size(); i++) {
			message.add(emailExistsBulletedMessage.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}
}
