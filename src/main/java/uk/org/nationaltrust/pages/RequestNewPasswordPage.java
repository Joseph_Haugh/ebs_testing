package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import uk.org.nationaltrust.framework.Helpers;


/**
 * Created by Pramod.Reguri on 18/02/2016.
 */
public class RequestNewPasswordPage extends PageBase {


	public By ENTER_NEW_PASSWORD_FIELD = By.cssSelector("#password");
	public By REQUEST_NEW_PASSWORD_BUTTON = By.cssSelector("#submit");
	public By REQUEST_NEWPASSWORD_EMAIL_FIELD = By.cssSelector("#email");

	protected WebDriver driver;

	public RequestNewPasswordPage(WebDriver dr) {

		this.driver = dr;
	}

	public void clickOnResetPasswordResetButton() {
		setLogs("Clicking the reset passowrd link");
		driver.findElement(signInPage().REQUEST_NEW_PASSWORD_LINK).click();
	}

	public void enterTheEmailToResetThePassword(String email) {
		setLogs("Entering the email to reset the password");
		Helpers.clearAndSetText(driver, REQUEST_NEWPASSWORD_EMAIL_FIELD, email);
		setLogs("Clicking the request new password button");
		driver.findElement(REQUEST_NEW_PASSWORD_BUTTON).click();

	}

	public void enterTheNewPassword(String password) {
		setLogs("Entering the new password in the enter new password field");
		Helpers.clearAndSetText(driver, ENTER_NEW_PASSWORD_FIELD, password);
		setLogs("Clicking the request reset password button");
		driver.findElement(REQUEST_NEW_PASSWORD_BUTTON).click();
	}

	public boolean expiredPwdResetLinkMessage(){
		driver.findElement(By.id("email")).isDisplayed();
		return driver.findElement(By.xpath("//p[contains(text(),'Your link has expired, please try resetting your password again')]")).isDisplayed();
	}
}
