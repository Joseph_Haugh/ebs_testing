package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by Nick.Thompson on 18/05/2017.
 */
public class BarclaysPage extends PageBase {
	private WebDriver driver;

	public static final By ORDER_REFERENCE_FIELD_LABEL =  By.cssSelector("#ncol_ref > tbody > tr:nth-child(1) > td.ncoltxtl > small");

	public By VISA_CARD_BUTTON = By.name("VISA_brand");

	public By MASTER_CARD_BUTTON = By.name ("Eurocard_brand");

	public By CARDHOLDER_NAME_FIELD = By.cssSelector("#Ecom_Payment_Card_Name");

	public By CARD_NUMBER_FIELD = By.cssSelector("#Ecom_Payment_Card_Number");

	public By EXPIREY_MONTH = By.cssSelector("#Ecom_Payment_Card_ExpDate_Month");

	public By EXPIREY_YEAR = By.cssSelector("#Ecom_Payment_Card_ExpDate_Year");

	public By CARD_VERIFICATION_CODE = By.cssSelector("#Ecom_Payment_Card_Verification");

	public By CONFIRM_MY_PAYMENT_BUTTON = By.cssSelector("#submit3");

	public By BACK_BUTTON = By.cssSelector("#btn_Back");

	public By CANCEL_BUTTON = By.cssSelector("#ncol_cancel");

	public String getBarclaysFormOrderRefTextDisplayed (){
		List<WebElement> barclaysFormOrderRefText = driver.findElements(ORDER_REFERENCE_FIELD_LABEL);
		if (barclaysFormOrderRefText.size() !=0){
			String barclaysFormOrderRefTextDisplayed = driver.findElement(ORDER_REFERENCE_FIELD_LABEL).getText();

			return barclaysFormOrderRefTextDisplayed;
		}
		return null;

	}

	public String setVisaCard() {
		List<WebElement> visaCardButton = driver.findElements(VISA_CARD_BUTTON);
		if (visaCardButton.size() != 0) {
			driver.findElement(VISA_CARD_BUTTON).click();

		}
		return null;
	}

	public String setMasterCard() {
		List<WebElement> MasterCardButton = driver.findElements(MASTER_CARD_BUTTON);
		if (MasterCardButton.size() != 0) {
			driver.findElement(MASTER_CARD_BUTTON).click();

		}
		return null;
	}

	public void setCardHolderName(String cardName) throws Exception {
	    Helpers.shortWait2s();
		driver.findElement(CARDHOLDER_NAME_FIELD).sendKeys(cardName);
	}

	public void setCardNumber(String cardNumber) {
		driver.findElement(CARD_NUMBER_FIELD).sendKeys(cardNumber);
	}

	public void setCardExpireyDate(String expireyMonth, String expireyYear) {
		driver.findElement(EXPIREY_MONTH).sendKeys(expireyMonth);
		driver.findElement(EXPIREY_YEAR).sendKeys(expireyYear);
	}


	public void setCardVerificationCode (String verificationNumber){
		driver.findElement(CARD_VERIFICATION_CODE).sendKeys(verificationNumber);
	}
	public void selectConfirmMyPaymentButton (){
		driver.findElement(CONFIRM_MY_PAYMENT_BUTTON).click();
	}

	public void selectCardBackButton (){
		driver.findElement(BACK_BUTTON).click();
	}
	//	public void selectCancelButton (){
	//		driver.findElement(CANCEL_BUTTON).click();
	//	}


	public String selectCancelButton (){
		List<WebElement> cancelButton = driver.findElements(CANCEL_BUTTON);
		if(cancelButton.size()!=0){
			driver.findElement(CANCEL_BUTTON).click();
		}
		return null;
	}

	public void setCardDetails(String name, String cardNo, String expiryMonth, String expiryYear, String verifyCode) {
		driver.findElement(CARDHOLDER_NAME_FIELD).sendKeys(name);
		driver.findElement(CARD_NUMBER_FIELD).sendKeys(cardNo);
		driver.findElement(EXPIREY_MONTH).sendKeys(expiryMonth);
		driver.findElement(EXPIREY_YEAR).sendKeys(expiryYear);
		driver.findElement(CARD_VERIFICATION_CODE).sendKeys(verifyCode);
	}
	public void completeBarclaysScreenByVisa () throws Exception {
	    Helpers.waitForPageLoad(driver);
		setVisaCard();
		setCardHolderName("Michael");
		setCardNumber("4111111111111111");
		setCardExpireyDate("01", "2020");
		setCardVerificationCode("123");
		selectConfirmMyPaymentButton();
		Thread.sleep(6000);
	}

	public void completeBarclaysScreenByMasterCard () throws Exception{
		setMasterCard();
		setCardHolderName("Michael");
		setCardNumber("4111111111111111");
		setCardExpireyDate("01", "2020");
		setCardVerificationCode("123");
		selectConfirmMyPaymentButton();

	}
	public void completeBarclaysScreenWithDeclinedCard () throws Exception {
		barclaysPage().setVisaCard();
		setCardHolderName("Michael");
		setCardNumber("4111113333333333");
		setCardExpireyDate("01", "2020");
		setCardVerificationCode("123");
		selectConfirmMyPaymentButton();

	}

	public BarclaysPage (WebDriver dr) {
		this.driver = dr;
	}

	public void completeJoinBarclaysScreenByVisa() throws Exception {
		barclaysPage().setVisaCard();
		barclaysPage().setCardHolderName("Michael");
		barclaysPage().setCardNumber("4111111111111111");
		barclaysPage().setCardExpireyDate("01", "2020");
		barclaysPage().setCardVerificationCode("123");
		barclaysPage().selectConfirmMyPaymentButton();

	}
}

