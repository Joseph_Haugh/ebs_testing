package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

import sun.net.www.HeaderParser;

public class ModerationPage extends PageBase {

	protected WebDriver driver;

	public ModerationPage(WebDriver dr) {
		this.driver = dr;
		PageFactory.initElements(driver, this);
	}

	public static By GET_USERNAME_FIELD = By.id("username");

	public static By GET_PASSWORD_FIELD = By.id("password");

	public static By GET_LOGIN_BUTTON = By.id("submit");

	public static By GET_LOGOUT_DROPDOWN = By.className("dropdown-toggle");

	//	public static By GET_LOGOUT_BUTTON = By.cssSelector("#__BVID__5 > div > a > div");
	public static By GET_LOGOUT_BUTTON = By.className("dropdown-item");
	//	By.cssSelector("div[Logout]");

	public static By GET_EDIT_BUTTON = By.cssSelector("button[data-tst-edit]");

	public static By GET_APPROVE_BUTTON = By.cssSelector("button[data-tst-approve]");

	public static By GET_AWAITING_BUTTON = By.xpath("//button[contains(text(),'Waiting')]");

	public static By GET_ARE_YOU_SURE_APPROVE_BUTTON = By.cssSelector("button[data-tst-modal-submit]");

	public static By GET_REJECT_BUTTON = By.cssSelector("button[data-tst-reject]");

	public static By GET_ARE_YOU_SURE_REJECT_BUTTON = By.cssSelector("#rejectModal___BV_modal_body_ > div.text-center > div.nt-btn-group > button.btn.mt-2.btn.btn-secondary.btn-danger");

	public static By GET_APPROVED_TAB = By.cssSelector("#__BVID__16 > label:nth-child(2)");

	public static By GET_REJECT_TAB = By.cssSelector("#__BVID__16 > label:nth-child(3)");

	public static By GET_CANCEL_BUTTON = By.cssSelector("button[data-tst-cancel]");

	public static By GET_SAVE_BUTTON = By.cssSelector("button[data-tst-save]");

	public static By EXPAND_DONATION = By.className("tst-donation");
	//*[@id="__BVID__23"]/tbody/tr[1]

	public static By EXPAND_MORE_DETAILS = By.cssSelector("button[data-tst-more-details]");

	//editable fields
	public static By GET_SUBJECT_EDIT_FIELD = By.cssSelector("input[data-tst-detail-subject]");

	public static By GET_OCCASION_EDIT_FIELD = By.cssSelector("input[data-tst-detail-occasion]");

	public static By GET_RELATIONSHIP_FIELD = By.cssSelector("input[data-tst-detail-relationship]");

	public static By GET_STORY_EDIT_FIELD = By.cssSelector("textarea[data-tst-detail-story]");

	public static By GET_STORY_TOO_LONG_ERROR = By.xpath("//*[@id=\"__BVID__13__details_0_\"]/td/div[1]/div[11]/div[2]/div");

	//static display fields

	// TODO selectors too fragile?. Need to fix them but these are designed to always select the top row data of the table
	public static By GET_EXPANDED_DATE = By.cssSelector("div[data-tst-detail-created]");

	public static By GET_EXPANDED_ID = By.cssSelector("div[data-tst-detail-donationid]");

	public static By GET_EXPANDED_FULL_NAME = By.cssSelector("div[data-tst-detail-donorfullname]");

	public static By GET_EXPANDED_EMAIL_ADDRESS = By.cssSelector("div[data-tst-detail-donoremail]");

	public static By GET_EXPANDED_EXPIRY_DATE = By.cssSelector("div[data-tst-detail-expirytimestamp]");

	public static By GET_EXPANDED_SUBJECT = By.cssSelector("div[data-tst-detail-subject]");

	public static By GET_EXPANDED_PLACE_NAME = By.cssSelector("div[data-tst-detail-placename]");

	public static By GET_EXPANDED_OCCASION = By.cssSelector("div[data-tst-detail-occasion]");

	public static By GET_EXPANDED_TONE = By.cssSelector("div[data-tst-detail-tone]");

	public static By GET_EXPANDED_PRIVACY_FLAG = By.cssSelector("div[data-tst-detail-privatelisting]");

	public static By GET_EXPANDED_STORY = By.cssSelector("div[data-tst-detail-story]");

	public static By GET_EXPANDED_RELATIONSHIP = By.cssSelector("div[data-tst-detail-relationship]");

	public static By CREATED_COLUMN_HEADING = By.className("tst-created");

	public static By LOGOUT_TEXT = By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[2]/div/div[1]/h2");

	@FindBy(xpath = "//button[contains(text(),'Edit')]")
	private WebElement EDIT_BUTTON;

	@FindBy(xpath = "//button[contains(text(),'Waiting')]")
	private WebElement WAITING_BUTTON;

	@FindBy(xpath = "//button[contains(text(),'Un-reject')]")
	private WebElement UN_REJECT_BUTTON;

	@FindBy(xpath = "//button[contains(text(),'Publish')]")
	private WebElement PUBLISH_BUTTON;

	@FindBy(xpath = "//button[contains(text(),'Unpublish')]")
	private WebElement UNPUBLISH_BUTTON;

	@FindBy(xpath = "//button[contains(text(),'Reject')]")
	private WebElement REJECT_BUTTON;

	@FindBy(id = "approvedTextArea")
	private WebElement COMMENTS_TEXTAREA;

	@FindBy(css = "button[data-tst-modal-submit='approved']")
	private WebElement COMMEMNTS_APPROVE_BUTTON;

	@FindBy(css = "button[data-tst-modal-cancel='approved']")
	private WebElement COMMEMNTS_CANCEL_APPROVAL_BUTTON;

	@FindBy(css = "button[data-tst-modal-submit='rejected']")
	private WebElement COMMEMNTS_REJECT_BUTTON;

	@FindBy(css = "button[data-tst-modal-cancel='rejected']")
	private WebElement COMMEMNTS_CANCEL_REJECT_BUTTON;

	@FindBy(css = "label[data-tst-cb='waiting']")
	private WebElement WAITING_RADIO_BUTTON;

	@FindBy(css = "label[data-tst-cb='approved']")
	private WebElement APPROVED_RADIO_BUTTON;

	@FindBy(css = "label[data-tst-cb='rejected']")
	private WebElement REJECTED_RADIO_BUTTON;

	@FindBy(id = "rejectedTextArea")
	private WebElement REJECT_COMMENTS_TEXTAREA;

	@FindBy(id = "waitingTextArea")
	private WebElement UNAPPROVE_COMMENTS_TEXTAREA;

	@FindBy(css = "button[data-tst-modal-submit='waiting']")
	private WebElement COMMEMNTS_WAITING_BUTTON;

	@FindBy(id = "sharingLink")
	private WebElement SHARING_LINK;

	@FindBy(id = "user-input")
	private WebElement USER_SEARCH_TEXT;

	@FindBy(css = ".tst-donation")
	private List<WebElement> DONATIONS_ROW_COUNT;

	@FindBy(xpath = "//button[contains(text(),'Clear')]")
	private WebElement CLEAR_BUTTON;

	public void selectModerationLoginButtonButton() {
		driver.findElement(GET_LOGIN_BUTTON).click();
	}

	public void selectEditButton() {
		driver.findElement(GET_EDIT_BUTTON).click();
	}

	public void selectApproveButton() {
		driver.findElement(GET_APPROVE_BUTTON).click();
	}

	public void selectCancelButton() {
		driver.findElement(GET_CANCEL_BUTTON).click();
	}

	public void selectSaveButton() {
		driver.findElement(GET_SAVE_BUTTON).click();
	}

	public void setModerationUsernameNameFieldText() {
		driver.findElement(GET_USERNAME_FIELD).sendKeys("donate.user");
	}

	// TODO Cant have an AD password in here
	public void setModerationPasswordFieldText() {
		driver.findElement(GET_PASSWORD_FIELD).sendKeys("donate.user");
	}

	public void clickCreatedColumn() {
		Helpers.waitForIsDisplayed(driver, CREATED_COLUMN_HEADING, 10);
		driver.findElement(CREATED_COLUMN_HEADING).click();

	}

	public void navigateAndLoginToModerationApp() throws Exception {
		driver.navigate().to(EnvironmentConfiguration.getText("moderationURL"));
		Helpers.waitForPageLoad(driver);
		if (driver.getCurrentUrl().contains("commemorative-giving-entries")) {
			Helpers.waitForIsDisplayed(driver, GET_LOGOUT_DROPDOWN, 10);
			driver.findElement(GET_LOGOUT_DROPDOWN).click();
			driver.findElement(GET_LOGOUT_BUTTON).click();
		}
		Thread.sleep(6000);
		setModerationUsernameNameFieldText();
		setModerationPasswordFieldText();
		selectModerationLoginButtonButton();
		Thread.sleep(6000);
	}

	public void logoutOfModerationApp(){
		Helpers.waitHandlingException(1000);
		driver.findElement(GET_LOGOUT_DROPDOWN).click();
		Helpers.waitHandlingException(1000);
		driver.findElement(GET_LOGOUT_BUTTON).click();
//		String logoutText = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[2]/div/div[1]/h2")).getText();
//		Assert.assertEquals(logoutText, "You have been logged out", "Wrong or no logout text displayed");
	}

	public void setSubjectText(String subject) {
		driver.findElement(GET_SUBJECT_EDIT_FIELD).sendKeys(subject);

	}

	public void setOccasionText(String occasion) {
		driver.findElement(GET_OCCASION_EDIT_FIELD).sendKeys(occasion);
	}

	public void setRelationshipText (String relationship){
		driver.findElement(GET_RELATIONSHIP_FIELD).sendKeys(relationship);
	}

	public void setStoryText(String story) {
		driver.findElement(GET_STORY_EDIT_FIELD).sendKeys(story);
	}

	public void setAllEditableFieldsToNewValuesInCelebration(String subject, String occasion, String story) throws Exception {
		driver.findElement(GET_EDIT_BUTTON).click();
		Thread.sleep(2000);
		//		String subject1 = driver.findElement(By.cssSelector("input[data-tst-detail-subject]")).getText();
		Helpers.clearAndSetText(driver, GET_SUBJECT_EDIT_FIELD, subject);
		Helpers.clearAndSetText(driver, GET_OCCASION_EDIT_FIELD, occasion);
		Helpers.clearAndSetText(driver, GET_STORY_EDIT_FIELD, story);
		driver.findElement(GET_SAVE_BUTTON).click();
	}

	public void setAllEditableFieldsToNewValuesInMemory(String subject, String story, String relationship) throws Exception {
		driver.findElement(GET_EDIT_BUTTON).click();
		Thread.sleep(2000);
		//		String subject1 = driver.findElement(By.cssSelector("input[data-tst-detail-subject]")).getText();
		Helpers.clearAndSetText(driver, GET_SUBJECT_EDIT_FIELD, subject);
		Helpers.clearAndSetText(driver, GET_STORY_EDIT_FIELD, story);
		Helpers.clearAndSetText(driver, GET_RELATIONSHIP_FIELD, relationship);
		driver.findElement(GET_SAVE_BUTTON).click();
		Thread.sleep(2000);

	}

	public String getStoryFieldTooLong() {
		List<WebElement> storyFieldError = driver.findElements(GET_STORY_TOO_LONG_ERROR);
		if (storyFieldError.size() != 0) {
			String storyFieldErrorText = driver.findElement(GET_STORY_TOO_LONG_ERROR).getText();
			setLogs("Get story text too long error");
			return storyFieldErrorText;

		}
		return null;
	}

	public ModerationPage expandUGCItem(String donationId) {
		Helpers.waitForElementToAppear(driver, By.cssSelector("div[data-tst-donation-id='" + donationId + "']"), 30, "Donation ID is not found");
		driver.findElement(By.cssSelector("div[data-tst-donation-id='" + donationId + "']")).click();
		return this;
	}

	public boolean UGCitemInWaitingState() {
		EDIT_BUTTON.isEnabled();
		PUBLISH_BUTTON.isEnabled();
		REJECT_BUTTON.isEnabled();
		return true;
	}

	public String isprivacyFlagTrue() {
		String privacyFlag = driver.findElement(GET_EXPANDED_PRIVACY_FLAG).getText();
		if (privacyFlag == "Yes");
		return "True";
	}


	public boolean UGCitemInApprovedState() {
		UNPUBLISH_BUTTON.isEnabled();
		return true;
	}

	public boolean UGCitemInRejectedState() {
		UN_REJECT_BUTTON.isEnabled();
		return true;
	}

	public ModerationPage clickPublishButton(){
		Helpers.waitForPageLoad(driver);
		PUBLISH_BUTTON.click();
		return this;
	}

	public ModerationPage clickUnRejectButton(){
		Helpers.waitForPageLoad(driver);
		UN_REJECT_BUTTON.click();
		return this;
	}

	public ModerationPage clickRejectButton(){
		Helpers.waitForPageLoad(driver);
		REJECT_BUTTON.click();
		return this;
	}

	public ModerationPage clickUnPublishButton(){
		Helpers.waitForVisibility(UNPUBLISH_BUTTON);
		UNPUBLISH_BUTTON.click();
		return this;
	}

	public ModerationPage enterApproveComments(String comments){
		Helpers.waitForVisibility(COMMENTS_TEXTAREA);
		COMMENTS_TEXTAREA.sendKeys(comments);
		return this;
	}

	public ModerationPage enterUnApproveComments(String comments){
		Helpers.waitForVisibility(UNAPPROVE_COMMENTS_TEXTAREA);
		UNAPPROVE_COMMENTS_TEXTAREA.sendKeys(comments);
		return this;
	}

	public ModerationPage enterRejectComments(String comments){
		Helpers.waitForVisibility(REJECT_COMMENTS_TEXTAREA);
		REJECT_COMMENTS_TEXTAREA.sendKeys(comments);
		return this;
	}

	public ModerationPage clickCommentsModalApprove(){
		Helpers.waitForVisibility(COMMEMNTS_APPROVE_BUTTON);
		COMMEMNTS_APPROVE_BUTTON.click();
		return this;
	}

	public ModerationPage clickCommentsModalCancel(){
		Helpers.waitForVisibility(COMMEMNTS_CANCEL_APPROVAL_BUTTON);
		COMMEMNTS_CANCEL_APPROVAL_BUTTON.click();
		return this;
	}

	public ModerationPage clickCommentsModalReject() throws Exception{
		Helpers.waitForVisibility(COMMEMNTS_REJECT_BUTTON);
		COMMEMNTS_REJECT_BUTTON.click();
		Thread.sleep(1500);
		return this;
	}

	public ModerationPage clickCommentsModalWaiting(){
		Helpers.waitForVisibility(COMMEMNTS_WAITING_BUTTON);
		COMMEMNTS_WAITING_BUTTON.click();
		return this;
	}


	public ModerationPage selectApprovedList(){
		Helpers.waitForVisibility(APPROVED_RADIO_BUTTON);
		while(true){
			try{
				APPROVED_RADIO_BUTTON.click();
				break;
			}catch (Throwable e){
				try {
					Thread.sleep(200);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		return this;
	}

	public ModerationPage selectWaitingList(){
		Helpers.waitForVisibility(WAITING_RADIO_BUTTON);
		WAITING_RADIO_BUTTON.click();
		return this;
	}

	public void selectRejectedList() {
		Helpers.waitForVisibility(REJECTED_RADIO_BUTTON);
		REJECTED_RADIO_BUTTON.click();

	}

	public boolean verifyTextInCommentsModal(String commentsModelDisplayedText ) {
		Helpers.waitForPageLoad(driver);
		return driver.findElement(By.xpath("//b[contains(text(),'"+commentsModelDisplayedText+"')]")).isDisplayed();
	}

	public String getSharinglinkFromApprovedListUGCItem(){
		return  SHARING_LINK.getAttribute("href");
	}

	public ModerationPage enterSearchText(String userInput){
		USER_SEARCH_TEXT.sendKeys(userInput);
		return this;
	}

	public String getSearchText(){
		return USER_SEARCH_TEXT.getAttribute("value");
	}

	public int getCountOfResults(){
		return DONATIONS_ROW_COUNT.size();
	}

	public void clearSearchText() {
		CLEAR_BUTTON.click();
	}

	public boolean checkIfOccasionIsDisplayed(){
		setLogs("check if Occasion field is displayed.......");
		return Helpers.waitForIsDisplayed(driver,GET_EXPANDED_OCCASION,10);
	}

	public boolean checkIfAwaitingButtonAppears (){
		setLogs("check if Awaiting button appears");
		return Helpers.waitForIsDisplayed(driver, GET_AWAITING_BUTTON, 10);
	}
}
