package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FootersAllApps extends PageBase {

	public FootersAllApps (WebDriver dr) {
		this.driver = dr;
	}

	public boolean modernSlaveryLink(){
		return driver.findElement(By.linkText("Modern slavery statement")).isDisplayed();
	}

}
