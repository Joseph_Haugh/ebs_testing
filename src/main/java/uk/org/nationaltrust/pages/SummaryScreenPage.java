package uk.org.nationaltrust.pages;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by Nick.Thompson on 21/12/2016.
 */
public class SummaryScreenPage extends PageBase {

	protected WebDriver driver;

	public SummaryScreenPage(WebDriver dr) {
		this.driver = dr;
	}

	public By MEMBERSHIP_TYPE_HEADER = By.cssSelector("#main-content > section > div > div > div.panel.large-12 > h1");

	public By WHAT_YOUVE_TOLD_US_HEADER = By.cssSelector("#main-content > section > div > div > h2");

//	public By JOIN_BUTTON = By.id("requestPayment");
		public By JOIN_BUTTON = By.id("submitApplication");

	public By JOIN_BUTTON_DD = By.id("submitApplication");

	public By GET_MEMBERSHIP_TYPE = By.id("membershipType");

	public By GET_MEMBERSHIP_NAMES = By.id("memberNames");

	public By GET_MEMBER_NAME_1 = By.cssSelector("#memberNames > ul > li:nth-child(1)");

	public By GET_MEMBER_NAME_2 = By.cssSelector("#memberNames > ul > li:nth-child(2)");

	public By GET_LEAD_MEMBER_ADDRESS = By.id("leadMemberAddress");

	public By GET_LEAD_MEMBER_ADDRESS_LINE1 = By.id("#leadMemberAddress > ul > li:nth-child(1");

	public By GET_LEAD_MEMBER_ADDRESS_TOWN = By.id("#leadMemberAddress > ul > li:nth-child(2)");

	public By GET_LEAD_MEMBER_ADDRESS_POST_CODE = By.id("#leadMemberAddress > ul > li:nth-child(3) - postcode");

	public By GET_LEAD_MEMBER_ADDRESS_COUNTRY = By.id("#leadMemberAddress > ul > li:nth-child(4)");

	public By GET_MEMBER_CONTACT_PHONE = By.cssSelector("#contactDetails > ul > li:nth-child(1)");

	public By GET_MEMBER_CONTACT_EMAIL = By.cssSelector("#contactDetails > ul > li:nth-child(2)");

	public By GET_PAYMENT_METHOD = By.id("paymentMethod");

	public By MEMBERSHIP_PRICE = By.id("membershipPrice");

	public By PROMO_DESCRIPTION = By.id("promoDescriptions");

	public By MYNT_ACCOUNT_CHECKBOX = By.cssSelector("label[for='optInToWebAccount");

	public By MYNT_PASSWORD_FIELD = By.id("webAccountPassword");

	public By MYNT_AUTOENROLL_HEADER_TEXT = By.cssSelector("#main-content > section > div > div > form > h2");

	public By MYNT_AUTOENROL_BULLET_TEXT = By.cssSelector("ul.nt-benefit-list>li");

	public static By AUTO_ENROL_CHECKBOX = By.cssSelector("#main-content > section > div > div > form > div.row.u-xtra-margin__small > div > div > div > label");

	public String membershipHeaderAppears() {
		List<WebElement> memberHeaderAppears = driver.findElements(MEMBERSHIP_TYPE_HEADER);
		if (memberHeaderAppears.size() != 0) {
			String memberHeaderText = driver.findElement(MEMBERSHIP_TYPE_HEADER).getText();
			return memberHeaderText;
		}
		return null;
	}
	public String subHeadingAppears() {
		List<WebElement> memberSubHeaderAppears = driver.findElements(WHAT_YOUVE_TOLD_US_HEADER);
		if (memberSubHeaderAppears.size() != 0) {
			String subHeaderText = driver.findElement(WHAT_YOUVE_TOLD_US_HEADER).getText();
			return subHeaderText;
		}
		return null;
	}

	public boolean checkIfAutoEnrolCheckboxAppears(){
		setLogs("check if AutoEnrolCheckbox appears is displayed.......");
		return Helpers.waitForIsDisplayed(driver,MYNT_ACCOUNT_CHECKBOX,10);
	}


	public String getMembershipTypeText(){
		return driver.findElement(GET_MEMBERSHIP_TYPE).getText();
	}

	public String getMemberName1(){
		return driver.findElement(GET_MEMBER_NAME_1).getText();
	}

	public String getMemberName2(){
		return driver.findElement(GET_MEMBER_NAME_2).getText();
	}

	public String getLeadMemberAddress(){
		return driver.findElement(GET_LEAD_MEMBER_ADDRESS).getText();
	}

	public String getLeadMemberAddresslLine1(){
		return driver.findElement(GET_LEAD_MEMBER_ADDRESS_LINE1).getText();
	}

	public String getLeadMemberAddresslTown(){
		return driver.findElement(GET_LEAD_MEMBER_ADDRESS_TOWN).getText();
	}

	public String getLeadMemberAddresslPostcode(){
		return driver.findElement(GET_LEAD_MEMBER_ADDRESS_POST_CODE).getText();
	}

	public String getLeadMemberAddresslCountry(){
		return driver.findElement(GET_LEAD_MEMBER_ADDRESS_COUNTRY).getText();
	}

	public String getMemberContactPhone(){
		return driver.findElement(GET_MEMBER_CONTACT_PHONE).getText();
	}

	public String getMemberContactEmail(){
		return driver.findElement(GET_MEMBER_CONTACT_EMAIL).getText();
	}

	public String getPaymentMethod(){
		return driver.findElement(GET_PAYMENT_METHOD).getText();
	}

	public String getMembershipPrice(){
		return driver.findElement(MEMBERSHIP_PRICE).getText();
	}

	public String getMembershipPriceShown() {
		setLogs("Getting the monthly price shown");
		List<WebElement> membershipPrice = driver.findElements(MEMBERSHIP_PRICE);
		if (membershipPrice.size() != 0) {
			String membershipPriceShown = driver.findElement(summaryPage().MEMBERSHIP_PRICE).getText();
			return membershipPriceShown;
		}
		return null;

	}

	public String getPromoDescription(){
		return driver.findElement(PROMO_DESCRIPTION).getText();
	}

	public void clickJoinDDBtn(){driver.findElement(JOIN_BUTTON_DD).click();}

	public void clickJoinBtn(){driver.findElement(JOIN_BUTTON).click();}

	public void clickMYNTAccountCheckbox (){driver.findElement(MYNT_ACCOUNT_CHECKBOX).click();
	}
	public void setPasswordOfSupporterIfMYNTQuestionExists(){
		List<WebElement> passwordQuestion = driver.findElements(MYNT_PASSWORD_FIELD);
		if(passwordQuestion.size() != 0){
			setLogs("Entering the password for the supporter - ");
			Helpers.clearAndSetText(driver, MYNT_PASSWORD_FIELD, "Story1234");
		}
		else {
			setLogs("No MY NT Question exists -- " );
		}
	}

	public void setMYNTPassword(String addressCounty) {
		driver.findElement(summaryPage().MYNT_PASSWORD_FIELD).sendKeys(addressCounty);
	}

	public String myNTAutoEnrolHeaderTextAppears() {
		List<WebElement> autoEnrolHeader = driver.findElements(MYNT_AUTOENROLL_HEADER_TEXT);
		if (autoEnrolHeader.size() != 0) {
			String autoEnrolHeaderText = driver.findElement(MYNT_AUTOENROLL_HEADER_TEXT).getText();
			return autoEnrolHeaderText;
		}
		return null;
	}

	public String getExpectedAutoEnrolBenefitsNonGiftMessage() {
		String expectedString = EnvironmentConfiguration.getMessageText("summary.page.autoEnrol.benefits").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getExpectedAutoEnrolBenefitsGiftMessage() {
		String expectedString = EnvironmentConfiguration.getMessageText("summary.page.autoEnrol.gift.benefits").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getAutoEnrolBenefitsMessage() {
		Helpers.waitForIsDisplayed(driver, MYNT_AUTOENROLL_HEADER_TEXT, 10);
		List<String> message = new ArrayList<String>();
		List<WebElement> autoEnrolBenefitsMessage = driver.findElements(MYNT_AUTOENROL_BULLET_TEXT);
		for (int i = 0; i < autoEnrolBenefitsMessage.size(); i++) {
			message.add(autoEnrolBenefitsMessage.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

}
