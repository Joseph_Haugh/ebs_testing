package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class DonateMapPage extends DonateCommons {

	protected WebDriver driver;

	public DonateMapPage (WebDriver dr) {
		this.driver = dr;
		PageFactory.initElements(driver,this);
	}

	public static By WHERE_FIELD = By.id("placeId");

	public static By PIN_DATA = By.id("map-pin-data-container");

	public static By IMAGE_URL_LINKS = By.xpath("/html/body/pre");

	public static By IMAGE_END_POINT_LINKS = By.xpath("//*[@id=\"stockImages\"]");

}
