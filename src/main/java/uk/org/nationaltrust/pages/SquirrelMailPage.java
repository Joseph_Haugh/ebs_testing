package uk.org.nationaltrust.pages;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by nick.thompson on 03/04/2017.
 */
public class SquirrelMailPage extends PageBase {
	protected WebDriver driver;

	public By NAME_FIELD = By.cssSelector("body > form > table > tbody > tr > td > center > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=\"text\"]");

	public By PASSWORD_FIELD = By.cssSelector("body > form > table > tbody > tr > td > center > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > input[type=\"password\"]:nth-child(1)");

	public By LOGIN_BUTTON = By.cssSelector("body > form > table > tbody > tr > td > center > table > tbody > tr:nth-child(3) > td > center > input[type=\"submit\"]");

	public static By EMAIL_HEADER = By.cssSelector("body > table:nth-child(6) > tbody > tr:nth-child(1) > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(4) > td > h2 > div");

	public static By ABANDONED_EMAIL_HEADER = By.cssSelector("body > table:nth-child(6) > tbody > tr:nth-child(1) > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > div > center:nth-child(3) > div > table > tbody > tr > td > table > tbody > tr:nth-child(1) > td > h2");

	public static By ABANDONED_EMAIL_NO_REPLY_TEXT = By.cssSelector("body > table:nth-child(6) > tbody > tr:nth-child(1) > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > div > center.wrapper.footer > div > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(1) > td > p:nth-child(3)");

	public static By PDF_ATTACHMENT_LINK = By.linkText("National-Trust-temporary-member-card.pdf");


//confirmation email

	public static By CONFIRMATION_EMAIL_HEADER = By.id("intro");

	public static By CONFIRMATION_GIFT_EMAIL_HEADER = By.cssSelector("#intro");

	public static By CONFIRMATION_EMAIL_MAIN_TEXT = By.cssSelector("#content > p:nth-child(1)");

	public static By CONFIRMATION_EMAIL_WELCOME_PACK_TEXT = By.cssSelector("#content > p:nth-child(4) > b");

	public static By CONFIRMATION_GIFT_EMAIL_MAIN_TEXT = By.cssSelector("#content > p:nth-child(1) > b");

	public static By CONFIRMATION_GIFT_EMAIL_WELCOME_PACK_TEXT = By.cssSelector("#content > p:nth-child(2)");

	public static By CONFIRMATION_EMAIL_WELCOME_PACK_BULLET1 = By.cssSelector("#content > ul:nth-child(7)");

	public static By CONFIRMATION_GIFT_EMAIL_WELCOME_PACK_BULLET1 = By.cssSelector("#content > ul:nth-child(5)");


	public static By CONFIRMATION_GIFT_LESS_21_EMAIL_WELCOME_PACK_BULLET1 = By.cssSelector("#content > ul:nth-child(5)");

	public static By CONFIRMATION_EMAIL_WELCOME_PACK_BULLET2 = By.cssSelector("#content > ul:nth-child(11)");

	public static By CONFIRMATION_GIFT_EMAIL_WELCOME_PACK_BULLET2 = By.cssSelector("#content > ul:nth-child(9)");

	public static By CONFIRMATION_GIFT_LESS_21_EMAIL_WELCOME_PACK_BULLET2 = By.cssSelector("#content > ul:nth-child(9)");

	public static By CONFIRMATION_GIFT_LIFE_EMAIL_WELCOME_PACK_BULLET1 = By.cssSelector("#content > ul:nth-child(5)");

	public static By CONFIRMATION_GIFT_LIFE_EMAIL_WELCOME_PACK_BULLET2 = By.cssSelector("#content > ul:nth-child(9)");

	//non bar code email

	public static By NON_BARCODE_EMAIL_HEADER = By.cssSelector("#intro");
	public static By NON_BARCODE_EMAIL_MAIN_TEXT = By.cssSelector("");

	//Ind memb
	public static By NON_BARCODE_IND_EMAIL_CARD_INFO_TEXT = By.cssSelector("#content > ul:nth-child(4)");
	public static By NON_BARCODE_IND_EMAIL_PARKING_INFO_TEXT = By.cssSelector("#content > ul:nth-child(8)");


	//Joint/fam memb

	public static By NON_BARCODE_MULTIPLE_EMAIL_CARD_INFO_TEXT = By.cssSelector("#content > ul:nth-child(4)");
	public static By NON_BARCODE_MULTIPLE_EMAIL_PARKING_INFO_TEXT = By.cssSelector("#content > ul:nth-child(8)");

	public static By NON_BARCODE_MULTIPLE_GIFT_EMAIL_CARD_INFO_TEXT = By.cssSelector("#content > ul:nth-child(9)");
	public static By NON_BARCODE_MULTIPLE_GIFT_EMAIL_PARKING_INFO_TEXT = By.cssSelector("#content > ul:nth-child(13)");

	public static By NON_BARCODE_MULTIPLE_GIFT_EMAIL_PARKING_INFO_BULLETS = By.cssSelector("#content > ul:nth-child(17)");


	//barcode email

	//joint/fam gift memb

	public static By BARCODE_MULTIPLE_EMAIL_CARD_INFO_TEXT = By.cssSelector("#content > ul");


	public static By BARCODE_EMAIL_HEADER = By.cssSelector("#intro");

	public static By BARCODE_EMAIL_BODY_TEXT = By.cssSelector("#content > p:nth-child(1)");

	public static By BARCODE_GIFT_EMAIL_BODY_TEXT = By.cssSelector("#content > p:nth-child(1)");

	public static By EMAIL_DONATION_AMOUNT_TEXT = By.cssSelector("body > table:nth-child(6) > tbody > tr:nth-child(1) > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > div > center:nth-child(2) > div > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td > div:nth-child(2) > p:nth-child(2) > span:nth-child(1)");

	public static By EMAIL_FIRST_NAME_TEXT = By.cssSelector("body > table:nth-child(6) > tbody > tr:nth-child(1) > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > div > center:nth-child(2) > div > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td > div:nth-child(2) > p:nth-child(1) > span");

//	public static By EMAIL_ADDRESS_USED = By.tagName("html");
public static By EMAIL_ADDRESS_USED = By.cssSelector("body > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2");

	public static By TEMPORARY_MEMBERSHIP_CARD_MEMBERSHIP_TYPE = By.cssSelector("body > table:nth-child(6) > tbody > tr:nth-child(1) > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > div > div > table > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(4) > td:nth-child(2)");

	public static By TEMPORARY_MEMBERSHIP_CARD = By.xpath("/html/body/table[4]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/div/div/table/tbody/tr[1]/td/table/tbody");

	public static By SEARCH = By.xpath(".//*[@href='/webmail/src/search.php?mailbox=INBOX']"); //By.linkText("Search"); //By.cssSelector("body > table > tbody > tr:nth-child(2) > td:nth-child(1) > a:nth-child(5)");

	public static By SEARCH_TEXT_BOX = By.name("what");

	public static By SEARCH_IN = By.name("where");

	public static By SEARCH_BTN = By.name("submit");

	public By SIGN_IN_TO_MY_NT_BUTTON = By.cssSelector("body > table:nth-child(6) > tbody > tr:nth-child(1) > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr > td > div > center:nth-child(2) > div > table > tbody > tr > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td > a");

	public static By SIGN_OUT = By.xpath(".//*[@href='/webmail/src/signout.php']");

	public void setNameFieldText (){
		driver.findElement(NAME_FIELD).sendKeys("catchall");
	}

	public void setPasswordFieldText (){driver.findElement(PASSWORD_FIELD).sendKeys("FunnyTreeString8");}

	public void selectLoginButtonButton (){driver.findElement(LOGIN_BUTTON).click();}

	public String getEmailHeaderTextDisplayed () {
		List<WebElement> emailHeader = driver.findElements(EMAIL_HEADER);
		if (emailHeader.size() != 0) {
			String emailHeaderText = driver.findElement(EMAIL_HEADER).getText();

			return emailHeaderText;
		}
		return null;
	}

	public String getABEmailHeaderTextDisplayed () {
		List<WebElement> abEmailHeader = driver.findElements(ABANDONED_EMAIL_HEADER);
		if (abEmailHeader.size() != 0) {
			String abEmailHeaderText = driver.findElement(ABANDONED_EMAIL_HEADER).getText();

			return abEmailHeaderText;
		}
		return null;
	}

	public String getABEmailNoReplyTextDisplayed () {
		List<WebElement> abEmailNoReply = driver.findElements(ABANDONED_EMAIL_NO_REPLY_TEXT);
		if (abEmailNoReply.size() != 0) {
			String abEmailNoReplyText = driver.findElement(ABANDONED_EMAIL_NO_REPLY_TEXT).getText();

			return abEmailNoReplyText;
		}
		return null;
	}






	public String getEmailAmountTextDisplayed () {
		List<WebElement> emailAmount = driver.findElements(EMAIL_DONATION_AMOUNT_TEXT);
		if (emailAmount.size() != 0) {
			String emailAmountText = driver.findElement(EMAIL_DONATION_AMOUNT_TEXT).getText();

			return emailAmountText;
		}
		return null;
	}

	public String getEmailFirstNameTextDisplayed () {
		List<WebElement> emailFirstName = driver.findElements(EMAIL_FIRST_NAME_TEXT);
		if (emailFirstName.size() != 0) {
			String emailFirstNameText = driver.findElement(EMAIL_FIRST_NAME_TEXT).getText();

			return emailFirstNameText;
		}
		return null;
	}

	public String getEmailAddressDisplayed () {
		List<WebElement> emailAddress = driver.findElements(EMAIL_ADDRESS_USED);
		if (emailAddress.size() != 0) {
			String emailAddressText = driver.findElement(EMAIL_ADDRESS_USED).getText();

			return emailAddressText;
		}
		return null;
	}

	public String getEmailMembershipTypeDisplayed () {
		List<WebElement> membType = driver.findElements(TEMPORARY_MEMBERSHIP_CARD_MEMBERSHIP_TYPE);
		if (membType.size() != 0) {
			String membTypeDisplayed = driver.findElement(TEMPORARY_MEMBERSHIP_CARD_MEMBERSHIP_TYPE).getText();

			return membTypeDisplayed;
		}
		return null;
	}

	public boolean getMembershipTemporaryCard (){
		setLogs("Check if Temporary membership card is displayed");
	return Helpers.waitForIsDisplayed(driver, TEMPORARY_MEMBERSHIP_CARD, 10 );
	}

	public SquirrelMailPage (WebDriver dr) {
		this.driver = dr;
	}

	public void searchWithToEmailId(String toEmailId){
		clickSearch();
		Helpers.clearAndSetText(driver,SEARCH_TEXT_BOX,toEmailId);
		new Select(driver.findElement(SEARCH_IN)).selectByValue("TO");
		driver.findElement(SEARCH_BTN).click();
	}

	public void clickSearch(){
		driver.findElement(SEARCH).click();
	}


	//Confirmation email

	//non gift

	public String getConfirmationEmailHeaderTextDisplayed(){

		List<WebElement>confEmailHeader = driver.findElements(CONFIRMATION_EMAIL_HEADER);
		if(confEmailHeader.size()!=0){
			String confEmail = driver.findElement(CONFIRMATION_EMAIL_HEADER).getText();
			return confEmail;
		}
		return null;

	}

	public String getConfirmationEmailMainTextDisplayed(){

		List<WebElement>welcome = driver.findElements(CONFIRMATION_EMAIL_MAIN_TEXT);
		if(welcome.size()!=0){
			String welcomeText = driver.findElement(CONFIRMATION_EMAIL_MAIN_TEXT).getText();
			return welcomeText;
		}
		return null;

	}

	public String getConfirmationEmailWelcomePackTextDisplayed(){

		List<WebElement>welcomePack = driver.findElements(CONFIRMATION_EMAIL_WELCOME_PACK_TEXT);
		if(welcomePack.size()!=0){
			String welcomePackText = driver.findElement(CONFIRMATION_EMAIL_WELCOME_PACK_TEXT).getText();
			return welcomePackText;
		}
		return null;

	}

	//gift

	public String getConfirmationGiftEmailHeaderTextDisplayed(){

		List<WebElement>confEmailHeader = driver.findElements(CONFIRMATION_GIFT_EMAIL_HEADER);
		if(confEmailHeader.size()!=0){
			String confEmail = driver.findElement(CONFIRMATION_GIFT_EMAIL_HEADER).getText();
			return confEmail;
		}
		return null;

	}

	public String getConfirmationGiftEmailMainTextDisplayed(){

		List<WebElement>welcome = driver.findElements(CONFIRMATION_GIFT_EMAIL_MAIN_TEXT);
		if(welcome.size()!=0){
			String welcomeText = driver.findElement(CONFIRMATION_GIFT_EMAIL_MAIN_TEXT).getText();
			return welcomeText;
		}
		return null;

	}

	public String getConfirmationGiftEmailWelcomePackTextDisplayed(){

		List<WebElement>welcomePack = driver.findElements(CONFIRMATION_GIFT_EMAIL_WELCOME_PACK_TEXT);
		if(welcomePack.size()!=0){
			String welcomePackText = driver.findElement(CONFIRMATION_GIFT_EMAIL_WELCOME_PACK_TEXT).getText();
			return welcomePackText;
		}
		return null;

	}

	//Temp Card email without barcode

	public String getNonBarcodeEmailHeaderTextDisplayed(){

		List<WebElement>noCodeEmailHeader = driver.findElements(NON_BARCODE_EMAIL_HEADER);
		if(noCodeEmailHeader.size()!=0){
			String noCodeEmail = driver.findElement(NON_BARCODE_EMAIL_HEADER).getText();
			return noCodeEmail;
		}
		return null;

	}

	public String getNonBarcodeEmailMainTextDisplayed(){

		List<WebElement>noCodeEmailHeader = driver.findElements(NON_BARCODE_EMAIL_MAIN_TEXT);
		if(noCodeEmailHeader.size()!=0){
			String noCodeEmail = driver.findElement(NON_BARCODE_EMAIL_MAIN_TEXT).getText();
			return noCodeEmail;
		}
		return null;

	}

	public boolean getTemporaryCardPDFLink (){
		setLogs("Check if Temporary membership card PDF Link is displayed");
		return Helpers.waitForIsDisplayed(driver, PDF_ATTACHMENT_LINK, 10 );
	}


	public String getConfEmailWelcomePackIncludesInformationBullets1() {
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.email.info.bullets1").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getConfEmailWelcomePackIncludesInformationBullets1Alt() {
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.email.info.bullets1.alt").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getConfEmailWelcomePackIncludesInformationBullets2() {
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.email.info.bullets2").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getConfGiftEmailWelcomePackIncludesInformationBullets1() {
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.gift.email.info.bullets1").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getConfGiftEmailWelcomePackIncludesInformationBullets2() {
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.gift.email.info.bullets2").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}


	public String getConfGiftLess21EmailWelcomePackIncludesInformationBullets1() {
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.gift.email.less21.info.bullets1").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getConfGiftLess21EmailWelcomePackIncludesInformationBullets2() {
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.gift.email.less21.info.bullets2").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getEmailTempCardndMembInformationBullets() {
		String expectedString = EnvironmentConfiguration.getMessageText("temporary.card.no.barcode.email.ind.memb.info.bullets").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getEmailTempParkingIndMembInformationBullets() {
		String expectedString = EnvironmentConfiguration.getMessageText("temporary.parking.no.barcode.email.ind.memb.info").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getEmailTempCardMultiMembInformationBullets() {
		String expectedString = EnvironmentConfiguration.getMessageText("temporary.card.no.barcode.email.info.bullets").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getEmailTempCardMultiMembInformationBulletsalt() {
		String expectedString = EnvironmentConfiguration.getMessageText("temporary.card.no.barcode.email.info.bullets1").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getEmailTempCardMultiMembInformationBullets1() {
		String expectedString = EnvironmentConfiguration.getMessageText("temporary.card.barcode.multi.email.info.bullets").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}


	public String getEmailTempcardMultiMembGiftInformationBullets() {
		String expectedString = EnvironmentConfiguration.getMessageText("temporary.card.gift.no.barcode.email.info.bullets").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getEmailTempParkingMultiMembGiftInformationBullets() {
		String expectedString = EnvironmentConfiguration.getMessageText("temporary.parking.no.barcode.gift.email.info").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public String getEmailTempParkingMultiMembGiftInformationBulletsAlt() {
		String expectedString = EnvironmentConfiguration.getMessageText("temporary.parking.no.barcode.gift.email.info.alt").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}



	public String getTempCardEmailNoBarcodeIndMembText () {
		List<WebElement> cardUseMessage = driver.findElements(NON_BARCODE_IND_EMAIL_CARD_INFO_TEXT);
		if (cardUseMessage.size() != 0) {
			String cardUsage = driver.findElement(NON_BARCODE_IND_EMAIL_CARD_INFO_TEXT).getText();

			return cardUsage;

		}
		return null;

	}
	public String getTempCardEmailNoBarcodeIndMembParkingText () {
		List<WebElement> parkingPassUseMessage = driver.findElements(NON_BARCODE_IND_EMAIL_PARKING_INFO_TEXT);
		if (parkingPassUseMessage.size() != 0) {
			String passUsage = driver.findElement(NON_BARCODE_IND_EMAIL_PARKING_INFO_TEXT).getText();
			return passUsage;
		}
		return null;

	}

	public String getTempCardEmailNoBarcodeMultipleMembText () {
		List<WebElement> cardUseMessage = driver.findElements(NON_BARCODE_MULTIPLE_EMAIL_CARD_INFO_TEXT);
		if (cardUseMessage.size() != 0) {
			String cardUsage = driver.findElement(NON_BARCODE_MULTIPLE_EMAIL_CARD_INFO_TEXT).getText();

			return cardUsage;

		}
		return null;

	}
	public String getTempCardEmailNoBarcodeMultipleMembParkingText () {
		List<WebElement> parkingPassUseMessage = driver.findElements(NON_BARCODE_MULTIPLE_EMAIL_PARKING_INFO_TEXT);
		if (parkingPassUseMessage.size() != 0) {
			String passUsage = driver.findElement(NON_BARCODE_MULTIPLE_EMAIL_PARKING_INFO_TEXT).getText();
			return passUsage;
		}
		return null;

	}

	public String getTempCardGiftEmailNoBarcodeMultipleMembText () {
		List<WebElement> cardUseMessage = driver.findElements(NON_BARCODE_MULTIPLE_GIFT_EMAIL_CARD_INFO_TEXT);
		if (cardUseMessage.size() != 0) {
			String cardUsage = driver.findElement(NON_BARCODE_MULTIPLE_GIFT_EMAIL_CARD_INFO_TEXT).getText();

			return cardUsage;

		}
		return null;

	}
	public String getTempCardGiftEmailNoBarcodeMultipleMembParkingText () {
			List<WebElement> parkingPassUseMessage = driver.findElements(NON_BARCODE_MULTIPLE_GIFT_EMAIL_PARKING_INFO_TEXT);
			if (parkingPassUseMessage.size() != 0) {
				String passUsage = driver.findElement(NON_BARCODE_MULTIPLE_GIFT_EMAIL_PARKING_INFO_TEXT).getText();
				return passUsage;
			}
			return null;

		}

	public String getTempCardGiftEmailNoBarcodeMultipleMembParkingTextAlt () {
		List<WebElement> parkingPassUseBullets = driver.findElements(NON_BARCODE_MULTIPLE_GIFT_EMAIL_PARKING_INFO_BULLETS);
		if (parkingPassUseBullets.size() != 0) {
			String passUsage = driver.findElement(NON_BARCODE_MULTIPLE_GIFT_EMAIL_PARKING_INFO_BULLETS).getText();
			return passUsage;
		}
		return null;

	}

	//Temp Card email with barcode

	public String getBarcodeEmailHeaderTextDisplayed(){

		List<WebElement>barcCodeEmailHeader = driver.findElements(BARCODE_EMAIL_HEADER);
		if(barcCodeEmailHeader.size()!=0){
			String barCodeEmail = driver.findElement(BARCODE_EMAIL_HEADER).getText();
			return barCodeEmail;
		}
		return null;

	}

	public String getBarcodeEmailBodyTextDisplayed(){

		List<WebElement>barcCodeEmailBodyText = driver.findElements(BARCODE_EMAIL_BODY_TEXT);
		if(barcCodeEmailBodyText.size()!=0){
			String barCodeEmailBody = driver.findElement(BARCODE_EMAIL_BODY_TEXT).getText();
			return barCodeEmailBody;
		}
		return null;

	}

	public String getBarcodeGiftEmailBodyTextDisplayed(){

		List<WebElement>barcCodeEmailBodyText = driver.findElements(BARCODE_GIFT_EMAIL_BODY_TEXT);
		if(barcCodeEmailBodyText.size()!=0){
			String barCodeEmailBody = driver.findElement(BARCODE_GIFT_EMAIL_BODY_TEXT).getText();
			return barCodeEmailBody;
		}
		return null;

	}

	public String getActualConfEmailWelcomePackBullets1Message() {

		List<String> message = new ArrayList<String>();
		List<WebElement> welcomePackBullets1Message = driver.findElements(CONFIRMATION_EMAIL_WELCOME_PACK_BULLET1);
		for (int i = 0; i < welcomePackBullets1Message.size(); i++) {
			message.add(welcomePackBullets1Message.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

	//more than 21 days

	public String getActualGiftConfEmailWelcomePackBullets1Message() {

		List<String> message = new ArrayList<String>();
		List<WebElement> welcomePackBullets1Message = driver.findElements(CONFIRMATION_GIFT_EMAIL_WELCOME_PACK_BULLET1);
		for (int i = 0; i < welcomePackBullets1Message.size(); i++) {
			message.add(welcomePackBullets1Message.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

	public String getActualGiftConfEmailWelcomePackBullets2Message() {

		List<String> message = new ArrayList<String>();
		List<WebElement> welcomePackBullets2Message = driver.findElements(CONFIRMATION_GIFT_EMAIL_WELCOME_PACK_BULLET2);
		for (int i = 0; i < welcomePackBullets2Message.size(); i++) {
			message.add(welcomePackBullets2Message.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

	public String getActualGiftLifeConfEmailWelcomePackBullets1Message() {

		List<String> message = new ArrayList<String>();
		List<WebElement> welcomePackBullets1Message = driver.findElements(CONFIRMATION_GIFT_LIFE_EMAIL_WELCOME_PACK_BULLET1);
		for (int i = 0; i < welcomePackBullets1Message.size(); i++) {
			message.add(welcomePackBullets1Message.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

	public String getActualGiftLifeConfEmailWelcomePackBullets2Message() {

		List<String> message = new ArrayList<String>();
		List<WebElement> welcomePackBullets2Message = driver.findElements(CONFIRMATION_GIFT_LIFE_EMAIL_WELCOME_PACK_BULLET2);
		for (int i = 0; i < welcomePackBullets2Message.size(); i++) {
			message.add(welcomePackBullets2Message.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

	public String getActualConfEmailWelcomePackBullets2Message() {

		List<String> message = new ArrayList<String>();
		List<WebElement> welcomePackBullets2Message = driver.findElements(CONFIRMATION_EMAIL_WELCOME_PACK_BULLET2);
		for (int i = 0; i < welcomePackBullets2Message.size(); i++) {
			message.add(welcomePackBullets2Message.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

	//less than 21 days

	public String getTempCardLess21DaysMultiMembEmailBarcodeMultipleMembText () {
		List<WebElement> cardUseMessage = driver.findElements(BARCODE_MULTIPLE_EMAIL_CARD_INFO_TEXT);
		if (cardUseMessage.size() != 0) {
			String cardUsage = driver.findElement(BARCODE_MULTIPLE_EMAIL_CARD_INFO_TEXT).getText();

			return cardUsage;

		}
		return null;

	}

	public String getActualGiftLess21DaysConfEmailWelcomePackBullets1Message() {

		List<String> message = new ArrayList<String>();
		List<WebElement> welcomePackBullets1Message = driver.findElements(CONFIRMATION_GIFT_LESS_21_EMAIL_WELCOME_PACK_BULLET1);
		for (int i = 0; i < welcomePackBullets1Message.size(); i++) {
			message.add(welcomePackBullets1Message.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

	public String getActualGiftLess21DaysConfEmailWelcomePackBullets2Message() {

		List<String> message = new ArrayList<String>();
		List<WebElement> welcomePackBullets2Message = driver.findElements(CONFIRMATION_GIFT_LESS_21_EMAIL_WELCOME_PACK_BULLET2);
		for (int i = 0; i < welcomePackBullets2Message.size(); i++) {
			message.add(welcomePackBullets2Message.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

	public String getConfirmationEmailHeader(){
		return driver.findElement(By.cssSelector("h1")).getText();
	}

	public String getEntireEmailContent(){
		return driver.getPageSource();
	}

	public String moderationConfirmationEmail() {
		driver.get(EnvironmentConfiguration.getSquirellURL());
		squirellPage().setNameFieldText();
		squirellPage().setPasswordFieldText();
		squirellPage().selectLoginButtonButton();
		driver.switchTo().frame(1);
		driver.findElement(By.linkText("Thank you, your dedication is now live")).click();
		driver.findElement(By.linkText("View as HTML")).click();
		squirellPage().getConfirmationEmailHeader().contains("Thank you for dedicating a donation");
		return squirellPage().getEntireEmailContent();
	}
}







