package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * Created by nick.thompson on 08/11/2016.
 */
public class PaymentOptionsPage extends PageBase {

	private static final By CHOOSE_HOW_TO_PAY_HEADER = By.cssSelector("#choosePayment > div > legend");

	public By PAY_BY_ANNUAL_DIRECT_DEBIT_RADIO_BUTTON = By.cssSelector("#annualDDPaymentOptionDiv > label");

	public By PAY_BY_MONTHLY_DIRECT_DEBIT_RADIO_BUTTON = By.cssSelector("#monthlyDDPaymentOptionDiv > label");

	public By PAY_BY_CREDIT_CARD_RADIO_BUTTON = By.cssSelector("#cardPaymentOptionDiv > label");

	private static final By MONTHLY_DD_PRICE_SHOWN = By.cssSelector("");

	private static final By ANNUAL_DD_PRICE_SHOWN = By.cssSelector("");

	private static final By ANNUAL_CARD_PRICE_SHOWN = By.cssSelector("");

	public By MONTHLY_DD_INFO_TEXT = By.cssSelector("#choosePayment > div > div > div:nth-child(1) > label > span.radio-panel--header > span.radio-panel--payment-option > span");

	public By ANNUAL_DD_INFO_TEXT = By.cssSelector("#choosePayment > div > div > div:nth-child(2) > label > span.radio-panel--header > span.radio-panel--payment-option");

	public By ANNUAL_CARD_INFO_TEXT = By.cssSelector("#choosePayment > div > div > div:nth-child(3) > label > span.radio-panel--header > span.radio-panel--payment-option");

	public By RENEW_PROMO_OPTION_1 = By.id("incentiveOption1");

	public By RENEW_PROMO_OPTION_2 = By.id("incentiveOption1");

	public String getPaymentOptionsPageHeaderLabel() {
		setLogs("Getting the manage direct debit payments label");
		return driver.findElement(PaymentOptionsPage.CHOOSE_HOW_TO_PAY_HEADER).getText();
	}

	public String getMonthlyDDPriceShown() {
		setLogs("Getting the monthly price shown");
		List<WebElement> monthlyDdPrice = driver.findElements(MONTHLY_DD_PRICE_SHOWN);
		if (monthlyDdPrice.size() != 0) {
			String monthlyDdPriceShown = driver.findElement(PaymentOptionsPage.MONTHLY_DD_PRICE_SHOWN).getText();
			return monthlyDdPriceShown;
		}
		return null;

	}

	public String getAnnualDDPriceShown() {
		setLogs("Getting the ANNUAL DD Price shown");
		List<WebElement> annualDdPrice = driver.findElements(ANNUAL_DD_PRICE_SHOWN);
		if (annualDdPrice.size() != 0) {
			String annualDDPriceShown = driver.findElement(PaymentOptionsPage.ANNUAL_DD_PRICE_SHOWN).getText();
			return annualDDPriceShown;
		}

		return null;
	}

	public String getAnnualCardPriceShown() {
		setLogs("Getting the Annual Card Price shown");
		List<WebElement> annualCardPrice = driver.findElements(ANNUAL_CARD_PRICE_SHOWN);
		if (annualCardPrice.size() != 0) {
			String annualCardPriceShown = driver.findElement(PaymentOptionsPage.ANNUAL_CARD_PRICE_SHOWN).getText();
			return annualCardPriceShown;
		}
		return null;
	}

	public String getMonthlyDDInfoText() {
		setLogs("Get Monthly DD info Text");
		List<WebElement> monthlyDdInfoText = driver.findElements(MONTHLY_DD_INFO_TEXT);
		if (monthlyDdInfoText.size() != 0) {
			String monthlyDdInfoTextShown = driver.findElement(paymentOptionPage().MONTHLY_DD_INFO_TEXT).getText();
			return monthlyDdInfoTextShown;
		}
		return null;
	}

	public String getAnnualDDInfoText() {
		setLogs("Get Annual DD Info Text");
		List<WebElement> annualDdInfoText = driver.findElements(ANNUAL_DD_INFO_TEXT);
		if (annualDdInfoText.size() != 0) {
			String annualDdInfoTextShown = driver.findElement(paymentOptionPage().ANNUAL_DD_INFO_TEXT).getText();
			return annualDdInfoTextShown;
		}
		return null;
	}

	public String getAnnualCardInfoText() {
		setLogs("Get Annual Card Info Text");
		List<WebElement> annualCardInfoText = driver.findElements(ANNUAL_CARD_INFO_TEXT);
		if (annualCardInfoText.size() != 0) {
			String annualCardInfoTextShown = driver.findElement(paymentOptionPage().ANNUAL_CARD_INFO_TEXT).getText();
			return annualCardInfoTextShown;
		}
		return null;
	}

	public void monthlyRadioButtonExists() {
		Helpers.waitForElementToAppear(driver, PAY_BY_MONTHLY_DIRECT_DEBIT_RADIO_BUTTON, 10, "Monthly DD radio button not displayed");
	}

	public boolean checkIfMonthlyRadioButtonIsDisplayed() {
		setLogs("check if Monthly Payment Option is displayed.......");
		return Helpers.waitForIsDisplayed(driver, PAY_BY_MONTHLY_DIRECT_DEBIT_RADIO_BUTTON, 10);
	}

	//		return driver.findElement(PAY_BY_MONTHLY_DIRECT_DEBIT_RADIO_BUTTON).isDisplayed();

	public Boolean annualDDRadioButtonExists() {
		return driver.findElement(PAY_BY_ANNUAL_DIRECT_DEBIT_RADIO_BUTTON).isDisplayed();

	}

	public boolean checkIfAnnualDDRadioButtonIsDisplayed() {
		setLogs("check if Annual DD Payment Option is displayed.......");
		return Helpers.waitForIsDisplayed(driver, PAY_BY_ANNUAL_DIRECT_DEBIT_RADIO_BUTTON, 10);
	}

	public Boolean annualCreditCardRadioButtonExists() {
		return driver.findElement(PAY_BY_CREDIT_CARD_RADIO_BUTTON).isDisplayed();
	}

	public boolean checkIfAnnualCardRadioButtonIsDisplayed() {
		setLogs("check if Annual Card Payment Option is displayed.......");
		return Helpers.waitForIsDisplayed(driver, PAY_BY_CREDIT_CARD_RADIO_BUTTON, 10);
	}

	public void monthlyRadioButtonSelected() {
		driver.findElement(PAY_BY_MONTHLY_DIRECT_DEBIT_RADIO_BUTTON).click();
	}

	public void annualRadioButtonSelected() {
		driver.findElement(PAY_BY_ANNUAL_DIRECT_DEBIT_RADIO_BUTTON).click();
	}

	public void annualCardRadioButtonSelected() {
		driver.findElement(PAY_BY_CREDIT_CARD_RADIO_BUTTON).click();
	}

	public String monthlyRadioButtonAppears() {
		List<WebElement> monthlyRadio = driver.findElements(PAY_BY_MONTHLY_DIRECT_DEBIT_RADIO_BUTTON);
		if (monthlyRadio.size() != 0) {

			setLogs("Monthly radio button appears when not expected to appear");
			//			driver.get(EnvironmentConfiguration.getBaseURL());
			driver.get(EnvironmentConfiguration.getText("MonthlyRadioButtonExists"));
			return "MonthlyRadioButtonExists";
		}

		return "NoMonthlyRadioButtonExists";

		//			setLogs("Monthly radio button appears when not expected to appear");

	}

	//RENEW

	public void setPayAndPromoOption(String promotionOption) {
		switch (promotionOption) {
			case "monthlyddOption1":
				driver.findElement(PAY_BY_MONTHLY_DIRECT_DEBIT_RADIO_BUTTON).click();
				//				driver.findElements(By.id("incentiveOption1")).stream()
				driver.findElements(RENEW_PROMO_OPTION_1).stream()
						.findFirst().ifPresent(WebElement::click);
				break;
			case "monthlyddOption2":
				driver.findElement(PAY_BY_MONTHLY_DIRECT_DEBIT_RADIO_BUTTON).click();
				driver.findElement(RENEW_PROMO_OPTION_2).click();
				break;
			case "annualddOption1":
				driver.findElement(PAY_BY_ANNUAL_DIRECT_DEBIT_RADIO_BUTTON).click();
				driver.findElement(RENEW_PROMO_OPTION_1).click();
				break;
			case "annualddOption2":
				driver.findElement(PAY_BY_ANNUAL_DIRECT_DEBIT_RADIO_BUTTON).click();
				driver.findElement(RENEW_PROMO_OPTION_2).click();
				break;
			case "juniorMonthlyDD":
				driver.findElement(PAY_BY_ANNUAL_DIRECT_DEBIT_RADIO_BUTTON).click();
				Assert.assertFalse(checkRenewMonthlyPromoOption1Appears(), "Promo options appearing when not expected to");
				break;
			case "youngMonthlyDD":
				driver.findElement(PAY_BY_ANNUAL_DIRECT_DEBIT_RADIO_BUTTON).click();
				Assert.assertTrue(checkRenewMonthlyPromoOption1Appears(), "Promo options not appearing when expected");
				break;
			case "cardPayment":
				driver.findElement(PAY_BY_CREDIT_CARD_RADIO_BUTTON).click();
				Assert.assertFalse(checkRenewMonthlyPromoOption1Appears(), "Promo options appearing when not expected");

				break;

		}
	}

	public boolean checkRenewMonthlyPromoOption1Appears() {
		setLogs("check if the promo option 1 appears");
		return Helpers.waitForIsDisplayed(driver, RENEW_PROMO_OPTION_1, 10);
	}

	protected WebDriver driver;

	public PaymentOptionsPage(WebDriver dr) {
		this.driver = dr;
	}

}



