package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by Nick.Thompson on 21/12/2016.
 */
public class DirectDebitPage extends PageBase {

	protected WebDriver driver;

	public DirectDebitPage(WebDriver dr) {
		this.driver = dr;
	}
	private static final By SORT_CODE_LABEL = By.cssSelector("#alternative-bank-details > div > div.medium-5.columns > label");

	public static By ACCOUNT_NAME_MISSING_ERROR_TEXT = By.cssSelector("#edit-form-container\\2e bankDetailsForm\\2e accountName-error > li");

	public static By ACCOUNT_NUMBER_WRONG_ERROR_TEXT = By.cssSelector("#edit-form-container\\2e bankDetailsForm\\2e accountNumber-error > li");

	public static By INVALID_ACCOUNT_SORT_CODE_COMBINATION_ERROR = By.cssSelector("#alternative-bank-details > div > div:nth-child(4) > ul > li");

	public static By SORT_CODE_ERROR_TEXT = By.cssSelector("#null-errors > ul > li");

	public static By TERMS_AND_CONDITIONS_NOT_ACCEPTED_ERROR = By.cssSelector ("#monthlyTermsContainer > div > div > div.alert-box.icon-block.danger.icon-danger > p");

	public By TERMS_AND_CONDITIONS_CHECKBOX = By.cssSelector("#main-content > section > form > div.group > div:nth-child(2) > div > fieldset > div > div > div:nth-child(2) > div > label");

	public By CONFIRM_AND_PAY_BUTTON = By.cssSelector("#confirmRenewSubmit");

	public By GET_ACCOUNT_NAME_FIELD = By.id("accountName");

	//public By RENEW_BANK_ACCOUNT_NAME=By.id("bankDetailsForm.accountName");
	//public By RENEW_BANK_ACCOUNT_NUMBER=By.id("bankDetailsForm.accountNumber");

	public By RENEW_BANK_ACCOUNT_NAME=By.cssSelector("input[id$='accountName']");

	public By RENEW_BANK_ACCOUNT_NUMBER=By.cssSelector("input[id$='accountNumber']");

	public By RENEW_SORT_CODE_ONE_FIELD = By.id("bankDetailsForm.sortCodeForm.sortCode1");

	public By RENEW_SORT_CODE_TWO_FIELD = By.id("bankDetailsForm.sortCodeForm.sortCode2");

	public By RENEW_SORT_CODE_THREE_FIELD = By.id("bankDetailsForm.sortCodeForm.sortCode3");

	public By GET_ACCOUNT_NUMBER_FIELD = By.id("accountNumber");

	public By GET_SORT_CODE_ONE_FIELD = By.cssSelector("#bankDetailsForm\\2e sortCodeForm\\2e sortCode1");

	public By GET_SORT_CODE_TWO_FIELD = By.cssSelector("#bankDetailsForm\\2e sortCodeForm\\2e sortCode2");

	public By GET_SORT_CODE_THREE_FIELD = By.cssSelector("#bankDetailsForm\\2e sortCodeForm\\2e sortCode3");

	public By MAIN_DIRECT_DEBIT_ERROR_MESSAGE = By.cssSelector("#renewMembershipForm > div:nth-child(1) > div:nth-child(1) > div > div > div > p");


	private static final By PAY_BY_MONTHLY_DIRECT_DEBIT_HEADER = By.cssSelector("#js-monthly-dd-intro");

	private static final By ACCOUNT_NAME_LABEL = By.cssSelector("#bankDetailsForm\\2e accountNameLabel");

	private static final By ACCOUNT_NUMBER_LABEL = By.cssSelector("#bankDetailsForm\\2e accountNumberLabel");

	public By GET_SORT_CODE_FIELD = By.id("sortCode");

	public By CONTINUE_BUTTON = By.cssSelector("#main-content > section > form > div:nth-child(3) > div > button");

	public By FREE_GIFT_CONSENTED_CHECKBOX = By.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset:nth-child(2) > div > div:nth-child(5) > label");

	public By GIFT_AID_CONSENT_CHECKBOX = By.cssSelector("#main-content > section > form > div.group > div:nth-child(2) > div > fieldset > div > div > div:nth-child(3) > div > label");

	public static By ACCOUNT_NAME_MISSING_ERROR = By.cssSelector("#accountName-error > li > span");

	public By ACCOUNT_NUMBER_MISSING_ERROR = By.id("accountNumber-error");

	public By SORT_CODE_MISSING_ERROR = By.id("sortCode-error");
	public static By SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR = By.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset > div > ul > li > span");

	public void populateRenewBankDetails(){
		String accountName ="test User Account";
		String accountNumber="55779911";
		String sortCodeFirst="20";
		String sortCodeMiddle="00";
		String sortCodeLast ="00";
		driver.findElement(RENEW_BANK_ACCOUNT_NAME).sendKeys(accountName);
		driver.findElement(RENEW_BANK_ACCOUNT_NUMBER).sendKeys(accountNumber);
		driver.findElement(RENEW_SORT_CODE_ONE_FIELD).sendKeys(sortCodeFirst);
		driver.findElement(RENEW_SORT_CODE_TWO_FIELD).sendKeys(sortCodeMiddle);
		driver.findElement(RENEW_SORT_CODE_THREE_FIELD).sendKeys(sortCodeLast);
	}

	public void setAccountName(String accountName) {
		driver.findElement(GET_ACCOUNT_NAME_FIELD).sendKeys(accountName);
	}

	public void setAccountNumber(String accountNumber) {
		driver.findElement(RENEW_BANK_ACCOUNT_NUMBER).sendKeys(accountNumber);
	}

	public void setSortCode(String sortCode) {
		driver.findElement(GET_SORT_CODE_FIELD).sendKeys(sortCode);
	}

	public void agreeTermsAndConditions(){
		driver.findElement(TERMS_AND_CONDITIONS_CHECKBOX).click();
	}

	public void setDirectDebitDetails (String ddName, String ddNumber, String ddSortCode) {
	setAccountName(ddName);
	setAccountNumber(ddNumber);
	setSortCode(ddSortCode);
}

	public String getSortCodeWrongErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, SORT_CODE_MISSING_ERROR, 10, "Sort code Missing text not displayed");
		List<WebElement> sortCodeErrorText = driver.findElements(SORT_CODE_MISSING_ERROR);
		if (sortCodeErrorText.size() != 0) {
			String sortCodeErrorTextDisplayed = driver.findElement(SORT_CODE_MISSING_ERROR).getText();

			return sortCodeErrorTextDisplayed;
		}
		return null;

	}
	public String getAccountNumberIncompleteErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, ACCOUNT_NUMBER_MISSING_ERROR, 10, "Account Number Missing text not displayed");
		List<WebElement> accountNumberErrorText = driver.findElements(ACCOUNT_NUMBER_MISSING_ERROR);
		if (accountNumberErrorText.size() != 0) {
			String accountNumberErrorTextDisplayed = driver.findElement(ACCOUNT_NUMBER_MISSING_ERROR).getText();

			return accountNumberErrorTextDisplayed;
		}
		return null;

	}
	public String getAccountNameIncompleteErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, ACCOUNT_NAME_MISSING_ERROR, 10, "Account Name Missing text not displayed");
		List<WebElement> accountNameErrorText = driver.findElements(ACCOUNT_NAME_MISSING_ERROR);
		if (accountNameErrorText.size() != 0) {
			String accountNameErrorTextDisplayed = driver.findElement(ACCOUNT_NAME_MISSING_ERROR).getText();

			return accountNameErrorTextDisplayed;
		}
		return null;

	}
	public String getSortCodeAndAccountNumberErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR, 10, "Bad Account text not displayed");
		List<WebElement> accountCombinationErrorText = driver.findElements(SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR);
		if (accountCombinationErrorText.size() != 0) {
			String accountCombinationErrorTextDisplayed = driver.findElement(SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR).getText();

			return accountCombinationErrorTextDisplayed;
		}
		return null;

	}

	public void setSortCode(String sortCodeOne, String sortCodeTwo, String sortCodeThree) {
		driver.findElement(GET_SORT_CODE_ONE_FIELD).sendKeys(sortCodeOne);
		driver.findElement(GET_SORT_CODE_TWO_FIELD).sendKeys(sortCodeTwo);
		driver.findElement(GET_SORT_CODE_THREE_FIELD).sendKeys(sortCodeThree);
	}
	public void setBankAccountDetails(String accountName, String accountNumber, String sortCode){
		setAccountName(accountName);
		setAccountNumber(accountNumber);
		setSortCode(sortCode);
		driver.findElement(CONTINUE_BUTTON).click();
	}

	public void selectOwnMoneyCheckbox() {
		driver.findElement(By.id("confirmOwnMoneyLabel")).click();
	}
	public void selectConfirmAndPayButton() {
		List<WebElement> confirmButton = driver.findElements(CONFIRM_AND_PAY_BUTTON);
		if (confirmButton.size() != 0) {
			driver.findElement(CONFIRM_AND_PAY_BUTTON).click();
		} else {
			setLogs("Confirm and pay button not displayed");
		}
	}


	public void populateDirectDebitDetails(){
		this.setAccountName("Mr Test");
		this.setAccountNumber("55779911");
		this.setSortCode("20", "00", "00");
	}
	public String getMonthlyPaymentDetailsHeaderText(){
		List<WebElement> payByDDHeader = driver.findElements(PAY_BY_MONTHLY_DIRECT_DEBIT_HEADER);
		if(payByDDHeader.size()!=0){
			String payByDirectDebitHeaderText = driver.findElement(PAY_BY_MONTHLY_DIRECT_DEBIT_HEADER).getText();
			return payByDirectDebitHeaderText;

		}
		return null;
	}

	public String getAccountNameFieldLabelText (){
		List<WebElement> accountNameLabel = driver.findElements(ACCOUNT_NAME_LABEL);
		if(accountNameLabel.size()!=0){
			String accountNameFieldLabeltext = driver.findElement(ACCOUNT_NAME_LABEL).getText();
			return accountNameFieldLabeltext;

		}
		return null;
	}
	public String getAccountNumberFieldLabelText(){
		List<WebElement> accountNumberLabel = driver.findElements(ACCOUNT_NUMBER_LABEL);
		if (accountNumberLabel.size()!=0){
			String accountNumberFieldLabeltext = driver.findElement(ACCOUNT_NUMBER_LABEL).getText();
			return accountNumberFieldLabeltext;

		}
		return null;
	}

	public String termsAndConditionsAppears (){
		List<WebElement> termsAppears = driver.findElements(TERMS_AND_CONDITIONS_CHECKBOX);
		if (termsAppears.size()!=0){
			setLogs("T & C radio button appears when not expected to appear");
			driver.get (EnvironmentConfiguration.getText("TermsAndConditionsCheckboxExists"));
			return "TermsAndConditionsCheckboxExists";
		}
		return "NoTermsAndConditionsCheckboxExists";
	}
	public boolean errorsList(){
		return driver.findElement(By.linkText("Please confirm the payment details you are providing match the member details shown")).isDisplayed();
	}

	public void selectTermAndConditionsCheckbox() {
		driver.findElement(TERMS_AND_CONDITIONS_CHECKBOX).click();
	}

	public String getPaymentDetailsIncompleteErrorText() {
		List<WebElement> ddIncompleteError = driver.findElements(MAIN_DIRECT_DEBIT_ERROR_MESSAGE);
		if (ddIncompleteError.size() != 0) {
			String ddIncompleteErrorText = driver.findElement(MAIN_DIRECT_DEBIT_ERROR_MESSAGE).getText();
			return ddIncompleteErrorText;
		}
		return null;
	}

	public String getPaymentDetailsInvalidErrorText() {
		List<WebElement> ddInvalidError = driver.findElements(INVALID_ACCOUNT_SORT_CODE_COMBINATION_ERROR);
		if (ddInvalidError.size() != 0) {
			String ddInvalidErrorText = driver.findElement(INVALID_ACCOUNT_SORT_CODE_COMBINATION_ERROR).getText();
			return ddInvalidErrorText;
		}
		return null;
	}

	public String getPaymentTermsNotAcceptedErrorText(){
		List<WebElement>  paymentTermsError = driver.findElements(TERMS_AND_CONDITIONS_NOT_ACCEPTED_ERROR);
		if (paymentTermsError.size()!=0){
			String paymentTermsNotAcceptedErrorText = driver.findElement(TERMS_AND_CONDITIONS_NOT_ACCEPTED_ERROR).getText();
			return paymentTermsNotAcceptedErrorText;
		}
		return null;
	}

	public String getSortCodeIncompleteErrorText () {
		List<WebElement> ddSortCodeIncomplete  = driver.findElements(SORT_CODE_ERROR_TEXT);
		if (ddSortCodeIncomplete.size()!=0){
			String ddSortCodeIncompleteErrorText = driver.findElement(SORT_CODE_ERROR_TEXT).getText();
			return ddSortCodeIncompleteErrorText;
		}
		return null;
	}

	public String getAccountNumberIncompleteErrorText () {
		List<WebElement> ddNumberIncompleteError = driver.findElements(ACCOUNT_NUMBER_WRONG_ERROR_TEXT);
		if (ddNumberIncompleteError.size()!=0){
			String ddNumberIncompleteErrorText = driver.findElement(ACCOUNT_NUMBER_WRONG_ERROR_TEXT).getText();
			return ddNumberIncompleteErrorText;
		}
		return null;
	}
	public String getAccountNumberMissingErrorText () {
		List<WebElement> ddNumberMissingError = driver.findElements(ACCOUNT_NUMBER_WRONG_ERROR_TEXT);
		if (ddNumberMissingError.size()!=0){
			String ddNumberMissingErrorText = driver.findElement(ACCOUNT_NUMBER_WRONG_ERROR_TEXT).getText();
			return ddNumberMissingErrorText;
		}
		return null;
	}

	public String getAccountNameMissingErrortext() {
		List<WebElement> ddNameMissingError = driver.findElements(ACCOUNT_NAME_MISSING_ERROR_TEXT);
		if (ddNameMissingError.size() != 0) {
			String ddNameMissingErrorText = driver.findElement(ACCOUNT_NAME_MISSING_ERROR_TEXT).getText();
			return ddNameMissingErrorText;
		}
		return null ;
	}
	public String getAccountSortCodeFieldLabelText (){
		List<WebElement> accountSortCodeLabel= driver.findElements(SORT_CODE_LABEL);
		if (accountSortCodeLabel.size()!=0){
			String accountSortCodeLabelText = driver.findElement(SORT_CODE_LABEL).getText();
			return accountSortCodeLabelText;
		}
		return null;
	}

}
