package uk.org.nationaltrust.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import uk.org.nationaltrust.apis.CreateAccountRequest;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.framework.TestBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sowjanya Annepu
 */
// DON-439 - 'Fundraising for us' Page form elements and action

public class FundraisingFormPage  extends PageBase{

    private  WebDriver driver;
    // Form elements
    @FindBy(id = "emailAddress")
    private WebElement EMAIL;

    @FindBy(id="telephoneNumber.number")
    private WebElement TELEPHONE;

    @FindBy(css = "#title")
    private WebElement TITLE;

    @FindBy(css = "#firstName")
    private WebElement FIRST_NAME ;

    @FindBy(css = "#lastName")
    private WebElement  LAST_NAME;

    @FindBy(css = "#postalAddress_countryCode")
    private WebElement COUNTRY ;

    @FindBy(id ="postcodeManualLink")
    private WebElement ADDRESS_LINK;

    @FindBy(css = "#postalAddress_addressLine1")
    private WebElement  ADDRESS_LINE1;

    @FindBy(css = "#postalAddress_postcode")
    private WebElement POSTCODE ;

    @FindBy(css = "#fundraisingType")
    private WebElement FUNDRAISER_TYPE ;

    @FindBy(id = "fundraisingDiscoverySource")
    private WebElement FUNDRAISING_DISCOVERY_SOURCE;

    @FindBy(id = "fundraisingDiscoverySourceOther")
    private WebElement FUNDRAISING_DISCOVERY_SOURCE_OTHER;

    @FindBy(id = "fundraisingReason")
    private WebElement FUNDRAISING_REASON ;

    @FindBy(id = "fundraisingReasonOther")
    private WebElement FUNDRAISING_REASON_OTHER ;

    @FindBy(id = "contactConsentForm.contactConsentedEmail-YesLabel")
    private WebElement CONTACT_EMAIL_YES_OPTION;

    @FindBy(id = "contactConsentForm.contactConsentedEmail-NoLabel")
    private WebElement CONTACT_EMAIL_NO_OPTION ;

    @FindBy(id = "contactConsentForm.contactConsentedPost-YesLabel")
    private WebElement  CONTACT_POST_YES_OPTION;

    @FindBy(id = "contactConsentForm.contactConsentedPost-NoLabel")
    private WebElement CONTACT_POST_NO_OPTION ;

    @FindBy(id = "contactConsentForm.contactConsentedTelephone-YesLabel")
    private WebElement  CONTACT_PHONE_YES_OPTION;

    @FindBy(id = "contactConsentForm.contactConsentedTelephone-NoLabel")
    private WebElement  CONTACT_PHONE_NO_OPTION;

    @FindBy(css ="label[for='confirmAge']")
    private WebElement CONFIRM_AGE ;

    @FindBy(id = "agreeTermsAndConditions")
    private WebElement TERMS_AND_CONDITIONS ;

    @FindBy(id="addressLookup")
    private WebElement ADDRESS_LOOKUP;

    @FindBy(id = "submitFundraisingPack")
    @CacheLookup
    private WebElement SUBMIT_BUTTON ;

    private String TITLE_ERROR="Please provide your title";
    private String FIRST_NAME_ERROR= "Please provide your first name";
    private String LAST_NAME_ERROR= "Please provide your last name";
    private String ADDRESS_LINE1_ERROR="entered the correct address";
    private String POSTCODE_ERROR= "entered the correct postcode";

    /* ----- Fundraiser type is not mandatory ----- */
    private String HEARD_ABPOUTUS_ERROR = "Please tell us how you heard about fundraising for us";
    private String FUNDRAISE_REASON_ERROR = "chosen to fundraise for us";
    private String CONFIRM_AGE_ERROR="Please confirm you are over the age of 18";
    private String TERMS_CONDITIONS_ERROR="Please confirm you have read  and agree to the terms and conditions";
    private String CONTACT_EMAIL_ERROR="Please select if you would like to be contacted by email";
    private String CONTACT_PHONE_ERROR="Please select if you would like to be contacted by telephone";
    private String CONTACT_POST_ERROR="Please select if you would like to be contacted by post";

    /* -----Text messages on the form---- */
    private String pageHaader="Request your fundraising kit";
    private String fundraiseKit="Please complete your details and submit the form to receive your free kit in the post";
    private String helpText="Please note, if you would like to fundraise for us and are under 18, please ask a parent or guardian to call us on 01793 817443 to register on your behalf.";
    private String FPNtext="We will use your email and phone number to talk to you about your fundraising activity. Your contact details will not be used for marketing unless you opt-in to receive marketing communications below.";
    private String fundraiseForUsPrivacyText="Your privacy is important to us. We will process your personal data securely for fundraising purposes, such as event administration or claiming Gift Aid. We will not use your details for marketing communications that you haven’t agreed to receive. At any time you can change your mind about whether to receive that information from us or how you’re contacted for marketing purposes at nationaltrust.org.uk or by contacting us on 0344 800 1895.";
    private String ageConfirmText="If you’re under 18, please ask a parent or guardian to call us on 01793 817443 to register on your behalf.";
    private String termsAConditionsText="By clicking submit, I confirm that I agree to the terms and conditions of the National Trust fundraising guidelines";
    private String contactingYouText=" TODO ";
    private String findAddressLink="If you can't find your address, you can enter it manually ";
    private String addressLookUpText="Please enter your post code or any part of your address";


    private boolean status=false;

    public FundraisingFormPage(WebDriver webDriver){
        this.driver=webDriver;
       // driver.get(EnvironmentConfiguration.getHomePageUrl()+"donate/fundraising-pack"); // TODO Old Url which is redirected to the actual URL
        driver.get(EnvironmentConfiguration.getHomePageUrl()+"donate/request-fundraising-kit");
        PageFactory.initElements(driver,this);

    }

    public ConfirmationPage buildFundraiseForm(CreateAccountRequest createAccountRequest,boolean otherFieldSelected){
        TELEPHONE.sendKeys(createAccountRequest.getTelephone());
        EMAIL.sendKeys(createAccountRequest.getActiveEmail());
        selectDropDown(TITLE,createAccountRequest.getTitle());
        FIRST_NAME.sendKeys(createAccountRequest.getFirstName());
        LAST_NAME.sendKeys(createAccountRequest.getLastName());
        ADDRESS_LINK.click();
        ADDRESS_LINE1.sendKeys(createAccountRequest.getAddressLine1());
        POSTCODE.sendKeys(createAccountRequest.getPostCode()); ;
        selectDropDown(FUNDRAISER_TYPE,"Organise my own event - other");
        if(otherFieldSelected)
        {
            Select dropDown= new Select(FUNDRAISING_DISCOVERY_SOURCE);
            dropDown.selectByValue("OTHER");
            FUNDRAISING_DISCOVERY_SOURCE_OTHER.sendKeys(createAccountRequest.getFundraisingDiscoverySourceOther());
            Select dropDown1= new Select(FUNDRAISING_REASON);
            dropDown1.selectByValue("OTHER");
            FUNDRAISING_REASON_OTHER.sendKeys(createAccountRequest.getFundraisingReasonOther());

        }
        else {
            selectDropDown(FUNDRAISING_DISCOVERY_SOURCE, createAccountRequest.getFundraisingDiscoverySource());
            selectDropDown(FUNDRAISING_REASON, createAccountRequest.getFundraisingReason());
        }
        CONTACT_EMAIL_YES_OPTION.click();
        CONTACT_POST_YES_OPTION.click();
        CONTACT_PHONE_YES_OPTION.click();
        CONFIRM_AGE.click();
        SUBMIT_BUTTON.click();
        Helpers.waitForJSandJQueryToLoad();
        if(driver.getPageSource().contains("Your kit is on its way!"))
            return confirmationPage();
        else
            return null;
    }


    public void getSupporterServiceDetails(){
    }


    public void selectDropDown(WebElement element, String dropDownValue){
        Select dropDown= new Select(element);
        dropDown.selectByVisibleText(dropDownValue);
    }

    public boolean formTextVerification(){
        String fundraiseFormPageSource=driver.getPageSource();
        fundraiseFormPageSource.contains("Thank you for your interest in fundraising for the National Trust.");
        fundraiseFormPageSource.contains(pageHaader);
        fundraiseFormPageSource.contains(fundraiseKit);
        fundraiseFormPageSource.contains(helpText);
        fundraiseFormPageSource.contains(FPNtext);
        fundraiseFormPageSource.contains(ageConfirmText);
        fundraiseFormPageSource.contains(termsAConditionsText);
        fundraiseFormPageSource.contains(contactingYouText);
        fundraiseFormPageSource.contains(fundraiseForUsPrivacyText);
        fundraiseFormPageSource.contains(findAddressLink);
        status=true;
        return status;
    }

    // TODO form starting link? - would it be a cms page

    public boolean validationErrorMessages(){
        SUBMIT_BUTTON.click();
        verifyErrorLink(TITLE_ERROR,"#title");
        verifyErrorLink(FIRST_NAME_ERROR,"#firstname");
        verifyErrorLink(LAST_NAME_ERROR,"#lastname");
        verifyErrorLink(ADDRESS_LINE1_ERROR,"#postalAddress_addressLine1");
        verifyErrorLink(POSTCODE_ERROR,"#postalAddress_postcode");
        verifyErrorLink(HEARD_ABPOUTUS_ERROR,"#fundraisingDiscoverySource");
        verifyErrorLink(FUNDRAISE_REASON_ERROR,"#fundraisingReason");
        verifyErrorLink(CONFIRM_AGE_ERROR,"#confirmAge");
        verifyErrorLink(CONTACT_EMAIL_ERROR,"#contactConsentForm_contactConsentedEmail");
        verifyErrorLink(CONTACT_PHONE_ERROR,"#contactConsentForm_contactConsentedTelephone");
        verifyErrorLink(CONTACT_POST_ERROR,"#contactConsentForm_contactConsentedPost");
       return true;
    }

    public boolean fundraiseFormBackLink() throws Exception{
        driver.findElement(By.linkText("fundraising for the National Trust")).click();
        Helpers.shortWait();
        return driver.getCurrentUrl().contains("features/fundraise-for-us");
    }

    public boolean metaTitle() throws Exception{
        try {
            driver.findElement(By.xpath("//title[contains(text(),'Request your fundraising kit | National Trust')]"));
            return true;
        }
        catch(Exception e){
            setLogs("wrong meta title is displayed");
            return false;
        }
    }

    public boolean findAddressLink(){
        ADDRESS_LINK.click();
        return ADDRESS_LINE1.isDisplayed();
    }

    public boolean addressLookup(){
        ADDRESS_LOOKUP.getText().contains(addressLookUpText);
        return true;
    }

    private boolean verifyErrorLink(String errorText, String linkCssValue){
       return  driver.findElement(By.xpath("//a[contains(text(),'"+errorText+"')]")).getAttribute("href").contains(linkCssValue);
    }

    public boolean titleOptions() {
        List<String> expectedTitles = Arrays.asList("Please select",
                "Dr.",
                "Master",
                "Miss",
                "Mr.",
                "Mrs.",
                "Ms.",
                "───",
                "Admiral");
        return getDropDownValuesFromWeb(TITLE).get(2).equalsIgnoreCase(expectedTitles.get(2));
    }

    private List<String> getDropDownValuesFromWeb(WebElement element){
        Select options= new Select(element);
        List<WebElement> dropdownOptions=options.getOptions();
        List<String> actualList= new ArrayList<>();
        for(int i=0; i<dropdownOptions.size(); i++) {
            actualList.add(dropdownOptions.get(i).getText());
        }
        return actualList;

    }

    public boolean fundraiserTypeOptions() {
        List<String> expectedFundraiserType = Arrays.asList("Please select","───", "Organise my own event - other'", "Join a challenge event", "Get sponsored for something", "Host a collection box", "I am not sure yet – please send me a fundraising kit for inspiration");
        for(int i=0; i<expectedFundraiserType.size()-1; i++) {
            status= false;
          getDropDownValuesFromWeb(FUNDRAISER_TYPE).get(i).equalsIgnoreCase(expectedFundraiserType.get(i));
            status= true;
        }
        return status;
    }

    public boolean heardAboutUsOptions(){
        List<String> expectedHeardAboutUs=Arrays.asList("Please select","───", "Email", "Social media", "Internet search", "Online advertisement","National Trust Magazine", "National Trust property", "National Trust website", "Friend / family / co-worker", "Poster or leaflet", "Other - please specify ");
        for(int i=0; i<expectedHeardAboutUs.size()-1; i++) {
            status= false;
            getDropDownValuesFromWeb(FUNDRAISING_DISCOVERY_SOURCE).get(i).equalsIgnoreCase(expectedHeardAboutUs.get(i));
            status= true;
        }

        return status;
    }

    public boolean fundraiseReasonOptions(){
        List<String> fundraiseReason=Arrays.asList("Please select",
                "───",
                "In memory of someone special",
                "In celebration of a special occasion or event",
                "I want to help look after special places",
                "Other - please specify");
        for(int i=0; i<fundraiseReason.size()-1; i++) {
            status= false;
            getDropDownValuesFromWeb(FUNDRAISING_REASON).get(i).equalsIgnoreCase(fundraiseReason.get(i));
            status= true;
        }
        return status;
    }

    public String DIYconfirmationEmail() {
        driver.get(EnvironmentConfiguration.getSquirellURL());
        squirellPage().setNameFieldText();
        squirellPage().setPasswordFieldText();
        squirellPage().selectLoginButtonButton();
        driver.switchTo().frame(1);
        driver.findElement(By.linkText("Thank you, your fundraising kit is on its way")).click();
        driver.findElement(By.linkText("View as HTML")).click();
        squirellPage().getConfirmationEmailHeader().contains("Thank you for supporting special places");
        return squirellPage().getEntireEmailContent();
    }

}