package uk.org.nationaltrust.pages;

import java.io.UnsupportedEncodingException;
import java.time.ZoneOffset;
import java.util.*;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.openqa.selenium.WebDriver;
import uk.org.nationaltrust.framework.*;

import static com.auth0.jwt.algorithms.Algorithm.HMAC256;
import static java.net.URLEncoder.encode;
import static java.time.LocalDateTime.now;

/**
 * Created by Nick.Thompson on 04/05/2017.
 */
public class Querys extends PageBase {
	protected WebDriver driver;

	CRMDBConnection crmdbConnection =new CRMDBConnection();

	SupporterServiceDBConnection supporterDBConnection = new SupporterServiceDBConnection();

	private static final String SUPPORTER_NUMBER_CLAIM = "supporterNumber";

	private static final String MASKED_EMAIL_CLAIM = "maskedEmail";

	private static final String CAMPAIGN_REFERENCE_CLAIM = "campaignReference";

	private final String secret = "secret";

	private String DONATE_Type="DONATE_commemorative";

	private String JOIN_type="JOIN_PromoCode";

	private String givenToken;

	public String sqlToGetMembershipDetailsFromCrmDB() {
		return "Select sp.SUPPORTER_FIRST_NAME as first_name, sp.SUPPORTER_LAST_NAME as last_name,sp.SUPPORTER_DOB as dob, " +
				"p.address1 as address, p.postal_code as postcode, p.country as country, p.status as status, p.party_number as supporterNumber, p.email_address as email," +
				"p.Primary_phone_area_code || primary_phone_number  as phone_number,mem.MEM_CHANNEL as channel,mem.MEM_START_DATE as mem_start_date, " +
				"mem.MEM_END_DATE as mem_end_date, mem.MEM_FIRST_JOINED as mem_first_joined,mem.MEM_PAYMENT_FREQ as payment_freq,mem.MEM_PAYMENT_METHOD as pay_method, " +
				"mem.MEM_STATUS as mem_status,mem.MEM_SOURCE_CODE as source, mem.MEM_TYPE_DESC as mem_type_desc, mem.MEM_TYPE as mem_type,ba.bank_account_num as bank_account_num, " +
				"bb.BRANCH_NUMBER as sortcode " +
				"from xx_membership mem, xx_supporter sp,hz_parties p,IBY_EXT_BANK_ACCOUNTS ba,APPS.IBY_EXT_BANK_BRANCHES_V bb " +
				"where mem.owner_party_id = sp.owner_party_id  and sp.owner_party_id = p.party_id and " +
				"mem.mem_payment_instr_id = ba.ext_bank_account_id and " +
				"ba.branch_id = bb.BRANCH_PARTY_ID and mem.mem_trip_pad_id =";
	}


	public Map<String, String> getTheSupporterDetailsForIndividualMembershipApplication( String applicationId, Set<String> labelNames) {
		String sqlSta = singleMembershipSql(applicationId);
		setLogs(sqlSta);
		return supporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleMembershipSql(String applicationId) {

		return String.format ("select MA.PROMOTIONAL_CODE, MA.PAYMENT_AMOUNT, MA.MEMBERSHIP_TYPE_LABEL, MA.APPLICATION_ID, PN.first_name, PN.last_name, PN.CONTACT_BY_EMAIL, PN.CONTACT_BY_PHONE, PN.CONTACT_BY_POST, MA.GIFT_AID_DECLARED, MA.GIFT_START_DATE, MA.PAYMENT_MEDIUM FROM"
				+ " SUPPORTERAPI_OWNER.MEMBERSHIP_APPLICATION MA,"
				+ " SUPPORTERAPI_OWNER.PERSON PN"
				+ " WHERE MA.LEAD_MEMBER_ID = pn.id"
				+ " AND MA.APPLICATION_ID = '%s'",applicationId);


	}

	public Map<String, String> getTheSupporterDetailsForIndividualMembershipApplicationGift(Set<String> labelNames) {
		String sqlSta = singleGiftMembershipSql();
		setLogs(sqlSta);
		return supporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleGiftMembershipSql() {

		return "select MA.PROMOTIONAL_CODE, MA.PAYMENT_AMOUNT, MA.MEMBERSHIP_TYPE_LABEL, MA.APPLICATION_ID, PN.first_name, PN.last_name,  MA.GIFT_AID_DECLARED, MA.PAYMENT_MEDIUM FROM"
				+ " SUPPORTERAPI_OWNER.MEMBERSHIP_APPLICATION MA,"
				+ " SUPPORTERAPI_OWNER.PERSON PN"
				+ " WHERE (select max(ma2.created)"
				+ " FROM supporterapi_owner.MEMBERSHIP_APPLICATION ma2) = MA.CREATED"
				+ " and MA.GIFT_GIVER_ID = pn.id";

	}

	public Map<String, String> getTheSupporterDetailsForIndividualMembershipApplicationGift1(String firstName, Set<String> labelNames) {
		String sqlSta = singleGiftMembershipSql1(firstName);
		setLogs(sqlSta);
		return supporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleGiftMembershipSql1(String firstName) {

		return String.format ("select MA.PROMOTIONAL_CODE, MA.PAYMENT_AMOUNT, MA.MEMBERSHIP_TYPE_LABEL, MA.APPLICATION_ID, PN.first_name, PN.last_name,  MA.GIFT_AID_DECLARED, MA.PAYMENT_MEDIUM FROM"
				+ " SUPPORTERAPI_OWNER.MEMBERSHIP_APPLICATION MA,"
				+ " SUPPORTERAPI_OWNER.PERSON PN"
				+ " WHERE MA.GIFT_GIVER_ID = pn.id"
				+ " AND PN.FIRST_NAME = '$s'",firstName);

	}

	public Map<String, String> getTheSupporterDetailsFromSupporterDb(String applicationId,Set<String>  labelNames) {
		String sqlSta = membershipDetailFromSupporterDbSql()+ "'"+applicationId+"'";
		setLogs(sqlSta);
		return supporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	public Map<String, String> getAdditionalMemberDetailsFromSupporterDb(String applicationId,Set<String>  labelNames) {
		String sqlSta = additionalMemberDetailsFromSupporterDbSql()+ "'"+applicationId+"'";
		setLogs(sqlSta);
		return supporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String membershipDetailFromSupporterDbSql(){
		return "SELECT p.id, t.name AS title, p.first_name AS first_name, p.last_name AS last_name," +
				" p.date_of_birth AS dob ,p.email_address AS email_id,tele.telephone_number as telephone_number ," +
				"pa.address_line_1 As address_line_1,pa.city as city ,pa.post_code as post_code,ma.MEMBERSHIP_TYPE_LABEL As Membership_Type, " +
				"ma.EMAIL_CONFIRMATION_STATUS as email_sent_status,ma.GIFT_AID_DECLARED as gift_aid_declared,ma.PAYMENT_AMOUNT as Paid_Amount," +
				"ma.PAYMENT_MEDIUM As Payment_Method, ma.application_id as order_reference, ma.PROMOTIONAL_CODE as promocode, " +
				"ma.EMAIL_CONFIRMATION_STATUS as email_confirmed FROM SUPPORTERAPI_OWNER.membership_application ma JOIN SUPPORTERAPI_OWNER.person p ON ma.lead_member_id = p.id " +
				"JOIN SUPPORTERAPI_OWNER.title t ON p.title_id = t.id JOIN SUPPORTERAPI_OWNER.postal_address_uk pa ON pa.person_id = p.id " +
				"JOIN SUPPORTERAPI_OWNER.telephone_number tele ON tele.id = p.PRIMARY_TELEPHONE_NUMBER_ID WHERE ma.application_id =" ;
	}

	public Map<String, String> getTheSupporterDetailsWithNewPrefsFromSupporterDb(String applicationId,Set<String>  labelNames) {
		String sqlSta = membershipDetailWithNewContactPrefsFromSupporterDbSql()+ "'"+applicationId+"'";
		setLogs(sqlSta);
		return supporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	public List<SupporterRecord> getTheSupporterDetailsWithNewPrefsFromSupporterDbUsingSupporterLombokReqquest(String applicationId) {
		String sqlSta = membershipDetailWithNewContactPrefsFromSupporterDbSql()+ "'"+applicationId+"'";
		setLogs(sqlSta);
		return supporterDBConnection.getSupporterData(sqlSta,JOIN_type);
		//		setLogs(supporterDetails);
	}

	private String membershipDetailWithNewContactPrefsFromSupporterDbSql(){
		return "SELECT p.id, t.name AS title, p.first_name AS first_name, p.last_name AS last_name," +
				" p.date_of_birth AS dob , MA.APPLICATION_ID AS application_id, p.email_address AS email_id,tele.telephone_number as telephone_number ," +
				"pa.address_line_1 As address_line_1,pa.city as city ,pa.post_code as post_code,ma.MEMBERSHIP_TYPE_LABEL As Membership_Type, " +
				"ma.EMAIL_CONFIRMATION_STATUS as email_sent_status,ma.GIFT_AID_DECLARED as gift_aid_declared,ma.PAYMENT_AMOUNT as Paid_Amount," +
				"ma.PAYMENT_MEDIUM As Payment_Method, ma.application_id as order_reference, ma.PROMOTIONAL_CODE as promocode, " +
				"p.CONTACT_BY_EMAIL, p.CONTACT_BY_PHONE, p.CONTACT_BY_POST, EMAIL_CONFIRMATION_STATUS as email_confirmed FROM SUPPORTERAPI_OWNER.membership_application ma JOIN SUPPORTERAPI_OWNER.person p ON ma.lead_member_id = p.id " +
				"JOIN SUPPORTERAPI_OWNER.title t ON p.title_id = t.id JOIN SUPPORTERAPI_OWNER.postal_address_uk pa ON pa.person_id = p.id " +
				"JOIN SUPPORTERAPI_OWNER.telephone_number tele ON tele.id = p.PRIMARY_TELEPHONE_NUMBER_ID WHERE ma.application_id =" ;
	}

	public Map<String, String> getTheGiftSupporterDetailsWithNewPrefsFromSupporterDb(String applicationId,Set<String>  labelNames) {
		String sqlSta = giftMembershipDetailWithNewContactPrefsFromSupporterDbSql()+ "'"+applicationId+"'";
		setLogs(sqlSta);
		return supporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String giftMembershipDetailWithNewContactPrefsFromSupporterDbSql(){
		return "SELECT p.id, t.name AS title, p.first_name AS first_name, p.last_name AS last_name," +
				" p.date_of_birth AS dob ,p.email_address AS email_id,tele.telephone_number as telephone_number ," +
				"pa.address_line_1 As address_line_1,pa.city as city ,pa.post_code as post_code,ma.MEMBERSHIP_TYPE_LABEL As Membership_Type, " +
				"ma.EMAIL_CONFIRMATION_STATUS as email_sent_status,ma.GIFT_AID_DECLARED as gift_aid_declared,ma.PAYMENT_AMOUNT as Paid_Amount," +
				"ma.PAYMENT_MEDIUM As Payment_Method, ma.application_id as order_reference, ma.PROMOTIONAL_CODE as promocode, " +
				"p.CONTACT_BY_EMAIL, p.CONTACT_BY_PHONE, p.CONTACT_BY_POST, EMAIL_CONFIRMATION_STATUS as email_confirmed FROM SUPPORTERAPI_OWNER.membership_application ma JOIN SUPPORTERAPI_OWNER.person p ON ma.gift_giver_id = p.id " +
				"JOIN SUPPORTERAPI_OWNER.title t ON p.title_id = t.id JOIN SUPPORTERAPI_OWNER.postal_address_uk pa ON pa.person_id = p.id " +
				"JOIN SUPPORTERAPI_OWNER.telephone_number tele ON tele.id = p.PRIMARY_TELEPHONE_NUMBER_ID WHERE ma.application_id =" ;
	}

	private String additionalMemberDetailsFromSupporterDbSql(){
		return "SELECT t.name AS title, p.first_name AS first_name,p.last_name as last_name, p.date_of_birth AS dob " +
				"FROM SUPPORTERAPI_OWNER.membership_application ma, SUPPORTERAPI_OWNER.person p, SUPPORTERAPI_OWNER.APPLICATION_ADDITIONAL_MEMBERS am, " +
				"SUPPORTERAPI_OWNER.title t WHERE ma.id = am.MEMBERSHIP_APPLICATION_ID AND am.person_id = p.id AND t.id = p.title_id AND ma.application_id =";
	}



	public String sqlToGetMembershipDetailsForDDPaymentsFromCrmDB(){
	    return "Select sp.SUPPORTER_TITLE as title, sp.SUPPORTER_FIRST_NAME as first_name, sp.SUPPORTER_LAST_NAME as last_name,sp.SUPPORTER_DOB as dob, " +
                "p.address1 as address, p.postal_code as postcode, p.country as country, p.status as status, p.party_number as supporterNumber, p.email_address as email," +
                "p.Primary_phone_area_code || primary_phone_number  as phone_number,mem.MEM_CHANNEL as channel,mem.MEM_START_DATE as mem_start_date, " +
                "mem.MEM_END_DATE as mem_end_date, mem.MEM_FIRST_JOINED as mem_first_joined,mem.MEM_PAYMENT_FREQ as payment_freq,mem.MEM_PAYMENT_METHOD as pay_method, " +
                "mem.MEM_STATUS as mem_status,mem.MEM_SOURCE_CODE as source, mem.MEM_TYPE_DESC as mem_type_desc, mem.MEM_TYPE as mem_type,ba.bank_account_num as bank_account_num, " +
                "bb.BRANCH_NUMBER as sortcode " +
                "from xx_membership mem, xx_supporter sp,hz_parties p,IBY_EXT_BANK_ACCOUNTS ba,APPS.IBY_EXT_BANK_BRANCHES_V bb " +
                "where mem.owner_party_id = sp.owner_party_id  and sp.owner_party_id = p.party_id and " +
                "mem.mem_payment_instr_id = ba.ext_bank_account_id and " +
                "ba.branch_id = bb.BRANCH_PARTY_ID and mem.mem_trip_pad_id =";
    }

    public String sqlToGetMembershipDetailsForCardPaymentsFromCrmDB(){
        return "Select sp.SUPPORTER_TITLE as title, sp.SUPPORTER_FIRST_NAME as first_name, sp.SUPPORTER_LAST_NAME as last_name," +
                "sp.SUPPORTER_DOB as dob, p.address1 as address, p.postal_code as postcode, p.country as country, p.status as status," +
                "p.email_address as email, p.Primary_phone_area_code || primary_phone_number  as phone_number,mem.MEM_CHANNEL as channel," +
                "mem.MEM_START_DATE as mem_start_date, mem.MEM_END_DATE as mem_end_date, mem.MEM_FIRST_JOINED as mem_first_joined," +
                "mem.MEM_PAYMENT_FREQ as payment_freq,mem.MEM_PAYMENT_METHOD as pay_method, mem.MEM_STATUS as mem_status," +
                "mem.MEM_SOURCE_CODE as source, mem.MEM_TYPE_DESC as mem_type_desc, mem.MEM_TYPE as mem_type " +
                " from xx_membership mem," +
                "xx_supporter sp,hz_parties p" +
                " where mem.owner_party_id = sp.owner_party_id  and " +
                "sp.owner_party_id = p.party_id " +
                " and mem.mem_trip_pad_id =";
    }


	public String sqlToGetContractPreferenceSelectionsForApplicationFromCrmDB(String applicationNumber){
		return String.format ("SELECT * FROM"
				+ "("
				+ "  SELECT hcp.contact_type,"
				+ "    ("
				+ "    CASE"
				+ "      WHEN ( hcp.preference_start_date          <= sysdate"
				+ "      AND NVL(hcp.preference_end_date,sysdate+1) > sysdate )"
				+ "      THEN 1"
				+ "      ELSE 0"
				+ "    END ) answer"
				+ "  FROM hz_contact_preferences hcp ,"
				+ "    fnd_lookup_values lkup1 ,"
				+ "    hz_parties hp,"
				+ "    xx_membership mem"
				+ "  WHERE contact_level_table     = 'HZ_PARTIES'"
				+ "  AND hcp.contact_type          = lkup1.lookup_code(+)"
				+ "  AND lkup1.lookup_type(+)      = 'CONTACT_TYPE'"
				+ "  AND hp.party_id               = contact_level_table_id"
				+ "  AND hcp.preference_start_date ="
				+ "    (SELECT MAX(hcp1.preference_start_date)"
				+ "    FROM hz_contact_preferences hcp1"
				+ "    WHERE hcp1.contact_type         = hcp.contact_type"
				+ "    AND hcp1.contact_level_table    = hcp.contact_level_table"
				+ "    AND hcp1.contact_level_table_id = hcp.contact_level_table_id"
				+ "    )"
				+ "  AND lkup1.attribute8='Y'"
				+ "  AND mem.owner_party_id = hp.party_id"
				+ "  AND mem.mem_trip_pad_id  = '%s'"
				+ ")"
				+ "PIVOT"
				+ "("
				+ "  sum(answer)"
				+ "  for contact_type in ('YEM', 'YTE', 'YPO', 'SS')"
				+ ")", applicationNumber);
	}

    public String sqlCheckLogTableInCrmDB(){
	    return "select tx_log_id as log_id,supporter_no as supporter_no, status as status, doc_ref as ref,doc_source as source from XX_TX_LOGS where doc_ref =";
    }

	public String sqlCheckAccountTableInAccountDB(){return "select STATE, SOURCE_SYSTEM, ACCOUNT_REFERENCE, SUPPORTER_NUMBER from ACCOUNTAPI_OWNER.ACCOUNT where ACCOUNT_EMAIL =";}

    public String sqlgetMembershipDetails(){return "SELECT p.id, t.name AS title, p.first_name AS first_name, p.last_name AS last_name,"
			+ " p.date_of_birth AS dob ,p.email_address AS email_id,tele.telephone_number as telephone_number ,"
			+ " pa.address_line_1 As address_line_1,pa.city as city ,pa.post_code as post_code,ma.MEMBERSHIP_TYPE_LABEL As Membership_Type,"
			+ " ma.EMAIL_CONFIRMATION_STATUS as email_sent_status,ma.GIFT_AID_DECLARED as gift_aid_declared,ma.PAYMENT_AMOUNT as Paid_Amount,"
			+ " ma.PAYMENT_MEDIUM As Payment_Method, ma.application_id as order_reference, ma.PROMOTIONAL_CODE as promocode, ma.TEMPORARY_CARD_EMAIL_STATUS as email_status,"
			+ " ma.EMAIL_CONFIRMATION_STATUS as email_confirmed, ma.LEAD_MEMBER_BARCODE as lead_barcode, ma.ADDITIONAL_MEMBER_BARCODE as additional_barcode, ma.PAID_REFERENCE as paid_ref FROM SUPPORTERAPI_OWNER.membership_application ma JOIN SUPPORTERAPI_OWNER.person p ON ma.lead_member_id = p.id"
			+ " JOIN SUPPORTERAPI_OWNER.title t ON p.title_id = t.id JOIN SUPPORTERAPI_OWNER.postal_address_uk pa ON pa.person_id = p.id"
			+ " JOIN SUPPORTERAPI_OWNER.telephone_number tele ON tele.id = p.PRIMARY_TELEPHONE_NUMBER_ID WHERE ma.application_id =";}

	public String sqlToGetPromoCode(){
    	return "select PROMOTIONAL_CODE as promocode, DESCRIPTION as description from SUPPORTERAPI_OWNER.promotion";
	}

    public List getThePromocodeDetailsFromSupporterDb() {
        List<String> promoDetails = new ArrayList<>();
        setLogs("getting promocode from supporter DB..............");
        String sqlSta = sqlToGetPromoCode();
        setLogs(sqlSta);
        promoDetails.add(0,supporterDBConnection.getData(sqlSta, "promocode"));
        promoDetails.add(1,supporterDBConnection.getData(sqlSta, "description"));
        setLogs("Promo code from db:- "+promoDetails.get(0));
        setLogs("promo code description:- "+ promoDetails.get(1));
        return promoDetails;
    }
	public String getTheAccountId(String email, String columnLable) {
		String accountId = supporterDBConnection.getData("select * from " + EnvironmentConfiguration.getText("account") + " where account_email=\'" + email + "\'", columnLable);
		return accountId;
	}

	public Querys (WebDriver dr) {
		this.driver = dr;
	}
	public String sqlCheckLogTableInCrmDBForNewMembershipRequest() {
		return "select * from "
				+ "XX_TX_LOGS where DOC_REF =";
	}
	public Map<String, String> getTheSupporterDetailsForDonatorFromDonateBasedOnFirstName(String usedFirstName, Set<String> labelNames) {
		String sqlSta = singleDonationBasedOnFirstNameSql(usedFirstName);
		setLogs(sqlSta);
		return supporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleDonationBasedOnFirstNameSql(String usedFirstName) {
		return String.format ("select DN.donation_id, DN.amount_pence, PN.first_name, PN.last_name, PN.email_address, PN.contact_by_phone, PN.contact_by_email, PN.contact_by_post, DN.gift_aid_consent, DN.crm_transaction_id, DN.created,PA.address_line_1, DN.donation_type, DN.submission_status, DN.payment_status FROM"
				+ " SUPPORTERAPI_OWNER.DONATION DN,"
				+ " SUPPORTERAPI_OWNER.PERSON PN,"
				+ " SUPPORTERAPI_OWNER.POSTAL_ADDRESS_UK PA"
				+ " WHERE dn.person_id = pn.id"
				+ " and dn.person_id = pa.person_id"
				+ " and PN.FIRST_NAME = '%s'", usedFirstName) ;
		//				+ " and rownum = 1"
		//				+ " ORDER BY DN.CREATED DESC";
	}

	public String GetDonationFromSupporterDB() {
		return  "select DN.donation_id, DN.amount_pence, PN.first_name, PN.last_name, PN.email_address, PN.contact_by_phone, PN.contact_by_email, PN.contact_by_post, DN.gift_aid_consent, DN.crm_transaction_id, DN.created,PA.address_line_1, DN.donation_type, DN.submission_status, DN.payment_status FROM"
				+ " SUPPORTERAPI_OWNER.DONATION DN,"
				+ " SUPPORTERAPI_OWNER.PERSON PN,"
				+ " SUPPORTERAPI_OWNER.POSTAL_ADDRESS_UK PA"
				+ " WHERE dn.person_id = pn.id"
				+ " and dn.person_id = pa.person_id"
				+ " and PN.FIRST_NAME = ";
	}

	public List<SupporterRecord> getTheSupporterDetailsForCommemorativeDonation(String userEmail) {
		String sqlSta = fetchCommemorativeDonationRecordSql(userEmail);
		setLogs(sqlSta);
		return supporterDBConnection.getSupporterData(sqlSta,DONATE_Type);
	}

	private String fetchCommemorativeDonationRecordSql(String userEmail){
		return String.format ("select DN.donation_id, DN.amount_pence, PN.first_name, PN.last_name, PN.email_address, PN.contact_by_phone, PN.contact_by_email, PN.contact_by_post, DN.gift_aid_consent, DN.crm_transaction_id, DN.created,PA.address_line_1, DN.donation_type, CM.relation, CM.subject, CM.occasion,CM.tone, DN.appeal_fund_code, DN.campaign_source_code  FROM"
				+ " SUPPORTERAPI_OWNER.DONATION DN,"
				+ " SUPPORTERAPI_OWNER.PERSON PN,"
				+ " SUPPORTERAPI_OWNER.POSTAL_ADDRESS_UK PA,"
				+ " SUPPORTERAPI_OWNER.COMMEMORATION CM"
				+ " WHERE dn.person_id = pn.id"
				+ " and dn.commemoration_id = cm.id"
				+ " and dn.person_id = pa.person_id"
				+ " and PN.email_address ='%s'", userEmail) ;
	}

	public List<SupporterRecord> getSupporterDetailsForDIYDonation(String email){
		String sqlstmt=fetchDIYDonationRecordSql(email);
		setLogs(sqlstmt);
		return supporterDBConnection.getSupporterData(sqlstmt,"DIY_DONATION");
	}

	private String fetchDIYDonationRecordSql(String email){
		return String.format("select * FROM SUPPORTERAPI_OWNER.FUNDRAISING_PACK"
				+ " WHERE email_address ='%s'",email);
	}

	public String sqlCheckStagingTableInCrmDB(String supporterNumber) {
		return String.format("select * from xx_cpc_supporter_stage"
				+ " where SUPPORTER_NUMBER = '%s'"
//				+ " And rownum <2"
				+ " order by TRX_LOG_ID desc", supporterNumber);
	}

//	public String sqlCheckStagingTableInCrmDB() {
//		return "select * from xx_cpc_supporter_stage "
//				+ "XX_TX_LOGS where EMAIL_Address =";
//
//	}
	public String sqlToGetContractPreferenceSelectionsForSupporterFromCrmDB(String supporterNumber) {
		return String.format("SELECT * FROM"
				+ "("
				+ "  SELECT hcp.contact_type,"
				+ "    ("
				+ "    CASE"
				+ "      WHEN ( hcp.preference_start_date          <= sysdate"
				+ "      AND NVL(hcp.preference_end_date,sysdate+1) > sysdate )"
				+ "      THEN 1"
				+ "      ELSE 0"
				+ "    END ) answer"
				+ "  FROM hz_contact_preferences hcp ,"
				+ "    fnd_lookup_values lkup1 ,"
				+ "    hz_parties hp"
				+ "  WHERE contact_level_table     = 'HZ_PARTIES'"
				+ "  AND hcp.contact_type          = lkup1.lookup_code(+)"
				+ "  AND lkup1.lookup_type(+)      = 'CONTACT_TYPE'"
				+ "  AND hp.party_id               = contact_level_table_id"
				+ "  AND hcp.preference_start_date ="
				+ "    (SELECT MAX(hcp1.preference_start_date)"
				+ "    FROM hz_contact_preferences hcp1"
				+ "    WHERE hcp1.contact_type         = hcp.contact_type"
				+ "    AND hcp1.contact_level_table    = hcp.contact_level_table"
				+ "    AND hcp1.contact_level_table_id = hcp.contact_level_table_id"
				+ "    )"
				+ "  AND lkup1.attribute8='Y'"
				+ " AND hp.party_number = '%s'"
				+ " )"
				+ " PIVOT"
				+ " ("
				+ "  sum(answer)"
				+ "  for contact_type in ('YEM','YTE','YPO','SS')"
				+ " )", supporterNumber);
	}
	public String sqlToGetSupporterNumberAndEmailAddress1() {
		return "SELECT hp.party_number, hl.postal_code , hp.EMAIL_ADDRESS"
				+ "    FROM hz_parties hp"
				+ "        JOIN hz_party_sites hps on hps.party_id = hp.party_id "
				+ "                               and nvl(hps.status,'A') = 'A' "
				+ "                               and hps.identifying_address_flag='Y'"
				+ "        JOIN hz_locations hl on hl.location_id = hps.location_id"
				+ "    WHERE hp.party_number =";
	}
	public String sqlCheckLogTableInCrmDBForNewPrefRequest(String supporterNumber) {
		return String.format("select tx_log_id, supporter_no, status, doc_ref,doc_source from "
				+ "XX_TX_LOGS where supporter_no ='%s'"
				+ "And rownum <2"
				+ "order by tx_log_id desc", supporterNumber);
	}


	public List<SupporterRecord> getTheSupporterDetailsForSingleMembInRenewalByMembershipType(String memType) {
		String sqlSta = membershipInRenewalByMembershipTypeSql(memType);
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String membershipInRenewalByMembershipTypeSql(String memType) {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = '"+memType+"'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}


	public List<SupporterRecord> getTheSupporterDetailsForSingleChildMembInRenewalPost1stMarch() {
		String sqlSta = singleChildMembershipInRenewalPost1stMarchSql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String singleChildMembershipInRenewalPost1stMarchSql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag" +
				"\n" +
				"FROM xx_csi_relationship xr" +
				"\n" +
				",xx_supporter xs" +
				"\n" +
				",xx_membership xm" +
				"\n" +
				",hz_parties hp" +
				"\n" +
				",hz_person_profiles hpp" +
				"\n" +
				",oe_order_lines_all oola" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn" +
				"\n" +
				"AND xr.subject_id = xs.support_id" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'" +
				"\n" +
				"AND xr.object_id = xm.support_id" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'" +
				"\n" +
				"and hpp.date_of_birth is not null" +
				"\n" +
				"and xm.mem_status = 'Active'" +
				"\n" +
				"and hp.email_address is not Null" +
				"\n" +
				"and xm.mem_type = 'CHD'" +
				"\n" +
				"and xm.mem_gift_flag is null" +
				"\n" +
				"and hpp.party_id = hp.party_id" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)" +
				"\n" +
				" And mem_start_date between to_date('" + Helpers.getRenewFutureDateTime(-11) + "','dd-Mon-yyyy') and to_date('" + Helpers.getRenewFutureDateTime(-9) + "','dd-Mon-yyyy')"+
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'" +
				"\n" +
				"and oola.line_type_id = 1047" +
				"\n" +
				"and oola.attribute1 = xm.support_id" +
				"\n" +
				"and rownum < 20";
	}

	public List<SupporterRecord> getTheSupporterDetailsForSingleFamilyMembInRenewal() {
		String sqlSta =singleFamilyMembershipInRenewalSql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String singleFamilyMembershipInRenewalSql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = 'FMG'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}
	public List<SupporterRecord> getTheSupporterDetailsForSingleJuniorMembInRenewal() {
		String sqlSta = singleJuniorMembershipInRenewalSql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String singleJuniorMembershipInRenewalSql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method != 'DIRECT_DEBIT'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = 'JUN'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}

	public List<SupporterRecord> getTheSupporterDetailsForSingleJuniorMembInRenewalPreMarch2018() {
		String sqlSta = singleJuniorMembershipInRenewalPreMarch2018Sql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
		//		setLogs(supporterDetails);
	}

	private String singleJuniorMembershipInRenewalPreMarch2018Sql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method != 'DIRECT_DEBIT'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = 'JUN'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				" And mem_start_date between to_date('" + Helpers.getRenewFutureDateTime(-14) + "','dd-Mon-yyyy') and to_date('" + Helpers.getRenewFutureDateTime(-12) + "','dd-Mon-yyyy')\n"+
				"\n" +
				"And mem_end_date < to_date('01-Mar-2018','dd-Mon-yyyy')\n"+
				"\n"+
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}

	public List<SupporterRecord> getTheSupporterDetailsForSingleJuniorMembInRenewalPostMarch2018() {
		String sqlSta = singleJuniorMembershipInRenewalPostMarch2018Sql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String singleJuniorMembershipInRenewalPostMarch2018Sql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = 'JUN'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n"+
				"And mem_end_date > to_date('01-Mar-2018','dd-Mon-yyyy')\n"+
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}

	public List<SupporterRecord> getTheSupporterDetailsForSingleChildMembInRenewal() {
		String sqlSta = singleChildMembershipInRenewalSql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
		//		setLogs(supporterDetails);
	}

	private String singleChildMembershipInRenewalSql() {
		// LocalDate startDate= new LocalDate.now().plusMonths();
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = 'CHD'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n" +
				" mem_end_date between to_date('25-Jan-2018','dd-Mon-yyyy') and to_date('26-Jul-2018','dd-Mon-yyyy')"+
//				" And mem_end_date between to_date('" + Helpers.getFutureDateTime(-3) + "','dd-Mon-yyyy') and to_date('" + Helpers.getFutureDateTime(-1) + "','dd-Mon-yyyy')\n"+
				"\n"+
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}

	public List<SupporterRecord> getTheSupporterDetailsForSingleFamilyOneAdultMembInRenewal() {
		String sqlSta = singleFamilyOneAdultMembershipInRenewalSql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
		//		setLogs(supporterDetails);
	}

	private String singleFamilyOneAdultMembershipInRenewalSql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type ='FOA'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}
	public List<SupporterRecord> getTheSupporterDetailsForSingleLapsedMembInRenewal() {
		String sqlSta = singleLapsedMembInRenewal();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
		//		setLogs(supporterDetails);
	}

	private String singleLapsedMembInRenewal() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Lapsed'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type ='FOA'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"And mem_start_date between to_date('" + Helpers.getRenewFutureDateTime(-14) + "','dd-Mon-yyyy') and to_date('" + Helpers.getRenewFutureDateTime(-12) + "','dd-Mon-yyyy')\n"+
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
		}

	public List<SupporterRecord> getTheSupporterDetailsForSingleIndividualMembInRenewalWithemail() {
		String sqlSta = singleMembershipInRenewalWithEmailSql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String singleMembershipInRenewalWithEmailSql() {
		return "select hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status, xm.mem_start_date, xm.mem_end_date, xm.mem_type, xm.mem_type_desc  FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp,"
				+ " oe_order_lines_all oola"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_payment_method != 'DIRECT_DEBIT'"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'IND'"
				+ " and hp.postal_code is not null"
				+ " And hp.email_address is not null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				+ " And mem_end_date between to_date('" + Helpers.getRenewFutureDateTime(-3) + "','dd-Mon-yyyy') and to_date('" + Helpers.getRenewFutureDateTime(-1) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date between to_date ('" + Helpers.getFutureDateTime(-14) + "','dd-Mon-yyyy')"
				//				+ " And to_date ('" + Helpers.getFutureDateTime(-12) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and NVL(oola.booked_flag, 'N') = 'N'"
				+ " and oola.line_type_id = 1047"
				+ " and oola.attribute1 = xm.support_id"
				+ " and rownum < 5";
	}

	public List<SupporterRecord>  getTheSupporterDetailsSingleMembershipNoEmail() {
		String sqlSta =singleMembershipWhereNoEmailExists();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
		//		setLogs(supporterDetails);
	}

	private String singleMembershipWhereNoEmailExists() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is null\n" +
				"\n" +
				"and xm.mem_type = 'IND'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}

	public List<SupporterRecord> getTheSupporterDetailsForSingleJointMembInRenewal() {
		String sqlSta = singleJointMembershipInRenewalSql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String singleJointMembershipInRenewalSql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = 'JNT'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}

	public List<SupporterRecord> getTheSupporterDetailsForSingleDDIndMembInRenewal() {
		String sqlSta = singleDDIndMembershipInRenewalSql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String singleDDIndMembershipInRenewalSql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'DIRECT_DEBIT'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = 'IND'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}


	public List<SupporterRecord> getTheSupporterDetailsForSingleIndividualMembInRenewal() {
		String sqlSta = singleMembershipInRenewalSql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String singleMembershipInRenewalSql() {
		return "select hp.party_number, hpp.date_of_birth, hp.postal_code,xm.mem_payment_method, xm.mem_status,xm.mem_start_date,xm.mem_end_date, xm.mem_type, xm.mem_type_desc FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp,"
				+ " oe_order_lines_all oola"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_payment_method != 'DIRECT_DEBIT'"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'IND'"
				+ " and hp.postal_code is not null"
//				+ " And hp.email_address is not null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				+ " And mem_end_date between to_date('" + Helpers.getRenewFutureDateTime(-2) + "','dd-Mon-yyyy') and to_date('" + Helpers.getRenewFutureDateTime(+1) + "','dd-Mon-yyyy')"
//				+ " And mem_start_date between to_date ('" + Helpers.getFutureDateTime(-14) + "','dd-Mon-yyyy')"
//				+ " And to_date ('" + Helpers.getFutureDateTime(-12) + "','dd-Mon-yyyy')"
//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and NVL(oola.booked_flag, 'N') = 'N'"
				+ " and oola.line_type_id = 1047"
				+ " and oola.attribute1 = xm.support_id"
				+ " and rownum < 5";
	}
	public List<SupporterRecord> getTheSupporterDetailsForSingleIndividualMembInRenewalRenewalDateCheck() {
		String sqlSta = singleMembershipInRenewalSqlRenewalDateCheck();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String singleMembershipInRenewalSqlRenewalDateCheck() {
		return "select hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status, xm.mem_start_date, xm.mem_end_date, xm.mem_type, xm.mem_type_desc FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp,"
				+ " oe_order_lines_all oola  "
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_payment_method != 'DIRECT_DEBIT'"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ " And xm.mem_type = 'IND'"
				+ " And hp.email_address is Not Null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5) "
				+ " And mem_start_date between to_date('" + Helpers.getRenewFutureDateTime(-14) + "','dd-Mon-yyyy') and to_date('" + Helpers.getRenewFutureDateTime(-12) + "','dd-Mon-yyyy') "
				//				+ " And mem_start_date between to_date ('" + Helpers.getFutureDateTime(-14) + "','dd-Mon-yyyy')"
				//				+ " And to_date ('" + Helpers.getFutureDateTime(-12) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and NVL(oola.booked_flag, 'N') = 'N' and oola.line_type_id = 1047 and oola.attribute1 = xm.support_id "
				+ " and rownum < 5";

	}

	public String getAccountReference() {
		return "select ACCOUNT_REFERENCE from "
				+ "ACCOUNTAPI_OWNER.ACCOUNT where SUPPORTER_NUMBER =";


	}


	public String sqlToGetPersonalDetailsFromAccountDB() {
		return  "select AC.SOURCE_SYSTEM, AC.ACCOUNT_EMAIL, AC.SUPPORTER_NUMBER, AC.REGISTRY_EXPORT_TIMESTAMP,AC.STATE, CP.CONTACT_BY_EMAIL, CP.CONTACT_BY_PHONE,"
				+ " CP.CONTACT_BY_POST, PD.CONTACT_EMAIL, PD.FIRST_NAME, PD.LAST_NAME,"
				+ " PD.ADDRESS_LINE_1, PD.ADDRESS_LINE_2, PD.ADDRESS_LINE_3, PD.ADDRESS_LINE_4,"
				+ " PD.CITY, PD.COUNTY, PD.POST_CODE, PD.TELEPHONE_NUMBER, PD.ALTERNATIVE_TELEPHONE_NUMBER"
				+ " FROM ACCOUNTAPI_OWNER.ACCOUNT AC, ACCOUNTAPI_OWNER.CONTACT_PREFERENCES CP, ACCOUNTAPI_OWNER.PERSONAL_DETAILS PD"
				+ " where CP.ACCOUNT_ID = AC.ID"
				+ " AND PD.ACCOUNT_ID = CP.ACCOUNT_ID"
				+ " AND AC.ACCOUNT_EMAIL = ";
	}
//	public String sqlToGetPersonalDetailsFromAccountDB() {
//		return "select SUPPORTER_NUMBER from ACCOUNTAPI_OWNER.ACCOUNT where ACCOUNT_EMAIL = ";
//	}

	public String sqlToGetSupporterNumberAndEmailAddress(String supporterNumber) {
		return String.format("SELECT hp.party_number as supporterNumber, hp.email_address as emailAddress"
				+ " from  xx_supporter xs"
				+ ",    hz_parties hp"
				+ ",    hz_person_profiles hpp"
				+ " WHERE hp.party_number = '%s'"
				+ " And rownum <2", supporterNumber);

	}
	public String sqlGetSupporterNumberFromCRMLogTable(String applicationId) {
		return String.format("select tx_log_id, supporter_no, status, doc_ref,doc_source from "
				+ "XX_TX_LOGS where doc_ref ='%s'"
				+ "And rownum <2"
				+ "order by tx_log_id desc", applicationId);
	}

	public String sqlCheckRegistrationExperienceEntry() {

		return  "select * from ACCOUNTAPI_OWNER.REGISTRATION_EXPERIENCE RE, ACCOUNTAPI_OWNER.ACCOUNT AC where AC.ID=RE.ACCOUNT_ID and AC.ACCOUNT_EMAIL =";

	}

	public String sqlCheckStagingTableForDDUpdate(String supporterNumber) {
		return String.format("select TRX_LOG_ID, MEMBERSHIP_NUMBER, SUPPORTER_NUMBER, TRX_STATUS, ACCOUNT_NAME, SORT_CODE, ACCOUNT_NUMBER from xx_direct_debit_stage"
				+ " where SUPPORTER_NUMBER = '%s'"
				//				+ " And rownum <2"
				+ " order by TRX_LOG_ID desc", supporterNumber);
	}

	public String sqlCheckCRMDBForDDForMembership(String membershipNumber) {
		return String.format("SELECT ba.bank_account_name\n"
				+ ",      ba.bank_account_num\n"
				+ ",      hop.BANK_OR_BRANCH_NUMBER\n"
				+ "FROM   iby_ext_bank_accounts ba \n"
				+ ",      hz_organization_profiles hop\n"
				+ ",      xx_membership xm\n"
				+ "WHERE  1=1\n"
				+ "AND    hop.party_id = ba.BRANCH_ID\n"
				+ "AND    TRUNC(sysdate) BETWEEN ba.start_date AND NVL(ba.end_date, TRUNC(SYSDATE + 1))\n"
				+ "and    TRUNC(SYSDATE) BETWEEN hop.effective_start_date AND NVL(hop.effective_end_date, TRUNC(SYSDATE + 1))\n"
				+ "AND    TRUNC(NVL(ba.END_DATE,SYSDATE+1)) >= TRUNC(SYSDATE)\n"
				+ "AND    ba.ext_bank_account_id = xm.mem_payment_instr_id\n"
				+ "AND    xm.mem_number = '%s'", membershipNumber);
	}

	private static String urlEncodeToken(String token) {
		try {
			return encode(token, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Unsupported encoding");
		}
	}

	//	@Test
	//
	//	public void createToken (){
	//		String getToken = createToken();
	//	}

	public void reset() {
		// no op because it is glue scope
	}

	public String createToken() {
		if (givenToken != null) {
			throw new IllegalStateException("given token already generated!");
		}

		givenToken = urlEncodeToken(JWT.create()
				.withIssuedAt(currentDate())
				.withClaim(SUPPORTER_NUMBER_CLAIM, "1234")
				.withClaim(MASKED_EMAIL_CLAIM, "a*****b@gmail.com")
				.withClaim(CAMPAIGN_REFERENCE_CLAIM, "ABC")
				.withExpiresAt(futureDate())
				.sign(algorithm()));

		return givenToken;
	}

	public String createExpiredToken() {
		if (givenToken != null) {
			throw new IllegalStateException("given token already generated!");
		}
		this.givenToken = JWT.create()
				.withClaim("supporterNumber", "123456789")
				.withClaim("maskedEmail", "d***********n@g***l.c*m")
				.withIssuedAt(currentDate())
				.withExpiresAt(expiredDate())
				.withClaim("campaignReference", "dev test token controller ")
				.sign(algorithm());
		return givenToken;
	}

	public String getGivenToken() {
		return givenToken;
	}

	private Algorithm algorithm() {
		Algorithm algorithm;
		try {
			algorithm = HMAC256(this.secret);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Failed to create algorithm for test jwt token", e);
		}
		return algorithm;
	}

	private Date currentDate() {
		return Date.from(now().toInstant(ZoneOffset.UTC));
	}

	private Date futureDate() {
		return Date.from(now().plusMonths(6).toInstant(ZoneOffset.UTC));
	}

	private Date expiredDate() {
		return Date.from(now().minusDays(1).toInstant(ZoneOffset.UTC));
	}

	public Map<String, String> getcontacPrefDetailsFromSOADB(String supporterNumber, Set<String> labelNames) {
		String sqlSta = sqlToGetContractPreferenceSelectionsForSupporterFromCrmDB(supporterNumber);
		setLogs(sqlSta);
		return crmdbConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String contactPrefData() {

		return String.format("select * FROM xx_tx_logs"
				+ " where SUPPORTER_No =");

	}

	public String sqlActivateNewShopCreatedMYNTAccount(String email) {
		return String.format("update ACCOUNTAPI_OWNER.ACCOUNT set STATE = 'ACTIVE' where ACCOUNT_EMAIL = '%s'", email);
	}

	public String supporterPersonalDetails1(String donationID) {
		return String.format( "SELECT hp.party_number as supporter_number, hp.person_pre_name_adjunct as title, hp.person_first_name as first_name,"
				+ "       hp.person_last_name as last_name,"
				+ "       hp.address1 as address1,"
				+ "       hp.address2 as address2,"
				+ "       hp.address3 as address3,"
				+ "       hp.address4 as address4,"
				+ "       hp.county as county,"
				+ "       hp.city as city,"
				+ "       hp.state as state,"
				+ "       hp.country as country,"
				+ "       hp.email_address as email,"
				+ "       (SELECT SS"
				+ "          FROM xx_contact_details_V"
				+ "         WHERE party_id = hp.party_id)"
				+ "          SS,"
				+ "       xx_don.don_ga_eligability,"
				+ "       xx_don.don_total as amount,"
				+ "       xx_don.don_category,"
				+ "       xx_don.don_type,"
				+ "       xx_don.don_channel,"
				+ "       xx_don.don_fund_Code,"
				+ "       xx_don.don_last_pay_date,"
				+ "       xx_don.don_last_pay_status,"
				+ "       xx_don.don_last_pay_amount,"
				+ "       xx_don.don_payment_method,"
				+ "       xx_don.don_property_id,"
				+ "       xx_don.don_source_code,"
				+ "       xx_don.don_start_date,"
				+ "       xx_don.creation_date as creation_date,"
				+ " xx_ga.CURRENT_GIFT_AID_TYPE as gift_aid"
				+ " FROM xx_Tx_logs x, hz_parties hp, xx_donation xx_don, xx_supporter_gift_aid_v xx_ga"
				+ " WHERE 1 = 1 "
				+ " AND xx_don.owner_party_id=hp.party_id"
				+ " AND xx_ga.party_id=hp.party_id"
				+ " AND hp.party_number =x.supporter_no"
				+ " AND  x.doc_Ref = '%s'", donationID);
	}
	public String supporterPersonalDetails(String donationID) {
		return String.format("SELECT hp.party_number,"
				+ "       hp.person_pre_name_adjunct,"
				+ "       hp.person_first_name,"
				+ "       hp.person_last_name,"
				+ "       hp.address1,"
				+ "       hp.address2,"
				+ "       hp.address3,"
				+ "       hp.address4,"
				+ "       hp.county,"
				+ "       hp.city,"
				+ "       hp.state,"
				+ "       hp.country,"
				+ "       hp.email_address,"
				+ "       (SELECT SS"
				+ "          FROM xx_contact_details_V"
				+ "         WHERE party_id = hp.party_id)"
				+ "          SS,"
				+ "       xx_don.don_ga_eligability,"
				+ "       xx_don.don_total,"
				+ "       xx_don.don_category,"
				+ "       xx_don.don_type,"
				+ "       xx_don.don_channel,"
				+ "       xx_don.don_fund_Code,"
				+ "       xx_don.don_last_pay_date,"
				+ "       xx_don.don_last_pay_status,"
				+ "       xx_don.don_last_pay_amount,"
				+ "       xx_don.don_payment_method,"
				+ "       xx_don.don_property_id,"
				+ "       xx_don.don_source_code,"
				+ "       xx_don.don_start_date,"
				+ "       xx_don.creation_date,"
				+ " xx_ga.CURRENT_GIFT_AID_TYPE"
				+ "  FROM xx_Tx_logs x, hz_parties hp, xx_donation xx_don, xx_supporter_gift_aid_v xx_ga"
				+ " WHERE 1 = 1 "
				+ " AND xx_don.owner_party_id=hp.party_id"
				+ " AND xx_ga.party_id=hp.party_id"
				+ " AND hp.party_number =x.supporter_no"
				+ " AND  x.doc_Ref = '%s'", donationID);

	}
	public List<SupporterRecord> getTheSupporterDetailsForSingleGiftMembInRenewal() {
		String sqlSta = singleGiftMembershipInRenewalSql();
		setLogs(sqlSta);
		return crmdbConnection.getSupporterData(sqlSta);
	}

	private String singleGiftMembershipInRenewalSql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = 'IND'\n" +
				"\n" +
				"and xm.mem_gift_flag ='TRUE'\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}

	private String UpdateMembershipCreatedDateInSupporterSql(String applicationId) {
		System.out.println( "Hello World!" );
		return String.format("Update SUPPORTERAPI_OWNER.SUPPORTER_INTERACTION SET CREATED = '05-MAR-17 07.33.19.354000000' "+" WHERE EMAIL_ADDRESS = '%s' "+"" ,applicationId);

	}



	public void setOlderDateAndTimeForApplication1(String newDate, String applicationID) {
		String sqlSta = "Update SUPPORTERAPI_OWNER.MEMBERSHIP_APPLICATION SET APPLICATION_CREATED = '"+newDate+"'" + " WHERE APPLICATION_ID = '"+applicationID+"'";
		setLogs(sqlSta);
		supporterDBConnection.setData(sqlSta);
	}
//this is a change for JOIN 854
	public void setOlderDateAndTimeForApplication(String newDate, String applicationID) {
		String sqlSta = "Update SUPPORTERAPI_OWNER.MEMBERSHIP_APPLICATION SET PAID_TIMESTAMP = '"+newDate+"'" + " WHERE APPLICATION_ID = '"+applicationID+"'";
		setLogs(sqlSta);
		supporterDBConnection.setData(sqlSta);
	}

	public void setDeliverySuccessEntryForApplicationToPreventItSubmitting(String id, String applicationID, String subID) {
		String sqlSta = "Insert INTO SUPPORTERAPI_OWNER.DELIVERY_SUCCESS (ID, VERSION, APPLICATION_ID, SUBMITTED_AT, SUBMISSION_ID, CREATED, LAST_UPDATED) "
				+ "VALUES ('"+id+"', '0', '"+applicationID+"', '22-SEP-18 12.00.02.397000000', '"+subID+"', '22-SEP-18 12.00.02.397000000', '22-SEP-18 12.00.02.397000000')";
		setLogs(sqlSta);
		supporterDBConnection.setData(sqlSta);
	}

	public void updateAccount(String supporter) {
		String sqlSta = "UPDATE ACCOUNTAPI_OWNER.ACCOUNT SET SUPPORTER_NUMBER = '' where SUPPORTER_NUMBER = '"+supporter+"'";
		setLogs(sqlSta);
		supporterDBConnection.setData(sqlSta);
	}

	public void updateAccountEmail(String email, String otherEmail) {
		String sqlSta = "UPDATE ACCOUNTAPI_OWNER.ACCOUNT SET ACCOUNT_EMAIL = '"+otherEmail+"' where ACCOUNT_EMAIL = '"+email+"'";
		setLogs(sqlSta);
		supporterDBConnection.setData(sqlSta);
	}

	public void updateUserNameAndPasswordEmail(String email, String otherEmail) {
		String sqlSta = "UPDATE ACCOUNTAPI_OWNER.USERNAME_AND_PASSWORD SET USERNAME = '"+otherEmail+"' where USERNAME = '"+email+"'";
		setLogs(sqlSta);
		supporterDBConnection.setData(sqlSta);
	}

	public void updateAccountToMakeETLOlderDate(String email, String newDate) {
		String sqlSta = "UPDATE ACCOUNTAPI_OWNER.ACCOUNT SET REGISTRY_EXPORT_TIMESTAMP = '"+newDate+"' where ACCOUNT_EMAIL = '"+email+"'";
		setLogs(sqlSta);
		supporterDBConnection.setData(sqlSta);
	}

	public void updatePriceListEndDate() {
		String sqlSta = "UPDATE SUPPORTERAPI_OWNER.PRICE_LIST  SET VALID_UNTIL = '01-MAR-2019' where ID = 'DD6C90BFFC394F518B5A5D6BE1C394C2'";
		setLogs(sqlSta);
		supporterDBConnection.setData(sqlSta);
	}

	public void resetPriceListEndDate() {
		String sqlSta = "UPDATE SUPPORTERAPI_OWNER.PRICE_LIST SET VALID_UNTIL = '28-FEB-2019' where ID = 'DD6C90BFFC394F518B5A5D6BE1C394C2'";
		setLogs(sqlSta);
		supporterDBConnection.setData(sqlSta);
	}

	public String getTheAccountIdFromSupporterNumber(String supporterNumber, String columnLable) {
		String accountId = supporterDBConnection.getData("select * from " + EnvironmentConfiguration.getText("account") + " where supporter_number=\'" + supporterNumber + "\'", columnLable);
		return accountId;
	}

	public String sqlToGetPersonalDetailsFromAccountDBForCPCResolicitAccount() {
		return  "select AC.SOURCE_SYSTEM, AC.ACCOUNT_EMAIL, AC.SUPPORTER_NUMBER, AC.REGISTRY_EXPORT_TIMESTAMP,AC.STATE"
				+ " FROM ACCOUNTAPI_OWNER.ACCOUNT AC"
				+ " where AC.ACCOUNT_EMAIL = ";
	}

	public String sqlCheckStagingTableInCrmDBByEmailAddress(String email) {
		return String.format("select * from xx.xx_cpc_supporter_stage"
				+ " where EMAIL_ADDRESS = '%s'"
				//				+ " And rownum <2"
				+ " order by TRX_LOG_ID desc", email);
	}

}
