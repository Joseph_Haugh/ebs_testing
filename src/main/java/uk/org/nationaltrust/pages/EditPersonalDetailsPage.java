package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * @author Pramod.Reguri
 */
public class EditPersonalDetailsPage extends PageBase {

	public static final By PERSONAL_DETAILS_HEADER_LABEL_TEXT = By.cssSelector(".columns>h1");

	private static final By REQUIRED_FIELDS_NOTE = By.cssSelector("#tab-your-details > form > p");

	public By TITLE_FIELD = By.cssSelector("#title");

	public By FIRST_NAME_FIELD = By.cssSelector("#firstName");

	public By LAST_NAME_FIELD = By.cssSelector("#lastName");

	public By ADDRESS_LINE1_FIELD = By.cssSelector("input[id*= addressLine1]");

	public By ADDRESS_LINE2_FIELD = By.cssSelector("input[id*= addressLine2]");

	public By ADDRESS_LINE3_FIELD = By.cssSelector("input[id*= addressLine3]");

	public By ADDRESS_LINE4_FIELD = By.cssSelector("input[id*= addressLine4]");

	public By ADDRESS_CITY_FIELD = By.cssSelector("input[id*= city]");

	public By ADDRESS_COUNTY_FIELD = By.cssSelector("input[id*= county]");

	public By ADDRESS_POSTCODE_FIELD = By.cssSelector("input[id*= postcode]");

	public By PRIMARY_PHONE_NUMBER_FIELD = By.cssSelector("input[id*= preferredTelephoneNumber]");

	public By CONTACT_EMAIL_FIELD = By.cssSelector("#preferredEmailAddress");

	public By UPDATE_DETAILS_BUTTON = By.cssSelector("#MyNTUpdateDetailsSubmit");

	private By CURRENT_EMAIL_ADDRESS = By.cssSelector("#preferredEmailAddress");

	public By SEND_ME_INCORRECT_NUMBER_TEXT = By
			.cssSelector("#tab-your-details > form > fieldset:nth-child(6) > div:nth-child(2) > div > div:nth-child(2) > ul.errors-list > li > span");

	public By SEND_ME_DUPLICATE_NUMBER_TEXT = By
			.cssSelector("#tab-your-details > form > fieldset:nth-child(6) > div:nth-child(3) > div > div:nth-child(2) > ul.errors-list > li > span");


	public By ALTERNATIVE_PHONE_NUMBER_FIELD = By.cssSelector ("input[id*='otherTelephoneNumber");

	public By CANCEL_AND_GO_BACK_LINK = By.cssSelector("#mainContent > section > div > main > div > form > div > div:nth-child(2) > a");

	public By SEND_ME_UPDATE_PERSONAL_DETAILS_CONFIRM_TEXT = By.cssSelector ("#tab-your-details > div > div");

	protected WebDriver driver;

	public EditPersonalDetailsPage(WebDriver dr) {
		this.driver = dr;
	}

	public void clickEditPersonalDetailsButton() {
		driver.findElement(userPage().EDIT_PERSONAL_DETAILS_BUTTON).click();
	}

	public void clickRequestATab() {
		driver.findElement(userPage().REQUEST_A_TAB).click();
	}

	public void setEmail(String email) {
		driver.findElement(CONTACT_EMAIL_FIELD).sendKeys(email);
	}

	public void setTitle(String title) {
		driver.findElement(TITLE_FIELD).sendKeys(title);
	}

	public void setFirstName(String firstName) {
		driver.findElement(FIRST_NAME_FIELD).sendKeys(firstName);
	}

	public void setLastName(String lastName) {
		driver.findElement(LAST_NAME_FIELD).sendKeys(lastName);
	}

	public void setAddressLine1(String addressLine1) {
		driver.findElement(ADDRESS_LINE1_FIELD).sendKeys(addressLine1);
	}

	public void setAddressLine2(String addressLine2) {
		driver.findElement(ADDRESS_LINE2_FIELD).sendKeys(addressLine2);
	}

	public void setAddressLine3(String addressLine3) {
		driver.findElement(ADDRESS_LINE3_FIELD).sendKeys(addressLine3);
	}

	public void setAddressLine4(String addressLine4) {
		driver.findElement(ADDRESS_LINE4_FIELD).sendKeys(addressLine4);
	}

	public void setAddressCity(String addressCity) {
		driver.findElement(ADDRESS_CITY_FIELD).sendKeys(addressCity);
	}

	public void setAddressCounty(String addressCounty) {
		driver.findElement(ADDRESS_COUNTY_FIELD).sendKeys(addressCounty);
	}

	public void setAddressPostcode(String addressPostcode) {
		driver.findElement(ADDRESS_POSTCODE_FIELD).sendKeys(addressPostcode);
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		driver.findElement(PRIMARY_PHONE_NUMBER_FIELD).sendKeys(primaryPhoneNumber);
	}
        public void setAlternativeNumber (String alternativeNumber){
        driver.findElement (ALTERNATIVE_PHONE_NUMBER_FIELD).sendKeys (alternativeNumber);
    }

	public String getPersonalDetailsHeaderLabelText() {
		setLogs("Waiting for personal details header label to be displayed");
		Helpers.waitForElementToAppear(driver, PERSONAL_DETAILS_HEADER_LABEL_TEXT, 10, "personal details header not displayed after 10 seconds");
		setLogs("Getting the personal details header label text");
		return Helpers.getText(driver, PERSONAL_DETAILS_HEADER_LABEL_TEXT);
	}

	public String getRequiredFieldsText() {
		setLogs("Waiting for required fields note header label to be displayed");
		Helpers.waitForElementToAppear(driver, REQUIRED_FIELDS_NOTE, 10, "required fields note header not displayed after 10 seconds");
		setLogs("Getting the personal details header label text");
		return Helpers.getText(driver, REQUIRED_FIELDS_NOTE);

	}

	public void setSupporterAddress(String addressLine1, String addressLine2, String addressLine3, String addressLine4, String addressCity, String addressCounty, String addressPostcode) {
//		setLogs("Navigate to PersonalDetail screen ");
//		driver.findElement(userPage().EDIT_PERSONAL_DETAILS_BUTTON).click();
		setLogs("Edit users Address");
		Helpers.clearAndSetText(driver, ADDRESS_LINE1_FIELD, addressLine1);
		Helpers.clearAndSetText(driver, ADDRESS_LINE2_FIELD, addressLine2);
		Helpers.clearAndSetText(driver, ADDRESS_LINE3_FIELD, addressLine3);
		Helpers.clearAndSetText(driver, ADDRESS_LINE4_FIELD, addressLine4);
		Helpers.clearAndSetText(driver, ADDRESS_CITY_FIELD, addressCity);
		Helpers.clearAndSetText(driver, ADDRESS_COUNTY_FIELD, addressCounty);
		Helpers.clearAndSetText(driver, ADDRESS_POSTCODE_FIELD, addressPostcode);
		driver.findElement(editPersonalDetailsPage().UPDATE_DETAILS_BUTTON).click();
	}

	public String getCurrentEMailAddress() {
		Helpers.waitForElementToAppear(driver, CURRENT_EMAIL_ADDRESS, 10, "Email address is not displayed");
		String memberEmailAddress = driver.findElement(CONTACT_EMAIL_FIELD).getText();
		return memberEmailAddress;
	}

	public void changeEmailInPersonalDetailsScreen(String email) {
		setLogs("Navigate to PersonalDetail screen ");
//		driver.findElement(userPage().EDIT_PERSONAL_DETAILS_BUTTON).click();
		Helpers.clearAndSetText(driver, CONTACT_EMAIL_FIELD, email);
		setLogs("Edit users email address");
		driver.findElement(editPersonalDetailsPage().UPDATE_DETAILS_BUTTON).click();
	}

	public void changePrimaryPhoneInPersonalDetailsScreen(String primaryPhone) {
		setLogs("Navigate to PersonalDetail screen ");
//		driver.findElement(userPage().EDIT_PERSONAL_DETAILS_BUTTON).click();
		Helpers.clearAndSetText(driver, PRIMARY_PHONE_NUMBER_FIELD, primaryPhone);
		setLogs("Edit users Primary Phone Number");
		driver.findElement(editPersonalDetailsPage().UPDATE_DETAILS_BUTTON).click();
	}

	public void changeAlternativePhoneInPersonalDetailsScreen(String alternativePhone) {
		setLogs("Navigate to PersonalDetails Screen");
		driver.findElement(userPage().EDIT_PERSONAL_DETAILS_BUTTON).click();
		Helpers.clearAndSetText(driver, ALTERNATIVE_PHONE_NUMBER_FIELD, alternativePhone);
		setLogs("Edit users Alternative Phone Number");
		driver.findElement(editPersonalDetailsPage().UPDATE_DETAILS_BUTTON).click();
	}

	public String getInvalidPhoneNumberText() {
		Helpers.waitForElementToAppear(driver, SEND_ME_INCORRECT_NUMBER_TEXT, 10, "Incorrect number error message not displayed");
		String incorrectNumberText = driver.findElement(SEND_ME_INCORRECT_NUMBER_TEXT).getText();
		return incorrectNumberText;
	}

	public String getDuplicatePhoneNumberText() {
		Helpers.waitForElementToAppear(driver, SEND_ME_DUPLICATE_NUMBER_TEXT, 10, "Duplicate number error message not displayed");
		String duplicateNumberText = driver.findElement(SEND_ME_DUPLICATE_NUMBER_TEXT).getText();
		return duplicateNumberText;
	}

	public String getCurrentUserEmailIdFromPersonalDetailPage() {

		Helpers.waitForIsDisplayed(driver, CURRENT_EMAIL_ADDRESS, 10);
		List<WebElement> currentEmail = driver.findElements(CURRENT_EMAIL_ADDRESS);
		if (currentEmail.size() != 0) {
			setLogs("Getting the Existing email address displayed");
			return driver.findElement(CURRENT_EMAIL_ADDRESS).getAttribute("value");
		}
		return null;
	}

	public String getFirstNameTextDisplayed() {
		Helpers.waitForElementToAppear(driver, FIRST_NAME_FIELD, 10, "first name text not displayed");
		List<WebElement> firstNameText = driver.findElements(FIRST_NAME_FIELD);
		if (firstNameText.size() != 0) {
			String firstNameTextDisplayed = driver.findElement(FIRST_NAME_FIELD).getText();

			return firstNameTextDisplayed;
		}
		return null;

	}

	public String getLastNameTextDisplayed() {
		Helpers.waitForElementToAppear(driver, LAST_NAME_FIELD, 10, "last name text not displayed");
		List<WebElement> lastNameText = driver.findElements(LAST_NAME_FIELD);
		if (lastNameText.size() != 0) {
			String lastNameTextDisplayed = driver.findElement(LAST_NAME_FIELD).getText();

			return lastNameTextDisplayed;
		}
		return null;

	}
//	public String getAddressLine1TextDisplayed() {
	////		Helpers.waitForElementToAppear(driver, ADDRESS_LINE1_FIELD, 10, "Address Line 1 text not displayed");
	////		List<WebElement> address1Text = driver.findElements(ADDRESS_LINE1_FIELD);
	////		if (address1Text.size() != 0) {
	////			String address1TextDisplayed = driver.findElement(ADDRESS_LINE1_FIELD).getText();
	////
	////			return address1TextDisplayed;
	////		}
	////		return null;
	////
	////	}

	public String getAddressLine1TextDisplayed(){
		return driver.findElement (ADDRESS_LINE1_FIELD).getText ();
	}
	public String getAddressLine2TextDisplayed() {
		Helpers.waitForElementToAppear(driver, ADDRESS_LINE2_FIELD, 10, "Address Line 2 text not displayed");
		List<WebElement> address2Text = driver.findElements(ADDRESS_LINE2_FIELD);
		if (address2Text.size() != 0) {
			String address2TextDisplayed = driver.findElement(ADDRESS_LINE2_FIELD).getText();

			return address2TextDisplayed;
		}
		return null;

	}

	public String getPostcodeTextDisplayed() {
		Helpers.waitForElementToAppear(driver, ADDRESS_POSTCODE_FIELD, 10, "Postcode text not displayed");
		List<WebElement> postcodeText = driver.findElements(ADDRESS_POSTCODE_FIELD);
		if (postcodeText.size() != 0) {
			String postcodeTextDisplayed = driver.findElement(ADDRESS_POSTCODE_FIELD).getText();

			return postcodeTextDisplayed;
		}
		return null;

	}

	public String getContactEmailTextDisplayed() {
		Helpers.waitForElementToAppear(driver, CURRENT_EMAIL_ADDRESS, 10, "Email text not displayed");
		List<WebElement> emailText = driver.findElements(CURRENT_EMAIL_ADDRESS);
		if (emailText.size() != 0) {
			String emailTextDisplayed = driver.findElement(CURRENT_EMAIL_ADDRESS).getText();

			return emailTextDisplayed;
		}
		return null;

	}
	public String getTelephoneTextDisplayed() {
		Helpers.waitForElementToAppear(driver, PRIMARY_PHONE_NUMBER_FIELD, 10, "Telephone text not displayed");
		List<WebElement> phoneText = driver.findElements(PRIMARY_PHONE_NUMBER_FIELD);
		if (phoneText.size() != 0) {
			String phoneTextDisplayed = driver.findElement(PRIMARY_PHONE_NUMBER_FIELD).getText();

			return phoneTextDisplayed;
		}
		return null;

	}

	public String getUpdatePersonalDetailsSuccessText (){
		return driver.findElement (SEND_ME_UPDATE_PERSONAL_DETAILS_CONFIRM_TEXT).getText ();
	}
}
