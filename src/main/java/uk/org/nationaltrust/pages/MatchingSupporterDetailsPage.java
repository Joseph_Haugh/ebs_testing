package uk.org.nationaltrust.pages;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import uk.org.nationaltrust.framework.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.lang.String.valueOf;
import static java.time.temporal.ChronoField.*;
import static uk.org.nationaltrust.framework.Helpers.clearAndSetText;

/**
 * Created by Pramod.Reguri on 24/02/2016.
 */
public class MatchingSupporterDetailsPage extends PageBase {

	protected WebDriver driver;

	CRMDBConnection CRMDBConnection = new CRMDBConnection();

	SupporterServiceDBConnection DBConnection = new SupporterServiceDBConnection();

	public By MEMBER_NUMBER_HEADER = By.cssSelector("#supporterNumberLabel");

	public By SUPPORTER_NUMBER_VALIDATION_MESSAGE = By.id("supporterNotFoundError");

	public By MATCH_POSTCODE_VALIDATION_MESSAGE = By.cssSelector("#postcode-error > li");

	public By SUPPORTER_NUMBER_LABLE = By.cssSelector("#supporterNumberLabel");

	public By YOUR_DETAILS_HEADER_LABLE = By.cssSelector(".small-12.columns>h1");

	public By GET_SUPPORTER_NUMBER_FIELD_FOR_MATCHING = By.id("supporterNumber");

	public By GET_PHONE_FIELD_FOR_MATCHING = By.id("telephoneNumber.value");

	public By GET_POSTCODE_FIELD_FOR_MATCHING = By.id("postcode.value");

	public By GET_RENEW_POSTCODE_FIELD_FOR_MATCHING = By.id("postcode");

	public By GET_EMAIL_FIELD_FOR_MATCHING = By.id("email.value");

	public By MATCHING_CONTINUE_BUTTON = By.id("matchSupporterContinue");

	public By RENEW_CONTINUE_BUTTON = By.id("verifySubmit");

	public By MATCHING_FIND_DETAILS_BUTTON = By.id("matchSupporterSubmit");

	public By HELP_US_FIND_YOUR_DETAILS_LABLE = By.cssSelector("#content > section > div > main > div > form > fieldset > legend > h1");

	public By POST_CODE_LABLE = By.id("postcodeLabel");

	public By DATE_OF_BIRTH_LABLE = By.cssSelector("#content > section > div > main > div > form > fieldset > div:nth-child(4) > div > span");

	public By MEMBER_POSTCODE_HEADER = By.cssSelector("#postcodeLabel");

	public By SUPPORTER_POST_CODE_FIELD = By.cssSelector("input#postcode");

	public By SUPPORTER_DOB_DATE_FIELD = By.id("dateOfBirth.value.day");

	public By SUPPORTER_DOB_MONTH_FIELD = By.id("dateOfBirth.value.month");

	public By SUPPORTER_DOB_YEAR_FIELD = By.id("dateOfBirth.value.year");

	public By MATCHING_EMAIL_TEXT = By.id("emailMatchesLoggedInUserMessage");

	public By CREATE_ACCOUNT_BUTTON = By.id("matchSupporterSubmit");

	public By FULL_ACCOUNT_BUTTON = By.cssSelector("#matchSupporterLink");

	public static By LITE_ACCOUNT_LINK_ON_MATCH_SCREEN = By.id("matchSupporterSkip");

	public static By IM_NOT_A_MEMBER_BUTTON_ON_MATCH_SCREEN = By.id("personalDataLink");

	public By LITE_ACCOUNT_PREFS_BUTTON = By.cssSelector("#MyNTPersonalDataAgreement");

	public By FAILED_MATCH_ATTEMPTS_ERROR_TEXT = By.id("max-attempts-message");

	public By RENEW_YOUR_MEMBERSHIP_HEADER = By.cssSelector("body > main > section > div.row > div:nth-child(1) > div > div > header > h1");

	public String getRenewMatchPageHeaderText() {
		return driver.findElement(RENEW_YOUR_MEMBERSHIP_HEADER).getText();
	}

	public MatchingSupporterDetailsPage(WebDriver dr) {
		this.driver = dr;
	}

	public String getFailedMatchErrorDisplayed() {
		List<WebElement> failedMatchText = driver.findElements(FAILED_MATCH_ATTEMPTS_ERROR_TEXT);
		if (failedMatchText.size() != 0) {
			String failedMatchTextDisplayed = driver.findElement(FAILED_MATCH_ATTEMPTS_ERROR_TEXT).getText();

			return failedMatchTextDisplayed;
		}
		return null;

	}

	public void setSupporterDetails(String supporterNumber, String supporterPostCode) {

		setLogs("Entering the supporter number in the field");
		clearAndSetText(driver, GET_SUPPORTER_NUMBER_FIELD_FOR_MATCHING, supporterNumber);
		setLogs("Entering the supporter postcode in the field");
		clearAndSetText(driver, SUPPORTER_POST_CODE_FIELD, supporterPostCode);

	}

	public void setSupporterNumber(String supporterNumber) {
		clearAndSetText(driver, GET_SUPPORTER_NUMBER_FIELD_FOR_MATCHING, supporterNumber);

	}

	public void setSupporterNumberMultiple(String supporterNumber) {
		clearAndSetText(driver, GET_SUPPORTER_NUMBER_FIELD_FOR_MATCHING, supporterNumber);
		driver.findElement(matchingPage().MATCHING_CONTINUE_BUTTON).click();

	}

	public void setSupporterDateOfBirth(LocalDate dateOfBirth) {
		setLogs("Enter the supporter date of birth");
		clearAndSetText(driver, SUPPORTER_DOB_DATE_FIELD, valueOf(dateOfBirth.get(DAY_OF_MONTH)));
		clearAndSetText(driver, SUPPORTER_DOB_MONTH_FIELD, valueOf(dateOfBirth.get(MONTH_OF_YEAR)));
		clearAndSetText(driver, SUPPORTER_DOB_YEAR_FIELD, valueOf(dateOfBirth.get(YEAR)));

	}

	public String getPostcodeFieldLabel() {
		return driver.findElement(MEMBER_POSTCODE_HEADER).getText();
	}

	public void setEmail(String email) {
		clearAndSetText(driver, GET_EMAIL_FIELD_FOR_MATCHING, email);
	}

	public void setPhone(String phone) {
		clearAndSetText(driver, GET_PHONE_FIELD_FOR_MATCHING, phone);
	}

	public void setPostcode(String postcode) {
		clearAndSetText(driver, GET_POSTCODE_FIELD_FOR_MATCHING, postcode);
	}

	public void setRenewPostcode(String postcode) {
		clearAndSetText(driver, GET_RENEW_POSTCODE_FIELD_FOR_MATCHING, postcode);
	}

	public void setSupporterDateOfBirth(String supportDOBdate, String supportDOBmonth, String supportDOByear) {
		setLogs("Enter the supporter date of birth");
		clearAndSetText(driver, SUPPORTER_DOB_DATE_FIELD, supportDOBdate);
		clearAndSetText(driver, SUPPORTER_DOB_MONTH_FIELD, supportDOBmonth);
		clearAndSetText(driver, SUPPORTER_DOB_YEAR_FIELD, supportDOByear);
	}

	public void clickCreateAccountButton() {
		setLogs("Clicking create my account button");
		driver.findElement(supporterDetailsPage().MATCHING_FIND_DETAILS_BUTTON).click();
	}

	public void clickCreateFullAccountButton() {
		setLogs("Clicking create my account button");
		driver.findElement(supporterDetailsPage().FULL_ACCOUNT_BUTTON).click();
	}

	public void clickCreateLiteAccountLink() {
		setLogs("Clicking create my lite account button");
		driver.findElement(supporterDetailsPage().LITE_ACCOUNT_LINK_ON_MATCH_SCREEN).click();
	}

	public void clickCreateLiteAccountFromMatchScreenButton() {
		setLogs("Clicking create my lite account button");
		driver.findElement(supporterDetailsPage().IM_NOT_A_MEMBER_BUTTON_ON_MATCH_SCREEN).click();
	}

	public void clickCreateLiteAccountPrefsButton() {
		setLogs("Clicking create my lite account button");
		driver.findElement(supporterDetailsPage().LITE_ACCOUNT_PREFS_BUTTON).click();
	}

	public boolean checkIfCreateLiteAccountButtonIsDisplayed() {
		setLogs("check if create lite option is displayed.......");
		return Helpers.waitForIsDisplayed(driver, LITE_ACCOUNT_PREFS_BUTTON, 10);
	}

	public boolean checkIfCreateLiteAccountLinkIsDisplayedOnMatchScreen() {
		setLogs("check if create lite option is displayed.......");
		return Helpers.waitForIsDisplayed(driver, LITE_ACCOUNT_LINK_ON_MATCH_SCREEN, 10);
	}

	public Boolean checkIfMYNTMatchPostcodeFieldAppears() {
		setLogs("check if Postcode Match field is displayed.......");
		return Helpers.waitForIsDisplayed(driver, GET_POSTCODE_FIELD_FOR_MATCHING, 10);
	}

	public Boolean checkIfMYNTMatchEmailFieldAppears() {
		setLogs("check if Email match field is displayed");
		return Helpers.waitForIsDisplayed(driver, GET_EMAIL_FIELD_FOR_MATCHING, 10);
	}

	public Boolean checkifMYNTMatchPhoneFieldAppears() {
		setLogs("check if Phone match field is displayed");
		return Helpers.waitForIsDisplayed(driver, GET_PHONE_FIELD_FOR_MATCHING, 10);
	}

	public Boolean checkifMYNTDOBDayFieldAppears() {
		setLogs("check if DOB Day field is displayed");
		return Helpers.waitForIsDisplayed(driver, SUPPORTER_DOB_DATE_FIELD, 10);
	}

	public Boolean checkIfMYNTMatchEmailTextAppears() {
		setLogs("check if Email info text appears");
		return Helpers.waitForIsDisplayed(driver, MATCHING_EMAIL_TEXT, 10);

	}

	public String getHelpUsFindYourDetailsLable() {
		setLogs("Getting the matching page header");
		String matchingPageHeader = driver.findElement(YOUR_DETAILS_HEADER_LABLE).getText();
		return matchingPageHeader;
	}

	public Map<String, String> getTheSupporterDetailsForSingleIndividualMemb(Set<String> labelNames) {
		String sqlSta = singleMembershipSql();
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleMembershipSql() {
		return "select distinct hp.party_number, hpp.date_of_birth, hp.postal_code FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'IND'"
				//				+ " And xm.mem_payment_method ='CREDIT_CARD'"
				+ " And hp.email_address is Not Null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				+ "And mem_start_date between sysdate-190 AND sysdate-160"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and rownum < 5";
	}

	public Map<String, String> getTheSupporterDetailsForJuniorMemb(Set<String> labelNames) {
		String sqlSta = singleJuniorMembershipSql();
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleJuniorMembershipSql() {
		return "select distinct hp.party_number, hpp.date_of_birth, hp.postal_code FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'JUN'"
				+ " And xm.mem_payment_method != 'DIRECT_DEBIT'"
				//				+ " And xm.mem_payment_method ='CREDIT_CARD'"
				+ " And hp.email_address is Not Null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				+ "And mem_start_date >= sysdate-190"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and rownum < 5";
	}

	public Map<String, String> getTheSupporterDetailsForJuniorMembStartingPreConcessionChange(Set<String> labelNames) {
		String sqlSta = singleJuniorMembershipPreConcessionSql();
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleJuniorMembershipPreConcessionSql() {
		return "select distinct hp.party_number, hpp.date_of_birth, hp.postal_code FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'JUN'"
				+ " And xm.mem_payment_method != 'DIRECT_DEBIT'"
				//				+ " And xm.mem_payment_method ='CREDIT_CARD'"
				+ " And hp.email_address is Not Null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				+ "And mem_start_date < to_date('01-MAR-2018','dd-Mon-yyyy')"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and rownum < 5";
	}

	public Map<String, String> getTheSupporterDetailsForJuniorMembStartingPostConcessionChange(Set<String> labelNames) {
		String sqlSta = singleJuniorMembershipPostConcessionSql();
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleJuniorMembershipPostConcessionSql() {
		return "select distinct hp.party_number, hpp.date_of_birth, hp.postal_code FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And hpp.date_of_birth is not null"
				+ " AND hpp.date_of_birth <= to_date('01-MAR-2004','dd-Mon-yyyy')"
				+ " And xm.mem_status = 'Active'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'CHD'"
				//				+ " And xm.mem_payment_method != 'DIRECT_DEBIT'"
				+ " And hp.email_address is Not Null"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				+ "And mem_start_date >= to_date('01-MAR-2018','dd-Mon-yyyy')"
				+ " and rownum < 5";
	}

	public Map<String, String> getTheSupporterDetailsForSingleIndividualMembCCPayer(Set<String> labelNames) {
		String sqlSta = singleMembershipCCSql();
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleMembershipCCSql() {
		return "select distinct hp.party_number, hpp.date_of_birth, hp.postal_code FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'IND'"
				+ " And xm.mem_payment_method ='CREDIT_CARD'"
				+ " And hp.email_address is Not Null"
				+ " and xs.supporter_dob is not null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				+ "And mem_start_date >= sysdate-210"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and rownum < 5";
	}

	public Map<String, String> getTheSupporterDetailsForSingleIndividualMembDD(Set<String> labelNames) {
		String sqlSta = singleMembershipDDSql();
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleMembershipDDSql() {
		return "select distinct hp.party_number, hpp.date_of_birth, hp.postal_code FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And hpp.date_of_birth is not null"
				//				+ " And hp.postal_code is not null"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'IND'"
				+ " And xm.mem_payment_method ='DIRECT_DEBIT'"
				+ " And hp.email_address is Not Null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				+ "And mem_start_date between sysdate-170 AND sysdate-140"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and rownum < 12"
				+ " Order by DBMS_RANDOM.RANDOM";
	}

	public Map<String, String> getCustomisableSupporterDetailsWithNoMembership(Set<String> labelNames, String dob, String postcode, String phone, String email) {
		String sqlSta = singleSupporterSQL(dob, postcode, phone, email);
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleSupporterSQL(String dobStatus, String postcodeStatus, String phoneStatus, String emailStatus) {
		return "select hp.party_number, hp.EMAIL_ADDRESS, hpp.date_of_birth, hp.postal_code, hp.primary_phone_number FROM"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE hp.status = 'A'"
				+ " and hpp.date_of_birth " + dobStatus + ""
				+ " and hp.email_address " + emailStatus + ""
				+ " and hp.primary_phone_number " + phoneStatus + ""
				+ " And hp.postal_code " + postcodeStatus + ""
				+ " And hpp.party_id = hp.party_id"
				+ " and rownum < 12"
				+ " Order by DBMS_RANDOM.RANDOM";
	}

	public Map<String, String> getTheSupporterDetailsForSingleJuniorMembDD(Set<String> labelNames) {
		String sqlSta = singleJuniorMembershipDDSql();
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleJuniorMembershipDDSql() {
		return "select distinct hp.party_number, hpp.date_of_birth, hp.postal_code FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'JUN'"
				+ " And xm.mem_payment_method ='DIRECT_DEBIT'"
				+ " And hp.email_address is Not Null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				+ "And mem_start_date between sysdate-170 AND sysdate-140"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and rownum < 12"
				+ " Order by DBMS_RANDOM.RANDOM";
	}

	public Map<String, String> getTheSupporterDetailsForSingleIndividualMembDDGift(Set<String> labelNames) {
		String sqlSta = singleMembershipDDGiftSql();
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleMembershipDDGiftSql() {
		return "select distinct party_number, hpp.date_of_birth, hp.postal_code FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " AND hp.party_id=xm.owner_party_id"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				//				+ " And xr.relationship_type_code = 'OWNER'"
				+ " And xr.object_id = xm.support_id"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is not null"
				+ "	And xm.mem_type = 'IND'"
				+ " And xm.mem_payment_method ='DIRECT_DEBIT'"
				+ " And hp.email_address is Not Null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				+ "And mem_start_date between sysdate-230 AND sysdate-170"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and rownum < 12"
				+ " Order by DBMS_RANDOM.RANDOM";
	}

	public Map<String, String> getTheSupporterDetailsForPaymentPendingMembership(Set<String> labelNames) {
		String sqlSta = singleMembershipPayPendingSql();
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleMembershipPayPendingSql() {
		return "select distinct hp.party_number, hpp.date_of_birth, hp.postal_code FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_status like 'Payment Pending'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'IND'"
				+ " And xm.mem_payment_method ='DIRECT_DEBIT'"
				+ " And hp.email_address is Not Null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				+ "And mem_start_date >= sysdate-200"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and rownum < 5";
	}

	public LocalDate convertDateFormat(String dates) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss.S");
		return LocalDate.parse(dates, formatter);
		//		String sourceDateTime = (dates);
		//		//			String sourceDateTime = "2016-12-31 00:00:00";
		//		DateTimeFormatter parser = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss.S");
		//
		//		TemporalAccessor ta = parser.parse(dates);
		//
		//		int month = ta.get(ChronoField.MONTH_OF_YEAR);
		//		System.out.println("x.get(Month) = " + month);
		//		int day = ta.get(ChronoField.DAY_OF_MONTH);
		//		System.out.println("x.get(Day) = " + day);
		//		int year = ta.get(ChronoField.YEAR);
		//		System.out.println("x.get(YEAR) = " + year);
		//		return day + "-" + month +month "-" + year;

		//		LocalDate date = parser.parse(sourceDateTime, LocalDate::from);
		//		System.out.println("Got it: " + date);
		//		DateTimeFormatter renderer = DateTimeFormatter.ofPattern("DD");
		//		String formattedDate = renderer.format(dates);
		//		return formattedDate;
		//				System.out.println(formattedDate);

	}

	public Map<String, String> getTheSupporterDetailsForSingleIndividualLifeMemb(Set<String> labelNames) {
		String sqlSta = singleLifeMembershipSql();
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleLifeMembershipSql() {
		return "select distinct hp.party_number, hpp.date_of_birth, hp.postal_code FROM"
				+ " xx_csi_relationship xr,"
				+ " xx_supporter xs,"
				+ " xx_membership xm,"
				+ " hz_parties hp,"
				+ " hz_person_profiles hpp"
				+ " WHERE xr.subject_txn = xs.relationship_txn"
				+ " And xr.subject_id = xs.support_id"
				+ " And xs.supporter_party_id = hp.party_id"
				+ " And xr.relationship_type_code = 'LEAD'"
				+ " And xr.object_id = xm.support_id"
				+ " And hpp.date_of_birth is not null"
				+ " And xm.mem_status = 'Active'"
				//				+ " And xm.MEM_LAST_PAY_STATUS_DETAIL.last_pay_status = 'Successful'"
				+ " And hpp.party_id = hp.party_id"
				+ " And xm.mem_gift_flag is null"
				+ "	And xm.mem_type = 'LIF'"
				//				+ " And xm.mem_payment_method ='CREDIT_CARD'"
				+ " And hp.email_address is Not Null"
				//				+ " And xm.mem_gift_flag is Null"
				//				+ "	And xm.mem_type_desc = 'IND'"
				+ " And sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				//				+ " And mem_start_date >= to_date('" + Helpers.getFutureDateTime(-6) + "','dd-Mon-yyyy')"
				+ "And mem_start_date >= sysdate-200"
				//				+ " And mem_start_date = to_date('02-NOV-2015','dd-Mon-yyyy')"
				+ " and rownum < 5";
	}

	public Map<String, String> getTheSupporterDetailsForSingleIndividualAnnualDDMemb(String endDate1, String endDate2, Set<String> labelNames) {
		String sqlSta = singleMembershipPaidBYAnnualDDEndDateVariableSql(endDate1, endDate2);
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleMembershipPaidBYAnnualDDEndDateVariableSql(String endDate1, String endDate2) {
		return "SELECT xm.mem_payment_method, hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_type, xm.mem_type_desc, xm.mem_payment_method, xm.mem_status, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n"
				+ "FROM xx_csi_relationship xr\n"
				+ ",    xx_supporter xs\n"
				+ ",    xx_membership xm\n"
				+ ",    hz_parties hp\n"
				+ ",    hz_person_profiles hpp\n"
				+ ",    oe_order_lines_all oola\n"
				+ "WHERE xr.subject_txn = xs.relationship_txn\n"
				+ "AND xr.subject_id = xs.support_id\n"
				+ "AND xs.supporter_party_id =  hp.party_id\n"
				+ "AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n"
				+ "AND xr.object_id = xm.support_id\n"
				+ "and hpp.date_of_birth is not null\n"
				+ "and xm.mem_status = 'Active'\n"
				+ "and hp.email_address is not Null\n"
				+ "and xm.mem_type = 'IND'\n"
				+ "and xm.mem_gift_flag is null\n"
				+ "and hpp.party_id = hp.party_id\n"
				+ "and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n"
				+ "and mem_end_date between " + endDate1 + " and " + endDate2 + "\n"
				+ "and oola.sold_to_org_id = xm.owner_party_account_id\n"
				+ "and NVL(oola.booked_flag, 'N') = 'N'\n"
				+ "and oola.line_type_id = 1047\n"
				+ "and oola.attribute1 = xm.support_id\n"
				+ "and rownum < 20\n";
	}

	public Map<String, String> getTheSupporterDetailsForSingleIndividualCashMemb(String endDate1, String endDate2, Set<String> labelNames) {
		String sqlSta = singleMembershipPaidBYCashEndDateVariableSql(endDate1, endDate2);
		setLogs(sqlSta);
		return CRMDBConnection.getData(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String singleMembershipPaidBYCashEndDateVariableSql(String endDate1, String endDate2) {
		return "SELECT xm.mem_payment_method, hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_type, xm.mem_type_desc, xm.mem_payment_method, xm.mem_status, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n"
				+ "FROM xx_csi_relationship xr\n"
				+ ",    xx_supporter xs\n"
				+ ",    xx_membership xm\n"
				+ ",    hz_parties hp\n"
				+ ",    hz_person_profiles hpp\n"
				+ ",    oe_order_lines_all oola\n"
				+ "WHERE xr.subject_txn = xs.relationship_txn\n"
				+ "AND xr.subject_id = xs.support_id\n"
				+ "AND xs.supporter_party_id =  hp.party_id\n"
				+ "AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n"
				+ "AND xr.object_id = xm.support_id\n"
				+ "and hpp.date_of_birth is not null\n"
				+ "and xm.mem_status = 'Active'\n"
				+ "and hp.email_address is not Null\n"
				+ "and xm.mem_type = 'IND'\n"
				+ "and xm.mem_payment_method != 'DIRECT_DEBIT'\n"
				+ "and xm.mem_gift_flag is null\n"
				+ "and hpp.party_id = hp.party_id\n"
				+ "and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n"
				+ "and mem_end_date between " + endDate1 + " and " + endDate2 + "\n"
				+ "and oola.sold_to_org_id = xm.owner_party_account_id\n"
				+ "and NVL(oola.booked_flag, 'N') = 'N'\n"
				+ "and oola.line_type_id = 1047\n"
				+ "and oola.attribute1 = xm.support_id\n"
				+ "and rownum < 20\n";
	}

	public String getSupporterNumberFieldLabelText() {
		List<WebElement> supporterNumberLabel = driver.findElements(SUPPORTER_NUMBER_LABLE);
		if (supporterNumberLabel.size() != 0) {
			String supporterNumberLabelText = driver.findElement(SUPPORTER_NUMBER_LABLE).getText();
			return supporterNumberLabelText;

		}
		return null;
	}

	public String getFailedToMatch3TimesMessage() {
		Helpers.waitForIsDisplayed(driver, FAILED_MATCH_ATTEMPTS_ERROR_TEXT, 10);
		List<String> message = new ArrayList<String>();
		List<WebElement> failedToMatch3TimesMessage = driver.findElements(FAILED_MATCH_ATTEMPTS_ERROR_TEXT);
		for (int i = 0; i < failedToMatch3TimesMessage.size(); i++) {
			message.add(failedToMatch3TimesMessage.get(i).getText());

		}
		String allMessage = String.join(", ", message);
		return allMessage;
	}

	public String getMatchfailedMessage() {
		String expectedString = EnvironmentConfiguration.getMessageText("match-supporter.failed.three.times").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);

	}

	public List<SupporterRecord> getTheSupporterDetailsForSingleMembInRenewal(String memType) {
		String sqlSta = singleMembershipInRenewalSql(memType);
		setLogs(sqlSta);
		return CRMDBConnection.getSupporterData(sqlSta);
	}

	private String singleMembershipInRenewalSql(String memType) {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method != 'DIRECT_DEBIT'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = '" + memType + "' \n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date ('" + Helpers.getRenewFutureDateTime(-14) + "','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and xs.supporter_expiry_date > xm.mem_end_date\n" +
				"\n" +
				"and rownum < 20";
	}

	public void AddSupporterNumberAndPostcodeInMatchingPage(String supporterNumber, String postcode) {
		navigateToMatchingPage();
		Helpers.clearAndSetText(driver, GET_SUPPORTER_NUMBER_FIELD_FOR_MATCHING, supporterNumber);
		Helpers.clearAndSetText(driver, GET_RENEW_POSTCODE_FIELD_FOR_MATCHING, postcode);
		driver.findElement(matchingPage().RENEW_CONTINUE_BUTTON).click();
	}

	public void navigateToMatchingPage() {
		List<WebElement> supporterNumberField = driver.findElements(GET_SUPPORTER_NUMBER_FIELD_FOR_MATCHING);
		if (supporterNumberField.size() == 0) {
			driver.get(EnvironmentConfiguration.getRenewBaseURL());
		}
	}

	public List<SupporterRecord> getTheSupporterDetailsForSingleJuniorMembInRenewalPostMarch2018() {
		String sqlSta = singleJuniorMembershipInRenewalPostMarch2018Sql();
		setLogs(sqlSta);
		return CRMDBConnection.getSupporterData(sqlSta);
	}

	private String singleJuniorMembershipInRenewalPostMarch2018Sql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method = 'CASH'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = 'JUN'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				"and mem_start_date >= to_date('01-JAN-2017','dd-Mon-yyyy')\n" +
				"\n" +
				"And mem_end_date > to_date('01-Mar-2018','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}

	public List<SupporterRecord> getTheSupporterDetailsForSingleJuniorMembInRenewalPreMarch2018() {
		String sqlSta = singleJuniorMembershipInRenewalPreMarch2018Sql();
		setLogs(sqlSta);
		return CRMDBConnection.getSupporterData(sqlSta);
		//		setLogs(supporterDetails);
	}

	private String singleJuniorMembershipInRenewalPreMarch2018Sql() {
		return "SELECT  hp.party_number, hpp.date_of_birth, hp.postal_code, xm.mem_payment_method, xm.mem_status,xm.mem_start_date, xm.MEM_END_DATE, hp.EMAIL_ADDRESS, xm.mem_gift_flag\n" +
				"\n" +
				"FROM xx_csi_relationship xr\n" +
				"\n" +
				",    xx_supporter xs\n" +
				"\n" +
				",    xx_membership xm\n" +
				"\n" +
				",    hz_parties hp\n" +
				"\n" +
				",    hz_person_profiles hpp\n" +
				"\n" +
				",    oe_order_lines_all oola\n" +
				"\n" +
				"WHERE xr.subject_txn = xs.relationship_txn\n" +
				"\n" +
				"AND xr.subject_id = xs.support_id\n" +
				"\n" +
				"AND xs.supporter_party_id =  hp.party_id\n" +
				"\n" +
				"AND xr.relationship_type_code = 'LEAD'--IN ('LEAD', 'ADULT_ADD')\n" +
				"\n" +
				"AND xr.object_id = xm.support_id\n" +
				"\n" +
				"--AND xm.mem_payment_term   = 'Monthly Direct Debit'\n" +
				"\n" +
				"AND xm.mem_payment_method != 'DIRECT_DEBIT'\n" +
				"\n" +
				"AND nvl(upper(xm.mem_in_renewal), 'FALSE') = 'TRUE'\n" +
				"\n" +
				"and hpp.date_of_birth is not null\n" +
				"\n" +
				"and xm.mem_status = 'Active'\n" +
				"\n" +
				"and hp.email_address is not Null\n" +
				"\n" +
				"and xm.mem_type = 'JUN'\n" +
				"\n" +
				"and xm.mem_gift_flag is null\n" +
				"\n" +
				"and hpp.party_id = hp.party_id\n" +
				"\n" +
				"and sysdate between hpp.effective_start_date and nvl(hpp.effective_end_date, sysdate+5)\n" +
				"\n" +
				" And mem_start_date between to_date('" + Helpers.getFutureDateTime(-14) + "','dd-Mon-yyyy') and to_date('" + Helpers.getFutureDateTime(-12) + "','dd-Mon-yyyy')\n" +
				"\n" +
				"And mem_end_date < to_date('01-Mar-2018','dd-Mon-yyyy')\n" +
				"\n" +
				"and oola.sold_to_org_id = xm.owner_party_account_id\n" +
				"\n" +
				"and NVL(oola.booked_flag, 'N') = 'N'\n" +
				"\n" +
				"and oola.line_type_id = 1047\n" +
				"\n" +
				"and oola.attribute1 = xm.support_id\n" +
				"\n" +
				"and rownum < 20";
	}

	public void verifyMatchScreenQuestions(String matchQuestions) {
		switch (matchQuestions) {
			case "allOptions":
				Assert.assertTrue(checkifMYNTDOBDayFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertFalse(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailTextAppears(),"Expected not to find the email info text but it was displayed");
				break;

			case "DOBAndPostcode":
				Assert.assertTrue(checkifMYNTDOBDayFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertFalse(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailTextAppears(),"Expected not to find the email info text but it was displayed");

				break;

			case "DOBAndPhone":
				Assert.assertTrue(checkifMYNTDOBDayFieldAppears());
				Assert.assertTrue(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailTextAppears(),"Expected not to find the email info text but it was displayed");
				break;

			case "DOBAndEmail":
				Assert.assertTrue(checkifMYNTDOBDayFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertFalse(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchEmailFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailTextAppears(),"Expected not to find the email info text but it was displayed");
				break;
			case "postcodeAndPhone":
				Assert.assertFalse(checkifMYNTDOBDayFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertTrue(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailTextAppears(),"Expected not to find the email info text but it was displayed");
				break;
			case "postcodeAndEmail":
				Assert.assertFalse(checkifMYNTDOBDayFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertFalse(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchEmailFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailTextAppears(),"Expected not to find the email info text but it was displayed");
				break;

			case "emailAndPhone":
				Assert.assertFalse(checkifMYNTDOBDayFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertTrue(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchEmailFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailTextAppears(),"Expected not to find the email info text but it was displayed");
				break;

			case "postcode":
				setLogs("This is used in a scenario where the email the custmer registers with matches that in their CRM record. So only one extra Q is asked");
				Assert.assertFalse(checkifMYNTDOBDayFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertFalse(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchEmailTextAppears(),"Expected to find the email info text but it wasnt displayed");
				break;

		}

	}

	public void verifyMatchScreenQuestionsV2(String matchQuestions, String supporterNumber, LocalDate dateOfBirth, String supporterPostcode, String supporterEmail, String supporterPhone) {
		switch (matchQuestions) {
			case "allOptions":
				supporterDetailsPage().setSupporterNumber(supporterNumber);
				driver.findElement(matchingPage().MATCHING_CONTINUE_BUTTON).click();
				Assert.assertTrue(checkifMYNTDOBDayFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertFalse(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailFieldAppears());
				supporterDetailsPage().setSupporterDateOfBirth(dateOfBirth);
				supporterDetailsPage().setPostcode(supporterPostcode);
				break;

			case "DOBAndPostcode":
				supporterDetailsPage().setSupporterNumber(supporterNumber);
				driver.findElement(matchingPage().MATCHING_CONTINUE_BUTTON).click();
				Assert.assertTrue(checkifMYNTDOBDayFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertFalse(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailFieldAppears());
				supporterDetailsPage().setSupporterDateOfBirth(dateOfBirth);
				supporterDetailsPage().setPostcode(supporterPostcode);
				break;

			case "DOBAndPhone":
				supporterDetailsPage().setSupporterNumber(supporterNumber);
				driver.findElement(matchingPage().MATCHING_CONTINUE_BUTTON).click();
				Assert.assertTrue(checkifMYNTDOBDayFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertTrue(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailFieldAppears());
				supporterDetailsPage().setSupporterDateOfBirth(dateOfBirth);
				supporterDetailsPage().setPostcode(supporterPostcode);
				break;

			case "DOBAndEmail":
				supporterDetailsPage().setSupporterNumber(supporterNumber);
				driver.findElement(matchingPage().MATCHING_CONTINUE_BUTTON).click();
				Assert.assertTrue(checkifMYNTDOBDayFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertFalse(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchEmailFieldAppears());
				supporterDetailsPage().setSupporterDateOfBirth(dateOfBirth);
				supporterDetailsPage().setEmail(supporterEmail);
				break;
			case "postcodeAndPhone":
				supporterDetailsPage().setSupporterNumber(supporterNumber);
				driver.findElement(matchingPage().MATCHING_CONTINUE_BUTTON).click();
				Assert.assertFalse(checkifMYNTDOBDayFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertTrue(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchEmailFieldAppears());
				supporterDetailsPage().setPostcode(supporterPostcode);
				supporterDetailsPage().setPhone(supporterPhone);
				break;
			case "postcodeAndEmail":
				supporterDetailsPage().setSupporterNumber(supporterNumber);
				driver.findElement(matchingPage().MATCHING_CONTINUE_BUTTON).click();
				Assert.assertFalse(checkifMYNTDOBDayFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertFalse(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchEmailFieldAppears());
				supporterDetailsPage().setPostcode(supporterPostcode);
				supporterDetailsPage().setEmail(supporterEmail);
				break;

			case "emailAndPhone":
				supporterDetailsPage().setSupporterNumber(supporterNumber);
				driver.findElement(matchingPage().MATCHING_CONTINUE_BUTTON).click();
				Assert.assertFalse(checkifMYNTDOBDayFieldAppears());
				Assert.assertFalse(checkIfMYNTMatchPostcodeFieldAppears());
				Assert.assertTrue(checkifMYNTMatchPhoneFieldAppears());
				Assert.assertTrue(checkIfMYNTMatchEmailFieldAppears());
				supporterDetailsPage().setEmail(supporterEmail);
				supporterDetailsPage().setPhone(supporterPhone);
				break;

		}

	}
}