package uk.org.nationaltrust.pages;

import org.openqa.selenium.WebDriver;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by Sowjanya Annepu
 */
public class DonateMapPinAddImagePage extends CommemorativeMapPinDonateSelectors {


    WebDriver driver;

    DonateMapPinAddImagePage(WebDriver driver){
        this.driver= driver;

    }

    public DonateMapPinAddImagePage selectImage(){
      //  Helpers.waitForVisibility(STEP3_SUMMARY_BUTTON);
     //   STEP3_CAROUSAL_IMAGES_NEXT_ARROW.click();
        // TODO  select image - yet to be implemented.
        return this;
    }

    public DonateMapPinSummaryPage clickSummary(){
        Helpers.waitHandlingException(2000);
        STEP3_SUMMARY_BUTTON.click();
        Helpers.waitHandlingException(1000);
        return new DonateMapPinSummaryPage(driver);
    }

    public void imageNextStep(){

    }

    public void addImagePreviousStep(){

    }

    public void addImageCloseButton(){

    }

}