package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SideNavPage extends PageBase {

    public By profileUserName = By.id("profileUsernameNavBar");


    public By myDashBoard = By.id("dashboardDetails");

    public By signOutBar = By.id("MyNTSignOutNavBar");

    public By memberships = By.id("membershipDetails");

    public By myPlaces = By.id("placesIWantToVisit");

    public By accountDetails = By.id("editPersonalDetails");

    protected WebDriver driver;

    public SideNavPage(WebDriver dr) {
        this.driver = dr;
    }

    public WebElement getProfileUserName(){
        return driver.findElement(profileUserName);
    }

    public WebElement getMyDashBoard(){
        return driver.findElement(myDashBoard);
    }

    public WebElement getSignOutBar(){
        return driver.findElement(signOutBar);
    }

    public WebElement getMemberships(){
        return driver.findElement(memberships);
    }

    public WebElement getMyPlaces(){
        return driver.findElement(myPlaces);
    }

    public WebElement getAccountDetails(){
        return driver.findElement(accountDetails);
    }
}
