package uk.org.nationaltrust.pages;
import org.openqa.selenium.WebDriver;
import uk.org.nationaltrust.apis.CreateDonateFormRequest;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by Sowjanya Annepu
 */
public class DonateMapPinStep1Page extends CommemorativeMapPinDonateSelectors {

    protected WebDriver driver;

    private  String donation_tone=null;

    private  String relation=null;

    private  String occasion=null;

    public DonateMapPinStep1Page(WebDriver driver) {
        this.driver = driver;
    }

    public DonateMapPinStep1Page  navigateToStep1(String step1Url){
        driver.navigate().to(step1Url);
        return this;
    }

    public boolean subjectAndToneValidationErrorText(){
        /* DON-603 */
        STEP1_NEXT_BUTTON.click();
        if(STEP1_SUBJECT_VALIDATION_ERROR_TEXT.isDisplayed() && STEP1_TONE_VALIDATION_ERROR_TEXT.isDisplayed()) {
            return true;
        }
        else
            return false;
    }

    public boolean subjectMaxLengthalidationErrorText(String bigValueString) throws Exception{
        Helpers.waitForVisibility(STEP1_SUBJECT);
        STEP1_SUBJECT.sendKeys(bigValueString);
        Helpers.shortWait();
        STEP1_NEXT_BUTTON.click();
        return STEP1_SUBJECT_MAX_LENGTH_ERR_TEXT.isDisplayed();
    }

    public DonateMapPinStep2Page submitDonationDetails(CreateDonateFormRequest createDonateFormRequest) {
        donation_tone= createDonateFormRequest.getDonationTone();
        occasion=createDonateFormRequest.getOccasion();
        relation=createDonateFormRequest.getCommemorativeRelation();
        Helpers.waitForVisibility(STEP1_SUBJECT);
        STEP1_SUBJECT.sendKeys(createDonateFormRequest.getCommemorativeSubject());
        Helpers.selectdropdownByVisibleText(STEP1_TONE,donation_tone);
        if(donation_tone.equalsIgnoreCase("in celebration")) {
            if(!(occasion==null)) {
                Helpers.selectdropdownByVisibleText(STEP1_OCCASION, createDonateFormRequest.getOccasion());
                if (occasion.equalsIgnoreCase("Other")) {
                    STEP1_OCCASION_OTHER.sendKeys(createDonateFormRequest.getOtherOccasion());
                }
            }
        }
        if(!(relation==null)){
            Helpers.selectdropdownByVisibleText(STEP1_RELATION,relation);
            if(relation.equalsIgnoreCase("Other")){
                 STEP1_RELATION_OTHER.sendKeys(createDonateFormRequest.getOtherRelation());
             }
        }
        if(createDonateFormRequest.getIsPrivateListing().equalsIgnoreCase("true")) {
            STEP1_PRIVATE_PIN_CHECKBOX.click();
        }
        STEP1_NEXT_BUTTON.click();
        return new DonateMapPinStep2Page(driver);
    }

    public DonateMapPinStep1Page verifySubjectHelpText() {
        if(STEP1_SUBJECT_HELP_TEXT.getText().contains(subjectHelptext))
            return this ;
        else
            return null;
    }

    public DonateMapPinStep1Page verifyPrivatePintHelpText(){
        if(STEP1_PRIVATE_PIN_HELP_TEXT.getText().contains(privatePinHelpText))
            return this;
        else
            return null;
    }

    public boolean backLinkTakestoAddPinPage(){
        PREVIOUS_BUTTON.click();
       return driver.getCurrentUrl().contains("donate/map-pin-home#/add-map-pin");
    }

}