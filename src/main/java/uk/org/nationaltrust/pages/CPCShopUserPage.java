package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

public class CPCShopUserPage extends PageBase {

	protected WebDriver driver;

	public CPCShopUserPage(WebDriver webdriver) {
		this.driver = webdriver;
	}

	private static final By CONTACT_PREFERENCE_HEADER_LABEL = By.cssSelector("#permissionsForm > div:nth-child(1) > div > header > h1");

	public By GET_CONTACT_BY_PHONE_YES_CHECKBOX = By.id("contactConsentedTelephone-YesLabel");

	public By GET_CONTACT_BY_PHONE_NO_CHECKBOX = By.id("contactConsentedTelephone-NoLabel");

	public By GET_CONTACT_BY_EMAIL_YES_CHECKBOX = By.id("contactConsentedEmail-YesLabel");

	public By GET_CONTACT_BY_EMAIL_NO_CHECKBOX = By.id("contactConsentedEmail-NoLabel");

	public By GET_CONTACT_BY_POST_YES_CHECKBOX = By.id("contactConsentedPost-YesLabel");

	public By GET_CONTACT_BY_POST_NO_CHECKBOX = By.id("contactConsentedPost-NoLabel");

	public By SUBMIT_CONTACT_PREFERENCES_BUTTON = By.id("submitContactPermissions");

	private static final By GET_INVALID_POSTCODE_ERROR_TEXT = By.id("postcode-error");

	private static final By GET_INVALID_SUPPORTER_NUMBER_ERROR_TEXT = By.id("supporterNumber-error");

	private static final By GET_INVALID_SUPPORTER_POSTCODE_COMBINATION_ERROR_TEXT = By.cssSelector("#supporter-validation-error-message > div > p");

	private static final By GET_INVALID_PASSWORD_ERROR_TEXT = By.id("password-error");

	public static final By GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT = By.cssSelector("#contactPref-errorEmail > li");

	public static final By GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT = By.cssSelector("#contactPref-errorPost > li");

	public static final By GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT = By.id("contactPref-errorTelephone");

	public By GET_SUPPORTER_NUMBER_FIELD = By.id("supporterNumber");

	public By GET_PASSWORD_FIELD = By.cssSelector("#password");

	public By GET_POSTCODE_FIELD = By.id("postcode");

	public static final By CONFIRMATION_MAIN_HEADER = By.cssSelector("#mainContent > section > div:nth-child(1) > div > header > h1");

	public static final By CPC_MYNT_ACCOUNT_CREATED = By.id("accountExistAndMarketingPermissionsPreexist");

	public static final By CONFIRM_PREFS_THANK_YOU_MESSAGE = By.cssSelector("#mainContent > section > div:nth-child(3) > div > p:nth-child(1) > b");

	public static final By CREATE_ACCOUNT_THANK_YOU_MESSAGE = By.id("create-account-header");

	public static final By VOUCHER_CODE = By.id("voucherCode");

	public static final By SHOP_WELCOME_AND_VOUCHER = By.id("voucherMessage");

	private static final By GET_ALREADY_HAVE_AN_ACCOUNT_THANK_YOU_PAGE_HEADER = By.cssSelector("#mainContent > section > div:nth-child(1) > div > header > h1");

	private static final By GET_ALREADY_HAVE_AN_ACCOUNT_THANK_YOU_PAGE_HEADER_OTHER_VERSION = By.id("update-account-header");

	private static final By GET_ALREADY_HAVE_A_MYNT_ACCOUNT_INFO_TEXT = By.cssSelector("#permissionsForm > div.group > div:nth-child(1) > div > fieldset > div > p:nth-child(3)");

	private static final By GET_NEW_CONTACT_PREF_INFO_TEXT = By.id("js-permissionStatementContainer");

	private static final By GET_GET_TEN_PERCENT_OFF_INFO_TEXT = By.id("sub-header");

	private static final By GET_EMAIL_IS_USERNAME_TEXT = By.cssSelector("#permissionsForm > div.group > div:nth-child(1) > div > fieldset > div > p:nth-child(3)");

	public By TERMS_AND_CONDITIONS = By.id("termsAndConditions");

	public String getContactPrefHeaderTextDisplayed() {
		return driver.findElement(CONTACT_PREFERENCE_HEADER_LABEL).getText();
	}

	public String getContactPrefForEmailNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPostNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPhoneNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getInvalidPostcodeErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_INVALID_POSTCODE_ERROR_TEXT, 10, "Bad Postcode error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_INVALID_POSTCODE_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String badPostcodeErrorTextDisplayed = driver.findElement(GET_INVALID_POSTCODE_ERROR_TEXT).getText();

			return badPostcodeErrorTextDisplayed;
		}
		return null;

	}

	public String getInvalidSupporterNumberErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_INVALID_SUPPORTER_NUMBER_ERROR_TEXT, 10, "Bad supporterNumber error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_INVALID_SUPPORTER_NUMBER_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String badSupporterErrorTextDisplayed = driver.findElement(GET_INVALID_SUPPORTER_NUMBER_ERROR_TEXT).getText();

			return badSupporterErrorTextDisplayed;
		}
		return null;
	}

	public String getInvalidSupporterPostcodeCombinationError() {
		Helpers.waitForElementToAppear(driver, GET_INVALID_SUPPORTER_POSTCODE_COMBINATION_ERROR_TEXT, 10, "Invalid Supporter number and Postcode error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_INVALID_SUPPORTER_POSTCODE_COMBINATION_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String supporterPostcodeErrorTextDisplayed = driver.findElement(GET_INVALID_SUPPORTER_POSTCODE_COMBINATION_ERROR_TEXT).getText();

			return supporterPostcodeErrorTextDisplayed;
		}
		return null;

	}

	public String getInvalidPasswordTextDisplayed() {
		Helpers.waitForIsClickable(driver, GET_INVALID_PASSWORD_ERROR_TEXT, 10);
		List<WebElement> passwordErrorText = driver.findElements(GET_INVALID_PASSWORD_ERROR_TEXT);
		if (passwordErrorText.size() != 0) {
			String supporterPasswordErrorTextDisplayed = driver.findElement(GET_INVALID_PASSWORD_ERROR_TEXT).getText();
			return supporterPasswordErrorTextDisplayed;
		}
		return null;
	}

	public String getAccountAlreadyExistsWarningTextHeaderDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_ALREADY_HAVE_AN_ACCOUNT_THANK_YOU_PAGE_HEADER, 10, "Account Exists warning text not displayed");
		List<WebElement> accountExists = driver.findElements(GET_ALREADY_HAVE_AN_ACCOUNT_THANK_YOU_PAGE_HEADER);
		if (accountExists.size() != 0) {
			String accountExistsTextDisplayed = driver.findElement(GET_ALREADY_HAVE_AN_ACCOUNT_THANK_YOU_PAGE_HEADER).getText();

			return accountExistsTextDisplayed;
		}
		return null;

	}

	public String getAccountAlreadyExistsWarningTextHeaderOtherDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_ALREADY_HAVE_AN_ACCOUNT_THANK_YOU_PAGE_HEADER_OTHER_VERSION, 10, "Account Exists warning text not displayed");
		List<WebElement> accountExists = driver.findElements(GET_ALREADY_HAVE_AN_ACCOUNT_THANK_YOU_PAGE_HEADER_OTHER_VERSION);
		if (accountExists.size() != 0) {
			String accountExistsTextDisplayed = driver.findElement(GET_ALREADY_HAVE_AN_ACCOUNT_THANK_YOU_PAGE_HEADER_OTHER_VERSION).getText();

			return accountExistsTextDisplayed;
		}
		return null;

	}

	public String getAccountAlreadyExistsInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_ALREADY_HAVE_A_MYNT_ACCOUNT_INFO_TEXT, 10, "Account Exists warning text not displayed");
		List<WebElement> accountExists = driver.findElements(GET_ALREADY_HAVE_A_MYNT_ACCOUNT_INFO_TEXT);
		if (accountExists.size() != 0) {
			String accountExistsTextDisplayed = driver.findElement(GET_ALREADY_HAVE_A_MYNT_ACCOUNT_INFO_TEXT).getText();

			return accountExistsTextDisplayed;
		}
		return null;

	}

	public void setSupporterNumber(String accountName) {
		driver.findElement(GET_SUPPORTER_NUMBER_FIELD).sendKeys(accountName);
	}

	public void setPostCode(String postCode) {
		driver.findElement(GET_POSTCODE_FIELD).sendKeys(postCode);
	}

	public void setSupporterPassword(String password) {
		driver.findElement(GET_PASSWORD_FIELD).sendKeys(password);
	}

	public void setSupporterDetails(String password, String postcode) {
		setSupporterPassword(password);
		setPostCode(postcode);

	}

	public String getConfirmationThankYouHeaderDisplayed() {
		List<WebElement> confirmationHeaderText = driver.findElements(CONFIRMATION_MAIN_HEADER);
		if (confirmationHeaderText.size() != 0) {
			String confirmationHeader = driver.findElement(CONFIRMATION_MAIN_HEADER).getText();
			return confirmationHeader;
		}
		return null;
	}

	public String getAccountCreatedText() {
		List<WebElement> myntAccountCreated = driver.findElements(CPC_MYNT_ACCOUNT_CREATED);
		if (myntAccountCreated.size() != 0) {
			String myntAccountCreatedText = driver.findElement(CPC_MYNT_ACCOUNT_CREATED).getText();
			return myntAccountCreatedText;
		}
		return null;
	}

	public String getThankYouForConfirmingPrefsTextDisplayed() {
		List<WebElement> confirmPrefsThankYou = driver.findElements(CONFIRM_PREFS_THANK_YOU_MESSAGE);
		if (confirmPrefsThankYou.size() != 0) {
			String confirmPrefsThankYouText = driver.findElement(CONFIRM_PREFS_THANK_YOU_MESSAGE).getText();
			return confirmPrefsThankYouText;
		}
		return null;
	}

	public String getThankYouForCreatingOnlineAccountTextDisplayed() {
		List<WebElement> accountThankYou = driver.findElements(CREATE_ACCOUNT_THANK_YOU_MESSAGE);
		if (accountThankYou.size() != 0) {
			String accountThankYouText = driver.findElement(CREATE_ACCOUNT_THANK_YOU_MESSAGE).getText();
			return accountThankYouText;
		}
		return null;
	}

	public String getVoucherCodeDisplayed() {
		List<WebElement> voucher = driver.findElements(VOUCHER_CODE);
		if (voucher.size() != 0) {
			String voucherCode = driver.findElement(VOUCHER_CODE).getText();
			return voucherCode;
		}
		return null;
	}

	public String getHaveAccountAndAlreadyGivenPrefsWelcomeAndVoucherText() {
		List<WebElement> voucher = driver.findElements(SHOP_WELCOME_AND_VOUCHER);
		if (voucher.size() != 0) {
			String voucherCode = driver.findElement(SHOP_WELCOME_AND_VOUCHER).getText();
			return voucherCode;
		}
		return null;
	}

	public String getContactPrefInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactInfoText = driver.findElements(GET_NEW_CONTACT_PREF_INFO_TEXT);
		if (contactInfoText.size() != 0) {
			String contactInfoTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREF_INFO_TEXT).getText();

			return contactInfoTextDisplayed;
		}
		return null;

	}

	public String getContactPref10PerventOffTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_GET_TEN_PERCENT_OFF_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> tenPercentInfoText = driver.findElements(GET_GET_TEN_PERCENT_OFF_INFO_TEXT);
		if (tenPercentInfoText.size() != 0) {
			String tenPercentInfoTextDisplayed = driver.findElement(GET_GET_TEN_PERCENT_OFF_INFO_TEXT).getText();

			return tenPercentInfoTextDisplayed;
		}
		return null;

	}

	public String getEmailAddressIsUsernameTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_EMAIL_IS_USERNAME_TEXT, 10, "Email is the same as username Info text not displayed");
		List<WebElement> emailIsUsernameText = driver.findElements(GET_EMAIL_IS_USERNAME_TEXT);
		if (emailIsUsernameText.size() != 0) {
			String emailIsUsernameTextDisplayed = driver.findElement(GET_EMAIL_IS_USERNAME_TEXT).getText();

			return emailIsUsernameTextDisplayed;
		}
		return null;

	}



	public String getContactPreferenceHeaderLableText() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_BY_POST_NO_CHECKBOX, 10, "Contact Post No checkbox Does not appear");
		return driver.findElement(CONTACT_PREFERENCE_HEADER_LABEL).getText();
	}

	public boolean checkNewContactPostNoPrefAppears() {
		setLogs("check if contact by Post No Checkbox is displayed......");
		return Helpers.waitForIsClickable(driver, GET_CONTACT_BY_POST_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPostYesPrefAppears() {
		setLogs("check if contact by Post Yes Checkbox is displayed......");
		return Helpers.waitForIsClickable(driver, GET_CONTACT_BY_POST_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailNoPrefAppears() {
		setLogs("check if contact by Email No Checkbox is displayed......");
		return Helpers.waitForIsClickable(driver, GET_CONTACT_BY_EMAIL_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailYesPrefAppears() {
		setLogs("check if contact by Email Yes Checkbox is displayed......");
		return Helpers.waitForIsClickable(driver, GET_CONTACT_BY_EMAIL_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneNoPrefAppears() {
		setLogs("check if contact by Phone No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneYesPrefAppears() {
		setLogs("check if contact by Phone Yes Checkbox is displayed......");
		return Helpers.waitForIsClickable(driver, GET_CONTACT_BY_PHONE_YES_CHECKBOX, 10);
	}

	public boolean checkPasswordQuestionAppears() {
		setLogs("check if Password field is displayed......");
		return Helpers.waitForIsClickable(driver, GET_PASSWORD_FIELD, 10);
	}

	public boolean checkPostcodeQuestionAppears() {
		setLogs("check if Postcode field is displayed......");
		return Helpers.waitForIsClickable(driver, GET_POSTCODE_FIELD, 10);
	}

	public boolean checkCreateAccountThankYouTextAppears() {
		setLogs("check if CreateAccountThank you text is displayed......");
		return Helpers.waitForIsDisplayed(driver, CREATE_ACCOUNT_THANK_YOU_MESSAGE, 10);
	}

	public boolean checkAlreadyHaveAccountThankYouTextAppears() {
		setLogs("check if AlreadyHave a MY NT Account text is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_ALREADY_HAVE_A_MYNT_ACCOUNT_INFO_TEXT, 10);
	}

	public boolean checkAlreadyEmailIsSameAsUsernameTextAppears() {
		setLogs("check if Username is the same as the Email text is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_EMAIL_IS_USERNAME_TEXT, 10);
	}

	public boolean checkDisplayedOptions(String optionSelected) {
		boolean status = false;

		switch (optionSelected) {
			case "HAS_ACCOUNT_NO_PREFS":
				//				String getAlreadyHaveAccountWarningText = cpcShopUserPage().getAccountAlreadyExistsInfoTextDisplayed();
				//				Assert.assertEquals(getAlreadyHaveAccountWarningText, "You already have a MyNT account");
				if (
						!checkPasswordQuestionAppears() &&
								!checkPostcodeQuestionAppears() &&
								checkNewContactPostYesPrefAppears() &&
								checkNewContactPostNoPrefAppears() &&
								checkNewContactEmailNoPrefAppears() &&
								checkNewContactEmailYesPrefAppears() &&
								checkNewContactPhoneNoPrefAppears() &&
								checkNewContactPhoneYesPrefAppears())
					status = true;
				break;
			case "HAS_ACCOUNT_HAS_PREFS":
				if (
						!checkPasswordQuestionAppears() &&
								!checkPostcodeQuestionAppears() &&
								!checkNewContactPostYesPrefAppears() &&
								!checkNewContactPostNoPrefAppears() &&
								!checkNewContactEmailNoPrefAppears() &&
								!checkNewContactEmailYesPrefAppears() &&
								!checkNewContactPhoneNoPrefAppears() &&
								!checkNewContactPhoneYesPrefAppears())
					status = true;
				break;
			case "NO_ACCOUNT_HAS_PREFS":
				//				Assert.assertFalse(checkNewContactPostYesPrefAppears());
				if (
						checkPasswordQuestionAppears() &&
								checkPostcodeQuestionAppears() &&
								!checkNewContactPostYesPrefAppears() &&
								!checkNewContactPostNoPrefAppears() &&
								!checkNewContactEmailNoPrefAppears() &&
								!checkNewContactEmailYesPrefAppears() &&
								!checkNewContactPhoneNoPrefAppears() &&
								!checkNewContactPhoneYesPrefAppears())
					status = true;
				break;
			case "NO_ACCOUNT_NO_PREFS":

				if (
						checkPasswordQuestionAppears() &&
								checkPostcodeQuestionAppears() &&
								checkNewContactPostYesPrefAppears() &&
								checkNewContactPostNoPrefAppears() &&
								checkNewContactEmailNoPrefAppears() &&
								checkNewContactEmailYesPrefAppears() &&
								checkNewContactPhoneNoPrefAppears() &&
								checkNewContactPhoneYesPrefAppears())
					status = true;
				break;

			default:
				break;
		}
		return status;
	}

}


