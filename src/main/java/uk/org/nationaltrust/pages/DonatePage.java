package uk.org.nationaltrust.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import uk.org.nationaltrust.apis.CreateDonateFormRequest;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.framework.TestBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by Sowjanya Annepu
 */
public class DonatePage extends DonateCommons {

	protected WebDriver driver;

	private String customAmount = "12.50";

	private String appealName = "protect-special-places-appeal";

	protected boolean status = false;

	public String pageContent=null;


	public DonatePage(WebDriver webDriver) {
		super();
		this.driver = webDriver;
	}

	protected void donationForm() {
		driver.get(EnvironmentConfiguration.getDonateBaseURL());
	}


	public DonatePage buildPersonalDetailsForm(CreateDonateFormRequest createDonateFormRequest, boolean alteredDetails) {
		boolean isCommemorativeDonation = createDonateFormRequest.isCommemorativeDonation();
		if (isCommemorativeDonation) {
			if (alteredDetails) {
				COMMEMORATIVE_RELATION.sendKeys(createDonateFormRequest.getCommemorativeRelation());
				PERSONAL_DETAILS_PAGE_SUBJECT.sendKeys(createDonateFormRequest.getCommemorativeSubject());
				PERSONAL_DETAILS_PAGE_MESSAGE.sendKeys(createDonateFormRequest.getDonationStory());
			}
		}
		donatePersonalDetailsFormExcludingCommemorativeOptions(createDonateFormRequest);
		return this;
	}

	public DonatePage donatePersonalDetailsFormExcludingCommemorativeOptions(CreateDonateFormRequest createDonateFormRequest){
		PERSONAL_DETAILS_OTHER_AMOUNT.sendKeys(createDonateFormRequest.getDonationAmount());
		Helpers.selectdropdownByVisibleText(TITLE, createDonateFormRequest.getDonateFormTitle());
		FIRST_NAME.sendKeys(createDonateFormRequest.getDonateFormFirstName());
		LAST_NAME.sendKeys(createDonateFormRequest.getDonateFormLastName());
		driver.findElement(donateForm().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		ADDRESS_LINE1.sendKeys(createDonateFormRequest.getDonateFormAddressLine1());
		POSTCODE.sendKeys(createDonateFormRequest.getDonateFormPostcode());
		EMAIL.sendKeys(createDonateFormRequest.getDonateFormEmail());
		driver.findElement(donateForm().GET_CONTACT_BY_EMAIL_YES_CHECKBOX).click();
		driver.findElement(donateForm().GET_CONTACT_BY_PHONE_YES_CHECKBOX).click();
		driver.findElement(donateForm().GET_CONTACT_BY_POST_YES_CHECKBOX).click();
		GIFT_AID_CONSENT_CHECKBOX.click();
		return this;
	}

	public DonatePage submitPersonalDetailsForm() {
		driver.findElement(donateForm().CONFIRM_AND_PAY_BUTTON).click();
		return this;
	}

	public void submitDonateIndexForm() {
		driver.findElement(donateForm().INDEX_SUBMIT_BUTTON).click();
	}

	public void getDonateIndexURL() {
		driver.navigate().to(EnvironmentConfiguration.getText("indexURL"));
	}

	public void clearAppealTitle() {
		driver.findElement(By.id("title")).clear();
	}

	public DonatePage navigateToDonateFormByOneOffDonation(String optionSelected) throws Exception {
		donationForm();
		Helpers.shortWait();
		driver.get(EnvironmentConfiguration.getSpecificAppealLink() + appealName);
		setLogs("Wait for appeal page to be displayed");
		driver.findElement(By.xpath("//span[@class='nt-appeal-sticky-donate__desc']")).click();
		Helpers.shortWait();
		switch (optionSelected) {
			case "oneOffCustomText":
				appealDonatePage().setCustomFieldAmountText("10");
				driver.findElement(appealDonatePage().DONATE_BUTTON).click();
				break;
			case "ONE_OFF_APPEAL_BASKET_AMOUNT_ONE":
				driver.findElement(By.xpath("//div[@id='donation-tab__one-off']//span[contains(text(),'£15')]")).click();
				Helpers.shortWait();
				break;
			case "ONE_OFF_APPEAL_BASKET_AMOUNT_TWO":
				driver.findElement(By.xpath("//div[@id='donation-tab__one-off']//span[contains(text(),'£25')]")).click();
				Helpers.shortWait();
				break;
			case "ONE_OFF_APPEAL_BASKET_AMOUNT_THREE":
				driver.findElement(By.xpath("//div[@id='donation-tab__one-off']//span[contains(text(),'£60')]")).click();
				Helpers.shortWait();
				break;
			default:
				break;
		}
		Thread.sleep(2000);
		Helpers.waitForElementToAppear(driver, DONATION_FORM_THANK_YOU_HEADER, 10, "Donate form Header not displayed after 10 seconds");
		return this;
	}

	public void navigateToDonateFormByMonthlyDonation(String optionSelected) throws Exception {
		donationForm();
		Helpers.shortWait();
		driver.get(EnvironmentConfiguration.getSpecificAppealLink() + appealName);
		setLogs("Wait for appeal page to be displayed");
		driver.findElement(By.xpath("//span[@class='nt-appeal-sticky-donate__desc']")).click();
		Helpers.shortWait();
		driver.findElement(By.id("nt-appeal-donate__tabs__regular")).click();
		switch (optionSelected) {
			case "monthlyCustomText":
				appealDonatePage().setCustomFieldAmountText(customAmount);
				driver.findElement(appealDonatePage().DONATE_BUTTON).click();
				break;
			case "MONTHLY_APPEAL_BASKET_AMOUNT_ONE":
				driver.findElement(By.xpath("//div[@id='donation-tab__regular']//span[contains(text(),'£3')]")).click();
				Helpers.shortWait();
				break;
			case "MONTHLY_APPEAL_BASKET_AMOUNT_TWO":
				driver.findElement(By.xpath("//div[@id='donation-tab__regular']//span[contains(text(),'£5')]")).click();
				Helpers.shortWait();
				break;
			case "MONTHLY_APPEAL_BASKET_AMOUNT_THREE":
				driver.findElement(By.xpath("//div[@id='donation-tab__regular']//span[contains(text(),'£10')]")).click();
				Helpers.shortWait();
				break;
			default:
				break;
		}
		Thread.sleep(2000);
		Helpers.waitForElementToAppear(driver, DONATION_FORM_THANK_YOU_HEADER, 10, "Donate form Header not displayed after 10 seconds");
	}

	public boolean checkDisplayedOption(String optionSelected) {
		boolean status = false;

		switch (optionSelected) {
			case "oneOffCustomText":
				driver.findElement(By.id("oneOffOtherAmountPoundsAndPence")).getText().contains(customAmount);
				status = true;
				break;
			case "ONE_OFF_APPEAL_BASKET_AMOUNT_ONE":
				if (driver.findElement(By.id("paymentTypeOneOff")).isSelected() &&
						driver.findElement(By.id("oneOffPaymentAmountChoice1")).isSelected())
					status = true;
				break;
			case "ONE_OFF_APPEAL_BASKET_AMOUNT_TWO":
				if (driver.findElement(By.id("paymentTypeOneOff")).isSelected() &&
						driver.findElement(By.id("oneOffPaymentAmountChoice2")).isSelected())
					status = true;
				break;
			case "ONE_OFF_APPEAL_BASKET_AMOUNT_THREE":
				if (driver.findElement(By.id("paymentTypeOneOff")).isSelected() &&
						driver.findElement(By.id("oneOffPaymentAmountChoice3")).isSelected())
					status = true;
				break;
			case "monthlyCustomText":
				driver.findElement(By.id("regularOtherAmountPoundsAndPence")).getText().contains(customAmount);
				status = true;
				break;
			case "MONTHLY_APPEAL_BASKET_AMOUNT_ONE":
				if (driver.findElement(By.id("paymentTypeMonthly")).isSelected() &&
						driver.findElement(By.id("regularPaymentAmountChoice1")).isSelected())
					status = true;
				break;
			case "MONTHLY_APPEAL_BASKET_AMOUNT_TWO":
				if (driver.findElement(By.id("paymentTypeMonthly")).isSelected() &&
						driver.findElement(By.id("regularPaymentAmountChoice2")).isSelected())
					status = true;
				break;
			case "MONTHLY_APPEAL_BASKET_AMOUNT_THREE":
				if (driver.findElement(By.id("paymentTypeMonthly")).isSelected() &&
						driver.findElement(By.id("regularPaymentAmountChoice3")).isSelected())
					status = true;
				break;
			default:
				break;
		}
		return status;
	}

	public boolean switchToOneOff() {
		String clearedText = "";
		driver.findElement(By.id("paymentTypeOneOffButton")).click();
		driver.findElement(By.id("oneOffOtherAmountPoundsAndPence")).getText().contains(clearedText);
		if (!checkDisplayedOption("ONE_OFF_APPEAL_BASKET_AMOUNT_TWO") &&
				!checkDisplayedOption("ONE_OFF_APPEAL_BASKET_AMOUNT_ONE") &&
				!checkDisplayedOption("ONE_OFF_APPEAL_BASKET_AMOUNT_THREE")
		) {
			return true;
		}
		return false;
	}

	public void setDirectDebitDetails(String ddName, String ddNumber, String ddSortCode1, String ddSortCode2, String ddSortCode3) {
		setAccountName(ddName);
		setAccountNumber(ddNumber);
		setSortCodeOne(ddSortCode1);
		setSortCodeTwo(ddSortCode2);
		setSortCodeThree(ddSortCode3);
	}

	public boolean completeDonation() {
		String email = UUID.randomUUID() + "test@nt.com";
		donateForm().setAllDonateFormText("Mr", "Test", "NT", "Nexus", "sn2 2na", email);
		driver.findElement(donateForm().GET_CONTACT_BY_EMAIL_YES_CHECKBOX).click();
		driver.findElement(donateForm().GET_CONTACT_BY_PHONE_YES_CHECKBOX).click();
		driver.findElement(donateForm().GET_CONTACT_BY_POST_YES_CHECKBOX).click();
		driver.findElement(donateForm().CONFIRM_AND_PAY_BUTTON).click();
		return true;
	}

	public void selectTypeOfDonationOnDonateForm(String optionSelected) throws Exception {
		switch (optionSelected) {
			case "ONE_OFF_DONATION":
				driver.findElement(GET_ONE_OFF_TAB).click();
				break;
			case "MONTHLY_DONATION":
				driver.findElement(GET_REGULAR_TAB).click();
				Helpers.shortWait();
				break;
		}
	}

	private boolean donationOneOffAmount(String amount) {
		return driver.findElement(By.cssSelector("input[name='oneOffPaymentAmountChoice'][value='" + amount + "']")).isDisplayed();
	}

	private boolean donationRegularAmount(String amount) {
		return driver.findElement(By.cssSelector("input[name='regularPaymentAmountChoice'][value='" + amount + "']")).isDisplayed();
	}

	private boolean donationOtherAmount(String amount) {
		return driver.findElement(By.cssSelector("input[id='oneOffOtherAmountPoundsAndPence']")).isDisplayed();
	}

	public boolean checkDisplayedDonateOptions(String commemorativeChoice) {
		Helpers.waitForJSandJQueryToLoad();
		status = false;
		driver.findElement(By.xpath("//h1[contains(text(), 'Your donation') ]")).isDisplayed();
		switch (commemorativeChoice) {
			case "IN_MEMORY_DEFAULT":
				if (driver.findElement(By.id("paymentTypeOneOff")).isSelected())
					if (donationOneOffAmount("15") && donationOneOffAmount("30") && donationOneOffAmount("60") && donationOtherAmount("10"))
						if (!donationRegularAmount("3"))
							if (driver.findElement(By.id("commemorativeYes")).getAttribute("value").equalsIgnoreCase("true"))
								status = true;
				break;
			case "IN_MEMORY_CUSTOM":
				if (driver.findElement(By.id("paymentTypeMonthly")).isSelected())
					if (driver.findElement(By.cssSelector("input[id='oneOffOtherAmountPoundsAndPence']")).getText().equals(""))
						if (donationRegularAmount("4") && donationRegularAmount("6") && donationRegularAmount("8"))
							if (driver.findElement(By.id("commemorativeYes")).getAttribute("value").equalsIgnoreCase("true") &&
									driver.findElement(By.id("commemorationForm_tone_IN_MEMORY")).getAttribute("value").equalsIgnoreCase("IN_MEMORY"))
								status = true;
				break;
			case "IN_CELEBRATION_OPTION":
				if (driver.findElement(By.id("paymentTypeOneOff")).isSelected())
					if (donationOneOffAmount("15") && donationOneOffAmount("25") && donationOneOffAmount("35"))
						if (driver.findElement(By.id("commemorativeYes")).getAttribute("value").equalsIgnoreCase("true") &&
								driver.findElement(By.id("commemorationForm_tone_IN_CELEBRATION")).getAttribute("value").equalsIgnoreCase("IN_CELEBRATION"))
							status = true;
				break;
			case "Commemorative_DEFAULT":
				if (driver.findElement(By.id("commemorativeYes")).getAttribute("value").equalsIgnoreCase("true") &&
						driver.findElement(By.id("commemorationForm_tone_IN_MEMORY")).getAttribute("value").equalsIgnoreCase("IN_MEMORY"))
					status = true;
				break;
			case "DONATE_DEFAULT_OPTIONS":
				if (driver.findElement(By.id("paymentTypeOneOff")).isSelected())
					if (donationOneOffAmount("15") && donationOneOffAmount("30") && donationOneOffAmount("60") && donationOtherAmount("10"))
						if (!donationRegularAmount("3"))
							if (driver.findElement(By.id("commemorativeGivingTone")).getAttribute("aria-hidden").equalsIgnoreCase("true"))
								status = true;
				break;
					default:
				setLogs("Nothing working as expected!!!!");
				break;
		}
		return status;
	}

	public DonatePage switchToNonCommemorative() {
		COMMMEMORATIVE_NO_RADIO.click();
		return this;
	}

	public void switchToCommemorative() {
		Helpers.waitForElementToAppear(driver, COMMMEMORATIVE_YES_RADIO, 30, "Commemorative option not displayed");
		driver.findElement(By.id("commemorativeYesButton")).click();
		driver.findElement(By.id("commemorativeInMemoryButton")).click();
	}

	public void setAmount(String amount) {
		Helpers.clearAndSetText(driver, DONATION_AMOUNT_DISPLAYED, amount);
	}

	public void setOneOffAmounts(String amounts) {
		driver.findElement(By.id("oneoffPredefinedValues")).clear();
		driver.findElement(By.id("oneoffPredefinedValues")).sendKeys(amounts);
	}

	public void setRegularAmounts(String amounts) {
		driver.findElement(By.id("regularPredefinedValues")).clear();
		driver.findElement(By.id("regularPredefinedValues")).sendKeys(amounts);
	}

	public void setBackGroudImage(String imageSet) {
		Select image = new Select(driver.findElement(By.id("useImageAsBackground")));
		image.selectByValue(imageSet);

	}

	public void setCommemorativeOption(String commemorativeOption) {
		Select option = new Select(driver.findElement(By.id("commemorativeOptionGivenSelect")));
		option.selectByValue(commemorativeOption);
	}

	public void setCommemorativeTone(String commemorativeTone) {
		Select toneOption = new Select(driver.findElement(By.id("commemorativeToneSelect")));
		toneOption.selectByValue(commemorativeTone);
	}

	public void setDonationType(String donateType) {
		driver.findElement(DONATION_TYPE_FIELD).sendKeys(donateType);
	}

	public void setDonationAppealCode(String donateAppealCode) {
		driver.findElement(DONATION_APPEAL_CODE).clear();
		driver.findElement(DONATION_APPEAL_CODE).sendKeys(donateAppealCode);
	}

	public void setCampaignSourceCode(String sourceCode) {
		driver.findElement(CAMPAIGN_SOURCE_CODE).clear();
		driver.findElement(CAMPAIGN_SOURCE_CODE).sendKeys(sourceCode);
	}

	public boolean userSelectsDonateFormLink() {
		getDonateIndexURL();
		submitDonateIndexForm();
		Helpers.waitForJSandJQueryToLoad();
		if (driver.findElement(By.xpath("//h1[contains(text(), 'Your donation') ]")).isDisplayed())
			status = true;
		return status;
	}

	public boolean userSelectsDonateURL() {
		getDonateIndexURL();
		driver.findElement(By.linkText("Donate using URL")).click();
		Helpers.waitForJSandJQueryToLoad();
		if (driver.findElement(By.xpath("//h1[contains(text(), 'Your donation') ]")).isDisplayed())
			status = true;
		return status;
	}

	public boolean userCopiesURLToClipboard() {
		getDonateIndexURL();
		String copiedURL = driver.findElement(By.id("copyToClipboard")).getAttribute("href");
		driver.navigate().to(copiedURL);
		Helpers.waitForJSandJQueryToLoad();
		if (driver.findElement(By.xpath("//h1[contains(text(), 'Your donation') ]")).isDisplayed())
			if (donationOneOffAmount("15") && donationOneOffAmount("30") && donationOneOffAmount("60") && donationOtherAmount("10"))
				status = true;
		return status;
	}

	public boolean isPaymentPageDisplayed() {
		return driver.getPageSource().contains("Please select a payment method by clicking on the logo.");
	}

	public boolean isEnteredCorrectDataSaved() {
		if (driver.findElement(By.id("postalAddress_postcode")).getAttribute("value").contains("sn2 2na"))
			if (driver.findElement(By.id("emailAddress")).getAttribute("value").contains("test@nt.com")) {
				return true;
			}
		return false;
	}

	private void setCommemorativeSubjectName(String name) {
		driver.findElement(By.id("commemorationForm_subject")).sendKeys(name);
	}

	public String getErrorList() {
		Helpers.waitForElementToAppear(driver, ERROR_SUMMARY_LIST, 20, "Error summary not displayed");
		return driver.findElement(ERROR_SUMMARY_LIST).getText();
	}

	public String relationShipMandatoryValidation() {
		setCommemorativeSubjectName("Donate test user");
		Select options = new Select(COMMEMORATIVE_RELATION);
		options.selectByValue("OTHER");
		this.completeDonation();
		return getErrorList(); //  This checks that if Other selected then it's mandatory to select field "what is their relationship to you"
		// TODO  add checks for error-list to validate the displayed error messages
		// check if the form saves the 'OTHER' value for a relationship after submitting form with errors
		// if so then
	}

	public boolean relationShipChosenOtherAndSubmitForm(boolean celebrationSelected) {
		if (celebrationSelected)
			driver.findElement(By.id("commemorationForm_celebrationReasonOther")).sendKeys("GLEE_MOMENT");
		if (!celebrationSelected)
			driver.findElement(By.id("commemorationForm_relationOther")).sendKeys("PET");
		driver.findElement(donateForm().CONFIRM_AND_PAY_BUTTON).click();
		return driver.getPageSource().contains("Please select a payment method by clicking on the logo.");
	}

	public boolean isCorrectRelationshipValuesDisplayed() {
		setCommemorativeSubjectName("RelationValues Test");
		Select options = new Select(COMMEMORATIVE_RELATION);
		List<WebElement> dropdownOptions = options.getOptions();
		List<String> relationActualList = new ArrayList<>();
		for (int i = 0; i < dropdownOptions.size(); i++) {
			relationActualList.add(dropdownOptions.get(i).getText());
		}
		List<String> relationshipExpectedList = Arrays.asList("Please select (optional)", "Wife", "Husband", "Mother", "Father", "Grandmother", "Grandfather", "Daughter", "Son", "Sister", "Brother",
				"Cousin", "Auntie", "Uncle", "Friend", "Colleague", "Other");

		if (relationActualList.equals(relationshipExpectedList))
			return true;
		return false;
	}

	public boolean setNameAndSubmitForm(String subjectName) {
		setCommemorativeSubjectName(subjectName);
		donatePage().completeDonation();
		return driver.getPageSource().contains("Please select a payment method by clicking on the logo.");
	}

	public boolean validateOccasionValues() {
		setCommemorativeSubjectName("Occasion Values test");
		Select options = new Select(driver.findElement(By.id("commemorationForm_celebrationReason")));
		List<WebElement> dropdownOptions = options.getOptions();
		List<String> occasionActualList = new ArrayList<>();
		for (int i = 0; i < dropdownOptions.size(); i++) {
			occasionActualList.add(dropdownOptions.get(i).getText());
		}
		List<String> relationshipExpectedList = Arrays
				.asList("Please select (optional)", "Birthday", "Anniversary", "Wedding", "Christmas", "Birth", "Christening", "Graduation", "Retirement", "Other");

		return occasionActualList.equals(relationshipExpectedList);
	}

	public String validateOccasionIfOtherOptionChosen() {
		setCommemorativeSubjectName("Commemorative Donation Subject name");
		Select occasionOptions = new Select(driver.findElement(By.id("commemorationForm_celebrationReason")));
		occasionOptions.selectByValue("OTHER");
		donatePage().completeDonation();
		return getErrorList();
	}

	public String completeCommemorativeDonation(String commType) {
		String email = UUID.randomUUID() + "test@nt.com";
		String firstName = UUID.randomUUID() + "testFirstName";
		switch (commType) {
			case "IN_CELEBRATION":
				Select options = new Select(COMMEMORATIVE_RELATION);
				options.selectByValue("MOTHER");
				Select occasionOptions = new Select(driver.findElement(By.id("commemorationForm_celebrationReason")));
				occasionOptions.selectByValue("BIRTHDAY");
				break;
			case "COMMEMORATIVE_DEFAULT":
				setLogs("Do nothing and Option NO is already selected");
				break;
			case "IN_MEMORY_MONTHLY":
				driver.findElement(By.name("regularOtherAmountPoundsAndPence")).sendKeys("24");
				options = new Select(COMMEMORATIVE_RELATION);
				options.selectByValue("OTHER");
				driver.findElement(By.id("commemorationForm_relationOther")).sendKeys("PET");
				driver.findElement(GET_ACCOUNT_NAME_FIELD).sendKeys("accountName");
				driver.findElement(GET_ACCOUNT_NUMBER_FIELD).sendKeys("55779944");
				driver.findElement(GET_SORT_CODE_ONE_FIELD).sendKeys("20");
				driver.findElement(GET_SORT_CODE_TWO_FIELD).sendKeys("00");
				driver.findElement(GET_SORT_CODE_THREE_FIELD).sendKeys("00");
				break;
			case "NON_COMMEMORATIVE":
				setLogs("DO nothing");
				break;
		}
		setCommemorativeSubjectName("commemorativeSubjectName");
		donateForm().setAllDonateFormText("Mr", firstName, "NT", "Nexus", "sn2 2na", email);
		driver.findElement(donateForm().GET_CONTACT_BY_EMAIL_YES_CHECKBOX).click();
		driver.findElement(donateForm().GET_CONTACT_BY_PHONE_YES_CHECKBOX).click();
		driver.findElement(donateForm().GET_CONTACT_BY_POST_YES_CHECKBOX).click();
		driver.findElement(donateForm().CONFIRM_AND_PAY_BUTTON).click();
		driver.getPageSource().contains("Please select a payment method by clicking on the logo.");
		return email;
	}

	public boolean persistPersonalDetailsOnCancellation() throws Exception {
		barclaysPage().selectCancelButton();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		setLogs("Wait for barclays to redirect back to donate form");
		Thread.sleep(20000);
		Helpers.waitForJSandJQueryToLoad();
		setLogs("Checking the cancellation error text is displayed on personal details form ");
		driver.getPageSource().contains("It looks like you changed your mind, and your payment was cancelled. If you’d still like to complete your donation, please try again.");
		setLogs("Cancellation error message is displayed");
		boolean isCommemorativeSelected = driver.findElement(By.id("commemorativeYes")).getAttribute("value").equalsIgnoreCase("true");
		boolean isCommemorativeInCelebration = driver.findElement(By.id("commemorationForm_tone_IN_CELEBRATION")).getAttribute("value").equalsIgnoreCase("IN_CELEBRATION");
		if (driver.findElement(By.id("paymentTypeOneOff")).isSelected())
			if (isCommemorativeSelected && isCommemorativeInCelebration)
				if (driver.findElement(By.id("commemorationForm_subject")).getAttribute("value").contains("Commemorative Donation Subject name"))
					status = true;
		return status;
	}

	public void donateViaAppealPage() {
		driver.navigate().to(EnvironmentConfiguration.getSpecificAppealLink() + SPECIAL_PLACES_APPEAL);
		driver.findElement(By.cssSelector(".nt-appeal-donate__amount-input")).sendKeys("12");
		driver.findElement(By.cssSelector(".nt-appeal-donate__submit")).click();
		Helpers.waitHandlingException(1000);
		if (driver.findElement(By.id("commemorativeGivingTone")).getAttribute("aria-hidden").equalsIgnoreCase("true"))
			switchToCommemorative();
	}

	public String getTitle() {
		Helpers.waitForJSandJQueryToLoad();
		return driver.findElement(By.id("thank-you-message")).getText();
	}

	public String getFairProcessingNoticeText() {
		return driver.findElement(By.xpath("//h3[contains(text(),'How we use your data' )]/following-sibling:: p")).getText();
	}

	public void getEntriesFromModerationTable() {
		List<WebElement> tr = driver.findElements(By.xpath("//*[@id='__BVID__8']/tbody//tr"));
	}

	public Boolean verifyStoryEditableFieldsNotDisplayed (){
	setLogs("check story editable field is not displayed");
		return Helpers.waitForIsDisplayed(driver, COMMMEMORATIVE_YES_RADIO, 10);


}

	public Boolean verifyCommYesButtonNotDisplayed (){
		setLogs("check In Memory Yes button is not displayed");
		return Helpers.waitForIsDisplayed(driver, COMMMEMORATIVE_YES_RADIO, 10);


	}
}

