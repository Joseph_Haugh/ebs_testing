package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import uk.org.nationaltrust.framework.Helpers;

public class HeadersAndFooters extends PageBase {
	protected WebDriver driver;

	public HeadersAndFooters (WebDriver dr) {
		this.driver = dr;
	}

	public By GET_ESI_HEADER_ID = By.id("nt-tst-esi-header");
	public By GET_ESI_FOOTER_ID = By.id("nt-tst-esi-footer");

	public Boolean doesESIHeaderAppear (){
		setLogs ("Wait for ESI header to appear");
		return Helpers.waitForIsDisplayed(driver, GET_ESI_HEADER_ID,10);
	}
	public Boolean doesESIFooterAppear (){
		setLogs ("Wait for ESI header to appear");
		return Helpers.waitForIsDisplayed(driver, GET_ESI_FOOTER_ID,10);
	}
}
