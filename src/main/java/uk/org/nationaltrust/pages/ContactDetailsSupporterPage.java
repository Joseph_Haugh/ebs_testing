package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * Created by nick.thompson on 28/10/2016.
 */
public class ContactDetailsSupporterPage extends PageBase {

	protected WebDriver driver;

	public By REVIEW_YOUR_CONTACT_DETAILS_HEADER = By.cssSelector("#renewMembershipForm > div:nth-child(4) > div > fieldset > div:nth-child(1) > legend");

	public By CONFIRMATION_EMAIL_HEADER = By.cssSelector("#mainContent > section > form > div:nth-child(4) > div > fieldset > div:nth-child(1) > div > div.small-12.medium-7.columns > label");

	public By POSTCODE_HEADER = By.cssSelector("#mainContent > section > form > div:nth-child(4) > div > fieldset > div:nth-child(1) > div > div.small-12.medium-5.columns > label");

	public By SUPPORTER_EMAIL = By.cssSelector("input[id*='emailAddress'][name='maskedEmailAddress']");

	public By SUPPORTER_EMAIL_ENTRY_FIELD = By.cssSelector("#emailAddress");

	public By SUPPORTER_POSTCODE = By.cssSelector("#postcode");

	public By CONTINUE_TO_PAYMENT_BUTTON = By.cssSelector("#confirmPersonalDetails");

	public static By EMAIL_MISSING_ERROR_TEXT = By.cssSelector("#emailAddress-error > li");

	private static final By OVER_60_SENIOR_TEXT = By.cssSelector("#renewMembershipForm > div:nth-child(1) > div:nth-child(2) > fieldset > div > p");

	public static By EMAIL_USAGE_TEXT = By.cssSelector("#renewMembershipForm > div:nth-child(4) > div > fieldset > div:nth-child(1) > div > div.small-12.medium-7.columns > p");
	public By PLEASE_SEND_ME_MY_FREE_GIFT = By.cssSelector("#receiveIncentiveLabel");

	public ContactDetailsSupporterPage(WebDriver dr) {
		this.driver = dr;
	}

	public String getReviewYourContactDetailsHeaderText() {
		return driver.findElement(REVIEW_YOUR_CONTACT_DETAILS_HEADER).getText();
	}

	public String getConfirmationEmailHeaderText() {
		return driver.findElement(CONFIRMATION_EMAIL_HEADER).getText();
	}

	public String getPostcodeHeaderText() {
		return driver.findElement(POSTCODE_HEADER).getText();
	}

	public void clickContinueToPaymentButton() {
		driver.findElement(CONTINUE_TO_PAYMENT_BUTTON).click();
	}

	public String getPostcodeDisplayed() {
		List<WebElement> postcodeField = driver.findElements(SUPPORTER_POSTCODE);
		if (postcodeField.size() != 0) {
			String postcodeDisplayed = driver.findElement(SUPPORTER_POSTCODE).getAttribute("value");
			setLogs("Getting the Postcode from the contact details section" + postcodeDisplayed);
			return postcodeDisplayed;
		}
		return null;

	}

	public String getEmailUsageTextDisplayed() {
		List<WebElement> emailUsageText = driver.findElements(EMAIL_USAGE_TEXT);
		if (emailUsageText.size() != 0) {
			String emailUsageTextDiaplayed = driver.findElement(EMAIL_USAGE_TEXT).getText();

			return emailUsageTextDiaplayed;
		}
		return null;

	}
	public String emailUsageTextDisplayed() {
		List<WebElement> emailUsageText = driver.findElements(EMAIL_USAGE_TEXT);
		if (emailUsageText.size() != 0) {
			setLogs("Email Usage text appears when not expected to appear");
			String emailUsageTextDiaplayed = driver.findElement(EMAIL_USAGE_TEXT).getText();
			return emailUsageTextDiaplayed;
		}
		return "NoEmailUsageTextAppears";

	}
	public boolean checkIfEmailTextExists(){
		setLogs("check if email text text is displayed.......");
		return Helpers.waitForIsDisplayed(driver,EMAIL_USAGE_TEXT,10);
	}

	public String getSupporterEmailDisplayed() {
		List<WebElement> emailField = driver.findElements(SUPPORTER_EMAIL);
		if (emailField.size() != 0) {
			String emailDisplayed = driver.findElement(SUPPORTER_EMAIL).getAttribute("value");
			setLogs("Getting the Email from the contact details section" + emailDisplayed);
			return emailDisplayed;

		}
		return null;
	}

	public String getEmailEnteredDisplayed() {
		List<WebElement> emailEnteredField = driver.findElements(SUPPORTER_EMAIL_ENTRY_FIELD);
		if (emailEnteredField.size() != 0) {
			String emailEntered = driver.findElement(SUPPORTER_EMAIL_ENTRY_FIELD).getText();
			setLogs("Getting the email that was entered by the supporter");
			return emailEntered;
		}
		return null;
	}

	public String getEmailMissingErrorText() {

		List<WebElement> emailMissingField = driver.findElements(EMAIL_MISSING_ERROR_TEXT);
		if (emailMissingField.size() != 0) {
			String emailMissing = driver.findElement(EMAIL_MISSING_ERROR_TEXT).getText();
			setLogs("Getting the email that was entered by the supporter");
			return emailMissing;
		}
		return null;
	}

	public void setTheEmailOfSupporter(String emailId){
		List<WebElement> supporterEmail = driver.findElements(SUPPORTER_EMAIL);
		if(supporterEmail.size() == 0){
			setLogs("Entering the emailId for the supporter - " + emailId);
			Helpers.clearAndSetText(driver, SUPPORTER_EMAIL_ENTRY_FIELD, emailId);
		}
		else {
			setLogs("Email already exist -- " + supporterEmail.get(0).getAttribute("value"));
		}
	}
	public String giftCheckboxAppears (){
		List<WebElement> giftCheckbox = driver.findElements(PLEASE_SEND_ME_MY_FREE_GIFT);
		if (giftCheckbox.size()!=0){
			setLogs("Send me gift checkbox appears");
			driver.get (EnvironmentConfiguration.getText("GiftCheckboxCheckboxExists"));
			return "GiftCheckboxExists";
		}
		return "NoGiftCheckboxExists";
	}


	public void selectGiftCheckbox (){
		driver.findElement(PLEASE_SEND_ME_MY_FREE_GIFT).click();

	}

	public String getOver60InfoTextDisplayed(){
		List<WebElement> over60 = driver.findElements(OVER_60_SENIOR_TEXT);
		if(over60.size()!=0){
			String over60Text = driver.findElement(OVER_60_SENIOR_TEXT).getText();
			return over60Text;

		}
		return null;
	}

}



