package uk.org.nationaltrust.pages;//package uk.org.nationaltrust.renew.pages;
//
//import java.util.List;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//
///**
// * Created by nick.thompson on 18/11/2016.
// */
//public class CardPaymentsPage extends PageBase {
//
//	protected WebDriver driver;
//
//	public By VISA_CARD_BUTTON = By.cssSelector("body > form > table > tbody > tr:nth-child(2) > td:nth-child(3) > input:nth-child(4)");
//
//	public By AMEX_CARD_BUTTON = By.cssSelector("body > form > table > tbody > tr:nth-child(2) > td:nth-child(3) > input:nth-child(1)");
//
//	public By JCB_CARD_BUTTON = By.cssSelector("body > form > table > tbody > tr:nth-child(3) > td:nth-child(3) > input");
//
//	public By MASTERCARD_BUTTON = By.cssSelector("body > form > table > tbody > tr:nth-child(2) > td:nth-child(3) > input:nth-child(5)");
//
//	public By MAESTRO_CARD_BUTTON = By.cssSelector("body > form > table > tbody > tr:nth-child(3) > td:nth-child(3) > input");
//
//	public By CARDHOLDER_NAME_FIELD = By.cssSelector("#Ecom_Payment_Card_Name");
//
//	public By CARDNUMBER_FIELD = By.cssSelector("#Ecom_Payment_Card_Number");
//
//	public By EXPIREY_MONTH = By.cssSelector("#Ecom_Payment_Card_ExpDate_Month");
//
//	public By EXPIREY_YEAR = By.cssSelector("#Ecom_Payment_Card_ExpDate_Year");
//
//	public By CONFIRM_MY_PAYMENT_BUTTON = By.cssSelector("#submit3");
//
//	public By BACK_BUTTON = By.cssSelector("#btn_Back");
//
//	public By CANCEL_BUTTON = By.cssSelector("#ncol_cancel");
//
//	public String getPaymentDetailsIncompleteErrorText() {
//		List<WebElement> visaCardButton = driver.findElements(VISA_CARD_BUTTON);
//		if (visaCardButton.size() != 0) {
//			String visaCardButtonText = driver.findElement(VISA_CARD_BUTTON).getText();
//			return visaCardButtonText;
//		}
//		return null;
//	}
//
//	public CardPaymentsPage(WebDriver dr) {
//		this.driver = dr;
//	}
//}
