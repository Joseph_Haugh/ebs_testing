package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.framework.SupporterServiceDBConnection;

import java.util.List;

/**
 * Created by Pramod.Reguri on 16/02/2016.
 */
public class RegistrationPage extends PageBase {

	public static final By CANNOT_USE_USER_NAME_AS_PASSWORD_MESSAGE = By.cssSelector("#regPassword-error");

	private By REGISTRATION_PAGE_PASSWORD_VIEW = By.cssSelector("#tab1 > form > fieldset > div.row > div > div:nth-child(3) > div.js-password-hideandshow > label > div");

	public By JOIN_NATIONAL_TRUST_LINK = By.cssSelector(".small-12.columns>form>p>a");

	public By SEND_ACTIVATION_LINK_BUTTON = By.cssSelector("#resendActivationSubmit");

	public By ACTIVATION_EMAIL_CONFIRMATION_HEADER_MESSAGE = By.cssSelector("#mainContent > section > div > main > div > h1");

	public By ACTIVATION_EMAIL_CONFIRMATION_MESSAGE = By.cssSelector("#mainContent > section > div > main > div > div > p:nth-child(1)");

	private String ACTIVATION_EMAIL_ALREADY_SENT_MESSAGE = "//*[@class=\"c-notice c-notice__warning\"]";

	public By REGISTRATION_EMAIL_TEXT_FILED = By.cssSelector("#regEmail");

	public By REGISTRATION_PASSWORD_TEXT_FILED = By.cssSelector("#regPassword");

	public By REGISTER_BUTTON = By.cssSelector("#regSubmit");

	public By WEEK_PASSWORD_MESSAGE = By.cssSelector("ul[id*='parsley-id'] > li[class*='parsley-pattern']");

	public By EMAIL_ALREADY_EXIST_LABLE = By.cssSelector("#tab1 > form > fieldset > div > div > p");

	public By JOIN_NATIONAL_TRUST_LABLE = By.cssSelector(".small-12.columns>form>p>a");

	public By REGISTER_OR_SIGNIN_TEXT = By.cssSelector("#mainContent > section > div.row.content-group.nt-palette-primary > div:nth-child(3) > h3");

	protected WebDriver driver;

	SupporterServiceDBConnection dbConnection = new SupporterServiceDBConnection();

	public RegistrationPage(WebDriver dr) {
		this.driver = dr;
	}

	private void navigateToRegistrationPage() throws Exception {
		//		timeCheck();
		setLogs("Navigating to Registration page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURLMYNT() + "register");
	}

	private void navigateToRegistrationPageFromDirectMarketing() throws Exception {
		//		timeCheck();
		setLogs("Navigating to Registration page");
		driver.navigate().to(EnvironmentConfiguration.getMarketingURL());
	}

	// the reason for this method is due to the Network causing a bad health on the hour every hour at 35 minutes past the hour. So this add a delay if the test tries to run between 34 and 37 minutes past
	public void timeCheck() throws Exception {
		String[] timeNow = Helpers.getTimeNow().split(":");
		if (timeNow[1].contains("04") || timeNow[1].contains("05") || timeNow[1].contains("06") || timeNow[1].contains("07") || timeNow[1].contains("33") || timeNow[1].contains("34") || timeNow[1]
				.contains("35") || timeNow[1].contains("36")) {
			while (!Helpers.getTimeNow().contains(":37")) {
				Thread.sleep(2000);
			}
		}
	}

	public String enterEmailIdAndPasswordToRegisterNewUser(String emailId, String password) throws Exception {
		navigateToRegistrationPage();
		Helpers.waitForElementToAppear(driver, REGISTRATION_EMAIL_TEXT_FILED, 10, "Registration email field not displayed after 10 second wait");
		Helpers.clearMYNTCookieMessage();
		setLogs("Entering the email id -- " + emailId);
		Helpers.clearAndSetText(driver, REGISTRATION_EMAIL_TEXT_FILED, emailId);
		String getEmailFromForm = driver.findElement(registrationPage().REGISTRATION_EMAIL_TEXT_FILED).getAttribute("value");
		setLogs("Entering the passowrd");
		Helpers.clearAndSetText(driver, REGISTRATION_PASSWORD_TEXT_FILED, password);
		setLogs("Clicking the register button");
		Thread.sleep(100);
		//		driver .findElement(By.cssSelector("#cookie-statement-cls-btn > span")).click();
		driver.findElement(REGISTER_BUTTON).click();
		Thread.sleep(3000);
		String registrationUrl = getTheRegistrationUrl(emailId);
		setLogs("Navigating to activation url -- " + registrationUrl);
		return registrationUrl;

	}

	public String enterEmailIdAndPasswordToRegisterNewUser1(String emailId, String password) throws Exception {
		navigateToRegistrationPage();
		Helpers.waitForElementToAppear(driver, REGISTRATION_EMAIL_TEXT_FILED, 10, "Registration email field not displayed after 10 second wait");
		setLogs("Entering the email id -- " + emailId);
		Helpers.clearAndSetText(driver, REGISTRATION_EMAIL_TEXT_FILED, emailId);
		String getEmailFromForm = driver.findElement(registrationPage().REGISTRATION_EMAIL_TEXT_FILED).getAttribute("value");
		setLogs("Entering the passowrd");
		Helpers.clearAndSetText(driver, REGISTRATION_PASSWORD_TEXT_FILED, password);
		setLogs("Clicking the register button");
		Thread.sleep(100);
		//		driver .findElement(By.cssSelector("#cookie-statement-cls-btn > span")).click();
		driver.findElement(REGISTER_BUTTON).click();
		Thread.sleep(3000);
		String registrationUrl = getTheRegistrationUrl(emailId);
		setLogs("Navigating to activation url -- " + registrationUrl);
		return registrationUrl;

	}

	public String enterEmailIdAndPasswordToRegister(String emailId, String password) throws Exception {
		navigateToRegistrationPage();
		Helpers.waitForElementToAppear(driver, REGISTRATION_EMAIL_TEXT_FILED, 10, "Registration email field not displayed after 10 second wait");
		setLogs("Entering the email id -- " + emailId);
		Helpers.clearAndSetText(driver, REGISTRATION_EMAIL_TEXT_FILED, emailId);
		String getEmailFromForm = driver.findElement(registrationPage().REGISTRATION_EMAIL_TEXT_FILED).getAttribute("value");
		setLogs("Entering the passowrd");
		Helpers.clearAndSetText(driver, REGISTRATION_PASSWORD_TEXT_FILED, password);
		setLogs("Clicking the register button");
		driver.findElement(REGISTER_BUTTON).click();
		Thread.sleep(3000);
		String registrationUrl = getTheRegistrationUrl(emailId);
		setLogs("Navigating to activation url -- " + registrationUrl);
		return registrationUrl;

	}

	public String enterEmailIdAndPasswordToRegisterNewUserFromMarketingPage(String emailId, String password) throws Exception {
		navigateToRegistrationPageFromDirectMarketing();
		Helpers.waitForElementToAppear(driver, REGISTRATION_EMAIL_TEXT_FILED, 10, "Registration email field not displayed after 10 second wait");
		setLogs("Entering the email id -- " + emailId);
		Helpers.clearAndSetText(driver, REGISTRATION_EMAIL_TEXT_FILED, emailId);
		String getEmailFromForm = driver.findElement(registrationPage().REGISTRATION_EMAIL_TEXT_FILED).getAttribute("value");
		setLogs("Entering the passowrd");
		Helpers.clearAndSetText(driver, REGISTRATION_PASSWORD_TEXT_FILED, password);
		setLogs("Clicking the register button");
		driver.findElement(REGISTER_BUTTON).click();
		Thread.sleep(3000);
		String registrationUrl = getTheRegistrationUrl(emailId);
		setLogs("Navigating to activation url -- " + registrationUrl);
		return registrationUrl;

	}

	public void enterEmailIdAndWrongPasswordToRegisterNewUser(String emailId, String password) throws Exception {
		navigateToRegistrationPage();
		Helpers.waitForElementToAppear(driver, REGISTRATION_EMAIL_TEXT_FILED, 10, "Registration email field not displayed after 10 second wait");
		setLogs("Entering the email id -- " + emailId);
		Helpers.clearAndSetText(driver, REGISTRATION_EMAIL_TEXT_FILED, emailId);
		setLogs("Entering the passowrd");
		Helpers.clearAndSetText(driver, REGISTRATION_PASSWORD_TEXT_FILED, password);
		setLogs("Clicking the register button");
		driver.findElement(REGISTER_BUTTON).click();
	}

	public String getWarningMessageText() {
		return driver.findElement(EMAIL_ALREADY_EXIST_LABLE).getText();
	}

	public String getTheAccountId(String email, String columnLable) {
		String accountId = dbConnection.getData("select * from " + EnvironmentConfiguration.getText("account") + " where account_email=\'" + email + "\'", columnLable);
		return accountId;
	}

	private String getToken(String id, String columnLable) {
		String sqlStement = "select * from " + EnvironmentConfiguration.getText("activationToken") + " where account_id=\'" + id + "\'";
		log.info(sqlStement);
		String token = dbConnection.getData(sqlStement, columnLable);
		return token;
	}

	public String getTheRegistrationUrl(String emailId) {
		String accountId = getTheAccountId(emailId, "ID");
		String accountReference = getTheAccountId(emailId, "ACCOUNT_REFERENCE");
		String accountActivationTocken = getToken(accountId, "VALUE");
		String activationUrl = EnvironmentConfiguration.getActivationUrl() + accountReference + "/activate/" + accountActivationTocken;
		return activationUrl;
	}

	public String getAccountReferenceNumber(String reference) {
		String accountRef = dbConnection.getData("select * from " + EnvironmentConfiguration.getText("account") + " where SUPPORTER_NUMBER=\'" + reference + "\'", "ACCOUNT_REFERENCE");
		return accountRef;
	}

	public void enterPasswordInPasswordField() throws Exception {
		navigateToRegistrationPage();
		setLogs("Clicking in the password field");
		Helpers.clearAndSetText(driver, REGISTRATION_PASSWORD_TEXT_FILED, " ");
	}

	public String getPasswordCheckboxVisibility() {
		return driver.findElement(REGISTRATION_PAGE_PASSWORD_VIEW).getText();

	}

	public String getEmailAlreadyExistsText() {
		List<WebElement> emailExistsText = driver.findElements(EMAIL_ALREADY_EXIST_LABLE);
		if (emailExistsText.size() != 0) {
			String emailExists = driver.findElement(EMAIL_ALREADY_EXIST_LABLE).getText();
			return emailExists;

		}
		return null;
	}

	public String getActivationEmailAlreadySentText() {
		return Helpers.getText(driver, By.xpath(ACTIVATION_EMAIL_ALREADY_SENT_MESSAGE));
	}
}