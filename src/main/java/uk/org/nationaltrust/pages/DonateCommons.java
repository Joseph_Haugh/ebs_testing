package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.util.Strings;
import uk.org.nationaltrust.framework.CRMDBConnection;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by Sowjanya Annepu
 */
public abstract class DonateCommons extends PageBase {

    CRMDBConnection CRMDBConnection = new CRMDBConnection();

    public DonateCommons() {
        PageFactory.initElements(driver,this);
    }

    protected boolean status = false;

    public String pageContent=null;

    protected  String ASHDOWN_HOUSE_PROPERTY="Ashdown House";


    public By INDEX_SUBMIT_BUTTON = By.id("submitDonation");

    public By GET_ACCOUNT_NAME_FIELD = By.id("bankDetailsForm_accountName");

    public By GET_ACCOUNT_NUMBER_FIELD = By.cssSelector("#bankDetailsForm_accountNumber");

    public By GET_SORT_CODE_ONE_FIELD = By.cssSelector("#sortCode1");

    public By GET_SORT_CODE_TWO_FIELD = By.cssSelector("#sortCode2");

    public By GET_SORT_CODE_THREE_FIELD = By.cssSelector("#sortCode3");

    public By DONATION_TYPE_FIELD = By.id("donationType");

    public By DONATION_FORM_THANK_YOU_HEADER = By.xpath("//h1[contains(text(),'Your donation')]");

    public By GET_ONE_OFF_TAB = By.id("paymentTypeOneOffButton");

    public By GET_REGULAR_TAB = By.id("paymentTypeRegularButton");

    public By DONATION_APPEAL_CODE = By.id("appealCode");

    public By CAMPAIGN_SOURCE_CODE=By.id("sourceCode");

    public By DONATION_AMOUNT_DISPLAYED = By.cssSelector("#amount");

    @FindBy(id="oneOffOtherAmountPoundsAndPence")
    public WebElement PERSONAL_DETAILS_OTHER_AMOUNT;

    public By COMMMEMORATIVE_YES_RADIO=By.id("commemorativeYesButton");

    public By ERROR_SUMMARY_LIST=By.className("error-summary-list");

    public String SPECIAL_PLACES_APPEAL= "protect-special-places-appeal";

    @FindBy(id="commemorativeNoButton")
    public WebElement COMMMEMORATIVE_NO_RADIO;

    @FindBy(id="commemorationForm_relation")
    public WebElement COMMEMORATIVE_RELATION;

    @FindBy (id="relationOther")
	public WebElement COMMEMORATIVE_RELATION_OTHER;

    @FindBy(id="placeId")
    public WebElement PLACE_ID;

    @FindBy(id="privateListing")
    public WebElement IS_PRIVATE_LISTING;

    @FindBy(css="#title")
    public WebElement TITLE;

    @FindBy(css="#firstName")
    public WebElement FIRST_NAME;

    @FindBy(css="#lastName")
    public WebElement LAST_NAME;

    @FindBy(id="emailAddress")
    public WebElement EMAIL;

    @FindBy(id="postalAddress.country")
    public WebElement COUNTRY;

    @FindBy(xpath="//input[contains(@id, 'addressLine1')]")
    public WebElement ADDRESS_LINE1;

    @FindBy(xpath ="//input[contains(@id, 'addressLine2')]")
    public WebElement ADDRESS_LINE2;

    @FindBy(xpath = "//input[contains(@id, 'addressLine3')]")
    public WebElement ADDRESS_LINE3 ;

    @FindBy(xpath = "//input[contains(@id, 'addressLine4')]")
    public WebElement ADDRESS_LINE4;

    @FindBy(xpath="//input[contains(@id, 'city')]")
    public WebElement CITY;

    @FindBy(css="//input[contains(@id, 'county')]")
    public WebElement COUNTY ;

    @FindBy(xpath = "//input[contains(@id, 'postcode')]")
    public WebElement POSTCODE;

    @FindBy(css="#contactConsentForm.contactConsentedEmail")
    public WebElement EMAIL_CONSENT;

    @FindBy(css="#contactConsentForm.contactConsentedPost")
    public WebElement POST_CONSENT;

    @FindBy(css="#contactConsentForm.contactConsentedTelephone")
    public WebElement PHONE_CONSENT;

    @FindBy(css="#giftAidLabel")
    public WebElement GIFT_AID_CONSENT_CHECKBOX;

    @FindBy(id="commemorationForm_tone_IN_MEMORY")
    public WebElement PERSONAL_DETAILS_PAGE_COMM_TONE_IN_MEMORY;

    @FindBy(id="commemorationForm_subject")
    public WebElement PERSONAL_DETAILS_PAGE_SUBJECT;

    @FindBy(id="commemorationForm_message")
    public WebElement PERSONAL_DETAILS_PAGE_MESSAGE;

    @FindBy(id="sourceCode")
    public WebElement MAP_PIN_PAGE_CAMPAIGN_SOURCE_CODE;

    @FindBy(id="commemorationForm_celebrationReason")
    public WebElement PERSONAL_DETAILS_PAGE_CELEBRATION_REASON;


    public String FAIR_PROCESSING_NOTICE="We need your customer data so that we can process and manage your donation, and communicate with you along the way.";

    public String PERSONAL_DETAILS_HEADER_WITH_NO_APPEAL_TITLE ="Thank you for choosing to donate. We just need a few more details.";

    public void setAccountName(String accountName) {
        driver.findElement(GET_ACCOUNT_NAME_FIELD).sendKeys(accountName);
    }


    public void setAccountNumber(String accountNumber) {
        driver.findElement(GET_ACCOUNT_NUMBER_FIELD).sendKeys(accountNumber);
    }

    public void setSortCodeOne(String sortCode) {
        driver.findElement(GET_SORT_CODE_ONE_FIELD).sendKeys(sortCode);

    }
    public void setSortCodeTwo(String sortCode) {
        driver.findElement(GET_SORT_CODE_TWO_FIELD).sendKeys(sortCode);

    }

    public void setSortCodeThree(String sortCode) {
        driver.findElement(GET_SORT_CODE_THREE_FIELD).sendKeys(sortCode);

    }

    public void setDirectDebitDetails(String ddName, String ddNumber, String ddSortCode1, String ddSortCode2, String ddSortCode3) {
        setAccountName(ddName);
        setAccountNumber(ddNumber);
        setSortCodeOne(ddSortCode1);
        setSortCodeTwo(ddSortCode2);
        setSortCodeThree(ddSortCode3);
    }

    public void setDonationType(String donateType) {
        driver.findElement(DONATION_TYPE_FIELD).sendKeys(donateType);
    }

    public void setDonationAppealCode(String donateAppealCode) {
        driver.findElement(DONATION_APPEAL_CODE).sendKeys(donateAppealCode);
    }


    /*this method will pool CRM DB Log table until the status changed to complete for up to 10mins */
    public String poolCrmLogTableForNewDonationStatus(String donationID){
        String status = null;
        int count =0;
        try{
            while(Strings.isNullOrEmpty(status) || status.equalsIgnoreCase("INITIAL")  && count < 70){
                count = count + 1;
                setLogs("sleep for 15 sec.........");
                Thread.sleep(15000);
                status = CRMDBConnection.checkCRMlogTableForNewMembership(querys().sqlCheckLogTableInCrmDB()+"'"+donationID+"'");
                setLogs("status in crm log table = "+ status);
            }
        }catch (InterruptedException e){
            System.out.print("Exception error while pooling crm log table...."+ e.getMessage());
            return "fail";
        }
        if(Strings.isNullOrEmpty(status) || status == "fail"){
            System.out.print("Join application not appeared in CRM DB after 5 mins.... status in crm log table = "+ status);
            return "fail";
        }else{
            return status;}
    }


}