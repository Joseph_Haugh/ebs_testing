package uk.org.nationaltrust.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.framework.TestBase;
import uk.org.nationaltrust.apis.ApiRequests;

/**
 * @author Pramod
 */
public class PageBase extends TestBase {

	public static ApiRequests setAppTime = new ApiRequests();

	public NationalTrustHomePage ntHomePage() {
		PageFactory.initElements(driver, NationalTrustHomePage.class);
		return new NationalTrustHomePage(driver);
	}

	public JoinPage joinPage() {
		PageFactory.initElements(driver, JoinPage.class);
		return new JoinPage(driver);
	}

	public IndividualMembershipPage individualMembershipPage() {
		PageFactory.initElements(driver, IndividualMembershipPage.class);
		return new IndividualMembershipPage(driver);
	}

	public JointMembershipPage jointMembershipPage(){
		PageFactory.initElements(driver,JointMembershipPage.class);
		return new JointMembershipPage(driver);
	}

	public FamilyMembershipPage familyMembershipPage(){
	    PageFactory.initElements(driver,FamilyMembershipPage.class);
	    return new FamilyMembershipPage(driver);
    }

	public PaymentMethodPage paymentMethodPage() {
		PageFactory.initElements(driver, PaymentMethodPage.class);

		return new PaymentMethodPage(driver);
	}

	public DirectDebitPage directDebitPage() {
		PageFactory.initElements(driver, DirectDebitPage.class);
		return new DirectDebitPage(driver);
	}

	public SummaryScreenPage summaryPage() {
		PageFactory.initElements(driver, SummaryScreenPage.class);
		return new SummaryScreenPage(driver);
	}

	public ConfirmationPage confirmationPage() {
		PageFactory.initElements(driver, ConfirmationPage.class);
		return new ConfirmationPage(driver);
	}

	public AbandonedPage abandonedPage() {
		PageFactory.initElements(driver, AbandonedPage.class);
		return new AbandonedPage(driver);
	}
	public SquirrelMailPage squirellPage(){
		PageFactory.initElements (driver, SquirrelMailPage.class);
		return new SquirrelMailPage(driver);
	}
	public Querys querys(){
		PageFactory.initElements(driver, Querys.class);
		return new Querys(driver);
	}

	public BarclaysPage barclaysPage(){
		PageFactory.initElements(driver, BarclaysPage.class);
		return new BarclaysPage(driver);

	}
	public SessionTimeoutPage sessionTimeoutPage(){
		PageFactory.initElements(driver, SessionTimeoutPage.class);
		return new SessionTimeoutPage(driver);
	}

	public NationalTrustJoinUsPage joinUsPage (){
		PageFactory.initElements (driver, NationalTrustJoinUsPage.class);
		return new NationalTrustJoinUsPage(driver);
	}

	public SelfServePage selfServePage (){
		PageFactory.initElements (driver, NationalTrustJoinUsPage.class);
		return new SelfServePage(driver);
	}

	public DataLayerPage dataLayerPage (){
		PageFactory.initElements (driver, NationalTrustJoinUsPage.class);
		return new DataLayerPage(driver);
	}

	public static ApiRequests unLinkrequest = new ApiRequests();

	public SignInPage signInPage() {
		PageFactory.initElements(driver, SignInPage.class);
		return new SignInPage(driver);
	}

	public RegistrationPage registrationPage() {
		PageFactory.initElements(driver, RegistrationPage.class);
		return new RegistrationPage(driver);
	}

	public SignoutPage signoutPage() {
		PageFactory.initElements(driver, SignoutPage.class);
		return new SignoutPage(driver);
	}

	public RequestNewPasswordPage requestNewPasswordPage() {
		PageFactory.initElements(driver, RequestNewPasswordPage.class);
		return new RequestNewPasswordPage(driver);
	}

	public ResetPasswordPage resetPasswordPage() {
		PageFactory.initElements(driver, ResetPasswordPage.class);
		return new ResetPasswordPage(driver);
	}

	public DashBoardPage userPage() {
		PageFactory.initElements(driver, DashBoardPage.class);
		return new DashBoardPage(driver);
	}
	public RequestReplacementCardPage newCardPage(){
		PageFactory.initElements(driver, RequestReplacementCardPage.class);
		return new RequestReplacementCardPage(driver);
	}
	public RequestCarStickerPage newStickerPage(){
		PageFactory.initElements (driver, RequestCarStickerPage.class);
		return new RequestCarStickerPage(driver);
	}

	public MatchingSupporterDetailsPage supporterDetailsPage() {
		PageFactory.initElements(driver, DashBoardPage.class);
		return new MatchingSupporterDetailsPage(driver);

	}

	public MembershipDetailsPage membershipDetailsPage() {
		PageFactory.initElements(driver, MembershipDetailsPage.class);
		return new MembershipDetailsPage(driver);
	}

	public ContactPreferencesPage contactPreferencesDetailsPage() {
		PageFactory.initElements(driver, ContactPreferencesPage.class);
		return new ContactPreferencesPage(driver);
	}

	public EditPersonalDetailsPage editPersonalDetailsPage()
	{
		PageFactory.initElements(driver, EditPersonalDetailsPage.class);
		return new EditPersonalDetailsPage(driver);
	}

	public EditDirectDebitPaymentPage editDirectDebitPaymentPage(){
		PageFactory.initElements(driver, EditDirectDebitPaymentPage.class);
		return new EditDirectDebitPaymentPage(driver);
	}

	public AccountSettingsPage accountSettingsPage(){
		PageFactory.initElements(driver, AccountSettingsPage.class);
		return new AccountSettingsPage(driver);
	}

	public AddDirectDebitPage addDirectDebit (){
		PageFactory.initElements (driver, AddDirectDebitPage.class);
		return new AddDirectDebitPage(driver);
	}

	public CPCTokenPage cpcPage(){
		PageFactory.initElements(driver, Querys.class);
		return new CPCTokenPage (driver);
	}

	public CPCUserPage cpcUserPage(){
		PageFactory.initElements(driver, Querys.class);
		return new CPCUserPage (driver);
	}

	public CPCDMUserPage cpcDMUserPage(){
		PageFactory.initElements(driver, Querys.class);
		return new CPCDMUserPage (driver);
	}

	public CPCShopUserPage cpcShopUserPage(){
		PageFactory.initElements(driver, CPCShopUserPage.class);
		return new CPCShopUserPage(driver);
	}

	public ShopPage shopPage (){
		PageFactory.initElements(driver, ShopPage.class);
		return new ShopPage(driver);
	}
	public MatchingSupporterDetailsPage matchingPage() {
		PageFactory.initElements(driver, MatchingSupporterDetailsPage.class);
		return new MatchingSupporterDetailsPage(driver);
	}

	public MembershipsPage membershipsPage() {
		PageFactory.initElements(driver, MembershipsPage.class);
		return new MembershipsPage(driver);

	}

	public ContactDetailsSupporterPage contactPage() {
		PageFactory.initElements(driver, ContactDetailsSupporterPage.class);
		return new ContactDetailsSupporterPage(driver);
	}

	public PaymentOptionsPage paymentOptionPage() {
		PageFactory.initElements(driver, PaymentOptionsPage.class);
		return new PaymentOptionsPage(driver);

	}

	public RenewalConfirmationPage renewconfirmationPage() {
		PageFactory.initElements(driver, RenewalConfirmationPage.class);
		return new RenewalConfirmationPage(driver);
	}

	public BarclaycardPage cardPage() {
		PageFactory.initElements(driver, BarclaycardPage.class);
		return new BarclaycardPage(driver);
	}

	public DonatePersonalDetailsForm donateForm (){
		PageFactory.initElements (driver, DonatePersonalDetailsForm.class);
		return new DonatePersonalDetailsForm(driver);
	}

	public AppealsPage appealPagePage() {
		PageFactory.initElements(driver, AppealsPage.class);
		return new AppealsPage(driver);
	}

	public AppealDonatePage appealDonatePage(){
		PageFactory.initElements (driver, AppealDonatePage.class);
		return new AppealDonatePage(driver);
	}

	public DonateConfirmationPage donateconfirmationPage(){
		PageFactory.initElements(driver, DonateConfirmationPage.class);
		return new DonateConfirmationPage(driver);
	}
	public DonatePage donatePage (){
		PageFactory.initElements(driver, DonatePage.class);
		return new DonatePage(driver);
	}

	public SideNavPage sideNavPage (){
		PageFactory.initElements(driver, SideNavPage.class);
		return new SideNavPage(driver);
	}

	public FundraisingFormPage fundraisingFormPage (){
		PageFactory.initElements(driver, FundraisingFormPage.class);
		return new FundraisingFormPage(driver);
	}

	public ModerationPage moderationPage(){
		PageFactory.initElements(driver, ModerationPage.class);
		return new ModerationPage(driver);
	}

	public DonateAddMapPinPage donateAddMapPinPage(){
		PageFactory.initElements(driver, DonateAddMapPinPage.class);
		return new DonateAddMapPinPage(driver);
	}

	public DonateMapPinStep1Page donateMapPinStep1Page(){
		PageFactory.initElements(driver, DonateMapPinStep1Page.class);
		return new DonateMapPinStep1Page(driver);
	}

	public DonateMapPinStep2Page  donateMapPinStep2Page(){
		PageFactory.initElements(driver, DonateMapPinStep2Page.class);
		return new DonateMapPinStep2Page(driver);
	}

	public DonateMapPinAddImagePage donateMapPinAddImagePage(){
		PageFactory.initElements(driver, DonateMapPinAddImagePage.class);
		return new DonateMapPinAddImagePage(driver);
	}

	public DonateMapPinSummaryPage donateMapPinSummaryPage(){
		PageFactory.initElements(driver, DonateMapPinSummaryPage.class);
		return new DonateMapPinSummaryPage(driver);
	}

	public HeadersAndFooters headersAndFooters(){
		PageFactory.initElements(driver, HeadersAndFooters.class);
		return new HeadersAndFooters(driver);
	}



}
