package uk.org.nationaltrust.pages;

import java.util.List;
import java.util.UUID;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by nick.thompson on 16/02/2018.
 */
public class CPCDMUserPage extends PageBase {

	protected WebDriver driver;

	public CPCDMUserPage(WebDriver dr) {
		this.driver = dr;
	}

	public By GET_CONTACT_BY_PHONE_YES_CHECKBOX = By.id("contactConsentedTelephone-YesLabel");

	public By GET_CONTACT_BY_PHONE_NO_CHECKBOX = By.id("contactConsentedTelephone-NoLabel");

	public By GET_CONTACT_BY_EMAIL_YES_CHECKBOX = By.id("contactConsentedEmail-YesLabel");

	public By GET_CONTACT_BY_EMAIL_NO_CHECKBOX = By.id("contactConsentedEmail-NoLabel");

	public By GET_CONTACT_BY_POST_YES_CHECKBOX = By.id("contactConsentedPost-YesLabel");

	public By GET_CONTACT_BY_POST_NO_CHECKBOX = By.id("contactConsentedPost-NoLabel");

	public By SUBMIT_CONTACT_PREFERENCES_BUTTON = By.id("submitContactPermissions");

	public static final By CONFIRMATION_MAIN_HEADER = By.cssSelector("#mainContent > section > div:nth-child(1) > div > header > h1");

	public static final By THANK_YOU_MESSAGE = By.cssSelector("#mainContent > section > div:nth-child(2) > div > p:nth-child(1) > b");

	public By GET_SUPPORTER_NUMBER_FIELD = By.id("supporterNumber");

	public By GET_POSTCODE_FIELD = By.id("postcode");

	public By GET_EMAIL_FIELD = By.id("emailAddress");

	public static final By TEMPT_OTHER_MEMBERS_HEADER_TEXT = By.cssSelector("#mainContent > section > div:nth-child(2) > div > div.info-panel.panel__readonly > h4");

	private static final By GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT = By.id("contactPref-errorEmail");

	private static final By GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT = By.id("contactPref-errorPost");

	private static final By GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT = By.id("contactPref-errorTelephone");

	private static final By GET_INVALID_SUPPORTER_NUMBER_ERROR_TEXT = By.id("supporterNumber-error");

	private static final By GET_INVALID_POSTCODE_ERROR_TEXT = By.id("postcode-error");

	private static final By GET_INVALID_EMAIL_ERROR_TEXT = By.id("emailAddress-error");

	private static final By GET_INVALID_SUPPORTER_POSTCODE_COMBINATION_ERROR_TEXT = By.cssSelector("#supporter-validation-error-message > div > p");

	private static final By GET_NEW_CONTACT_PREF_INFO_TEXT = By.id("js-permissionStatementContainer");

	private static final By CONTACT_PREFERENCE_HEADER_LABEL = By.cssSelector("#permissionsForm > div:nth-child(1) > div > header > h1");

	public By MY_NT_SIGN_IN_LINK = By.cssSelector("#yesMyNT > p > a");
	public By MY_NT_SIGN_IN_LINK_TEXT = By.cssSelector("#yesMyNT > p");

	public By MY_NT_REGISTRATION_LINK = By.cssSelector("#noMyNT > p > a");

	public By MY_NT_REGISTRATION_LINK_TEXT = By.cssSelector("#noMyNT > p");

	public String getConfirmationHeaderDisplayed() {
		List<WebElement> confirmationHeaderText = driver.findElements(CONFIRMATION_MAIN_HEADER);
		if (confirmationHeaderText.size() != 0) {
			String confirmationHeader = driver.findElement(CONFIRMATION_MAIN_HEADER).getText();
			return confirmationHeader;
		}
		return null;
	}

	public String getThankYouTextDisplayed() {
		List<WebElement> thankYou = driver.findElements(THANK_YOU_MESSAGE);
		if (thankYou.size() != 0) {
			String thankYouText = driver.findElement(THANK_YOU_MESSAGE).getText();
			return thankYouText;
		}
		return null;
	}

	public List<WebElement> getMarketingCookie() {
		List<WebElement> cookie = (List<WebElement>) driver.manage().getCookieNamed("marketingPreferencesUpdated");
		if (cookie != null) {
			return cookie;
		}
		return null;
	}

	public void setSupporterNumber(String accountName) {
		driver.findElement(GET_SUPPORTER_NUMBER_FIELD).sendKeys(accountName);
	}

	public void setPostCode(String postCode) {
		driver.findElement(GET_POSTCODE_FIELD).sendKeys(postCode);
	}

	public void setEmailAddress(String emailAddress) {
		driver.findElement(GET_EMAIL_FIELD).sendKeys(emailAddress);
	}

	public void setSupporterDetails(String supporter, String postcode) {
		setSupporterNumber(supporter);
		setPostCode(postcode);

	}

	public String getContactPrefForEmailNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPostNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPhoneNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getInvalidSupporterNumberErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_INVALID_SUPPORTER_NUMBER_ERROR_TEXT, 10, "Bad supporterNumber error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_INVALID_SUPPORTER_NUMBER_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String badSupporterErrorTextDisplayed = driver.findElement(GET_INVALID_SUPPORTER_NUMBER_ERROR_TEXT).getText();

			return badSupporterErrorTextDisplayed;
		}
		return null;

	}

	public String getInvalidPostcodeErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_INVALID_POSTCODE_ERROR_TEXT, 10, "Bad Postcode error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_INVALID_POSTCODE_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String badPostcodeErrorTextDisplayed = driver.findElement(GET_INVALID_POSTCODE_ERROR_TEXT).getText();

			return badPostcodeErrorTextDisplayed;
		}
		return null;

	}

	public String getInvalidEmailErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_INVALID_EMAIL_ERROR_TEXT, 10, "Bad Email error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_INVALID_EMAIL_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String badEmailErrorTextDisplayed = driver.findElement(GET_INVALID_EMAIL_ERROR_TEXT).getText();

			return badEmailErrorTextDisplayed;
		}
		return null;

	}

	public String getInvalidSupporterPostcodeCombinationError() {
		Helpers.waitForElementToAppear(driver, GET_INVALID_SUPPORTER_POSTCODE_COMBINATION_ERROR_TEXT, 10, "Invalid Supporter number and Postcode error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_INVALID_SUPPORTER_POSTCODE_COMBINATION_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String supporterPostcodeErrorTextDisplayed = driver.findElement(GET_INVALID_SUPPORTER_POSTCODE_COMBINATION_ERROR_TEXT).getText();

			return supporterPostcodeErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactInfoText = driver.findElements(GET_NEW_CONTACT_PREF_INFO_TEXT);
		if (contactInfoText.size() != 0) {
			String contactInfoTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREF_INFO_TEXT).getText();

			return contactInfoTextDisplayed;
		}
		return null;

	}

	public String getContactPreferenceHeaderLableText() {
		return driver.findElement(CONTACT_PREFERENCE_HEADER_LABEL).getText();
	}

	public String setTheEmailOfSupporter() {
		List<WebElement> supporterEmail = driver.findElements(GET_EMAIL_FIELD);
		if (supporterEmail.size() == 0) {
			setLogs("Entering the emailId for the supporter - ");
		} else {
			Helpers.clearAndSetText(driver, GET_EMAIL_FIELD, (UUID.randomUUID()) + "@gmail.com");
			String getEmailFromForm = driver.findElement(cpcDMUserPage().GET_EMAIL_FIELD).getAttribute("value");
			return getEmailFromForm;
		}
		return null;

	}

	public String getTemptOtherMembersText() {
		Helpers.waitForElementToAppear(driver, TEMPT_OTHER_MEMBERS_HEADER_TEXT, 10, "Tempt other members to submit prefs text not displayed");
		List<WebElement> temptMembersText = driver.findElements(TEMPT_OTHER_MEMBERS_HEADER_TEXT);
		if (temptMembersText.size() != 0) {
			String temptMembersTextDisplayed = driver.findElement(TEMPT_OTHER_MEMBERS_HEADER_TEXT).getText();

			return temptMembersTextDisplayed;
		}
		return null;

	}

	public String getMYNTSignInLinkText() {
		return driver.findElement (MY_NT_SIGN_IN_LINK_TEXT).getText ();
	}

	public String getMYNTRegistrationLinkText() {
		return driver.findElement (MY_NT_REGISTRATION_LINK_TEXT).getText ();
	}
}