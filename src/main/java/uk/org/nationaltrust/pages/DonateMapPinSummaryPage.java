package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import uk.org.nationaltrust.apis.CreateDonateFormRequest;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by Sowjanya Annepu
 */
public class DonateMapPinSummaryPage extends CommemorativeMapPinDonateSelectors {

    public DonateMapPinSummaryPage(WebDriver driver){
        this.driver= driver;
        pageDefaultText();
    }

    public DonateMapPinSummaryPage viewSummary(){

        return this;
    }

    public DonatePage submitPinDetailsToStartPersonalDetailsForm(){
        Helpers.waitHandlingException(2000);
        STEP4_CONFIRM_AND_DONATE_BUTTON.click();
        return new DonatePage(driver);
    }

    public DonateMapPinAddImagePage backButton() {
        PREVIOUS_BUTTON.click(); // TODO ALL PREVIOUS /BACK BUTTONS SHOULD BE NAMED CONSISTENTLY THROUGH OUT THE JOURNEY
        return new DonateMapPinAddImagePage(driver);
    }

    public void pageDefaultText(){
        STEP4_HELPTEXT.getText().contains("Check you are happy before proceeding to making your donation");
    }

    public DonateMapPinSummaryPage verifyPreview(CreateDonateFormRequest createDonateFormRequest){
        Helpers.waitHandlingException(1000);
        String subjectText= createDonateFormRequest.getCommemorativeSubject();
        String story= createDonateFormRequest.getDonationStory();
        String relation=createDonateFormRequest.getCommemorativeRelation();
        driver.findElement(By.xpath("//h3/span[contains(text(),'"+subjectText+"')]")).isDisplayed();
        Helpers.waitHandlingException(1000);
        driver.findElement(By.xpath("//h4/span[contains(text(),'"+relation+"')]")).isDisplayed();
        Helpers.waitHandlingException(1000);
        driver.findElement(By.xpath("//pre[contains(text(),'"+story+"')]")).isDisplayed();
        return this;

    }
}
