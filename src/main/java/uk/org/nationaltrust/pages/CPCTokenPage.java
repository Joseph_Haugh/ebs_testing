package uk.org.nationaltrust.pages;

import static com.auth0.jwt.algorithms.Algorithm.HMAC256;
import static java.net.URLEncoder.encode;
import static java.time.LocalDateTime.now;

import java.io.UnsupportedEncodingException;
import java.time.ZoneOffset;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import uk.org.nationaltrust.framework.TestBase;

/**
 * Created by nick.thompson on 16/02/2018.
 */
public class CPCTokenPage extends TestBase {

	protected WebDriver driver;
	private static final String SUPPORTER_NUMBER_CLAIM = "supporterNumber";

	private static final String MASKED_EMAIL_CLAIM = "maskedEmail";

	private static final String CAMPAIGN_REFERENCE_CLAIM = "campaignReference";

	private final String secret = "secret";

	private String givenToken;

	private static String urlEncodeToken(String token) {
		try {
			return encode(token, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Unsupported encoding");
		}
	}


//	@Test
//
//	public void createToken (){
//		String getToken = createToken();
//	}

	public void reset() {
		// no op because it is glue scope
	}

	public String createToken(String supporterNumber, String emailAddress) throws Exception {
		if (givenToken != null) {
			throw new IllegalStateException("given token already generated!");
		}

		givenToken = urlEncodeToken(JWT.create()
				.withIssuedAt(currentDate())
				.withClaim(SUPPORTER_NUMBER_CLAIM, supporterNumber)
				.withClaim(MASKED_EMAIL_CLAIM, emailAddress)
				.withClaim(CAMPAIGN_REFERENCE_CLAIM, "ABC")
				.withExpiresAt(futureDate())
				.sign(algorithm()));

		return givenToken;

	}

	public String createExpiredToken(String supporterNumber, String emailAddress) {
		if (givenToken != null) {
			throw new IllegalStateException("given token already generated!");
		}
		this.givenToken = JWT.create()
				.withClaim("supporterNumber", supporterNumber)
				.withClaim("maskedEmail", emailAddress)
				.withIssuedAt(currentDate())
				.withExpiresAt(expiredDate())
				.withClaim("campaignReference", "dev test token controller ")
				.sign(algorithm());
		return givenToken;
	}

	public String createTokenWIthNoExpireyDate(String supporterNumber, String emailAddress) {
		if (givenToken != null) {
			throw new IllegalStateException("given token already generated!");
		}
		this.givenToken = JWT.create()
				.withClaim("supporterNumber", supporterNumber)
				.withClaim("maskedEmail", emailAddress)
				.withIssuedAt(currentDate())
//				.withExpiresAt(expiredDate())
				.withClaim("campaignReference", "dev test token controller ")
				.sign(algorithm());
		return givenToken;
	}

	public String createTokenForShopPURL(String supporterNumber, String emailAddress, String consent){
		if (givenToken !=null){
			throw new IllegalStateException("given token already generated");
		}
		this.givenToken = JWT.create()
				.withClaim("supporterNumber", supporterNumber)
				.withClaim("email", emailAddress)
				.withClaim("hasEmailPermission", Boolean.valueOf(consent))
				.withIssuedAt(currentDate())
				.withExpiresAt(futureDate())
//				.withClaim("campaignReference", "dev test token controller")
				.sign(algorithm());
		return givenToken;
	}

	public String createExpiredTokenForShopPURL(String supporterNumber, String emailAddress, String consent){
		if (givenToken !=null){
			throw new IllegalStateException("given token already generated");
		}
		this.givenToken = JWT.create()
				.withClaim("supporterNumber", supporterNumber)
				.withClaim("email", emailAddress)
				.withClaim("hasEmailPermission", Boolean.valueOf(consent))
				.withIssuedAt(currentDate())
				.withExpiresAt(expiredDate())
				//				.withClaim("campaignReference", "dev test token controller")
				.sign(algorithm());
		return givenToken;
	}

	public String getGivenToken() {
		return givenToken;
	}

	private Algorithm algorithm() {
		Algorithm algorithm;
		try {
			algorithm = HMAC256(this.secret);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Failed to create algorithm for test jwt token", e);
		}
		return algorithm;
	}

	private Date currentDate() {
		return Date.from(now().minusDays(2).toInstant(ZoneOffset.UTC));
	}

	private Date futureDate() {
		return Date.from(now().plusMonths(6).toInstant(ZoneOffset.UTC));
	}

	private Date expiredDate() {
		return Date.from(now().minusDays(1).toInstant(ZoneOffset.UTC));
	}

	public CPCTokenPage (WebDriver dr) {
		this.driver = dr;
	}
}