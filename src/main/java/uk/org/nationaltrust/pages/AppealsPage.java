package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * Created by nick.thompson on 21/02/2017.
 */
public class AppealsPage extends PageBase {

	protected WebDriver driver;

	public AppealsPage(WebDriver dr) {
		this.driver = dr;
	}

	public By DONATION_PAGE_LINK = By.cssSelector(EnvironmentConfiguration.getText("teaser"));
//	public By DONATION_PAGE_BUTTON = By.xpath("//*[@id='main-content']/div[17]/div/div/div/div/a");
	//*[@id="main-content"]/div[17]/div/div/div/div/a

	public By DONATION_PAGE_BUTTON = By.xpath("//a[@class='button round nt-button']");


	public static final By APPEAL_PAGE_HEADER =By.cssSelector("#main-content > div.nt-header-module-houses-buildings.nt-palette-primary > div.row.nt-anim-header-fadein > div > div > h1") ;


public String getAppealLink (){
	Helpers.waitForIsClickable(driver, DONATION_PAGE_LINK, 15);
	List<WebElement> appealLink = driver.findElements(DONATION_PAGE_LINK);
	if(appealLink.size()!=0){
		driver.findElement(DONATION_PAGE_LINK).click();
	}
	return null;
}

public String getAppealButton (){
	Helpers.waitForIsClickable(driver, DONATION_PAGE_BUTTON, 15);
	List<WebElement> appealButton = driver.findElements(DONATION_PAGE_BUTTON);
	if(appealButton.size()!=0){
		driver.findElement(DONATION_PAGE_BUTTON).click();
	}
	return null;
}

	public String getMainAppealPageHeaderTextDisplayed() {
		Helpers.waitForElementToAppear(driver, APPEAL_PAGE_HEADER, 10, "Appeal Page Header text not displayed");
		List<WebElement> appealHeaderText = driver.findElements(APPEAL_PAGE_HEADER);
		if (appealHeaderText.size() != 0) {
			String appealHeaderTextDisplayed = driver.findElement(APPEAL_PAGE_HEADER).getText();

			return appealHeaderTextDisplayed;
		}
		return null;

	}

}


