package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.framework.SupporterServiceDBConnection;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by nick.thompson on 18/11/2016.
 */
public class RenewalConfirmationPage extends PageBase {

	protected WebDriver driver;

	public RenewalConfirmationPage(WebDriver dr) {
		this.driver = dr;
	}

	SupporterServiceDBConnection SupporterDBConnection = new SupporterServiceDBConnection();

	public static By RENEWAL_CONFIRMATION_THANK_YOU_TEXT = By.cssSelector("#mainHeading");

	public static By YOUR_MEMBERSHIP_HAS_BEEN_RENEWED_TEXT = By.cssSelector("body > main > section > div.row > div.small-12.medium-7.columns > h3:nth-child(3)");

	public By REGISTER_FOR_MY_NT_BUTTON = By.cssSelector("#mainContent > section > div.row > div.small-12.medium-5.columns.right > div > div.contentPane--teaser__content > a");

	public static By REGISTER_FOR_MY_NT_TEXT = By.cssSelector("#registerWithMyNtHeader > h3");

	public static By NEW_CARD_ARRIVAL_TEXT = By.cssSelector("#mainContent > section > div.row > div.small-12.medium-7.columns > p:nth-child(5)");

	public static By MY_NT_REGISTER_TAB = By.cssSelector("#mainContent > section > div.row.content-group.nt-palette-primary > div.medium-6.columns.right > ul > li.tab-link.active > a");

	public String getRenewalConfirmationThankYouText() {
		Helpers.waitForIsClickable(driver, RENEWAL_CONFIRMATION_THANK_YOU_TEXT, 10);
		List<WebElement> renewalThankYouConfirmation = driver.findElements(RENEWAL_CONFIRMATION_THANK_YOU_TEXT);
		if (renewalThankYouConfirmation.size() != 0) {
			String renewalThankYouConfirmationText = driver.findElement(RENEWAL_CONFIRMATION_THANK_YOU_TEXT).getText();
			return renewalThankYouConfirmationText;
		}
		return null;
	}

	public String getRenewMembershipHasBeenRenewedText() {
		Helpers.waitForIsClickable(driver, YOUR_MEMBERSHIP_HAS_BEEN_RENEWED_TEXT, 10);
		List<WebElement> renewalMembershipRenewedText = driver.findElements(YOUR_MEMBERSHIP_HAS_BEEN_RENEWED_TEXT);
		if (renewalMembershipRenewedText.size() != 0) {
			String renewalMembershipRenewed = driver.findElement(YOUR_MEMBERSHIP_HAS_BEEN_RENEWED_TEXT).getText();
			return renewalMembershipRenewed;
		}
		return null;
	}

	public String getRegisterForMyNTText (){
		List<WebElement> registerForMyNTText = driver.findElements(REGISTER_FOR_MY_NT_TEXT);
		if (registerForMyNTText.size()!=0){
			String registerForNT = driver.findElement(REGISTER_FOR_MY_NT_TEXT).getText();
			return registerForNT;
		}
		return null;

	}
	public String getMyNTRegisterTabText (){
		List<WebElement> registerTabForMyNTText = driver.findElements(MY_NT_REGISTER_TAB);
		if (registerTabForMyNTText.size()!=0){
			String registerTab = driver.findElement(MY_NT_REGISTER_TAB).getText();
			return registerTab;
		}
		return null;

	}

	//	public String getTheRenewalStatus(String columnLable) {
	//		String renewStatus = dbConnection.getData("select * from " + EnvironmentConfiguration.getText("membership_Renewal") + " where confirmation_email_address=\''", columnLable);
	//		return renewStatus;
	//	}

	public Map<String, String> getTheRenewalDetailsForSingleIndividualMembInRenewalFromSupporterBD(Set<String> labelNames) {
		String sqlSta = membershipRenewedInDBSql("331896874");
		setLogs(sqlSta);
		return SupporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}


	private String membershipRenewedInDBSql(String supporterNumber) {
		System.out.println( "Hello World!" );
		return String.format("select SUPPORTER_NUMBER, SUBMISSION_STATUS, INCENTIVE_SOURCE_CODE FROM" + " SUPPORTERAPI_OWNER.MEMBERSHIP_RENEWAL" + " WHERE SUPPORTER_NUMBER = '%s'" + " order by CREATED desc" ,supporterNumber);

	}

	public Map<String, String> getTheRenewalDetailsForRenewalPrePostMarchFromSupporterBD(String usedSupporterNumber, Set<String> labelNames) {
		String sqlSta = membershipRenewedPreMarchInDBSql(usedSupporterNumber);
		setLogs(sqlSta);
		return SupporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}


	private String membershipRenewedPreMarchInDBSql(String usedSupporterNumber) {
		System.out.println( "Hello World!" );
		return String.format("select SUPPORTER_NUMBER, SUBMISSION_STATUS, INCENTIVE_SOURCE_CODE FROM" + " SUPPORTERAPI_OWNER.MEMBERSHIP_RENEWAL" + " WHERE SUPPORTER_NUMBER = '%s'" + " order by CREATED desc" ,usedSupporterNumber);

	}

//	private String membershipRenewedInDBSql() {
//		System.out.println("Hello World!");
//		return "select supporter_number, submission_status FROM"
//				+ " SUPPORTERAPI_OWNER.MEMBERSHIP_RENEWAL"
//				+ " WHERE supporter_number = '256666753'"
//				+ " AND rownum = 1"
//				+ " order BY created desc ";
//	}

	private void getThankYouRenewConfirmationStandardText() {
		renewconfirmationPage().getRenewalConfirmationThankYouText();
		String thankYou = renewconfirmationPage().getRenewalConfirmationThankYouText();
		Assert.assertTrue(thankYou.contains("Thank you"),"Wrong Thank you text displayed");
	}

	private void getMembershipRenewedConfirmationText() {
		renewconfirmationPage().getRenewMembershipHasBeenRenewedText();
		String membershipRenewed = renewconfirmationPage().getRenewMembershipHasBeenRenewedText();
		Assert.assertEquals(membershipRenewed, "Your membership has been renewed.", "Wrong Membership Renewed Text Displayed");
	}

	public void deleteRenewedRecord(){
		setLogs("Delete record before Renew Supporter submitter sends the transaction to update in CRM so we have some test records in renewal state");
		String sqlSta = "Delete FROM" + " SUPPORTERAPI_OWNER.MEMBERSHIP_RENEWAL where" + " Created >= sysdate -5" ;
		setLogs(sqlSta);
		SupporterDBConnection.setData(sqlSta);
	}

	public void getAllRenewConfirmationStandardText() {
		getThankYouRenewConfirmationStandardText();
		getMembershipRenewedConfirmationText();
	}

}
