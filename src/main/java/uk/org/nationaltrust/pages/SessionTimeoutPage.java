package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by nick.thompson on 26/05/2017.
 */
public class SessionTimeoutPage extends PageBase {




	protected WebDriver driver;


	public By START_AGAIN_BUTTON =By.cssSelector("#main-content > section > div.row > div > div:nth-child(2) > a");

	public By SESSION_TIMEOUT_HEADER = By.cssSelector("#main-content > section > div.row > div > div.panel.large-12 > h1");


	public String getSessionTimeoutHeaderTextDisplayed() {
		Helpers.waitForElementToAppear(driver, SESSION_TIMEOUT_HEADER, 10, "Session Timeout header text not displayed");
		List<WebElement> sessionTimeoutHeader = driver.findElements(SESSION_TIMEOUT_HEADER);
		if (sessionTimeoutHeader.size() != 0) {
			String sessionTimeoutHeaderTextDisplayed = driver.findElement(SESSION_TIMEOUT_HEADER).getText();

			return sessionTimeoutHeaderTextDisplayed;
		}
		return null;

	}

	public String clickStartAgainButton() {
		List<WebElement> startAgainButton = driver.findElements(START_AGAIN_BUTTON);
		if (startAgainButton.size() != 0) {
			driver.findElement(START_AGAIN_BUTTON).click();

		}
		return null;
	}

	public SessionTimeoutPage (WebDriver dr) {
		this.driver = dr;
	}
}
