package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by nick.thompson on 26/05/2017.
 */
public class NationalTrustJoinUsPage extends PageBase {

	public By INDIVIDUAL_MEMBERSHIP_BUTTON = By.cssSelector("#tabmembership-individual > div > div > h4");

	public  By JOINT_MEMBERSHIP_BUTTON = By.cssSelector("#tabmembership-joint > div > div");

	public  By FAMILY_MEMBERSHIP_BUTTON = By.cssSelector("#tabmembership-family > div > div > h4");

	public  By LIFE_MEMBERSHIP_BUTTON = By.xpath("//*[@id=\"tabmembership-life\"]/div/div");

	public By BUY_ADULT_MEMBERSHIP_BUTTON = By.cssSelector("#membership-individual > div > div > div.nt-pricing-table--content > div:nth-child(1) > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(1)");

	public By BUY_ADULT_GIFT_MEMBERSHIP_BUTTON = By.cssSelector("#membership-individual > div > div > div.nt-pricing-table--content > div:nth-child(1) > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(2)");

	public By BUY_YOUNG_PERSON_MEMBERSHIP_BUTTON = By.cssSelector("#membership-individual > div > div > div.nt-pricing-table--content > div:nth-child(2) > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(1)");

	public By BUY_YOUNG_PERSON_GIFT_MEMBERSHIP_BUTTON = By.cssSelector("#membership-individual > div > div > div.nt-pricing-table--content > div:nth-child(2) > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(2)");

	public By BUY_JUNIOR_MEMBERSHIP_BUTTON = By.cssSelector("#membership-individual > div > div > div.nt-pricing-table--content > div:nth-child(3) > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(1)");

	public By BUY_JUNIOR_GIFT_MEMBERSHIP_BUTTON = By.cssSelector("#membership-individual > div > div > div.nt-pricing-table--content > div:nth-child(3) > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(2)");

	public By BUY_JOINT_MEMBERSHIP_BUTTON = By.cssSelector("#membership-joint > div > div > div.nt-pricing-table--content > div > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(1)");

	public By BUY_JOINT_GIFT_MEMBERSHIP_BUTTON = By.cssSelector("#membership-joint > div > div > div.nt-pricing-table--content > div > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(2)");

	public By BUY_FAMILY_1ADULTS_MEMBERSHIP_BUTTON = By.cssSelector("#membership-family > div > div > div.nt-pricing-table--content > div:nth-child(2) > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(1)");

	public By BUY_FAMILY_1ADULTS_GIFT_MEMBERSHIP_BUTTON = By.cssSelector("#membership-family > div > div > div.nt-pricing-table--content > div:nth-child(2) > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(2)");

	public By BUY_FAMILY_2ADULTS_MEMBERSHIP_BUTTON = By.cssSelector("#membership-family > div > div > div.nt-pricing-table--content > div:nth-child(1) > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(1)");

	public By BUY_FAMILY_2ADULTS_GIFT_MEMBERSHIP_BUTTON = By.cssSelector("#membership-family > div > div > div.nt-pricing-table--content > div:nth-child(1) > div.nt-pricing-table--action.nt-group.small-12.medium-12.large-3.column.end > a:nth-child(2)");

	protected WebDriver driver;

	public By JOIN_US_HEADER_TEXT = By.cssSelector("#main-content > div.nt-header-module-houses-buildings.nt-palette-primary > div.row.nt-anim-header-fadein > div > div > h1");


	public String getJoinUsHeaderTextDisplayed() {
		Helpers.waitForElementToAppear(driver, JOIN_US_HEADER_TEXT, 10, "Session Timeout header text not displayed");
		List<WebElement> joinUsHeader = driver.findElements(JOIN_US_HEADER_TEXT);
		if (joinUsHeader.size() != 0) {
			String joinUsHeaderHeaderTextDisplayed = driver.findElement(JOIN_US_HEADER_TEXT).getText();

			return joinUsHeaderHeaderTextDisplayed;
		}
		return null;

	}

	public void selectIndividualMembershipButton() {
		List<WebElement> confirmCardButton = driver.findElements(INDIVIDUAL_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(INDIVIDUAL_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Individual Membership button not displayed");
		}
	}

	public void selectJointMembershipButton() {
		List<WebElement> confirmCardButton = driver.findElements(JOINT_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(JOINT_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Joint Membership button not displayed");
		}
	}

	public void selectFamilyMembershipButton() {
		List<WebElement> confirmCardButton = driver.findElements(FAMILY_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(FAMILY_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Family Membership button not displayed");
		}
	}

	public void selectLifeMembershipButton() {
		List<WebElement> confirmCardButton = driver.findElements(LIFE_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(LIFE_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Life Membership button not displayed");
		}
	}

	public void selectIndAdultBuyForMyself() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_ADULT_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_ADULT_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Individual Adult button not displayed");
		}
	}

	public void selectIndAdultBuyAsAGift() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_ADULT_GIFT_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_ADULT_GIFT_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Individual Adult Gift button not displayed");
		}
	}

	public void selectYoungPersonAdultBuyForMyself() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_YOUNG_PERSON_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_YOUNG_PERSON_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Young Person Buy for myself button not displayed");
		}
	}


	public void selectYoungPersonAdultBuyAsAGift() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_YOUNG_PERSON_GIFT_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_YOUNG_PERSON_GIFT_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Young Person Buy as a gift button not displayed");
		}
	}

	public void selectJuniorAdultBuyForMyself() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_JUNIOR_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_JUNIOR_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Junior Buy for myself button not displayed");
		}
	}

	public void selectJuniorAdultBuyAsAGift() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_JUNIOR_GIFT_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_JUNIOR_GIFT_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Junior Buy as a gift button not displayed");
		}
	}

	public void selectJointBuyForMyself() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_JOINT_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_JOINT_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Joint Buy for myself button not displayed");
		}
	}
	public void selectJointBuyAsAGift() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_JOINT_GIFT_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_JOINT_GIFT_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Joint Buy as a gift button not displayed");
		}
	}

	public void selectFamily1AdultBuyForMyself() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_FAMILY_1ADULTS_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_FAMILY_1ADULTS_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Family Buy for myself button not displayed");
		}
	}

	public void selectFamily1AdultBuyAsAGift() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_FAMILY_1ADULTS_GIFT_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_FAMILY_1ADULTS_GIFT_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Family Buy as a gift button not displayed");
		}
	}

	public void selectFamily2AdultBuyForMyself() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_FAMILY_2ADULTS_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_FAMILY_1ADULTS_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Family Buy for myself button not displayed");
		}
	}

	public void selectFamily2AdultBuyAsAGift() {
		List<WebElement> confirmCardButton = driver.findElements(BUY_FAMILY_2ADULTS_GIFT_MEMBERSHIP_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(BUY_FAMILY_1ADULTS_GIFT_MEMBERSHIP_BUTTON).click();
		} else {
			setLogs("Family Buy as a gift button not displayed");
		}
	}


	public NationalTrustJoinUsPage (WebDriver dr) {
		this.driver = dr;
	}

}
