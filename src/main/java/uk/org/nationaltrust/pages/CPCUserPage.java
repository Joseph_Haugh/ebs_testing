package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by nick.thompson on 16/02/2018.
 */
public class CPCUserPage extends PageBase {

	protected WebDriver driver;

	public CPCUserPage(WebDriver dr) {
		this.driver = dr;
	}

	public By GET_CONTACT_BY_PHONE_YES_CHECKBOX = By.id("contactConsentedTelephone-YesLabel");

	public By GET_CONTACT_BY_PHONE_NO_CHECKBOX = By.id("contactConsentedTelephone-NoLabel");

	public By GET_CONTACT_BY_EMAIL_YES_CHECKBOX = By.id("contactConsentedEmail-YesLabel");

	public By GET_CONTACT_BY_EMAIL_NO_CHECKBOX = By.id("contactConsentedEmail-NoLabel");

	public By GET_CONTACT_BY_POST_YES_CHECKBOX = By.id("contactConsentedPost-YesLabel");

	public By GET_CONTACT_BY_POST_NO_CHECKBOX = By.id("contactConsentedPost-NoLabel");

	public By SUBMIT_CONTACT_PREFERENCES_BUTTON = By.id("submitContactPermissions");

	private static final By BAD_LINK_ERROR_TEXT = By.cssSelector("#cpc-error-message");

	public By TAKE_ME_TO_THE_CONTACT_PREFERNECE_PAGE_BUTTON = By.cssSelector("body > div > div.nt-404-content > p:nth-child(6) > a");

	public By CONFIRMATION_MAIN_HEADER = By.cssSelector("#mainContent > section > div:nth-child(1) > div > header > h1");

	public By THANK_YOU_MESSAGE = By.cssSelector("#mainContent > section > div:nth-child(2) > div > p:nth-child(1) > b");

	public static final By TEMPT_OTHER_MEMBERS_HEADER_TEXT = By.cssSelector("#mainContent > section > div:nth-child(2) > div > div.info-panel.panel__readonly > h4");

	private static final By CONTACT_PREFERENCE_HEADER_LABEL = By.cssSelector("#permissionsForm > div:nth-child(1) > div > header");

	private static final By GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT = By.id("contactPref-errorEmail");

	private static final By GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT = By.id("contactPref-errorPost");

	private static final By GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT = By.id("contactPref-errorTelephone");

	private static final By GET_NEW_CONTACT_PREF_INFO_TEXT = By.id("js-permissionStatementContainer");

	public By MY_NT_SIGN_IN_LINK = By.cssSelector("#yesMyNT > p > a");
	public By MY_NT_SIGN_IN_LINK_TEXT = By.cssSelector("#yesMyNT > p");
	public By MY_NT_REGISTRATION_LINK = By.cssSelector("#noMyNT > p > a");
	public By MY_NT_REGISTRATION_LINK_TEXT = By.cssSelector("#noMyNT > p");

	public String getContactPreferenceHeaderLableText() {
		return driver.findElement(CONTACT_PREFERENCE_HEADER_LABEL).getText();
	}

	public String getConfirmationHeaderDisplayed() {
		List<WebElement> confirmationHeaderText = driver.findElements(CONFIRMATION_MAIN_HEADER);
		if (confirmationHeaderText.size() != 0) {
			String confirmationHeader = driver.findElement(CONFIRMATION_MAIN_HEADER).getText();
			return confirmationHeader;
		}
		return null;
	}

	public String getThankYouTextDisplayed() {
		List<WebElement> thankYou = driver.findElements(THANK_YOU_MESSAGE);
		if (thankYou.size() != 0) {
			String thankYouText = driver.findElement(THANK_YOU_MESSAGE).getText();
			return thankYouText;
		}
		return null;
	}

	public String getContactPrefForEmailNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPostNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPhoneNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactInfoText = driver.findElements(GET_NEW_CONTACT_PREF_INFO_TEXT);
		if (contactInfoText.size() != 0) {
			String contactInfoTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREF_INFO_TEXT).getText();

			return contactInfoTextDisplayed;
		}
		return null;

	}

	public String getBadTokenErrorPageText() {
		Helpers.waitForElementToAppear(driver, BAD_LINK_ERROR_TEXT, 10, "Bad Token error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(BAD_LINK_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String badTokenErrorTextDisplayed = driver.findElement(BAD_LINK_ERROR_TEXT).getText();

			return badTokenErrorTextDisplayed;
		}
		return null;

	}


	public String getTemptOtherMembersText() {
		Helpers.waitForElementToAppear(driver, TEMPT_OTHER_MEMBERS_HEADER_TEXT, 10, "Tempt other members to submit prefs text not displayed");
		List<WebElement> temptMembersText = driver.findElements(TEMPT_OTHER_MEMBERS_HEADER_TEXT);
		if (temptMembersText.size() != 0) {
			String temptMembersTextDisplayed = driver.findElement(TEMPT_OTHER_MEMBERS_HEADER_TEXT).getText();

			return temptMembersTextDisplayed;
		}
		return null;

	}

	public String getMYNTSignInLinkText() {
		return driver.findElement (MY_NT_SIGN_IN_LINK_TEXT).getText ();
	}

	public String getMYNTRegistrationLinkText() {
		return driver.findElement (MY_NT_REGISTRATION_LINK_TEXT).getText ();
	}

}
