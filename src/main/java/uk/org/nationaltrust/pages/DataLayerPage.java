package uk.org.nationaltrust.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by nick.thompson on 09/01/2018.
 */
public class DataLayerPage extends PageBase {
	protected WebDriver driver;

	public String verifyBootstrapjs(String bootstraplink){
		//		Helpers.waitForPageLoad();
		boolean value=false;
		String pagesoucetext=driver.getPageSource();
		int i=1;
		loop:
		while (i<=20) {
			if (pagesoucetext.contains(bootstraplink)) {
				value = true;
				break;
			}
			else
			{
				i++;
				continue loop;
			}
		}
		return pagesoucetext;
	}



	public String getDataLayer(String PropertyName) {

		//		Helpers.waitForPageLoad();
		setLogs("Get datalayer object to verify whether the taxonomy tags tracked or not");
		//"$('body').html($('body').html().replace(...));
		String dataLayerElements = ((JavascriptExecutor) driver).executeScript("return globalDataLayer."+PropertyName+"").toString().replace("\"", "").replace(";", " ").trim();
		List<String> tagsList = new ArrayList<String>(Arrays.asList(dataLayerElements.split(" ")));
		int i=tagsList.size();
		int n=0;
		String propertytext="";
		while(n<tagsList.size()){
			propertytext=propertytext+tagsList.get(n);
			setLogs("The list of element is"+propertytext);
			n++;
		}
		return propertytext;
	}



	public boolean getDataLayerPages(String PropertyName) {

		boolean value=false;
		Helpers.waitForPageLoad(driver);
		setLogs("Get datalayer object to verify whether the taxonomy tags tracked or not");
		//"$('body').html($('body').html().replace(...));
		String dataLayerElements = ((JavascriptExecutor) driver).executeScript("return globalDataLayer."+PropertyName+"").toString().replace("\"", "").replace(";", " ").trim();
		List<String> tagsList = new ArrayList<String>(Arrays.asList(dataLayerElements.split(" ")));
		int i=tagsList.size();
		int n=0;
		String propertytext="";
		if(tagsList.size()==0){
			value=true;
		}
		return value;
	}


	public DataLayerPage (WebDriver dr) {
		this.driver = dr;

	}}


