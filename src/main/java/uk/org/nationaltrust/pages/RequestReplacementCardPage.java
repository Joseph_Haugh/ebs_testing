package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by Nick.Thompson on 15/06/2016.
 */
public class RequestReplacementCardPage extends PageBase {

	protected WebDriver driver;

	public By REQUEST_MEMBERSHIP_CARD_BUTTON = By.cssSelector("#MyNTRequestMembershipCardSubmit");

	private By REQUEST_REPLACEMENT_CARD_HEADER = By.cssSelector("#content > section > div > main > div > h1");

	public By REQUEST_REPLACEMENT_CARD_PREVIOUSLY_ORDERED_TEXT = By.cssSelector("#content > section > div > main > div > div > form > div.alert-box.icon-block.info.icon-info.tst-order-not-available > p");

	public By REQUEST_CAR_STICKER_BUTTON = By.id("MyNTRequestMembershipCardSubmit");

	public RequestReplacementCardPage(WebDriver dr) {
		this.driver = dr;
	}

	public String getSendMeReplacementCardHeaderText() {
		return driver.findElement(REQUEST_REPLACEMENT_CARD_HEADER).getText();
	}

	public String getRequestMembershipCardButtonText() {
		return driver.findElement(REQUEST_MEMBERSHIP_CARD_BUTTON).getText();
	}

	public String getRequestMembershipCardPreviouslyOrderedText() {
		Helpers.waitForIsDisplayed(driver,REQUEST_REPLACEMENT_CARD_PREVIOUSLY_ORDERED_TEXT,10);
		return driver.findElement(REQUEST_REPLACEMENT_CARD_PREVIOUSLY_ORDERED_TEXT).getText();
	}


	public void selectConfirmCardButton() {
		List<WebElement> confirmCardButton = driver.findElements(REQUEST_MEMBERSHIP_CARD_BUTTON);
		if (confirmCardButton.size() != 0) {
			driver.findElement(REQUEST_MEMBERSHIP_CARD_BUTTON).click();
		} else {
			setLogs("Confirm card button not displayed");
		}
	}

}
