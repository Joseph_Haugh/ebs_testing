package uk.org.nationaltrust.pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Sowjanya Annepu
 */
public abstract class CommemorativeMapPinDonateSelectors extends PageBase {

    public CommemorativeMapPinDonateSelectors() {
        PageFactory.initElements(driver,this);
    }

    protected boolean status = false;

    protected String ASHDOWN_HOUSE_PLACEID="15";

    protected String TYNESFIELD_PLACEID="2362";

    protected  String ASHDOWN_HOUSE_PROPERTY="Ashdown House";

    protected String ABERCONWAY_HOUSE_PROPERTY="Aberconway";

    protected String PLACE_NAME="Tyntesfield";

    protected String donationId=null;

    @FindBy(css="input[type='search']")
    public WebElement ADD_MAP_PIN_SEARCH_BOX;

    @FindBy(xpath = "//li/div/div[contains(text(),'Tyntesfield')]")
    public WebElement ADD_MAP_PIN_TYNTESFIELD_PLACE;

    @FindBy(css = "h3[data-tst-map-info-window-title]")
    public WebElement ADD_MAP_PIN_PLACE_NAME_ON_PIN;

    @FindBy(css = "h3[data-tst-property-card-title]")
    public WebElement ADD_MAP_PIN_PLACE_ON_PROPERTY_CARD;

    @FindBy(xpath = "//div[@class='nt-card__content']//button[@class='nt-button round'][contains(text(),'Dedicate a donation')]")
    public WebElement ADD_MAP_PIN_DEDICATE_A_DONATION;

    @FindBy(xpath="//div[@id='button']//button[@class='nt-button round'][contains(text(),'Dedicate a donation')]")
    public WebElement MAP_PIN_POP_UP_DEDICATE_A_DONATION_BUTTON;

    @FindBy(css=".nt-button--link")
    public WebElement ADD_MAP_PIN_MEMORIES_LINK;

    @FindBy(xpath="//button[contains(text(),'dedications')]")
    public WebElement ADD_MAP_PIN_MEMORIES_TEXT_ON_PIN;

    @FindBy(id="donation-candidate")
    public WebElement STEP1_SUBJECT;

    @FindBy(id="message")
    public WebElement STEP1_MESSAGE;

    @FindBy(id="donation-tone")
    public WebElement STEP1_TONE;

    @FindBy(id="occasion-option")
    public WebElement STEP1_OCCASION;

    @FindBy(id = "relationship-option")
    public WebElement STEP1_RELATION;

    @FindBy(id = "other-relationship")
    public WebElement STEP1_RELATION_OTHER;

    @FindBy(id = "other-occasion")
    public WebElement STEP1_OCCASION_OTHER;

    @FindBy(xpath="//button[contains(text(),'Add my message')]")
    public WebElement STEP1_NEXT_BUTTON;

    @FindBy(css="label[for='private-pin']")
    public WebElement STEP1_PRIVATE_PIN_CHECKBOX;

    @FindBy(id="privatePinHelpText")
    public WebElement STEP1_PRIVATE_PIN_HELP_TEXT;

    @FindBy(id="donationCandidateHelpText")
    public WebElement STEP1_SUBJECT_HELP_TEXT;

    @FindBy(id = "story")
    public WebElement STEP2_STORY;

    @FindBy(css = ".nt-label-note:nth-of-type(1)")
    public WebElement STEP2_STORY_MAX_CHAR_TEXT;

    @FindBy(css = "div[class='nt-label-note text-red'] > div")
    public WebElement STEP2_STORY_NEGATIVE_COUNTER_TEXT;

    @FindBy(id = "story-help-text")
    public WebElement STEP2_STORY_HELP_TEXT;

    @FindBy(xpath = "//button[contains(text(),'Choose an image')]")
    public WebElement STEP2_ADD_IMAGE_BUTTON;

    @FindBy (id = "public-story-help-text")
	public WebElement STEP2_PUBLIC_PIN_HELP;

	@FindBy (id = "private-story-help-text")
	public WebElement STEP2_PRIVATE_PIN_HELP;

    @FindBy(xpath="//button[contains(text(),'Go back')]")
    public WebElement PREVIOUS_BUTTON;

    @FindBy(css="h3")
    public WebElement STEP3_IMAGE_TITLE;

    @FindBy(xpath="//button[contains(text(),'Preview my dedication')]")
    public WebElement STEP3_SUMMARY_BUTTON;

    @FindBy(css="button[aria-label='Next page']")
    public WebElement STEP3_CAROUSAL_IMAGES_NEXT_ARROW;

    @FindBy(css="button[aria-label='Previous page']")
    public WebElement STEP3_CAROUSAL_IMAGES_PREVIOUS_ARROW;

    @FindBy(xpath ="//button[contains(text(),'Confirm and donate')]" )
    public WebElement STEP4_CONFIRM_AND_DONATE_BUTTON;

    @FindBy(css="legend + p ")
    public WebElement STEP4_HELPTEXT;

    @FindBy(xpath="//div[contains(text(),'Please tell us who your dedication is for')]")
    public WebElement STEP1_SUBJECT_VALIDATION_ERROR_TEXT;

    @FindBy(xpath="//div[contains(text(),'Please tell us why you are making this dedication')]")
    public WebElement STEP1_TONE_VALIDATION_ERROR_TEXT;

    @FindBy(xpath="//div[contains(text(),'You can only enter 240 characters')]")
    public WebElement STEP1_SUBJECT_MAX_LENGTH_ERR_TEXT;

    protected String donationToneInCelebration="in celebration";

    protected String donationToneInMemory="in memory";

    @FindBy(xpath="//div[contains(text(),'Please tell us why this place is important to you and your loved one.')]")
    public WebElement STEP2_STORY_VALIDATION_ERROR_TEXT;

    protected String subjectHelptext="What you enter will be publicly viewable so please bear this in mind. You may want to use initials, just the first name or first initial and last name. Think about how they might feel about being publicly identifiable through this dedication.";

    protected String privatePinHelpText="A private dedication will still appear on our map but the details will only be visible to you and anyone you send your personal link to.";

    protected String storyMaxCharText="650 characters remaining.";

    protected String step4PreviewHelpText="Check you're happy before proceeding to making your donation.";

    @FindBy(css="h3>span" )
    public WebElement RHS_CARD_SUBJECT;

    @FindBy(css="pre" )
    public WebElement RHS_CARD_STORY;

    @FindBy(css="h4>span")
    public WebElement RHS_CARD_RELATION;

    @FindBy(css=".nt-close-button")
    public WebElement RHS_CARD_CLOSE_BUTTON;

    protected String CONFIRMATION_TEXT="Thank you for dedicating a donation ";
}