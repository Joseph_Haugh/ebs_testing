package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * Created by Nick.Thompson on 24/02/2017.
 */
public class DonatePersonalDetailsForm extends DonateCommons {

	protected WebDriver driver;

	private String appealNameWithRegular = "protect-special-places-appeal";

	private String appealNameOneOffOnly = "monuments-to-freedom-appeal";

	public By DONATE_BASKET_AMOUNT_ONE = By.id("oneOffPaymentAmountChoice1");

	public By DONATE_BASKET_AMOUNT_TWO = By.id("oneOffPaymentAmountChoice2");

	public By DONATE_BASKET_AMOUNT_THREE = By.id("oneOffPaymentAmountChoice2");

	public By TITLE_FIELD = By.cssSelector("#title");

	public By FIRST_NAME_FIELD = By.cssSelector("#firstName");

	public By LAST_NAME_FIELD = By.cssSelector("#lastName");

	public By EMAIL_FIELD = By.id("emailAddress");

	public By COUNTRY_FIELD = By.id("postalAddress.country");

	public By ADDRESS_LINE1_FIELD = By.xpath("//input[contains(@id, 'addressLine1')]");

	public By ADDRESS_LINE2_FIELD = By.xpath("//input[contains(@id, 'addressLine2')]");

	public By ADDRESS_LINE3_FIELD = By.xpath("//input[contains(@id, 'addressLine3')]");

	public By ADDRESS_LINE4_FIELD = By.xpath("//input[contains(@id, 'addressLine4')]");

	public By CITY_FIELD = By.xpath("//input[contains(@id, 'city')]");

	public By COUNTY_FIELD = By.cssSelector("//input[contains(@id, 'county')]");

	public By POSTCODE_FIELD = By.xpath("//input[contains(@id, 'postcode')]");

	public By GET_ENTER_ADDRESS_MANUALLY_LINK  = By.id("postcodeManualLink");

	public By CONTACT_YES_RADIO_BUTTON = By.cssSelector("#contactPrefYesLabel");

	public By CONTACT_NO_RADIO_BUTTON = By.cssSelector("#contactPrefNoLabel");

	public By GIFT_AID_CONSENT_CHECKBOX = By.cssSelector("#giftAidLabel");

	public By CONFIRM_AND_PAY_BUTTON = By.id("submitPersonalDetails");

	public By APPEAL_NAME_DISPLAYED = By.cssSelector("#appealPageLink > span");

	public By CLICK_TO_CHAT_BUTTON_TEXT = By.id("#K2C_570_CH_3_W > a");

	public By BAD_TITLE_ANCHOR_LINK = By.linkText("Please provide your title");

	public By BAD_FIRST_NAME_ANCHOR_LINK = By.linkText("Please provide your first name");

	public By BAD_LAST_NAME_ANCHOR_LINK = By.linkText("Please provide your last name");

	public By BAD_ADDRESS_1_ANCHOR_LINK = By.linkText("Please check your address line 1");

	public By BAD_COUNTY_ANCHOR_LINK = By.linkText("Please check you've entered the correct county");

	public By BAD_POSTCODE_ANCHOR_LINK = By.linkText("Please check you’ve entered the correct postcode");

	public By BAD_EMAIL_ANCHOR_LINK = By.linkText("Please provide a valid email address");

	public By BAD_CONTACT_ANCHOR_LINK = By.cssSelector("#mainContent > section > form > div:nth-child(7) > div > div > ul > li:nth-child(7) > a");

	public By BAD_DD_ACCOUNT_NAME_ANCHOR_LINK = By.linkText("Please check your account name");

	public static By BAD_EMAIL_ERROR_TEXT = By.cssSelector("#emailAddress-error > li");

	public static By TITLE_ERROR_TEXT = By.cssSelector("#title-errors > li");

	public static By FIRST_NAME_ERROR_TEXT = By.cssSelector("#firstName-error > li");

	public static By LAST_NAME_ERROR_TEXT = By.cssSelector("#lastName-error > li");

	public static By POSTCODE_ERROR_TEXT = By.cssSelector("#postalAddress_postcode-error > li");

	public static By ADDRESS_LINE1_ERROR_TEXT = By.cssSelector("#postalAddress_addressLine1-error > li");

	public static By CONTACT_PREF_ERROR_TEXT = By.cssSelector("#contactPref-error > li");

	public static By CITY_MAX_LENGTH_ERROR_TEXT = By.cssSelector("#postalAddress_city-error > li");

	public static By COUNTY_MAX_LENGTH_ERROR_TEXT = By.cssSelector("#postalAddress_county-error > li");

	public static By NO_CONTACT_WARNING_MESSAGE = By.cssSelector("#contactPermissionsConfirmDecline > p");

	public static By CANCEL_FROM_BARCLAYS_TEXT = By.cssSelector("#warningMessage");

	public By GET_ACCOUNT_NAME_FIELD = By.id("bankDetailsForm_accountName");

	public By DIRECT_DEBIT_GUARANTEE_LINK = By.cssSelector("#mainContent > section > form > div:nth-child(8) > div > fieldset > div > details:nth-child(4) > summary > span");

	public static By SORT_CODE_MISSING_ERROR = By.id("bankDetailsForm_sortCodeForm-errors");

	public static By ACCOUNT_NUMBER_MISSING_ERROR = By.id("bankDetailsForm_accountNumber-error");

	public static By ACCOUNT_NAME_MISSING_ERROR = By.id("bankDetailsForm_accountName-error");

	public static By BAD_ACCOUNT_NAME_ERROR = By.cssSelector("#bankDetailsForm_accountName-error > li");

	public static By SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR = By.cssSelector("#bankDetailsForm-error > li");

	public By GET_CONTACT_BY_PHONE_YES_CHECKBOX = By.xpath("//*[@id=\"contactConsentForm.contactConsentedTelephone-YesLabel\"]");

	public By GET_CONTACT_BY_PHONE_NO_CHECKBOX = By.xpath("//*[@id=\"contactConsentForm.contactConsentedTelephone-NoLabel\"]");

	public By GET_CONTACT_BY_EMAIL_YES_CHECKBOX = By.xpath("//*[@id=\"contactConsentForm.contactConsentedEmail-YesLabel\"]");

	public By GET_CONTACT_BY_EMAIL_NO_CHECKBOX = By.xpath("//*[@id=\"contactConsentForm.contactConsentedEmail-NoLabel\"]");

	public By GET_CONTACT_BY_POST_YES_CHECKBOX = By.xpath("//*[@id=\"contactConsentForm.contactConsentedPost-YesLabel\"]");

	public By GET_CONTACT_BY_POST_NO_CHECKBOX = By.xpath("//*[@id=\"contactConsentForm.contactConsentedPost-NoLabel\"]");

	private static By GET_NEW_CONTACT_PREAMBLE_INFO_TEXT = By.xpath("//legend[contains(text(),'Contacting you')]");

	public By GET_NEW_CONTACT_PREF_INFO_TEXT = By.id("js-permissionStatementContainer");

	private static final By GET_CONTACT_PREF_EMAIL_NOT_SELECTED_ERROR_TEXT = By.cssSelector("#contactPref-errorEmail > li");

	private static final By GET_CONTACT_PREF_POST_NOT_SELECTED_ERROR_TEXT = By.cssSelector("#contactPref-errorPost > li");

	private static final By GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT = By.cssSelector("#contactPref-errorTelephone > li");

	private static final By SUBJECT_NAME_MISSING_ERROR_TEXT = By.id("commemorationForm_subject-error");

	private static final By CUSTOM_AMOUNT_THRESHHOLD_ONE_OFF_ERROR_TEXT = By.id("oneOffDonationAmounts-errors");

	private static final By CUSTOM_AMOUNT_THRESHHOLD_REGULAR_ERROR_TEXT = By.id("regularDonationAmounts-errors");

	public String getOneOffCustomAmountTooLowErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, CUSTOM_AMOUNT_THRESHHOLD_ONE_OFF_ERROR_TEXT, 10, "Custom amount too low error text not displayed");
		List<WebElement> customAmountErrorText = driver.findElements(CUSTOM_AMOUNT_THRESHHOLD_ONE_OFF_ERROR_TEXT);
		if (customAmountErrorText.size() != 0) {
			String customAmountErrorTextDisplayed = driver.findElement(CUSTOM_AMOUNT_THRESHHOLD_ONE_OFF_ERROR_TEXT).getText();

			return customAmountErrorTextDisplayed;
		}
		return null;

	}

	public String getRegularCustomAmountTooLowErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, CUSTOM_AMOUNT_THRESHHOLD_REGULAR_ERROR_TEXT, 10, "Custom amount too low error text not displayed");
		List<WebElement> customAmountErrorText = driver.findElements(CUSTOM_AMOUNT_THRESHHOLD_REGULAR_ERROR_TEXT);
		if (customAmountErrorText.size() != 0) {
			String customAmountErrorTextDisplayed = driver.findElement(CUSTOM_AMOUNT_THRESHHOLD_REGULAR_ERROR_TEXT).getText();

			return customAmountErrorTextDisplayed;
		}
		return null;

	}

	public String getSubjectNameErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, SUBJECT_NAME_MISSING_ERROR_TEXT, 10, "Subject missing text not displayed");
		List<WebElement> subjectErrorText = driver.findElements(SUBJECT_NAME_MISSING_ERROR_TEXT);
		if (subjectErrorText.size() != 0) {
			String subjectErrorTextDisplayed = driver.findElement(SUBJECT_NAME_MISSING_ERROR_TEXT).getText();

			return subjectErrorTextDisplayed;
		}
		return null;

	}

	public String getSortCodeWrongErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, SORT_CODE_MISSING_ERROR, 10, "Sort code Missing text not displayed");
		List<WebElement> sortCodeErrorText = driver.findElements(SORT_CODE_MISSING_ERROR);
		if (sortCodeErrorText.size() != 0) {
			String sortCodeErrorTextDisplayed = driver.findElement(SORT_CODE_MISSING_ERROR).getText();

			return sortCodeErrorTextDisplayed;
		}
		return null;

	}

	public String getAccountNumberIncompleteErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, ACCOUNT_NUMBER_MISSING_ERROR, 10, "Account Number Missing text not displayed");
		List<WebElement> accountNumberErrorText = driver.findElements(ACCOUNT_NUMBER_MISSING_ERROR);
		if (accountNumberErrorText.size() != 0) {
			String accountNumberErrorTextDisplayed = driver.findElement(ACCOUNT_NUMBER_MISSING_ERROR).getText();

			return accountNumberErrorTextDisplayed;
		}
		return null;

	}

	public String getAccountNameIncompleteErrorTextDisplayed() {
		//		Helpers.waitForElementToAppear(driver, ACCOUNT_NAME_MISSING_ERROR, 10, "Account Name Missing text not displayed");
		List<WebElement> accountNameErrorText = driver.findElements(ACCOUNT_NAME_MISSING_ERROR);
		if (accountNameErrorText.size() != 0) {
			String accountNameErrorTextDisplayed = driver.findElement(ACCOUNT_NAME_MISSING_ERROR).getText();

			return accountNameErrorTextDisplayed;
		}
		return null;

	}

	public String getBadAccountNameErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, BAD_ACCOUNT_NAME_ERROR, 10, "Account Name Missing text not displayed");
		List<WebElement> badAccountNameErrorText = driver.findElements(BAD_ACCOUNT_NAME_ERROR);
		if (badAccountNameErrorText.size() != 0) {
			String badAccountNameErrorTextDisplayed = driver.findElement(BAD_ACCOUNT_NAME_ERROR).getText();

			return badAccountNameErrorTextDisplayed;
		}
		return null;

	}

	public String getBadAccountNameAchorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, BAD_DD_ACCOUNT_NAME_ANCHOR_LINK, 10, "Account Name Missing text not displayed");
		List<WebElement> badAccountNameAnchorText = driver.findElements(BAD_DD_ACCOUNT_NAME_ANCHOR_LINK);
		if (badAccountNameAnchorText.size() != 0) {
			String badAccountNameAnchorTextDisplayed = driver.findElement(BAD_DD_ACCOUNT_NAME_ANCHOR_LINK).getText();

			return badAccountNameAnchorTextDisplayed;
		}
		return null;

	}

	public String getSortCodeAndAccountNumberErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR, 10, "Bad Account text not displayed");
		List<WebElement> accountCombinationErrorText = driver.findElements(SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR);
		if (accountCombinationErrorText.size() != 0) {
			String accountCombinationErrorTextDisplayed = driver.findElement(SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR).getText();

			return accountCombinationErrorTextDisplayed;
		}
		return null;

	}

	public String getDonateFormHeaderTextDisplayed() {
		List<WebElement> donateFormHeader = driver.findElements(DONATION_FORM_THANK_YOU_HEADER);
		if (donateFormHeader.size() != 0) {
			String donateFormHeaderText = driver.findElement(DONATION_FORM_THANK_YOU_HEADER).getText();

			return donateFormHeaderText;
		}
		return null;

	}

	public String getClickToChatButtonText() {
		List<WebElement> clickToChat = driver.findElements(CLICK_TO_CHAT_BUTTON_TEXT);
		if (clickToChat.size() != 0) {
			String clickToChatButtonText = driver.findElement(CLICK_TO_CHAT_BUTTON_TEXT).getText();

			return clickToChatButtonText;
		}
		return null;

	}

	public void setTitle(String title) {
		driver.findElement(TITLE_FIELD).sendKeys(title);
	}

	public void firstName(String firstName) {
		driver.findElement(FIRST_NAME_FIELD).sendKeys(firstName);
	}

	public void lastName(String lastName) {
		driver.findElement(LAST_NAME_FIELD).sendKeys(lastName);
	}

	public void country(String country) {
		driver.findElement(COUNTRY_FIELD).sendKeys(country);
	}

	public void addressLine1(String addressLine1) {
		driver.findElement(ADDRESS_LINE1_FIELD).sendKeys(addressLine1);
	}

	public void addressLine2(String addressLine2) {
		driver.findElement(ADDRESS_LINE2_FIELD).sendKeys(addressLine2);
	}

	public void addressLine3(String addressLine3) {
		driver.findElement(ADDRESS_LINE3_FIELD).sendKeys(addressLine3);
	}

	public void addressLine4(String addressLine4) {
		driver.findElement(ADDRESS_LINE4_FIELD).sendKeys(addressLine4);
	}

	public void city(String city) {
		driver.findElement(CITY_FIELD).sendKeys(city);
	}

	public void county(String county) {
		driver.findElement(COUNTY_FIELD).sendKeys(county);
	}

	public void postcode(String postcode) {
		driver.findElement(POSTCODE_FIELD).sendKeys(postcode);
	}

	public void email(String email) {
		driver.findElement(EMAIL_FIELD).sendKeys(email);
	}

	public void selectPayButton() {
		driver.findElement(CONFIRM_AND_PAY_BUTTON).click();
	}

	public void selectContactYesButton() {
		driver.findElement(CONTACT_YES_RADIO_BUTTON).click();
	}

	public void selectContactNoButton() {
		driver.findElement(CONTACT_NO_RADIO_BUTTON).click();
	}

	public void selectGiftAidCheckbox() {
		driver.findElement(GIFT_AID_CONSENT_CHECKBOX).click();
	}

	public void setAllDonateFormText(String title, String firstName, String lastName, String addressLine1, String postcode, String email) {
		driver.findElement(TITLE_FIELD).sendKeys(title);
		donateForm().firstName(firstName);
//		driver.findElement(FIRST_NAME_FIELD).sendKeys(firstName);
		driver.findElement(LAST_NAME_FIELD).sendKeys(lastName);
		//		driver.findElement(COUNTRY_FIELD).sendKeys(country);
		driver.findElement(donateForm().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		driver.findElement(ADDRESS_LINE1_FIELD).sendKeys(addressLine1);
		driver.findElement(POSTCODE_FIELD).sendKeys(postcode);
		driver.findElement(EMAIL_FIELD).sendKeys(email);
	}

	public String getBadEmailErrorText() {
		Helpers.waitForElementToAppear(driver, donateForm().BAD_EMAIL_ERROR_TEXT, 10, "Email error text not appearing");
		List<WebElement> badEmailError = driver.findElements(BAD_EMAIL_ERROR_TEXT);
		if (badEmailError.size() != 0) {
			String badEmailErrorTextDisplayed = driver.findElement(BAD_EMAIL_ERROR_TEXT).getText();
			return badEmailErrorTextDisplayed;
		}
		return null;
	}

	public String getBadEmailLinkAnchorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, BAD_EMAIL_ANCHOR_LINK, 10, "Bad email Anchor Link not displayed");
		List<WebElement> emailAnchorText = driver.findElements(BAD_EMAIL_ANCHOR_LINK);
		if (emailAnchorText.size() != 0) {
			String emailAnchorTextDisplayed = driver.findElement(BAD_EMAIL_ANCHOR_LINK).getText();

			return emailAnchorTextDisplayed;
		}
		return null;

	}

	public String getBadTitleErrorText() {
		Helpers.waitForElementToAppear(driver, donateForm().TITLE_ERROR_TEXT, 10, "Title error text not appearing");
		List<WebElement> badTitleError = driver.findElements(TITLE_ERROR_TEXT);
		if (badTitleError.size() != 0) {
			String badTitleErrorTextDisplayed = driver.findElement(TITLE_ERROR_TEXT).getText();
			return badTitleErrorTextDisplayed;
		}
		return null;
	}

	public String getBadTitleAnchorText() {
		Helpers.waitForElementToAppear(driver, donateForm().BAD_TITLE_ANCHOR_LINK, 10, "Title Anchor link text not appearing");
		List<WebElement> badTitleAnchor = driver.findElements(BAD_TITLE_ANCHOR_LINK);
		if (badTitleAnchor.size() != 0) {
			String badTitleAnchorTextDisplayed = driver.findElement(BAD_TITLE_ANCHOR_LINK).getText();
			return badTitleAnchorTextDisplayed;
		}
		return null;
	}

	public String getBadFirstNameErrorText() {
		Helpers.waitForElementToAppear(driver, donateForm().FIRST_NAME_ERROR_TEXT, 10, "First Name error text not appearing");
		List<WebElement> badFirstNameError = driver.findElements(FIRST_NAME_ERROR_TEXT);
		if (badFirstNameError.size() != 0) {
			String badFirstNameErrorTextDisplayed = driver.findElement(FIRST_NAME_ERROR_TEXT).getText();
			return badFirstNameErrorTextDisplayed;
		}
		return null;
	}

	public String getBadFirstNameAnchorText() {
		Helpers.waitForElementToAppear(driver, donateForm().BAD_FIRST_NAME_ANCHOR_LINK, 10, "First Name Anchor Link text not appearing");
		List<WebElement> badFirstNameAnchor = driver.findElements(BAD_FIRST_NAME_ANCHOR_LINK);
		if (badFirstNameAnchor.size() != 0) {
			String badFirstNameAnchorTextDisplayed = driver.findElement(BAD_FIRST_NAME_ANCHOR_LINK).getText();
			return badFirstNameAnchorTextDisplayed;
		}
		return null;
	}

	public String getBadLastNameErrorText() {
		Helpers.waitForElementToAppear(driver, donateForm().LAST_NAME_ERROR_TEXT, 10, "Last Name error text not appearing");
		List<WebElement> badLastNameError = driver.findElements(LAST_NAME_ERROR_TEXT);
		if (badLastNameError.size() != 0) {
			String badLastNameErrorTextDisplayed = driver.findElement(LAST_NAME_ERROR_TEXT).getText();
			return badLastNameErrorTextDisplayed;
		}
		return null;
	}

	public String getBadLastNameAnchorText() {
		Helpers.waitForElementToAppear(driver, donateForm().BAD_LAST_NAME_ANCHOR_LINK, 10, " Bad Last Name Anchor link text not appearing");
		List<WebElement> badLastNameAnchor = driver.findElements(BAD_LAST_NAME_ANCHOR_LINK);
		if (badLastNameAnchor.size() != 0) {
			String badLastNameAnchorTextDisplayed = driver.findElement(BAD_LAST_NAME_ANCHOR_LINK).getText();
			return badLastNameAnchorTextDisplayed;
		}
		return null;
	}

	public String getContactPrefSelectionErrorText() {
		Helpers.waitForElementToAppear(driver, donateForm().CONTACT_PREF_ERROR_TEXT, 10, "No contact selected error text");
		List<WebElement> ContactPrefError = driver.findElements(CONTACT_PREF_ERROR_TEXT);
		if (ContactPrefError.size() != 0) {
			String ContactPrefErrorTextDisplayed = driver.findElement(CONTACT_PREF_ERROR_TEXT).getText();
			return ContactPrefErrorTextDisplayed;
		}
		return null;
	}

	public String getBadAddressLine1ErrorText() {
		Helpers.waitForElementToAppear(driver, donateForm().ADDRESS_LINE1_ERROR_TEXT, 10, "Address error text not appearing");
		List<WebElement> badAddressLine1Error = driver.findElements(ADDRESS_LINE1_ERROR_TEXT);
		if (badAddressLine1Error.size() != 0) {
			String badAddressLine1ErrorTextDisplayed = driver.findElement(ADDRESS_LINE1_ERROR_TEXT).getText();
			return badAddressLine1ErrorTextDisplayed;
		}
		return null;
	}

	public String getBadAddressLine1AnchorText() {
		Helpers.waitForElementToAppear(driver, donateForm().BAD_ADDRESS_1_ANCHOR_LINK, 10, "Bad Address1 anchor text not appearing");
		List<WebElement> badAddressLine1Anchor = driver.findElements(BAD_ADDRESS_1_ANCHOR_LINK);
		if (badAddressLine1Anchor.size() != 0) {
			String badAddressLine1AnchorTextDisplayed = driver.findElement(BAD_ADDRESS_1_ANCHOR_LINK).getText();
			return badAddressLine1AnchorTextDisplayed;
		}
		return null;
	}

	public String getBadPostcodeErrorText() {
		Helpers.waitForElementToAppear(driver, donateForm().POSTCODE_ERROR_TEXT, 10, "Postcode error text not appearing");
		List<WebElement> badPostcodeError = driver.findElements(POSTCODE_ERROR_TEXT);
		if (badPostcodeError.size() != 0) {
			String badPostcodeErrorTextDisplayed = driver.findElement(POSTCODE_ERROR_TEXT).getText();
			return badPostcodeErrorTextDisplayed;
		}
		return null;
	}

	public String getBadPostcodeAnchorText() {
		Helpers.waitForElementToAppear(driver, donateForm().BAD_POSTCODE_ANCHOR_LINK, 10, "Bad Postcode Anchor Link text not appearing");
		List<WebElement> badPostcodeAnchor = driver.findElements(BAD_POSTCODE_ANCHOR_LINK);
		if (badPostcodeAnchor.size() != 0) {
			String badPostcodeAnchorTextDisplayed = driver.findElement(BAD_POSTCODE_ANCHOR_LINK).getText();
			return badPostcodeAnchorTextDisplayed;
		}
		return null;
	}

	public String getContactPrefSelectionAnchorText() {
		Helpers.waitForElementToAppear(driver, donateForm().BAD_CONTACT_ANCHOR_LINK, 10, "No contact selected Anchor Link text");
		List<WebElement> ContactPrefAnchor = driver.findElements(BAD_COUNTY_ANCHOR_LINK);
		if (ContactPrefAnchor.size() != 0) {
			driver.findElement(BAD_CONTACT_ANCHOR_LINK).click();

		}
		return null;
	}

	//	public String getNoContactPrefSelectionErrorText (){
	//
	//		Helpers.waitForElementToAppear(driver, donateForm().CONTACT_PREF_NO_ERROR_TEXT, 10, "No contact selected error text");
	//		List<WebElement> noContactPrefError = driver.findElements(CONTACT_PREF_NO_ERROR_TEXT);
	//		if (noContactPrefError.size() !=0){
	//			String nContactPrefErrorTextDisplayed = driver.findElement(CONTACT_PREF_NO_ERROR_TEXT).getText();
	//			return nContactPrefErrorTextDisplayed;
	//		}
	//		return null;
	//	}

	public String getCityMaxLengthErrorText() {
		Helpers.waitForElementToAppear(driver, donateForm().CITY_MAX_LENGTH_ERROR_TEXT, 10, "No max field length error text");
		List<WebElement> cityMaxError = driver.findElements(CITY_MAX_LENGTH_ERROR_TEXT);
		if (cityMaxError.size() != 0) {
			String cityMaxLengthErrorTextDisplayed = driver.findElement(CITY_MAX_LENGTH_ERROR_TEXT).getText();
			return cityMaxLengthErrorTextDisplayed;
		}
		return null;
	}

	public String getCountyMaxLengthErrorText() {
		Helpers.waitForElementToAppear(driver, donateForm().COUNTY_MAX_LENGTH_ERROR_TEXT, 10, "No max field length error text");
		List<WebElement> countyMaxError = driver.findElements(COUNTY_MAX_LENGTH_ERROR_TEXT);
		if (countyMaxError.size() != 0) {
			String countyMaxLengthErrorTextDisplayed = driver.findElement(COUNTY_MAX_LENGTH_ERROR_TEXT).getText();
			return countyMaxLengthErrorTextDisplayed;
		}
		return null;
	}

	public String getNoContactMessageText() {
		Helpers.waitForElementToAppear(driver, donateForm().NO_CONTACT_WARNING_MESSAGE, 10, "No contact selected error text");
		List<WebElement> noContactMessage = driver.findElements(NO_CONTACT_WARNING_MESSAGE);
		if (noContactMessage.size() != 0) {
			String noContactMessageDisplayed = driver.findElement(NO_CONTACT_WARNING_MESSAGE).getText();
			return noContactMessageDisplayed;
		}
		return null;
	}

	public String getDonationAmountText() {
		Helpers.waitForElementToAppear(driver, donateForm().DONATION_AMOUNT_DISPLAYED, 10, "No Donation amount displayed");
		List<WebElement> donationAmount = driver.findElements(DONATION_AMOUNT_DISPLAYED);
		if (donationAmount.size() != 0) {
			String donationAmountDisplayed = driver.findElement(DONATION_AMOUNT_DISPLAYED).getText();
			return donationAmountDisplayed;
		}
		return null;
	}

	public String getAppealNameText() {
		Helpers.waitForElementToAppear(driver, donateForm().APPEAL_NAME_DISPLAYED, 10, "No appeal name displayed");
		List<WebElement> appealName = driver.findElements(APPEAL_NAME_DISPLAYED);
		if (appealName.size() != 0) {
			String appealNameDisplayed = driver.findElement(APPEAL_NAME_DISPLAYED).getText();
			return appealNameDisplayed;
		}
		return null;
	}

	public String getCancelFromBarclaysText() {
		Helpers.waitForElementToAppear(driver, donateForm().CANCEL_FROM_BARCLAYS_TEXT, 10, "No cancel from barclays text displayed");
		List<WebElement> cancelBarclays = driver.findElements(CANCEL_FROM_BARCLAYS_TEXT);
		if (cancelBarclays.size() != 0) {
			String cancelBarclaysTextDisplayed = driver.findElement(CANCEL_FROM_BARCLAYS_TEXT).getText();
			return cancelBarclaysTextDisplayed;
		}
		return null;
	}

	public void setCityText(String text) {
		driver.findElement(CITY_FIELD).sendKeys(text);
	}

	public void setCountyText(String text) {
		driver.findElement(COUNTY_FIELD).sendKeys(text);
	}

	public DonatePersonalDetailsForm(WebDriver dr) {
		this.driver = dr;
	}

	public void donationForm() {
		driver.get(EnvironmentConfiguration.getDonateBaseURL());
	}

	public String navigateToDonationForm() throws Exception {
		donationForm();
		driver.get(EnvironmentConfiguration.getSpecificAppealLink()+appealNameOneOffOnly);
		setLogs("Wait for appeal page to be displayed");
		Helpers.waitForJSandJQueryToLoad();
		setLogs("Add custom amount");
		appealDonatePage().setCustomFieldAmountText("10");
		driver.findElement(appealDonatePage().DONATE_BUTTON).click();
		//		driver.findElement(appealDonatePage().APPEAL_BASKET_AMOUNT_ONE).click();
		Thread.sleep(2000);
		Helpers.waitForElementToAppear(driver, DONATION_FORM_THANK_YOU_HEADER, 10, "Donate form Header not displayed after 10 seconds");
		List<WebElement> donateFormHeader = driver.findElements(DONATION_FORM_THANK_YOU_HEADER);
		if (donateFormHeader.size() != 0) {
			setLogs("Got Donate Form Header");

		}
		return null;
	}

	public String getContactPrefPreAmbleTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREAMBLE_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactText = driver.findElements(GET_NEW_CONTACT_PREAMBLE_INFO_TEXT);
		if (contactText.size() != 0) {
			String contactInfoTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREAMBLE_INFO_TEXT).getText();

			return contactInfoTextDisplayed;
		}
		return null;

	}

	public String getContactPrefInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactInfoText = driver.findElements(GET_NEW_CONTACT_PREF_INFO_TEXT);
		if (contactInfoText.size() != 0) {
			String contactInfoTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREF_INFO_TEXT).getText();

			return contactInfoTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForEmailNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREF_EMAIL_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref email error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREF_EMAIL_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREF_EMAIL_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPostNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREF_POST_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref post error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREF_POST_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREF_POST_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPhoneNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref post error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public boolean checkNewContactPostNoPrefAppears() {
		setLogs("check if contact by Post No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_POST_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPostYesPrefAppears() {
		setLogs("check if contact by Post Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_POST_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailNoPrefAppears() {
		setLogs("check if contact by Email No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_EMAIL_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailYesPrefAppears() {
		setLogs("check if contact by Email Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_EMAIL_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneNoPrefAppears() {
		setLogs("check if contact by Phone No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneYesPrefAppears() {
		setLogs("check if contact by Phone Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactInfoTextAppears() {
		setLogs("Check ContactPref info text appears.......");
		return Helpers.waitForIsDisplayed(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10);
	}

	public boolean checkNewContactPreAmbleTextAppears() {
		setLogs("Check if Contact Pref pre amble text appears.......");
		return Helpers.waitForIsDisplayed(driver, GET_NEW_CONTACT_PREAMBLE_INFO_TEXT, 10);
	}

	public boolean checkOldContactYesOptionAppears() {
		setLogs("Check if Contact Yes option appears");
		return Helpers.waitForIsDisplayed(driver, CONTACT_YES_RADIO_BUTTON, 10);
	}

	public boolean checkOldContactNoOptionAppears() {
		setLogs("Check if Contact No option appears");
		return Helpers.waitForIsDisplayed(driver, CONTACT_NO_RADIO_BUTTON, 10);
	}

	public boolean checkDirectDebitAccountNameFieldAppears() {
		setLogs("Check if Account Name field appears");
		return Helpers.waitForIsDisplayed(driver, GET_ACCOUNT_NAME_FIELD, 10);
	}

	public boolean checkIfBasketOneAmountIsSelected() {
		driver.findElement(DONATE_BASKET_AMOUNT_ONE).isSelected();

		return true;

	}

	public boolean checkIfBasketTwoAmountIsSelected() {
		driver.findElement(DONATE_BASKET_AMOUNT_TWO).isSelected();

		return true;

	}

	public boolean checkIfBasketThreeAmountIsSelected() {
		driver.findElement(DONATE_BASKET_AMOUNT_THREE).isSelected();

		return true;

	}

	public void setDirectDebitDetails(String ddName, String ddNumber, String ddSortCode1, String ddSortCode2, String ddSortCode3) {
		setAccountName(ddName);
		setAccountNumber(ddNumber);
		setSortCodeOne(ddSortCode1);
		setSortCodeTwo(ddSortCode2);
		setSortCodeThree(ddSortCode3);
	}

}
