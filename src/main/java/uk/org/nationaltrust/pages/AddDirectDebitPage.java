package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * Created by Nick.Thompson on 09/06/2017.
 */
public class AddDirectDebitPage extends PageBase {

	protected WebDriver driver;

	private static final By MANAGE_DIRECT_DEBIT_DETAILS_HEADER = By.cssSelector("#formContainer > div:nth-child(1) > fieldset > legend > h1");

	public static final By ACCOUNT_NAME_LABEL = By.cssSelector("#accountNameLabel");

	public static final By ACCOUNT_NUMBER_LABEL = By.id("accountNumberLabel");

	public static final By SORT_CODE_LABEL = By.cssSelector("#formContainer > div:nth-child(1) > fieldset > div:nth-child(5) > div > div > label");

	public By ACCOUNT_NAME_FIELD = By.cssSelector("#accountName");

	public By ACCOUNT_NUMBER_FIELD = By.cssSelector("#accountNumber");

	public By SORT_CODE_ONE_FIELD = By.cssSelector("#sortCodeForm\\2e sortCode1");

	public By SORT_CODE_TWO_FIELD = By.cssSelector("#sortCodeForm\\2e sortCode2");

	public By SORT_CODE_THREE_FIELD = By.cssSelector("#sortCodeForm\\2e sortCode3");

	public By PAYMENT_TERM_MONTHLY_CHECKBOX = By.cssSelector("");

	public By PAYMENT_TERM_YEARLY_CHECKBOX = By.cssSelector("");

	public By BANK_DETAILS_SUBMIT_BUTTON = By.cssSelector("#addBankDetails");

	public By CANCEL_LINK = By.id("cancelButton");

	public static By SORT_CODE_MISSING_ERROR = By.cssSelector("#null-errors > div > ul > li");

	public static By ACCOUNT_NUMBER_MISSING_ERROR = By.cssSelector("#accountNumber-error > li");

	public static By ACCOUNT_NAME_MISSING_ERROR = By.cssSelector("#accountName-error > li");

	public static By BAD_ACCOUNT_NAME_ERROR = By.cssSelector("#accountName-error > li");

	public static By SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR = By.cssSelector("#formContainer > div:nth-child(1) > fieldset > div:nth-child(4) > div > div > ul > li > span");

	public void setAccountName(String accountName) {
		driver.findElement(ACCOUNT_NAME_FIELD).sendKeys(accountName);
	}

	public void setAccountNumber(String accountNumber) {
		driver.findElement(ACCOUNT_NUMBER_FIELD).sendKeys(accountNumber);
	}

	public void setSortCodeOne(String sortCode) {
		driver.findElement(SORT_CODE_ONE_FIELD).sendKeys(sortCode);
	}

	public void setSortCodeTwo(String sortCode) {
		driver.findElement(SORT_CODE_TWO_FIELD).sendKeys(sortCode);
	}

	public void setSortCodeThree(String sortCode) {
		driver.findElement(SORT_CODE_THREE_FIELD).sendKeys(sortCode);
	}

	public void setDirectDebitDetails(String ddName, String ddNumber, String ddSortCodeOne, String ddSortCodeTwo, String ddSortCodeThree) {
		setAccountName(ddName);
		setAccountNumber(ddNumber);
		setSortCodeOne(ddSortCodeOne);
		setSortCodeTwo(ddSortCodeTwo);
		setSortCodeThree(ddSortCodeThree);
	}

	public String getSortCodeWrongErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, SORT_CODE_MISSING_ERROR, 10, "Sort code Missing text not displayed");
		List<WebElement> sortCodeErrorText = driver.findElements(SORT_CODE_MISSING_ERROR);
		if (sortCodeErrorText.size() != 0) {
			String sortCodeErrorTextDisplayed = driver.findElement(SORT_CODE_MISSING_ERROR).getText();

			return sortCodeErrorTextDisplayed;
		}
		return null;

	}

	public String getAccountNumberIncompleteErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, ACCOUNT_NUMBER_MISSING_ERROR, 10, "Account Number Missing text not displayed");
		List<WebElement> accountNumberErrorText = driver.findElements(ACCOUNT_NUMBER_MISSING_ERROR);
		if (accountNumberErrorText.size() != 0) {
			String accountNumberErrorTextDisplayed = driver.findElement(ACCOUNT_NUMBER_MISSING_ERROR).getText();

			return accountNumberErrorTextDisplayed;
		}
		return null;

	}

	public String getAccountNameIncompleteErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, ACCOUNT_NAME_MISSING_ERROR, 10, "Account Name Missing text not displayed");
		List<WebElement> accountNameErrorText = driver.findElements(ACCOUNT_NAME_MISSING_ERROR);
		if (accountNameErrorText.size() != 0) {
			String accountNameErrorTextDisplayed = driver.findElement(ACCOUNT_NAME_MISSING_ERROR).getText();

			return accountNameErrorTextDisplayed;
		}
		return null;

	}

	public String getBadAccountNameErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, BAD_ACCOUNT_NAME_ERROR, 10, "Account Name Missing text not displayed");
		List<WebElement> badAccountNameErrorText = driver.findElements(BAD_ACCOUNT_NAME_ERROR);
		if (badAccountNameErrorText.size() != 0) {
			String badAccountNameErrorTextDisplayed = driver.findElement(BAD_ACCOUNT_NAME_ERROR).getText();

			return badAccountNameErrorTextDisplayed;
		}
		return null;

	}

	public String getSortCodeAndAccountNumberErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR, 10, "Bad Account text not displayed");
		List<WebElement> accountCombinationErrorText = driver.findElements(SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR);
		if (accountCombinationErrorText.size() != 0) {
			String accountCombinationErrorTextDisplayed = driver.findElement(SORT_CODE_AND_ACCOUNT_NUMBER_BAD_COMBINATION_ERROR).getText();

			return accountCombinationErrorTextDisplayed;
		}
		return null;

	}

	public String getDirectDebitDetailsHeader() {
		setLogs("Getting the direct debit details label");
		return driver.findElement(MANAGE_DIRECT_DEBIT_DETAILS_HEADER).getText();
	}

	public String getDirectDebitDetailsPageAccountNameLabel() {
		setLogs("Getting the account name label");
		return driver.findElement(ACCOUNT_NAME_LABEL).getText();
	}

	public String getDirectDebitDetailsPageAccountNumberLabel() {
		setLogs("Getting the account number label");
		return driver.findElement(ACCOUNT_NUMBER_LABEL).getText();
	}

	public String getDirectDebitDetailsPageSortCodeLabel() {
		setLogs("Getting the sort code label");
		return driver.findElement(SORT_CODE_LABEL).getText();
	}

	public AddDirectDebitPage(WebDriver dr) {
		this.driver = dr;
	}

}
