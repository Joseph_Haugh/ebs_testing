package uk.org.nationaltrust.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Pramod.Reguri on 17/02/2016.
 */
public class SignoutPage extends PageBase {

	protected WebDriver driver;

	public SignoutPage(WebDriver dr) {
		this.driver = dr;
	}

	public void signOutOfAccount() {
		setLogs("Clicking logout button");
//		clickAccountSettingButton();
		accountSettingsPage().selectAccountSettingsHamburgerOrPawn();
//		driver.navigate().to(baseUrl + "account-settings");
		driver.findElement(signInPage().SIGNOUT_BUTTON).click();
	}

	public void clickAccountSettingButton(){
		List<WebElement> accountSettingsButton = driver.findElements(signInPage().ACCOUNT_SETTINGS_BUTTON);
		if(accountSettingsButton.size()!=0){
			driver.findElement(signInPage().ACCOUNT_SETTINGS_BUTTON).click();
		}

	}
	public void clickAccountSettingLink(){
		List<WebElement> accountSettingsLink = driver.findElements(signInPage().ACCOUNT_SETTINGS_LINK);
		if(accountSettingsLink.size()!=0){
			driver.findElement(signInPage().ACCOUNT_SETTINGS_LINK).click();
		}

	}
}
