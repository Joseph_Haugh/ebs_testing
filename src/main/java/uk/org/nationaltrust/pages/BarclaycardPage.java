package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by nick.thompson on 09/12/2016.
 */
public class BarclaycardPage extends PageBase {

	protected WebDriver driver;

	public BarclaycardPage(WebDriver dr) {
		this.driver = dr;
	}

//	public By VISA_CARD_BUTTON = By.cssSelector("body > form > table > tbody > tr:nth-child(2) > td:nth-child(3) > input:nth-child(4)");

	public By VISA_CARD_BUTTON = By.name("VISA_brand");

	public By AMEX_CARD_BUTTON = By.cssSelector("body > form > table > tbody > tr:nth-child(2) > td:nth-child(3) > input:nth-child(1)");

	public By JCB_CARD_BUTTON = By.cssSelector("body > form > table > tbody > tr:nth-child(3) > td:nth-child(3) > input");

	public By MASTERCARD_BUTTON = By.cssSelector("body > form > table > tbody > tr:nth-child(2) > td:nth-child(3) > input:nth-child(5)");

	public By MAESTRO_CARD_BUTTON = By.cssSelector("body > form > table > tbody > tr:nth-child(3) > td:nth-child(3) > input");

	public By CARDHOLDER_NAME_FIELD = By.cssSelector("#Ecom_Payment_Card_Name");

	public By CARD_NUMBER_FIELD = By.cssSelector("#Ecom_Payment_Card_Number");

	public By EXPIREY_MONTH = By.cssSelector("#Ecom_Payment_Card_ExpDate_Month");

	public By EXPIREY_YEAR = By.cssSelector("#Ecom_Payment_Card_ExpDate_Year");

	public By CARD_VERIFICATION_CODE = By.cssSelector("#Ecom_Payment_Card_Verification");

	public By CONFIRM_MY_PAYMENT_BUTTON = By.cssSelector("#submit3");

	public By BACK_BUTTON = By.cssSelector("#btn_Back");

	public By CANCEL_BUTTON = By.cssSelector("#ncol_cancel");

	public By TOTAL_CHARGE_SHOWN = By.cssSelector("#ncol_ref > tbody > tr:nth-child(2) > td.ncoltxtr");



	public void setVisaCard() {
		Helpers.waitForElementToAppear(driver,VISA_CARD_BUTTON, 60,"VISA Card button not displayed");
		driver.findElement(VISA_CARD_BUTTON).click();
		setLogs("VISA Card selected");
	}

	public void setCardHolderName(String cardName) {
		Helpers.waitForElementToAppear(driver,CARDHOLDER_NAME_FIELD, 30,"Card Holder name not displayed");
		driver.findElement(CARDHOLDER_NAME_FIELD).sendKeys(cardName);
	}

	public void setCardNumber(String cardNumber) {
		driver.findElement(CARD_NUMBER_FIELD).sendKeys(cardNumber);
	}

	public void setCardExpireyDate(String expireyMonth, String expireyYear) {
		driver.findElement(EXPIREY_MONTH).sendKeys(expireyMonth);
		driver.findElement(EXPIREY_YEAR).sendKeys(expireyYear);
	}


	public void setCardVerificationCode (String verificationNumber){
		driver.findElement(CARD_VERIFICATION_CODE).sendKeys(verificationNumber);
	}
	public void selectConfirmMyPaymentButton (){
		driver.findElement(CONFIRM_MY_PAYMENT_BUTTON).click();
	}

	public void selectCardBackButton (){
		driver.findElement(BACK_BUTTON).click();
		}
	public void selectCancelButton (){
		driver.findElement(CANCEL_BUTTON).click();
	}

	public void populateCardDetails(){
		this.setVisaCard();
		this.setCardHolderName("Mr&Mrs Tester");
		this.setCardNumber("4111111111111111");
		this.setCardExpireyDate("01", "2020");
		this.setCardVerificationCode("123");
	}
}
