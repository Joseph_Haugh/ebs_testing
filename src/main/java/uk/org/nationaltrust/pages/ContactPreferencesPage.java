package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * Created by nick.thompson on 09/05/2016.
 */
public class ContactPreferencesPage extends PageBase {

	private WebDriver driver;

	public ContactPreferencesPage(WebDriver dr) {
		this.driver = dr;
	}

	public By CONTACT_PREFERENCE_HEADER_LABEL = By.cssSelector("#tab-contact-preferences > form > fieldset > legend > h1");

	public By CONTACT_PREFERNECE_ACCEPT_CHECKBOX = By.cssSelector("#singleStatementAgreedLabel");

	public By CONTACT_PREFERENCE_CONFIRM_BUTTON = By.id("MyNTUpdateContactPreferencesSubmit");

	public By CONTACT_PREFERENCE_EMAIL_CHECKBOX = By.id("emailAgreedLabel");

	public By CONTACT_PREFERENCE_POST_CHECKBOX = By.id("postAgreedLabel");

	public By CONTACT_PREFERENCE_TEXT_CHECKBOX = By.id("textAgreedLabel");

	public By CONTACT_PREFERENCE_PHONE_CHECKBOX = By.id("phoneAgreedLabel");

	public By CONTACT_PREFERENCE_LETS_KEEP_IN_TOUCH_LABEL = By.cssSelector("#singleStatementContainer > legend");

	public By PRIVACY_POLICY_LINK = By.cssSelector("#mainContent > div > div > main > div > div.dashboard-card.contact-preferences > form > div:nth-child(2) > fieldset > div > p:nth-child(5) > a:nth-child(2)");

	public By GET_CONTACT_BY_PHONE_YES_CHECKBOX = By.id("phoneAgreedYesLabel");

	public By GET_CONTACT_BY_PHONE_NO_CHECKBOX = By.id("phoneAgreedNoLabel");

	public By GET_CONTACT_BY_EMAIL_YES_CHECKBOX = By.id("emailAgreedYesLabel");

	public By GET_CONTACT_BY_EMAIL_NO_CHECKBOX = By.id("emailAgreedNoLabel");

	public By GET_CONTACT_BY_POST_YES_CHECKBOX = By.id("postAgreedYesLabel");

	public By GET_CONTACT_BY_POST_NO_CHECKBOX = By.id("postAgreedNoLabel");

	public By GET_EMAIL_YES_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='emailAgreed']:checked");

	public By GET_POST_YES_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='postAgreed']:checked");

	public By GET_PHONE_YES_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='phoneAgreed']:checked");

	public By GET_TEXT_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='contactPermissions.contactTextConsented']:checked");

	private static final By GET_NEW_CONTACT_PREF_INFO_TEXT = By.id("js-permissionStatementContainer");

	private static final By GET_NEW_CONTACT_PREAMBLE_INFO_TEXT = By.cssSelector("#tab-contact-preferences > form > fieldset > div:nth-child(3) > p:nth-child(1)");

	private static final By GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT = By.id("emailAgreed-errors");

	private static final By GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT = By.id("postAgreed-errors");

	private static final By GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT = By.id("phoneAgreed-errors");

	private static final By GET_INVALID_SUPPORTER_DETAILS_ERROR_TEXT = By.id("phoneAgreed-errors");


	public String getcontactPreferenceHeaderLableText() {
		return driver.findElement(CONTACT_PREFERENCE_HEADER_LABEL).getText();
	}

	public String getContactPreferenceKeepInTouchText() {
		return driver.findElement(CONTACT_PREFERENCE_LETS_KEEP_IN_TOUCH_LABEL).getText();
	}

	public void changeContactPreferencesToAllPreferences() {
		driver.findElement(contactPreferencesDetailsPage().CONTACT_PREFERNECE_ACCEPT_CHECKBOX).click();
		driver.findElement(contactPreferencesDetailsPage().CONTACT_PREFERENCE_CONFIRM_BUTTON).click();

	}

	public boolean checkIfPrivacyPolicyLinkIsDisplayed(){
		setLogs("check if Monthly Direct debit option is displayed.......");
		return Helpers.waitForIsDisplayed(driver,PRIVACY_POLICY_LINK,10);
	}

	public void changeContactPreferencesToNoPreferences() {
		driver.findElement(contactPreferencesDetailsPage().CONTACT_PREFERENCE_EMAIL_CHECKBOX).click();
		Helpers.waitForIsClickable(driver, contactPreferencesDetailsPage().CONTACT_PREFERENCE_POST_CHECKBOX);
		driver.findElement(contactPreferencesDetailsPage().CONTACT_PREFERENCE_POST_CHECKBOX).click();
		Helpers.waitForIsClickable(driver, contactPreferencesDetailsPage().CONTACT_PREFERENCE_TEXT_CHECKBOX);
		driver.findElement(contactPreferencesDetailsPage().CONTACT_PREFERENCE_TEXT_CHECKBOX).click();
		Helpers.waitForIsClickable(driver, contactPreferencesDetailsPage().CONTACT_PREFERENCE_PHONE_CHECKBOX);
		driver.findElement(contactPreferencesDetailsPage().CONTACT_PREFERENCE_PHONE_CHECKBOX).click();
		driver.findElement(contactPreferencesDetailsPage().CONTACT_PREFERENCE_CONFIRM_BUTTON).click();
		Helpers.waitForElementToAppear(driver, userPage().CONTACT_PREFERENCE_UPDATE_PREFERENCE_BUTTON, 10, "Navigate to contact preference screen button not displayed ");
		driver.findElement(userPage().CONTACT_PREFERENCE_UPDATE_PREFERENCE_BUTTON).click();

		//		Helpers.getDisplayedElement(driver, contactPreferencesDetailsPage().CONTACT_PREFERENCE_EMAIL_CHECKBOX).isSelected();
		//		Helpers.getDisplayedElement(driver, contactPreferencesDetailsPage().CONTACT_PREFERENCE_POST_CHECKBOX).isSelected();
		//		Helpers.getDisplayedElement(driver, contactPreferencesDetailsPage().CONTACT_PREFERENCE_TEXT_CHECKBOX).isSelected();
		//		Helpers.getDisplayedElement(driver, contactPreferencesDetailsPage().CONTACT_PREFERENCE_PHONE_CHECKBOX).isSelected();
	}

	public boolean checkNewContactPostNoPrefAppears() {
		setLogs("check if contact by Post No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_POST_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPostYesPrefAppears() {
		setLogs("check if contact by Post Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_POST_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailNoPrefAppears() {
		setLogs("check if contact by Email No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_EMAIL_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailYesPrefAppears() {
		setLogs("check if contact by Email Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_EMAIL_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneNoPrefAppears() {
		setLogs("check if contact by Phone No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneYesPrefAppears() {
		setLogs("check if contact by Phone Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactInfoTextAppears() {
		setLogs("Check ContactPref inf0 text appears.......");
		return Helpers.waitForIsDisplayed(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10);
	}

	public String getContactPrefPreAmbleTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREAMBLE_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactText = driver.findElements(GET_NEW_CONTACT_PREAMBLE_INFO_TEXT);
		if (contactText.size() != 0) {
			String contactPreAmbleTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREAMBLE_INFO_TEXT).getText();

			return contactPreAmbleTextDisplayed;
		}
		return null;

	}

	public String getContactPrefInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactInfoText = driver.findElements(GET_NEW_CONTACT_PREF_INFO_TEXT);
		if (contactInfoText.size() != 0) {
			String contactInfoTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREF_INFO_TEXT).getText();

			return contactInfoTextDisplayed;
		}
		return null;

	}

	public boolean checkOldContactTextPrefAppears() {
		setLogs("check if old contact by Text Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, CONTACT_PREFERENCE_TEXT_CHECKBOX, 10);
	}

	public String getContactPrefForEmailNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPostNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPhoneNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getInvalidSUpporterDetailscombinationErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_INVALID_SUPPORTER_DETAILS_ERROR_TEXT, 10, "invalid supporter error text not displayed");
		List<WebElement> invalidSupporterText = driver.findElements(GET_INVALID_SUPPORTER_DETAILS_ERROR_TEXT);
		if (invalidSupporterText.size() != 0) {
			String invalidSupporterTextDisplayed = driver.findElement(GET_INVALID_SUPPORTER_DETAILS_ERROR_TEXT).getText();

			return invalidSupporterTextDisplayed;
		}
		return null;

	}

//	public String email (){
//		List<WebElement> emailPref = driver.findElements(GET_EMAIL_YES_CHECKBOX_VALUE);
//		if(emailPref.contains("null") ){
//
//			return "null";
//		}
//		else {
//			String getEmailCheckboxValue = driver.findElement(GET_EMAIL_YES_CHECKBOX_VALUE).getAttribute("value");
//			return getEmailCheckboxValue;
//		}

	}



