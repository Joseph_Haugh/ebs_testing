package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by nick.thompson on 26/08/2016.
 */
public class PaymentMethodPage extends PageBase {

	public By GET_PROMOTION_CODE_FIELD = By.id("annualDirectDebitIncentives.promoCode");

	public By GET_PROMOTION_CODE_BUTTON = By
			.name("_eventId_applyPromo"); // By.cssSelector("#main-content > section > div > div > form:nth-child(3) > div > div > div.medium-4.large-2.columns > button");

	public By GET_PAY_BY_MONTHLY_DD_BUTTON = By.id("paymentOption-DIRECT_DEBIT_MONTHLY-label");

	public By GET_PAY_BY_ANNUAL_DD_BUTTON = By.id("paymentOption-DIRECT_DEBIT_ANNUAL-label");

	public By GET_PAY_BY_CREDIT_CARD_BUTTON = By.id("paymentOption-CARD-label");

	public By GET_GIFT_AID_CHECKBOX = By.cssSelector("#giftAidLabel");

	public By GET_JOIN_GIFT_AID_CHECKBOX = By.cssSelector("label[for='giftAidConsented']");

	private static final By GET_GIFT_AID_MAIN_TEXT = By.cssSelector("#js-toggle-giftAidConsented > p:nth-child(1)");

	public By GET_ANNUAL_PROMO_DESCRIPTION_TEXT = By.id("annualPromoCodeDescription");

	public By GET_MONTHLY_PROMO_DESCRIPTION_TEXT = By.id("monthlyPromoCodeDescription");

	public By GET_CUSTOM_PROMO_ERROR_TEXT = By.id("monthlyDirectDebitIncentives.promoCode-error");

	public static final By GET_GIFT_AID_HEADER_TEXT = By.cssSelector("#main-content > section > form > div.group > div:nth-child(2) > div > fieldset > div > div > legend");

	public By GET_PAYMENT_CONTINUE_BUTTON = By.cssSelector("#main-content > section > div > div > form:nth-child(5) > div.button-group.top-separator > button");

	public By CONTINUE_BUTTON = By.id("continueButton");

	public By I_WANT_FREE_GIFT_CHECKBOX = By.cssSelector("#directDebitIncentiveOption1");

	public By I_HAVE_A_PROMO_CHECKBOX = By.cssSelector("label[for='directDebitIncentiveOption3']");

	public static final By APPLY_ANNUAL_PROMO_BUTTON = By.id("annualPromoCodeButton");

	public static final By APPLY_MONTHLY_PROMO_BUTTON = By.id("monthlyPromoCodeButton");

	public By I_DONT_WANT_THE_FREE_GIFT = By.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset:nth-child(2) > div > div:nth-child(4) > label");

	public By GET_CARD_DECLINED_WARNING_TEXT = By.cssSelector("#paymentStatus > div > p");

	public By GET_CARD_PAYMENT_CANCELLED_WARNING_TEXT = By.cssSelector("#paymentStatus > div > p");

	private static final By MONTHLY_DD_PRICE_SHOWN = By
			.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset:nth-child(1) > div:nth-child(1) > div > div:nth-child(1) > label > span > span.pymtOptPrice");

	private static final By ANNUAL_DD_PRICE_SHOWN = By
			.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset:nth-child(1) > div:nth-child(1) > div > div:nth-child(2) > label > span > span.pymtOptPrice");

	private static final By ANNUAL_DD_PRICE_SHOWN_GIFT = By
			.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset:nth-child(1) > div:nth-child(1) > div > div:nth-child(1) > label > span > span.pymtOptPrice");
	//*[@id="main-content"]/section/form/div[2]/div[1]/div/fieldset[1]/div[1]/div/div[1]/label/span/span[1]

	private static final By ANNUAL_CARD_PRICE_SHOWN = By
			.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset:nth-child(1) > div:nth-child(1) > div > div:nth-child(3) > label > span > span.pymtOptPrice");

	private static final By CHOOSE_HOW_TO_PAY_HEADER = By.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset:nth-child(1) > div:nth-child(1) > legend > h2");

	private static final By PAYMENT_BY_CREDIT_OR_DEBIT_CARD_HEADER = By.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset > div:nth-child(1) > legend > h2");

	private static final By LIFE_PAY_BY_CARD_ONLY_INFO_TEXT = By.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset > div:nth-child(1) > p");

	private static final By MAIN_PAGE_ERROR_TEXT = By.cssSelector("#form-errors-alert-box > div > h4");

	private static final By MONTHLY_PROMO_OPTION1 = By.id("monthlyDirectDebitIncentives.incentiveOption1");

	private static final By MONTHLY_PROMO_OPTION2 = By.id("monthlyDirectDebitIncentives.incentiveOption2");

	private static final By MONTHLY_PROMO_OPTION3 = By.id("monthlyDirectDebitIncentives.incentiveOption3");

	private static final By ANNUAL_PROMO1 = By.id("annualDirectDebitIncentives.incentiveOption1");

	private static final By ANNUAL_PROMO2 =By.id("annualDirectDebitIncentives.incentiveOption2");

	private static final By ANNUAL_PROMO3 =By.id("annualDirectDebitIncentives.incentiveOption3");

	protected WebDriver driver;

	public String getMonthlyDDPriceShown() {
		setLogs("Getting the monthly price shown");
		List<WebElement> monthlyDdPrice = driver.findElements(MONTHLY_DD_PRICE_SHOWN);
		if (monthlyDdPrice.size() != 0) {
			String monthlyDdPriceShown = driver.findElement(PaymentMethodPage.MONTHLY_DD_PRICE_SHOWN).getText();
			return monthlyDdPriceShown;
		}
		return null;

	}

	public String getAnnualDDPriceShown() {
		setLogs("Getting the ANNUAL DD Price shown");
		List<WebElement> annualDdPrice = driver.findElements(ANNUAL_DD_PRICE_SHOWN);
		if (annualDdPrice.size() != 0) {
			String annualDDPriceShown = driver.findElement(PaymentMethodPage.ANNUAL_DD_PRICE_SHOWN).getText();
			return annualDDPriceShown;
		}

		return null;
	}

	public String getGiftAidHeader() {
		setLogs("Getting the ANNUAL DD Price shown");
		List<WebElement> giftAidHeader = driver.findElements(GET_GIFT_AID_HEADER_TEXT);
		if (giftAidHeader.size() != 0) {
			String giftAidHeaderTextShown = driver.findElement(PaymentMethodPage.GET_GIFT_AID_HEADER_TEXT).getText();
			return giftAidHeaderTextShown;
		}

		return null;
	}

	public String getGiftAidMainText() {
		setLogs("Getting the main gift aid text");
		List<WebElement> giftAidMainText = driver.findElements(GET_GIFT_AID_MAIN_TEXT);
		if (giftAidMainText.size() != 0) {
			String giftAidMainTextShown = driver.findElement(PaymentMethodPage.GET_GIFT_AID_MAIN_TEXT).getText();
			return giftAidMainTextShown;
		}

		return null;
	}public String getJoinGiftAidMainText() {
		setLogs("Getting the main gift aid text");
		List<WebElement> giftAidMainText = driver.findElements(GET_GIFT_AID_MAIN_TEXT);
		if (giftAidMainText.size() != 0) {
			String giftAidMainTextShown = driver.findElement(PaymentMethodPage.GET_GIFT_AID_MAIN_TEXT).getText();
			return giftAidMainTextShown;
		}

		return null;
	}

	public String getgiftAndJuniorAnnualDDPriceShown() {
		setLogs("Getting the ANNUAL DD Price shown");
		List<WebElement> annualDdPrice = driver.findElements(ANNUAL_DD_PRICE_SHOWN_GIFT);
		if (annualDdPrice.size() != 0) {
			String annualDDPriceShown = driver.findElement(PaymentMethodPage.ANNUAL_DD_PRICE_SHOWN_GIFT).getText();
			return annualDDPriceShown;
		}

		return null;
	}

	public String getAnnualCardPriceShown() {
		setLogs("Getting the Annual Card Price shown");
		List<WebElement> annualCardPrice = driver.findElements(ANNUAL_CARD_PRICE_SHOWN);
		if (annualCardPrice.size() != 0) {
			String annualCardPriceShown = driver.findElement(PaymentMethodPage.ANNUAL_CARD_PRICE_SHOWN).getText();
			return annualCardPriceShown;
		}
		return null;
	}

	public String getPromotionCodeDisplayed() {
		List<WebElement> promoCode = driver.findElements(GET_PROMOTION_CODE_FIELD);
		if (promoCode.size() != 0) {
			String promoCodeText = driver.findElement(GET_PROMOTION_CODE_FIELD).getText();

			return promoCodeText;
		}
		return null;
	}

	public String getAnnualPromotionDescriptionText() {
		List<WebElement> promoDescription = driver.findElements(GET_ANNUAL_PROMO_DESCRIPTION_TEXT);
		if (promoDescription.size() != 0) {
			String promoDescriptionText = driver.findElement(GET_ANNUAL_PROMO_DESCRIPTION_TEXT).getText();
			return promoDescriptionText;
		}
		return null;
	}

	public String getMonthlyPromotionDescriptionText() {
		List<WebElement> promoDescription = driver.findElements(GET_MONTHLY_PROMO_DESCRIPTION_TEXT);
		if (promoDescription.size() != 0) {
			String promoDescriptionText = driver.findElement(GET_MONTHLY_PROMO_DESCRIPTION_TEXT).getText();
			return promoDescriptionText;
		}
		return null;
	}

	public String getPaymentOptionHeaderDisplayed() {
		List<WebElement> payScreenHeader = driver.findElements(CHOOSE_HOW_TO_PAY_HEADER);
		if (payScreenHeader.size() != 0) {
			String payScreenHeaderText = driver.findElement(CHOOSE_HOW_TO_PAY_HEADER).getText();

			return payScreenHeaderText;
		}
		return null;
	}

	public String getPaymentOptionLifeHeaderDisplayed() {
		List<WebElement> payLifeScreenHeader = driver.findElements(PAYMENT_BY_CREDIT_OR_DEBIT_CARD_HEADER);
		if (payLifeScreenHeader.size() != 0) {
			String payLifeScreenHeaderText = driver.findElement(PAYMENT_BY_CREDIT_OR_DEBIT_CARD_HEADER).getText();

			return payLifeScreenHeaderText;
		}
		return null;
	}

	public String getPayByCardOnlyTextDisplayed() {
		List<WebElement> payCardOnly = driver.findElements(LIFE_PAY_BY_CARD_ONLY_INFO_TEXT);
		if (payCardOnly.size() != 0) {
			String payCardOnlyText = driver.findElement(LIFE_PAY_BY_CARD_ONLY_INFO_TEXT).getText();

			return payCardOnlyText;
		}
		return null;
	}

	public String getApplyPromoButtonText() {
		List<WebElement> promoCodeApply = driver.findElements(APPLY_ANNUAL_PROMO_BUTTON);
		if (promoCodeApply.size() != 0) {
			String promoCodeApplyText = driver.findElement(APPLY_ANNUAL_PROMO_BUTTON).getText();

			return promoCodeApplyText;
		}
		return null;
	}

	public String getApplyMonthlyPromoButtonText() {
		List<WebElement> promoCodeApply = driver.findElements(APPLY_MONTHLY_PROMO_BUTTON);
		if (promoCodeApply.size() != 0) {
			String promoCodeApplyText = driver.findElement(APPLY_MONTHLY_PROMO_BUTTON).getText();

			return promoCodeApplyText;
		}
		return null;
	}

	public String getContinueButton() {
		List<WebElement> continueButton = driver.findElements(CONTINUE_BUTTON);
		if (continueButton.size() != 0) {
			driver.findElement(CONTINUE_BUTTON).click();
		}
		return null;
	}

	public String getCardDeclinedWarningTextDisplayed() {
		List<WebElement> cardDeclined = driver.findElements(GET_CARD_DECLINED_WARNING_TEXT);
		if (cardDeclined.size() != 0) {
			String cardDeclinedText = driver.findElement(GET_CARD_DECLINED_WARNING_TEXT).getText();

			return cardDeclinedText;
		}
		return null;
	}

	public String getCardCancelledWarningTextDisplayed() {
		List<WebElement> cardCancelled = driver.findElements(GET_CARD_PAYMENT_CANCELLED_WARNING_TEXT);
		if (cardCancelled.size() != 0) {
			String cardCancelledText = driver.findElement(GET_CARD_PAYMENT_CANCELLED_WARNING_TEXT).getText();

			return cardCancelledText;
		}
		return null;
	}

	public void setPromotionCode(String promo) {
		driver.findElement(GET_PROMOTION_CODE_FIELD).sendKeys(promo);
	}

	public void usePromocode(String promocode) {
		driver.findElement(By.cssSelector("label[for='annualDirectDebitIncentives.incentiveOption3']")).click();
		setPromotionCode(promocode);
		Helpers.waitForElementToAppear(driver, GET_PROMOTION_CODE_BUTTON, 20, "Use code button not displayed");
		driver.findElement(GET_PROMOTION_CODE_BUTTON).click();
	}

	public String getMainErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, MAIN_PAGE_ERROR_TEXT, 10, "Main error text not displayed");
		List<WebElement> mainErrorText = driver.findElements(MAIN_PAGE_ERROR_TEXT);
		if (mainErrorText.size() != 0) {
			String mainErrorTextDisplayed = driver.findElement(MAIN_PAGE_ERROR_TEXT).getText();

			return mainErrorTextDisplayed;
		}
		return null;

	}

	public String getApplyAnnualPromoButton() {
		List<WebElement> applyPromoButton = driver.findElements(APPLY_ANNUAL_PROMO_BUTTON);
		if (applyPromoButton.size() != 0) {
			driver.findElement(APPLY_ANNUAL_PROMO_BUTTON).click();
		}
		return null;
	}

	public String getApplyMonthlyPromoButton() {
		List<WebElement> applyPromoButton = driver.findElements(APPLY_MONTHLY_PROMO_BUTTON);
		if (applyPromoButton.size() != 0) {
			driver.findElement(APPLY_MONTHLY_PROMO_BUTTON).click();
		}
		return null;
	}

	public String getPromoErrorText(){
		List<WebElement> promoError = driver.findElements(GET_CUSTOM_PROMO_ERROR_TEXT);
		if (promoError.size()!=0){
			String errorText = driver.findElement(GET_CUSTOM_PROMO_ERROR_TEXT).getText();
			return errorText;
		}
		return null;
	}

	public PaymentMethodPage(WebDriver dr) {
		this.driver = dr;
	}

	public void setPaymentMethod(String paymentMethod) {
		switch (paymentMethod) {
			case "annualdd":
				driver.findElement(GET_PAY_BY_ANNUAL_DD_BUTTON).click();
				break;
			case "monthlydd":
				driver.findElement(GET_PAY_BY_MONTHLY_DD_BUTTON).click();
				break;
			case "card":
				driver.findElement(GET_PAY_BY_CREDIT_CARD_BUTTON).click();
				break;
		}
		driver.findElement(CONTINUE_BUTTON).click();
	}

	public void setPayAndPromoOption(String promotionOption) {
		switch (promotionOption) {
			case "monthlyddOption1":
				driver.findElement(GET_PAY_BY_MONTHLY_DD_BUTTON).click();
				driver.findElements(By.cssSelector("label[for='monthlyDirectDebitIncentives.incentiveOption1']")).stream()
						.findFirst().ifPresent(WebElement::click);
				break;
			case "monthlyddOption2":
				driver.findElement(GET_PAY_BY_MONTHLY_DD_BUTTON).click();
				driver.findElement(By.cssSelector("label[for='monthlyDirectDebitIncentives.incentiveOption2']")).click();
				break;
			case "monthlyddOption3AddPromo":
				driver.findElement(GET_PAY_BY_MONTHLY_DD_BUTTON).click();
				driver.findElements(By.cssSelector("label[for='monthlyDirectDebitIncentives.incentiveOption3']")).stream()
						.findFirst().ifPresent(WebElement::click);
				driver.findElement(By.id("monthlyDirectDebitIncentives.promoCode")).sendKeys("BINOCS");
				break;
			case "monthlyddOption3AddBadPromo":
				driver.findElement(GET_PAY_BY_MONTHLY_DD_BUTTON).click();
				driver.findElements(By.cssSelector("label[for='monthlyDirectDebitIncentives.incentiveOption3']")).stream()
						.findFirst().ifPresent(WebElement::click);
				driver.findElement(By.id("monthlyDirectDebitIncentives.promoCode")).sendKeys("BI111CS");
				getApplyMonthlyPromoButton();
				break;
			case "monthlyddOption3NoPromo":
				driver.findElement(GET_PAY_BY_MONTHLY_DD_BUTTON).click();
				driver.findElement(By.cssSelector("label[for='monthlyDirectDebitIncentives.incentiveOption3']")).click();
				driver.findElement(By.id("monthlyDirectDebitIncentives.promoCode")).sendKeys("");

			case "monthlyddOptionOnlyCustomPromoBadPromo":
				driver.findElement(GET_PAY_BY_MONTHLY_DD_BUTTON).click();
				driver.findElement(By.id("monthlyDirectDebitIncentives.promoCode")).sendKeys("BI111CS");
				getApplyMonthlyPromoButton();
				break;
			case "monthlyddAddPromo":
				driver.findElement(GET_PAY_BY_MONTHLY_DD_BUTTON).click();
				driver.findElements(By.cssSelector("label[for='monthlyDirectDebitIncentives.incentiveOption3']")).stream()
						.findFirst().ifPresent(WebElement::click);
				driver.findElement(By.id("monthlyDirectDebitIncentives.promoCode")).sendKeys("NT16035M2");
				getApplyMonthlyPromoButton();
				break;
			case "monthlyddNoPromo":
				driver.findElement(GET_PAY_BY_MONTHLY_DD_BUTTON).click();
				driver.findElements(By.cssSelector("label[for='monthlyDirectDebitIncentives.incentiveOption3']")).stream()
						.findFirst().ifPresent(WebElement::click);
				driver.findElement(By.id("monthlyDirectDebitIncentives.promoCode")).sendKeys("");
				break;
			case "annualddOption1":
				driver.findElement(GET_PAY_BY_ANNUAL_DD_BUTTON).click();
				driver.findElement(By.cssSelector("label[for='annualDirectDebitIncentives.incentiveOption1']")).click();
				break;
			case "annualddOption2":
				driver.findElement(GET_PAY_BY_ANNUAL_DD_BUTTON).click();
				driver.findElement(By.cssSelector("label[for='annualDirectDebitIncentives.incentiveOption2']")).click();
				break;
			case "annualddOption3AddPromo":
				driver.findElement(GET_PAY_BY_ANNUAL_DD_BUTTON).click();
				driver.findElement(By.cssSelector("label[for='annualDirectDebitIncentives.incentiveOption3']")).click();
				driver.findElement(By.id("annualDirectDebitIncentives.promoCode")).sendKeys("NT16035M2");
				getApplyAnnualPromoButton();
				break;
			case "annualddOption3NoPromo":
				driver.findElement(GET_PAY_BY_ANNUAL_DD_BUTTON).click();
				driver.findElement(By.cssSelector("label[for='annualDirectDebitIncentives.incentiveOption3']")).click();
				driver.findElement(By.id("annualDirectDebitIncentives.promoCode")).sendKeys("");
				break;
		}
	}

	public void selectAnnualDirectDebit() {
		driver.findElement(GET_PAY_BY_ANNUAL_DD_BUTTON).click();
	}

	public void selectPayByCard() {
		driver.findElement(GET_PAY_BY_CREDIT_CARD_BUTTON).click();
	}

	public boolean checkIfPromocodeSectionIsDisplayed() {
		setLogs("check if promocode section is displayed.......");
		return Helpers.waitForIsDisplayed(driver, I_HAVE_A_PROMO_CHECKBOX, 10);
	}

	public boolean checkIfMonthlyDirectDebitOptionIsDisplayed() {
		setLogs("check if Monthly Direct debit option is displayed.......");
		return Helpers.waitForIsDisplayed(driver, GET_PAY_BY_MONTHLY_DD_BUTTON, 10);
	}

	public boolean giftAidHeaderDisplayed() {
		setLogs("check if Gift Aid Header is displayed.......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_AID_HEADER_TEXT, 10);
	}

	public boolean checkIfAnnualDirectDebitOptionIsDisplayed() {
		setLogs("check if Annual Direct debit option is displayed.......");
		return Helpers.waitForIsDisplayed(driver, GET_PAY_BY_ANNUAL_DD_BUTTON, 10);
	}

	public boolean checkGiftAidCheckboxAppears() {
		setLogs("check if gift aid checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_AID_CHECKBOX, 10);
	}

	public boolean checkMonthlyPromoOption1Appears() {
		setLogs("check if the promo option 1 appears");
		return Helpers.waitForIsDisplayed(driver, MONTHLY_PROMO_OPTION1, 10);
	}
	public boolean checkMonthlyPromoOption2Appears() {
		setLogs("check if the promo option 2 appears");
		return Helpers.waitForIsDisplayed(driver, MONTHLY_PROMO_OPTION2, 10);
	}
}