package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by nick.thompson on 15/12/2017.
 */
public class SelfServePage extends PageBase {

	protected WebDriver driver;

	public SelfServePage (WebDriver dr) {
		this.driver = dr;
	}

	public By CONTACT_PREFERENCE_UPDATE_PREFERENCE_BUTTON = By.cssSelector ("#editContactPreferences");

	public static final By SIGNIN_TAB = By.cssSelector("#mainContent > section > div.row.content-group.nt-palette-primary > div.medium-6.columns.right > ul > li.tab-link.active > a");

	public By USER_EMAIL_TEXT_FILED = By.cssSelector("#loginEmail");

	public static final By USER_PASSWORD_TEXT_FILED = By.cssSelector("input[id=loginPassword]");

	public By SIGN_BUTTON = By.cssSelector("#loginSubmit");

	public By SUCCESSFULLY_SIGNED_OUT_MESSAGE = By.cssSelector(".c-notice.c-notice__info");

	public final By SIGNOUT_BUTTON = By.cssSelector("#MyNTSignOut");

	private static final By HAMBURGER_BUTTON = By.cssSelector(".menu-icon > span");

	public By CURRENT_EMAIL_ADDRESS = By.cssSelector("#account > div.nt-pod__item-content > div.details-list > span.tst-account-pod-email");


	public static final By ACCOUNT_SETTINGS_BUTTON = By.cssSelector("#accountSettingsButton > span.icon.icon--profile");

	public By CONTACT_PREFERNECE_ACCEPT_CHECKBOX = By.cssSelector("#singleStatementAgreedLabel");

	public By GET_CONTACT_BY_PHONE_YES_CHECKBOX = By.id("contactPermissions.contactPhoneConsented1-label");

	public By GET_CONTACT_BY_PHONE_NO_CHECKBOX = By.id("contactPermissions.contactPhoneConsented2-label");

	public By GET_CONTACT_BY_EMAIL_YES_CHECKBOX = By.id("contactPermissions.contactEmailConsented1-label");

	public By GET_CONTACT_BY_EMAIL_NO_CHECKBOX = By.id("contactPermissions.contactEmailConsented2-label");

	public By GET_CONTACT_BY_POST_YES_CHECKBOX = By.id("contactPermissions.contactPostConsented1-label");

	public By GET_CONTACT_BY_POST_NO_CHECKBOX = By.id("contactPermissions.contactPostConsented2-label");

	public By GET_EMAIL_YES_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='emailAgreed']:checked");

	public By GET_POST_YES_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='postAgreed']:checked");

	public By GET_PHONE_YES_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='phoneAgreed']:checked");

	public By GET_TEXT_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='contactPermissions.contactTextConsented']:checked");

	public By ACCOUNT_SETTINGS_HAMBURGER =By.cssSelector("body > div.row > div > header > a.menu-icon");

	public void login(String username, String password) throws Exception {

		navigateToSingInPage();
		setLogs("Entering the email id -- " + username);
		driver.findElement(selfServePage().USER_EMAIL_TEXT_FILED).sendKeys(username);
		setLogs("Entering the passowrd");
		driver.findElement(selfServePage().USER_PASSWORD_TEXT_FILED).sendKeys(password);
		setLogs("Clicking the singIn button");
		//		driver.findElement(By.xpath(SIGN_BUTTON)).click();
		driver.findElement(SIGN_BUTTON).click();

	}

	public void navigateToSingInPage() {
		setLogs("Navigating to signIn  page");
		if (driver.findElements(getSignoutButton()).size() != 0) {
			clickSignOutButton();
			Helpers.waitForIsDisplayed(driver, SUCCESSFULLY_SIGNED_OUT_MESSAGE, 10);
		}
		driver.navigate().to(baseUrlMYNT + "sign-in");
		Helpers.waitForIsDisplayed(driver, SIGNIN_TAB, 10);
	}
	public By getSignoutButton() {
		boolean hamburgerIcon = hamburgerIconDisplay();
		if (hamburgerIcon == true) {
			clickHamBurgerIFDisplayed();
			return SIGNOUT_BUTTON;
		} else if (hamburgerIcon == false) {
			clickAccountSettingButton();
			return SIGNOUT_BUTTON;
		}
		return null;
	}
	private void clickSignOutButton() {
		if (driver.findElements(SIGNOUT_BUTTON).size() != 0)
			driver.findElement(SIGNOUT_BUTTON).click();
	}
	public boolean hamburgerIconDisplay() {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		String dispalyed = js.executeScript("return $('.menu-icon').css('display')").toString();
		if (dispalyed.equals("block")) {
			setLogs("Hamburger icon displayed");
			return true;
		} else {
			setLogs("Hamburger icon not displayed");
			return false;
		}
	}
	public void clickHamBurgerIFDisplayed() {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		String dispalyed = js.executeScript("return $('.menu-icon').css('display')").toString();
		if (dispalyed.equals("block")) {
			setLogs("Clicking the hamburger icon");
			driver.findElement(HAMBURGER_BUTTON).click();
		}
	}
	public void clickAccountSettingButton() {
		List<WebElement> accountSettingsButton = driver.findElements(ACCOUNT_SETTINGS_BUTTON);
		if (accountSettingsButton.size() != 0) {
			driver.findElement(ACCOUNT_SETTINGS_BUTTON).click();
		}
	}

	public String getAccountEmailTextDisplayed() {
		Helpers.waitForElementToAppear(driver, CURRENT_EMAIL_ADDRESS, 10, "Email Address Missing text not displayed");
		List<WebElement> accountEmailText = driver.findElements(CURRENT_EMAIL_ADDRESS);
		if (accountEmailText.size() != 0) {
			String accountEmailTextDisplayed = driver.findElement(CURRENT_EMAIL_ADDRESS).getText();

			return accountEmailTextDisplayed;
		}
		return null;

	}
	public void clickUpdateContactPreferencesButton() {
		driver.findElement (CONTACT_PREFERENCE_UPDATE_PREFERENCE_BUTTON).click ();
	}
	public boolean checkOldContactPrefOptionAppears() {
		setLogs("Check if Contact No option appears");
		return Helpers.waitForIsDisplayed(driver,CONTACT_PREFERNECE_ACCEPT_CHECKBOX , 10);
	}
	public boolean checkNewContactPostNoPrefAppears() {
		setLogs("check if contact by Post No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_POST_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPostYesPrefAppears() {
		setLogs("check if contact by Post Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_POST_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailNoPrefAppears() {
		setLogs("check if contact by Email No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_EMAIL_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailYesPrefAppears() {
		setLogs("check if contact by Email Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_EMAIL_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneNoPrefAppears() {
		setLogs("check if contact by Phone No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneYesPrefAppears() {
		setLogs("check if contact by Phone Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_YES_CHECKBOX, 10);
	}

	public String email (){
		String emailPref = driver.findElement(selfServePage().GET_EMAIL_YES_CHECKBOX_VALUE).getAttribute("value");
		if(emailPref.contains(null) ){

			return "null";
		}
		else {
			String getEmailCheckboxValue = driver.findElement(selfServePage().GET_EMAIL_YES_CHECKBOX_VALUE).getAttribute("value");
			return getEmailCheckboxValue;
		}

	}

	public void selectAccountSettingsHamburgerOrPawn (){
		//		Helpers.waitForIsDisplayed(driver, ACCOUNT_SETTINGS_BUTTON, 50);
		List <WebElement> accountSettingButton = driver.findElements(ACCOUNT_SETTINGS_HAMBURGER);
		if(accountSettingButton.stream().filter(webElement -> webElement.isDisplayed()).count() == 1){
			setLogs ("Getting the account Sign out Button");
			driver.findElement(ACCOUNT_SETTINGS_HAMBURGER).click();
		}
		else driver.findElement(ACCOUNT_SETTINGS_BUTTON).click();
	}
}
