package uk.org.nationaltrust.pages;

import java.util.Map;
import java.util.Set;
import org.openqa.selenium.WebDriver;
import uk.org.nationaltrust.framework.SupporterServiceDBConnection;

/**
 * Created by nick.thompson on 06/03/2017.
 */
public class AbandonedPage extends PageBase {
	protected WebDriver driver;

	SupporterServiceDBConnection SupporterDBConnection = new SupporterServiceDBConnection();

	public Map<String, String> getTheSupporterDetailsForIndividualAbandonedMembFromSupporterInteractionTable(String email, Set<String> labelNames) {
		String sqlSta = abandonedSupporterInDBSql(email);
		setLogs(sqlSta);
		return SupporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String abandonedSupporterInDBSql(String email) {
		System.out.println( "Hello World!" );
		return String.format("select STATE, FIRST_NAME, MEMBERSHIP_APPLICATION_TYPE, EMAIL_ADDRESS, GIFT, ABANDONED_NOTIFICATION_COUNT, CONTACT_CONSENTED FROM" + " SUPPORTERAPI_OWNER.SUPPORTER_INTERACTION" + " WHERE EMAIL_ADDRESS = '%s'" + " order by CREATED desc" ,email);

	}

	public Map<String, String> getTheSupporterDetailsForJuniorAbandonedMembFromSupporterInteractionTable(Set<String> labelNames) {
		String sqlSta = abandonedJuniorSupporterInDBSql("testjun@gmail.com");
		setLogs(sqlSta);
		return SupporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String abandonedJuniorSupporterInDBSql(String email) {
		System.out.println( "Hello World!" );
		return String.format("select STATE, FIRST_NAME, MEMBERSHIP_APPLICATION_TYPE, EMAIL_ADDRESS, GIFT, CONTACT_CONSENTED FROM" + " SUPPORTERAPI_OWNER.SUPPORTER_INTERACTION" + " WHERE EMAIL_ADDRESS = '%s'" + " order by CREATED desc" ,email);

	}

	public Map<String, String> updateTheSupporterDetailsForIndividualAbandonedMembOnSupporterInteractionTable(Set<String> labelNames) {
		String sqlSta = abandonedSupporterUpdateInDBSql("test@gmail.com");
		setLogs(sqlSta);
		return SupporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String abandonedSupporterUpdateInDBSql(String email) {
		System.out.println( "Hello World!" );
		return String.format("Update SUPPORTERAPI_OWNER.SUPPORTER_INTERACTION SET LAST_INTERACTION = '05-MAR-17 07.33.19.354000000' "+" WHERE EMAIL_ADDRESS = '%s' "+"" ,email);

	}

	public AbandonedPage (WebDriver dr) {
		this.driver = dr;
	}
}
