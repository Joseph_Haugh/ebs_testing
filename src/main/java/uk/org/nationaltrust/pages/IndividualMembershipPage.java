package uk.org.nationaltrust.pages;

import static org.openqa.selenium.By.xpath;

import java.time.LocalDate;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

/**
 * @author Pramod
 */
public class IndividualMembershipPage extends PageBase {

	private static final By PERSONAL_DETAILS_LABEL = By.cssSelector(".active-step>.join-steps-content");

	private static final By INDIVIDUAL_MEMBERSHIP_TEXT = By.cssSelector("#main-content > section > div > div > div > h1");

	private static final By NAME_LABEL = By.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset > div > legend");

	private static final By YOUR_DETAILS_HEADER_LABEL = By.cssSelector("h2.nt-form-subheading");

	private static final By TITLE_LABEL = By.cssSelector(".large-4>label[for*=title]>span:nth-of-type(1)");

	private static final By TITLE_LABEL_REQUIRED_INDICATOR = By.cssSelector(".large-4>label[for*=title]>span:nth-of-type(2)");

	private static final By FIRST_NAME_REQUIRED_INDICATOR = By.cssSelector("label[for*=firstName] > span[class=required]");

	private static final By ADDRESS_LINE_1_MISSING_ERROR = By.cssSelector("#address\\2e addressLine1-error > li > span");

	private static final By GIFT_START_DATE_INVALID_ERROR = By.cssSelector("#giftPackStartDate-errors > ul > li > span");

	private static final By GIFT_START_DAE_INFORMATIVE_TEXT = By.cssSelector("#main-content > section > form > div.group > div:nth-child(6) > div > fieldset > div > p:nth-child(7)");

	private static final By LEAD_DOB_INVALID_ERROR = By.cssSelector("#dateOfBirth-errors > ul > li > span");

	public By PERSONAL_DETAILS_CONTINUE_BUTTON = By.cssSelector("#main-content > section > form > div:nth-child(3) > div > button");

	public By GIFT_GIVER_PERRSONAL_DETAILS_CONTINUE_BUTTON = By.cssSelector("#main-content > section > form > div:nth-child(3) > div > button");

	public By GET_CONTACT_YOU_YES_CHECKBOX = By.cssSelector("#main-content > section > form > div.group > div:nth-child(5) > div > fieldset > div > div > div:nth-child(3) > label");

	public By GET_CONTACT_YOU_NO_CHECKBOX = By.cssSelector("#main-content > section > form > div:nth-child(2) > div:nth-child(5) > div > fieldset > div > div > div:nth-child(4) > label");

	public By GET_CONTACT_BY_PHONE_YES_CHECKBOX = By.id("contactPermissions.contactPhoneConsented1-label");

	public By GET_CONTACT_BY_PHONE_NO_CHECKBOX = By.id("contactPermissions.contactPhoneConsented2-label");

	public By GET_CONTACT_BY_EMAIL_YES_CHECKBOX = By.id("contactPermissions.contactEmailConsented1-label");


	public By GET_CONTACT_BY_EMAIL_NO_CHECKBOX = By.id("contactPermissions.contactEmailConsented2-label");

	public By GET_CONTACT_BY_POST_YES_CHECKBOX = By.id("contactPermissions.contactPostConsented1-label");

	public By GET_CONTACT_BY_POST_NO_CHECKBOX = By.id("contactPermissions.contactPostConsented2-label");

	public By GET_EMAIL_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='contactPermissions.contactEmailConsented']:checked");

	public By GET_POST_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='contactPermissions.contactPostConsented']:checked");

	public By GET_PHONE_CHECKBOX_VALUE = By.cssSelector("input[type=radio][name='contactPermissions.contactPhoneConsented']:checked");

	public By GET_GIFT_CONTACT_BY_PHONE_YES_CHECKBOX = By.id("giftGiver.contactPermissions.contactPhoneConsented1-label");

	public By GET_GIFT_CONTACT_BY_PHONE_NO_CHECKBOX = By.id("giftGiver.contactPermissions.contactPhoneConsented2-label");

	public By GET_GIFT_CONTACT_BY_EMAIL_YES_CHECKBOX = By.id("giftGiver.contactPermissions.contactEmailConsented1-label");

	public By GET_GIFT_CONTACT_BY_EMAIL_NO_CHECKBOX = By.id("giftGiver.contactPermissions.contactEmailConsented2-label");

	public By GET_GIFT_CONTACT_BY_POST_YES_CHECKBOX = By.id("giftGiver.contactPermissions.contactPostConsented1-label");

	public By GET_GIFT_CONTACT_BY_POST_NO_CHECKBOX = By.id("giftGiver.contactPermissions.contactPostConsented2-label");

	public By DATE_PICKER_OPTION = By.cssSelector("#datePickerBtn > svg");

	public By DATE_PICKER_WITH_TODAYS_DATE = xpath("//span[contains(text(), '23')]");

	public By DATE_PICKER_WITH_VARIABLE_DATE = xpath("//span[contains(text(), '23')]");

	public By DATE_PICKER_MONTH_FORWARD = By.cssSelector("tr.monthSelector > td:nth-child(3)");

			public By DATE_PICKER_MONTH_BACKWARDS = By.cssSelector("tr.monthSelector > td:nth-child(1)");

	private static final By GET_CONTACTING_YOU_HEADER_DISPLAYED = By.cssSelector("#permission-statement > legend");

	private static final By GET_NEW_CONTACT_PREAMBLE_INFO_TEXT = By.id("permission-statement-description");

	private static final By GET_NEW_CONTACT_PREF_INFO_TEXT = By.id("js-permissionStatementContainer");

	private static final By GET_PRIVACY_POLICY_TEXT = By.id("fairProcessingNoticeText");

	private static final By GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT = By.id("contactEmailConsented-errors");

	private static final By GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT = By.id("contactPostConsented-errors");

	private static final By GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT = By.id("contactPhoneConsented-errors");

	public By GET_MEMBERSHIP_PACK_TO_ME_CHECKBOX = By.cssSelector("label[for='giftPackRecipient-GIFT_GIVER']");

	public By GET_MEMBERSHIP_PACK_TO_RECIPIENT_CHECKBOX = By.cssSelector("label[for='giftPackRecipient-GIFT_RECIPIENT']");

	//	public By GET_TITLE_FIELD = By.cssSelector("#name\\2e title");
	public By GET_TITLE_FIELD = By.id("name.title");

	public By GET_GIFT_GIVER_TITLE_FIELD = By.id("giftGiver.name.title");

	//	public By GET_FIRST_NAME_FIELD = By.cssSelector("#name\\2e firstName");
	public By GET_FIRST_NAME_FIELD = By.id("name.firstName");

	public By GET_GIFT_GIVER_FIRST_NAME_FIELD = By.id("giftGiver.name.firstName");

	//	public By GET_LAST_NAME_FIELD = By.cssSelector("#name\\2e lastName");
	public By GET_LAST_NAME_FIELD = By.id("name.lastName");

	public By GET_GIFT_GIVER_LAST_NAME_FIELD = By.id("giftGiver.name.lastName");

	//	public By GET_DOB_DAY_FIELD = By.cssSelector("#dateOfBirth\\2e day");
	public By GET_DOB_DAY_FIELD = By.id("dateOfBirth.day");

	public By GET_GIFT_GIVER_DOB_DAY_FIELD = By.id("giftGiver.dateOfBirth.day");

	//	public By GET_DOB_MONTH_FIELD = By.cssSelector("#dateOfBirth\\2e month");
	public By GET_DOB_MONTH_FIELD = By.id("dateOfBirth.month");

	public By GET_GIFT_GIVER_DOB_MONTH_FIELD = By.id("giftGiver.dateOfBirth.month");

	//	public By GET_DOB_YEAR_FIELD = By.cssSelector("#dateOfBirth\\2e year");
	public By GET_DOB_YEAR_FIELD = By.id("dateOfBirth.year");

	public By GET_GIFT_GIVER_DOB_YEAR_FIELD = By.id("giftGiver.dateOfBirth.year");

	//	public By GET_ADDRESS_LINE_1_FIELD = By.cssSelector("#address\\2e addressLine1");
	public By GET_ADDRESS_LINE_1_FIELD = By.id("address.addressLine1");

	public By GET_GIFT_GIVER_ADDRESS_LINE_1_FIELD = By.id("giftGiver.address.addressLine1");

	//	public By GET_ADDRESS_LINE_2_FIELD = By.cssSelector("#address\\2e addressLine2");
	public By GET_ADDRESS_LINE_2_FIELD = By.id("address.addressLine2");

	public By GET_GIFT_GIVER_ADDRESS_LINE_2_FIELD = By.id("giftGiver.address.addressLine2");

	//	public By GET_ADDRESS_LINE_3_FIELD = By.cssSelector("#address\\2e addressLine3");
	public By GET_ADDRESS_LINE_3_FIELD = By.id("address.addressLine3");

	public By GET_GIFT_GIVER_ADDRESS_LINE_3_FIELD = By.id("giftGiver.address.addressLine3");

	//	public By GET_ADDRESS_LINE_4_FIELD = By.cssSelector("#address\\2e addressLine4");
	public By GET_ADDRESS_LINE_4_FIELD = By.id("address.addressLine4");

	public By GET_GIFT_GIVER_ADDRESS_LINE_4_FIELD = By.id("giftGiver.address.addressLine4");

	//	public By GET_ADDRESS_CITY_FIELD = By.cssSelector("#address\\2e city");
	public By GET_ADDRESS_CITY_FIELD = By.id("address.city");

	public By GET_GIFT_GIVER_ADDRESS_CITY_FIELD = By.id("giftGiver.address.city");

	//	public By GET_ADDRESS_COUNTY_FIELD = By.cssSelector("#address\\2e county");
	public By GET_ADDRESS_COUNTY_FIELD = By.id("address.county");

	public By GET_GIFT_GIVER_ADDRESS_COUNTY_FIELD = By.id("giftGiver.address.county");

	//	public By GET_ADDRESS_POSTCODE_FIELD = By.cssSelector("#address\\2e postcode");
	public By GET_ADDRESS_POSTCODE_FIELD = By.id("address.postcode");

	public By GET_GIFT_GIVER_POSTCODE_FIELD = By.id("giftGiver.address.postcode");

	//	public By GET_PRIMARY_PHONE_NUMBER_FIELD = By.cssSelector("#contactDetails\\2e preferredTelephoneNumber");
	public By GET_PRIMARY_PHONE_NUMBER_FIELD = By.id("contactDetails.preferredTelephoneNumber");

	public By GET_GIFT_GIVER_PRIMARY_PHONE_NUMBER_FIELD = By.id("giftGiver.contactDetails.preferredTelephoneNumber");

	public By GET_RECIPIENT_PHONE_NUMBER_FIELD = By.id("giftRecipientContactDetails.telephoneNumber");

	//	public By GET_ALTERNATIVE_PHONE_NUMBER_FIELD = By.cssSelector("#contactDetails\\2e alternativeTelephoneNumber");
	public By GET_ALTERNATIVE_PHONE_NUMBER_FIELD = By.id("contactDetails.alternativeTelephoneNumber");

	public By GET_GIFT_GIVER_ALTERNATIVE_PHONE_NUMBER_FIELD = By.id("giftGiver.contactDetails.alternativeTelephoneNumber");

	//	public By GET_EMAIL_ADDRESS_FIELD = By.cssSelector("#contactDetails\\2e emailAddress");
	public By GET_EMAIL_ADDRESS_FIELD = By.id("contactDetails.emailAddress");

	public By GET_GIFT_GIVER_EMAIL_ADDRESS_FIELD = By.id("giftGiver.contactDetails.emailAddress");

	public By GET_GIFT_START_DAY_FIELD = By.id("giftPackStartDate.day");

	public By GET_GIFT_START_MONTH_FIELD = By.id("giftPackStartDate.month");

	public By GET_GIFT_START_YEAR_FIELD = By.id("giftPackStartDate.year");

	public By GET_ENTER_ADDRESS_MANUALLY_LINK = By.id("postcodeManualLink");

	protected WebDriver driver;

	public IndividualMembershipPage(WebDriver dr) {
		this.driver = dr;
	}

	public void navigateToIndividualMemberShipPage() {
		setLogs("Navigating to the Individual membership page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("individualUrl"));
	}

	public void navigateToIndividualLifeMemberShipPage() {
		setLogs("Navigating to the Individual membership page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("individualLifeURL"));
	}

	public void navigateToIndividualLifeGiftMemberShipPage() {
		setLogs("Navigating to the Individual membership page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("individualLifeGiftURL"));
	}

	public void navigateToIndividualGiftMemberShipPage() {
		setLogs("Navigating to the Individual membership page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("individualGiftUrl"));
	}

	public void navigateToSessionTimeoutPage() {
		setLogs("Navigating to the Individual membership page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("sessionTimeoutURL"));
	}

	public String getPersonalDetailsText() {
		navigateToIndividualMemberShipPage();
		setLogs("Getting the personal details text");
		return Helpers.getText(driver, PERSONAL_DETAILS_LABEL);
	}

	public String getIndividualMemberShipHeaderText() {
		navigateToIndividualMemberShipPage();
		Helpers.waitForIsClickable(driver, INDIVIDUAL_MEMBERSHIP_TEXT);
		setLogs("Getting the personal details text");
		return Helpers.getText(driver, INDIVIDUAL_MEMBERSHIP_TEXT);
	}

	public String getGiftStartDateInvalidText() {
		setLogs("Getting invalid Gift start date error text");
		return Helpers.getText(driver, GIFT_START_DATE_INVALID_ERROR);
	}

	public String getGiftStartDateInformativeText() {
		setLogs("Getting Gift Start date informative text");
		return Helpers.getText(driver, GIFT_START_DAE_INFORMATIVE_TEXT);
	}

	public String getLeadDOBInvalidText() {
		setLogs("Getting Lead DOB error text");
		return Helpers.getText(driver, LEAD_DOB_INVALID_ERROR);
	}

	public String getYourDetailsLabelText() {
		navigateToIndividualMemberShipPage();
		setLogs("Getting the your details header label text");
		return Helpers.getText(driver, YOUR_DETAILS_HEADER_LABEL);
	}

	public String getHeaderLabelText() {
		setLogs("Getting the your details header label text");
		return Helpers.getText(driver, INDIVIDUAL_MEMBERSHIP_TEXT);
	}

	public String getNameLabelText() {
		navigateToIndividualMemberShipPage();
		setLogs("Getting the name label text");
		return Helpers.getText(driver, NAME_LABEL);

	}

	public String getTitleLabelText() {
		navigateToIndividualMemberShipPage();
		setLogs("Getting the title label text");
		return Helpers.getText(driver, TITLE_LABEL);
	}

	public String getTitleLabelRequiredIndicator() {
		navigateToIndividualMemberShipPage();
		setLogs("Getting the title field required");
		return Helpers.getText(driver, TITLE_LABEL_REQUIRED_INDICATOR);
	}

	public String getFirstNameLabelRequiredIndicator() {
		navigateToIndividualMemberShipPage();
		setLogs("Getting the first name field required indicator");
		return Helpers.getText(driver, FIRST_NAME_REQUIRED_INDICATOR);
	}

	public String getAddressLine1RequiredError() throws Exception {
		navigateToIndividualMemberShipPage();
		addPersonalDetailsForLead("Mr", "Zephr", "Bishop", "10", "12", "1974", "", "Paved with Gold", "Hill with a view", "Walled Garden", "Swindon", "Wiltshire", "SN22NA", "01793123730", "",
				"bishop1234@gmail.com");
		driver.findElement(individualMembershipPage().GET_CONTACT_BY_EMAIL_YES_CHECKBOX).click();
		driver.findElement(individualMembershipPage().GET_CONTACT_BY_POST_YES_CHECKBOX).click();
		driver.findElement(individualMembershipPage().GET_CONTACT_BY_PHONE_YES_CHECKBOX).click();
		driver.findElement(individualMembershipPage().PERSONAL_DETAILS_CONTINUE_BUTTON).click();
		setLogs("Getting the Address Line 1 missing error");
		Thread.sleep(2000);
		return Helpers.getText(driver, ADDRESS_LINE_1_MISSING_ERROR);
	}

	public void clickPersonalDetailsContinueButton() {
		driver.findElement(PERSONAL_DETAILS_CONTINUE_BUTTON).click();
	}

	public void setTitle(String title) {
		Helpers.waitForIsClickable(driver, individualMembershipPage().GET_TITLE_FIELD, 10);
		driver.findElement(individualMembershipPage().GET_TITLE_FIELD).sendKeys(title);
	}

	public void setGiftGiverTitle(String title) {
		driver.findElement(GET_GIFT_GIVER_TITLE_FIELD).sendKeys(title);
	}

	public void setFirstName(String firstName) {
		driver.findElement(individualMembershipPage().GET_FIRST_NAME_FIELD).sendKeys(firstName);
	}

	public void setGiftGiverFirstName(String firstName) {
		driver.findElement(GET_GIFT_GIVER_FIRST_NAME_FIELD).sendKeys(firstName);
	}

	public void setLastName(String lastName) {
		driver.findElement(individualMembershipPage().GET_LAST_NAME_FIELD).sendKeys(lastName);
	}

	public void setGiftGiverLastName(String lastName) {
		driver.findElement(GET_GIFT_GIVER_LAST_NAME_FIELD).sendKeys(lastName);
	}

	public void setDOBDay(String DOBDay) {
		driver.findElement(individualMembershipPage().GET_DOB_DAY_FIELD).sendKeys(DOBDay);
	}

	public void setGiftGiverDOBDay(String DOBDay) {
		driver.findElement(GET_GIFT_GIVER_DOB_DAY_FIELD).sendKeys(DOBDay);
	}

	public void setDOBMonth(String DOBMonth) {
		driver.findElement(individualMembershipPage().GET_DOB_MONTH_FIELD).sendKeys(DOBMonth);
	}

	public void setGiftGiverDOBMonth(String DOBMonth) {
		driver.findElement(GET_GIFT_GIVER_DOB_MONTH_FIELD).sendKeys(DOBMonth);
	}

	public void setDOBYear(String DOBYear) {
		driver.findElement(individualMembershipPage().GET_DOB_YEAR_FIELD).sendKeys(DOBYear);
	}

	public void setGiftGiverDOBYear(String DOBYear) {
		driver.findElement(GET_GIFT_GIVER_DOB_YEAR_FIELD).sendKeys(DOBYear);
	}

	public void setAddressLine1(String addressLine1) {
		driver.findElement(individualMembershipPage().GET_ADDRESS_LINE_1_FIELD).sendKeys(addressLine1);
	}

	public boolean checkIfAddressSectionIsDisplayed() {
		setLogs("check if address section is displayed.......");
		return Helpers.waitForIsDisplayed(driver, GET_ADDRESS_LINE_1_FIELD, 10);
	}

	public void setGiftGiverAddressLine1(String addressLine1) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_LINE_1_FIELD).sendKeys(addressLine1);
	}

	public void setAddressLine2(String addressLine2) {
		driver.findElement(individualMembershipPage().GET_ADDRESS_LINE_2_FIELD).sendKeys(addressLine2);
	}

	public void setGiftGiverAddressLine2(String addressLine2) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_LINE_2_FIELD).sendKeys(addressLine2);
	}

	public void setAddressLine3(String addressLine3) {
		driver.findElement(individualMembershipPage().GET_ADDRESS_LINE_3_FIELD).sendKeys(addressLine3);
	}

	public void setGiftGiverAddressLine3(String addressLine3) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_LINE_3_FIELD).sendKeys(addressLine3);
	}

	public void setAddressLine4(String addressLine4) {
		driver.findElement(individualMembershipPage().GET_ADDRESS_LINE_4_FIELD).sendKeys(addressLine4);
	}

	public void setGiftGiverAddressLine4(String addressLine4) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_LINE_4_FIELD).sendKeys(addressLine4);
	}

	public void setAddressCity(String addressCity) {
		driver.findElement(individualMembershipPage().GET_ADDRESS_CITY_FIELD).sendKeys(addressCity);
	}

	public void setGiftGiverAddressCity(String addressCity) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_CITY_FIELD).sendKeys(addressCity);
	}

	public void setAddressCounty(String addressCounty) {
		driver.findElement(individualMembershipPage().GET_ADDRESS_COUNTY_FIELD).sendKeys(addressCounty);
	}

	public void setGiftGiverAddressCounty(String addressCounty) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_COUNTY_FIELD).sendKeys(addressCounty);
	}

	public void setAddressPostcode(String addressPostcode) {
		driver.findElement(individualMembershipPage().GET_ADDRESS_POSTCODE_FIELD).sendKeys(addressPostcode);
	}

	public void setGiftGiverAddressPostcode(String addressPostcode) {
		driver.findElement(GET_GIFT_GIVER_POSTCODE_FIELD).sendKeys(addressPostcode);
	}

	public void setPrimaryPhone(String primaryPhone) {
		driver.findElement(individualMembershipPage().GET_PRIMARY_PHONE_NUMBER_FIELD).sendKeys(primaryPhone);
	}

	public void setGiftGiverPrimaryPhone(String primaryPhone) {
		driver.findElement(GET_GIFT_GIVER_PRIMARY_PHONE_NUMBER_FIELD).sendKeys(primaryPhone);
	}

	public void setRecipientPhone(String recipientPhone) {
		driver.findElement(GET_RECIPIENT_PHONE_NUMBER_FIELD).sendKeys(recipientPhone);
	}

	public void setGiftGiverAlternativePhone(String alternativePhone) {
		driver.findElement(GET_GIFT_GIVER_ALTERNATIVE_PHONE_NUMBER_FIELD).sendKeys(alternativePhone);
	}

	public void setAlternativePhone(String alternativePhone) {
		driver.findElement(individualMembershipPage().GET_ALTERNATIVE_PHONE_NUMBER_FIELD).sendKeys(alternativePhone);
	}

	public void setEmailAddress(String emailAddress) {
		driver.findElement(individualMembershipPage().GET_EMAIL_ADDRESS_FIELD).sendKeys(emailAddress);
	}

	public void setGiftGiverEmailAddress(String emailAddress) {
		driver.findElement(GET_GIFT_GIVER_EMAIL_ADDRESS_FIELD).sendKeys(emailAddress);
	}

	public void selectContactYesButton() {
		driver.findElement(GET_CONTACT_YOU_YES_CHECKBOX).click();
	}

	public void selectContactNoButton() {
		driver.findElement(GET_CONTACT_YOU_NO_CHECKBOX).click();
	}

	public void selectSendMeGift() {
		driver.findElement(GET_MEMBERSHIP_PACK_TO_ME_CHECKBOX).click();
	}

	public void selectSendRecipientGift() {
		driver.findElement(GET_MEMBERSHIP_PACK_TO_RECIPIENT_CHECKBOX).click();
	}

	public void selectGiftGiverPersonalDetailsContinueBtn() {
		driver.findElement(GIFT_GIVER_PERRSONAL_DETAILS_CONTINUE_BUTTON).click();
	}

	public void setGiftPackDay(String giftDay) {
		driver.findElement(GET_GIFT_START_DAY_FIELD).sendKeys(giftDay);
	}

	public void openDatePicker(){driver.findElement(DATE_PICKER_OPTION).click();}
	public void setGiftPackDateToTodaysDate(){driver.findElement(DATE_PICKER_WITH_TODAYS_DATE).click();}

	public void setGiftPackMonth(String giftMonth) {
		driver.findElement(GET_GIFT_START_MONTH_FIELD).sendKeys(giftMonth);
	}

	public void setGiftPackYear(String giftYear) {
		driver.findElement(GET_GIFT_START_YEAR_FIELD).sendKeys(giftYear);
	}

	public void addPersonalDetailsForLead(String title, String firstName, String lastName, String DOBDay, String DOBMonth, String DOBYear, String addressLine1, String addressLine2,
			String addressLine3, String addressLine4, String addressCity, String addressCounty, String addressPostcode, String primaryPhone, String alternativePhone, String emailAddress) {
		setTitle(title);
		setFirstName(firstName);
		setLastName(lastName);
		setDOBDay(DOBDay);
		setDOBMonth(DOBMonth);
		setDOBYear(DOBYear);
		driver.findElement(individualMembershipPage().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		setAddressLine1(addressLine1);
		setAddressLine2(addressLine2);
		setAddressLine3(addressLine3);
		setAddressLine4(addressLine4);
		setAddressCity(addressCity);
		setAddressCounty(addressCounty);
		setAddressPostcode(addressPostcode);
		setPrimaryPhone(primaryPhone);
		setAlternativePhone(alternativePhone);
		setEmailAddress(emailAddress);

	}

	public void setRecipientPersonalDetails(String title, String firstName, String lastName, String DOBDay, String DOBMonth, String DOBYear, String addressLine1, String addressLine2,
			String addressLine3, String addressLine4, String addressCity, String addressCounty, String addressPostcode, String recipientPhone) {
		setTitle(title);
		setFirstName(firstName);
		setLastName(lastName);
		setDOBDay(DOBDay);
		setDOBMonth(DOBMonth);
		setDOBYear(DOBYear);
		driver.findElement(individualMembershipPage().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		setAddressLine1(addressLine1);
		setAddressLine2(addressLine2);
		setAddressLine3(addressLine3);
		setAddressLine4(addressLine4);
		setAddressCity(addressCity);
		setAddressCounty(addressCounty);
		setAddressPostcode(addressPostcode);
		setRecipientPhone(recipientPhone);

	}

	public void setGiftGiverPersonalDetails(String title, String firstName, String lastName, String DOBDay, String DOBMonth, String DOBYear, String addressLine1, String addressLine2,
			String addressLine3, String addressLine4, String addressCity, String addressCounty, String addressPostcode, String primaryPhone, String alternativePhone, String emailAddress,
			String giftDay, String giftMonth, String giftYear) {
		setGiftGiverTitle(title);
		setGiftGiverFirstName(firstName);
		setGiftGiverLastName(lastName);
		setGiftGiverDOBDay(DOBDay);
		setGiftGiverDOBMonth(DOBMonth);
		setGiftGiverDOBYear(DOBYear);
		driver.findElement(individualMembershipPage().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		setGiftGiverAddressLine1(addressLine1);
		setGiftGiverAddressLine2(addressLine2);
		setGiftGiverAddressLine3(addressLine3);
		setGiftGiverAddressLine4(addressLine4);
		setGiftGiverAddressCity(addressCity);
		setGiftGiverAddressCounty(addressCounty);
		setGiftGiverAddressPostcode(addressPostcode);
		setGiftGiverPrimaryPhone(primaryPhone);
		setGiftGiverAlternativePhone(alternativePhone);
		setGiftGiverEmailAddress(emailAddress);

//		driver.findElement(individualMembershipPage().DATE_PICKER_MONTH_FORWARD).click();


	}

	public void setGiftGiverPersonalDetailsEmailContactYes(String title, String firstName, String lastName, String DOBDay, String DOBMonth, String DOBYear, String addressLine1, String addressLine2,
			String addressLine3, String addressLine4, String addressCity, String addressCounty, String addressPostcode, String primaryPhone, String alternativePhone, String emailAddress,
			String giftDay, String giftMonth, String giftYear) {
		int currentDay=LocalDate.now().getDayOfMonth();
		setGiftGiverTitle(title);
		setGiftGiverFirstName(firstName);
		setGiftGiverLastName(lastName);
		setGiftGiverDOBDay(DOBDay);
		setGiftGiverDOBMonth(DOBMonth);
		setGiftGiverDOBYear(DOBYear);
		driver.findElement(individualMembershipPage().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		setGiftGiverAddressLine1(addressLine1);
		setGiftGiverAddressLine2(addressLine2);
		setGiftGiverAddressLine3(addressLine3);
		setGiftGiverAddressLine4(addressLine4);
		setGiftGiverAddressCity(addressCity);
		setGiftGiverAddressCounty(addressCounty);
		setGiftGiverAddressPostcode(addressPostcode);
		setGiftGiverPrimaryPhone(primaryPhone);
		setGiftGiverAlternativePhone(alternativePhone);
		setGiftGiverEmailAddress(emailAddress);
		driver.findElement(individualMembershipPage().GET_GIFT_CONTACT_BY_EMAIL_YES_CHECKBOX).click();
		driver.findElement(individualMembershipPage().GET_GIFT_CONTACT_BY_POST_NO_CHECKBOX).click();
		driver.findElement(individualMembershipPage().GET_GIFT_CONTACT_BY_PHONE_NO_CHECKBOX).click();
		selectSendRecipientGift();
		driver.findElement(individualMembershipPage().DATE_PICKER_OPTION).click();
		this.selectDateOnDatePicker(currentDay);


	}

	public boolean checkNewContactPostNoPrefAppears() {
		setLogs("check if contact by Post No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_POST_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPostYesPrefAppears() {
		setLogs("check if contact by Post Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_POST_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailNoPrefAppears() {
		setLogs("check if contact by Email No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_EMAIL_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailYesPrefAppears() {
		setLogs("check if contact by Email Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_EMAIL_YES_CHECKBOX, 10);
	}

	public boolean checkIndividualHeaderExists() {
		setLogs("check ifIndividual Header is displayed......");
		return Helpers.waitForIsDisplayed(driver, INDIVIDUAL_MEMBERSHIP_TEXT, 10);
	}

	public boolean checkNewContactPhoneNoPrefAppears() {
		setLogs("check if contact by Phone No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneYesPrefAppears() {
		setLogs("check if contact by Phone Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactInfoTextAppears() {
		setLogs("Check ContactPref inf0 text appears.......");
		return Helpers.waitForIsDisplayed(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10);
	}

	public boolean checkOldContactYesOptionAppears() {
		setLogs("Check if Contact Yes option appears");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_YOU_YES_CHECKBOX, 10);
	}

	public boolean checkOldContactNoOptionAppears() {
		setLogs("Check if Contact No option appears");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_YOU_NO_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactPostNoPrefAppears() {
		setLogs("check if contact by Post No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_POST_NO_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactPostYesPrefAppears() {
		setLogs("check if contact by Post Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_POST_YES_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactEmailNoPrefAppears() {
		setLogs("check if contact by Email No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_EMAIL_NO_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactEmailYesPrefAppears() {
		setLogs("check if contact by Email Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_EMAIL_YES_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactPhoneNoPrefAppears() {
		setLogs("check if contact by Phone No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_PHONE_NO_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactPhoneYesPrefAppears() {
		setLogs("check if contact by Phone Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_PHONE_YES_CHECKBOX, 10);
	}


	public String getContactPrefContactingYouHeaderDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACTING_YOU_HEADER_DISPLAYED, 10, "Contact Header text not displayed");
		List<WebElement> contactHeaderText = driver.findElements(GET_CONTACTING_YOU_HEADER_DISPLAYED);
		if (contactHeaderText.size() != 0) {
			String contactHeaderTextDisplayed = driver.findElement(GET_CONTACTING_YOU_HEADER_DISPLAYED).getText();

			return contactHeaderTextDisplayed;
		}
		return null;

	}

	public String getContactPrefPreAmbleTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREAMBLE_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactText = driver.findElements(GET_NEW_CONTACT_PREAMBLE_INFO_TEXT);
		if (contactText.size() != 0) {
			String contactInfoTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREAMBLE_INFO_TEXT).getText();

			return contactInfoTextDisplayed;
		}
		return null;

	}

	public String getContactPrefInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactInfoText = driver.findElements(GET_NEW_CONTACT_PREF_INFO_TEXT);
		if (contactInfoText.size() != 0) {
			String contactInfoTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREF_INFO_TEXT).getText();

			return contactInfoTextDisplayed;
		}
		return null;

	}

	public String getContactPrivacyPolicyInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_PRIVACY_POLICY_TEXT, 10, "Privacy policy Info text not displayed");
		List<WebElement> privacyInfoText = driver.findElements(GET_PRIVACY_POLICY_TEXT);
		if (privacyInfoText.size() != 0) {
			String privacyInfoTextDisplayed = driver.findElement(GET_PRIVACY_POLICY_TEXT).getText();

			return privacyInfoTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForEmailNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_EMAIL_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPostNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_POST_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public String getContactPrefForPhoneNotSelectedErrorTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT, 10, "Contact Pref error text not displayed");
		List<WebElement> contactErrorText = driver.findElements(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT);
		if (contactErrorText.size() != 0) {
			String contactErrorTextDisplayed = driver.findElement(GET_CONTACT_PREFS_PHONE_NOT_SELECTED_ERROR_TEXT).getText();

			return contactErrorTextDisplayed;
		}
		return null;

	}

	public void setGiftGiverPersonalDetailsNoPrefs(String title, String firstName, String lastName, String DOBDay, String DOBMonth, String DOBYear, String addressLine1, String addressLine2,
			String addressLine3, String addressLine4, String addressCity, String addressCounty, String addressPostcode, String primaryPhone, String alternativePhone, String emailAddress,
			String giftDay, String giftMonth, String giftYear) {
		setGiftGiverTitle(title);
		setGiftGiverFirstName(firstName);
		setGiftGiverLastName(lastName);
		setGiftGiverDOBDay(DOBDay);
		setGiftGiverDOBMonth(DOBMonth);
		setGiftGiverDOBYear(DOBYear);
		driver.findElement(individualMembershipPage().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		setGiftGiverAddressLine1(addressLine1);
		setGiftGiverAddressLine2(addressLine2);
		setGiftGiverAddressLine3(addressLine3);
		setGiftGiverAddressLine4(addressLine4);
		setGiftGiverAddressCity(addressCity);
		setGiftGiverAddressCounty(addressCounty);
		setGiftGiverAddressPostcode(addressPostcode);
		setGiftGiverPrimaryPhone(primaryPhone);
		setGiftGiverAlternativePhone(alternativePhone);
		setGiftGiverEmailAddress(emailAddress);
		selectSendRecipientGift();


	}

	public void selectDateOnDatePicker (int day){

		By selectorForDay = xpath("//td[contains(@class,'day')]/span[text() = "+day+"]");
		driver.findElement(selectorForDay).click();

	}
}