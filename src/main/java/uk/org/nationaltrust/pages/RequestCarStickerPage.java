package uk.org.nationaltrust.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by nick.thompson on 16/06/2016.
 */
public class RequestCarStickerPage extends PageBase {
    protected WebDriver driver;

    public By REQUEST_CAR_STICKER_LINK = By.cssSelector("#carStickerLink");
    public By REQUEST_CAR_STICKER_BUTTON = By.id("MyNTRequestCarStickerSubmit");
    public RequestCarStickerPage(WebDriver dr) {this.driver = dr;}

    public String getRequestCarStickerButtonText(){
        return driver.findElement(REQUEST_CAR_STICKER_BUTTON).getText();
    }
    private By REQUEST_REPLACEMENT_STICKER_PREVIOUSLY_ORDERED_TEXT = By.cssSelector("#mainContent > section > div > main > div > div.dashboard-card.request-car-sticker > form > div.alert-box.icon-block.info.icon-info > p");

    private By REQUEST_CAR_STICKER_HEADER = By.cssSelector("#content > section > div > main > div > h1");
    public String getSendMeCarStickerPageHeaderText(){
        return driver.findElement(REQUEST_CAR_STICKER_HEADER).getText();
    }
    public String getRequestMembershipStickerPreviouslyOrderedText(){
        String replacementParking = driver.findElement(REQUEST_REPLACEMENT_STICKER_PREVIOUSLY_ORDERED_TEXT).getText();
        return replacementParking;
}

	public void selectConfirmCarStickerButton() {
		List<WebElement> confirmCarStickerButton = driver.findElements(REQUEST_CAR_STICKER_BUTTON);
		if (confirmCarStickerButton.size() != 0) {
			driver.findElement(REQUEST_CAR_STICKER_BUTTON).click();
		} else {
			setLogs("Confirm car sticker button not displayed");
		}
	}

}
