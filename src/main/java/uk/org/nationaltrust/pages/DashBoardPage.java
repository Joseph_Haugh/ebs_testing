package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.Helpers;

import java.util.List;

/**
 * Created by Pramod.Reguri on 22/02/2016.
 */
public class DashBoardPage extends PageBase {

	protected WebDriver driver;

    public By CONTACT_PREFERENCE_UPDATE_PREFERENCE_BUTTON = By.id("tab-results-contact-preferences");

	public By SEE_SHOP_ORDERS_BUTTON = By.cssSelector ("#shop-link");

    private static final By DIRECT_DEBIT_PAYMENTS_POD_LABEL = By.cssSelector ("#directDebit > .dashboard-card-heading>h3");

    private static final By DIRECT_DEBIT_PAYMENTS_POD_DESCRIPTION = By.cssSelector ("#directDebit>.c-dashboard-card__inner>p");

    private static final By DIRECT_DEBIT_PAYMENTS_MANAGE_DIRECT_DEBITS_LINK = By.cssSelector ("#direct-debit-link");

    private static final By DIRECT_DEBIT_PAYMENTS_POD = By.cssSelector ("#directDebit > div.dashboard-card-heading > h3");

    private static final By DIRECT_DEBIT_PAYMENTS_MANAGE_DIRECT_DEBITS_LINK_DISABLED = By.cssSelector ("#direct-debit-link-disabled");

    public static final By PERSONAL_DETAILS_HEADER = By.cssSelector ("#personalDetails > .dashboard-card-heading>h3");

    public static final By USER_ACCOUNT_SETTINGS_BUTTON = By.cssSelector ("#accountSettingsButton");

    public By SEND_ME_DIRECT_DEBIT_LINK = By.cssSelector ("#direct-debit-link");

	public By SEND_ME_NEW_DIRECT_DEBIT_LINK = By.cssSelector("#direct-debit-link");

	public static final By DashboardHamburgerMenu=By.xpath("//a[@class='menu-icon']");

public static By CONTACT_PREF_UPDATE_NOTICE = By.cssSelector("#content > section > div.row > div.nt-wrapper-s--trbl > div.small-12.columns > dialog");


	public static By CONTACT_PREF_UPDATE_NOTICE_BUTTON = By.cssSelector("#content > section > div.row > div.nt-wrapper-s--trbl > div.small-12.columns > dialog > a");

	public By SEND_ME_CAR_STICKER_REQUEST_CONFIRM_TEXT = By.cssSelector("#tab-request-a > div > div.c-dashboard-card__inner > div > p");
    public By SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT = By.cssSelector("#tab-my-memberships > div > div.alert-box.info > div");
//	public By SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT = By.xpath("//div[@class='tst-dd-success']");
//	(By.xpath("//span[@class='nt-appeal-sticky-donate__desc']")
	//*[@id="tab-my-memberships"]/div/div[2]/div
	//*[@id="main-content"]/div[8]/div/span/span[1]



    public By SEND_ME_UPDATE_ACCOUNT_SETTINGS_CONFIRM_TEXT = By.cssSelector ("#tab-name-and-email > div > div");
    public By SEND_ME_UPDATE_PERSONAL_DETAILS_CONFIRM_TEXT = By.cssSelector ("#tab-your-details > div > div");
	public By SEND_ME_UPDATE_CONTACT_PREFERENCE_CONFIRM_TEXT = By.cssSelector("#tab-contact-preferences > div > div");

    public static By MEMBERSHIP_POD_MORE_DETAILS_BUTTON = By.cssSelector ("#membership-0-details-link");


    public static By MEMBERSHIP_TYPE_DISPLAYED = By.className("tst-membership-pod-type");


    public static By MEMBERSHIP_TYPE_DISPLAYED_CHILD = By.cssSelector ("h5[id*='personalMembership']");

    public static By MEMBERSHIP_TYPE_DISPLAYED_LIFE = By.cssSelector ("h5[id*=personalMembership]");

    public static By MEMBERSHIP_NUMBER_DISPLAYED = By.className("tst-membership-pod-supporter-number");
	//*[@id="personalMembership427644570SupporterNumber"]

    public static By MEMBERSHIP_ADDRESS_LINE1 = By.cssSelector ("#personalDetails > div.c-dashboard-card__inner > div.row > div:nth-child(1) > dl > dd:nth-child(4)");


	public By I_HAVE_MORE_DETAILS_LINK = By.cssSelector("#membership-0-details-link");

    public static By MEMBERSHIP_NUMBER_DISPLAYED_CHILD = By.cssSelector ("h5[id*=personalMembership]");

    public static String MEMBERSHIP_NUMBER_DISPLAYED_LIFE = "//*[@id=\"personal-membership-319382089-supporter-number\"]";

    public static String MEMBERSHIP_NUMBER_DISPLAYED_IND = "//*[@id=\"personal-membership-445693370-supporter-number\"]";

    public static String ADDRESS_FIELD_FIELD_LABLE = "//dt[contains( . , 'Address')]";

    private By REQUEST_CAR_STICKER_HEADER = By.cssSelector ("#main-content > section > div > main > div > div.row > header > h1");

	public By REQUEST_CAR_STICKER_LINK = By.id("carStickerLink");

	public By REQUEST_A_TAB = By.cssSelector("#tab-results-request-a");

    private By REQUEST_REPLACEMENT_CARD_HEADER = By.cssSelector ("#main-content > section > div > main > div > div.row > header > h1");

    public static String DATE_OF_BIRTH_FIELD_LABLE = "//dt[contains( . , 'Date of Birth')]";

    public static String CONTACT_FIELD_LABLE = "//dt[contains( . , 'Contact details')]";


    public static By DASHBOARD_WELCOME_HEADER = By.id("welcomeMessage");

	private By USER_NAME_FIELD_DETAILS = By.cssSelector ("div[id*=personalDetails] > div.c-dashboard-card__inner>div.row > div:nth-of-type(1) > dl > dd:nth-of-type(1)");


    public By NAME_FIELD_LABLE = By.cssSelector ("div[id*=personalDetails] > .c-dashboard-card__inner > div > div:nth-of-type(1) >dl > dt:nth-of-type(1)");

    public By DATE_OF_BIRTH_DETAILS = By.cssSelector ("div[id*=personalDetails] > div.c-dashboard-card__inner>div.row > div:nth-of-type(2) > dl > dd:nth-of-type(1)");

    public By CONTACT_NUMBER_OF_MEMBER = By.id ("preferredTelephoneNumber.number");

    public By CONTACT_EMAIL_ID_OF_MEMBER = By.cssSelector ("#preferredEmailAddress");

    public static String EDIT_DETAILS_BUTTON = "//*[@id='editPersonalDetails']";

    private By MEMBERSHIP_POD_HEADER_LABLE = By.cssSelector("#membership > div.nt-pod__item-content > div.nt-pod__heading-container > div");

	private By SHOP_POD_HEADER_LABLE = By.cssSelector ("#shopPod > div.dashboard-card-heading > h3");

    private By DATE_OF_BIRTH_DETAILS_HEADER_LABEL = By.cssSelector ("#tab-your-details > form > fieldset:nth-child(3) > legend");

    private By NAME_HEADER_LABEL = By.cssSelector ("#personalDetails  > .c-dashboard-card__inner > div.row > div:nth-of-type(1) > dl>dt:nth-of-type(1)");

    private By CONTACT_DETAILS_HEADER_LABEL = By.cssSelector ("#personalDetails > div:nth-child(1) > div.row > div:nth-child(2) > dl > dt:nth-child(3)");

    private String MEMBERSHIP_TYPE_HEADER_LABLE = "//dt[contains( . , 'Type')]";

    private By MEMBERSHIP_NUMBER_HEADER_LABLE = By.cssSelector ("div[id=personalMemberships] > div > div > div.c-dashboard-card__inner > div.row >div > dl > dt");

    private By MEMBERSHIP_REVEWAL_HEADER_LABLE = By.cssSelector("#membership > div.nt-pod__item-content > div.c-dashboard-card__inner > div > div:nth-child(4) > ul > li > span:nth-child(1)");


    public By MEMBERSHIP_RENEWAL_DATE_DISPLAYED = By.className("tst-membership-pod-renewal-date");


    public By MEMBERSHIP_RENEWAL_DATE_DISPLAYED_IND = By.className("tst-membership-pod-renewal-date");

    public By MEMBERSHIP_RENEWAL_DATE_DISPLAYED_CHILD = By.className("tst-membership-pod-renewal-date");


public By RENEWAL_BUTTON = By.className("tst-renew-btn");

    public By MEMBERSHIP_POD = By.id("membership");

    public String PERSONAL_DETAILS_POD = "//*[@id='personalDetails']/div";

    public By CONTACT_PREFERENCE_POD = By.cssSelector ("#contactPreferences");

	public By SHOP_ORDERS_POD = By.id("shopPod");

    public By SEND_ME_POD_HEADER_LABEL = By.cssSelector ("#tab-request-a > h1");

    public By DOWNLOAD_GETTING_HERE_GUIDE = By.cssSelector ("#getting-here-link");

    public By SEND_ME_CAR_STICKER_LINK = By.cssSelector ("#carStickerLink");

    public By SEND_ME_REPLACE_CARD_LINK = By.cssSelector ("#replacementCardLink");

    public By SEND_ME_REPLACE_CARD_AGAIN = By.cssSelector("#replacementCardLink");

    public By EDIT_PERSONAL_DETAILS_BUTTON = By.id("tab-results-your-details");

    public static By IM_A_MEMBER_BUTTON = By.id("matchSupporterLink");

    public By SEE_YOUR_MEMBERSHIPS_BUTTON = By.id("membershipsLink");

    public By SEE_YOUR_ACCOUNT_SETTINGS_PAGE = By.id("accountLink");

	public By SIDE_NAV_DASHBOARD_OPTION = By.cssSelector("body > main > div.row.nt-side-nav__button-container > div > button > svg > path");

	public By STATIC_SIDE_NAV_DASHBOARD = By.cssSelector("#dashboardDetails > span.left-nav-label");
	private By LEAD_MEMBERSHIP_TYPE = By.cssSelector("#myDetailsMembershipType");

	public static By DD_MEMBERSHIP_IN_RENEWAL_TEST = By.cssSelector ("#autoDDMessage > p");

    public DashBoardPage(WebDriver dr) {
        this.driver = dr;
    }

	public String getWelcomeHeaderDisplayed (){
		List<WebElement> welcomeHeaderText = driver.findElements(DASHBOARD_WELCOME_HEADER);
		if (welcomeHeaderText.size() !=0){
			String welcomeHeaderTextDisplayed = driver.findElement(DASHBOARD_WELCOME_HEADER).getText();

			return welcomeHeaderTextDisplayed;
		}
		return null;

	}

    public String getUserName() {
        String userName = driver.findElement (USER_NAME_FIELD_DETAILS).getText ();
        setLogs ("Getting the name of the member -- " + userName);
        return userName;
    }

    public void clickContactPreferencesTab() {
        driver.findElement (CONTACT_PREFERENCE_UPDATE_PREFERENCE_BUTTON).click ();
    }

    public String getUserNameFieldLable() {
        String userNameFieldLable = driver.findElement (NAME_FIELD_LABLE).getText ();
        setLogs ("Getting the user name lable -- " + userNameFieldLable);
        return userNameFieldLable;
    }

    public String getContactDetailsFieldLable() {
        String contactDetailsLable = driver.findElement (CONTACT_DETAILS_HEADER_LABEL).getText ();
        setLogs ("Getting Contact Details Header Label");
        return contactDetailsLable;
    }

    public String getUserDateOfBirth() {
        String dateOfBirth = driver.findElement (DATE_OF_BIRTH_DETAILS).getText ();
        setLogs ("Getting the member data of birth details -- " + dateOfBirth);
        return dateOfBirth;
    }

    public String getUserContactNumber() {
        String memberContactNumber = driver.findElement (CONTACT_NUMBER_OF_MEMBER).getText ();
        setLogs ("Getting the member contact phone number -- " + memberContactNumber);
        return memberContactNumber;
    }
    public String getUserAddressLine1(){
        String membershipAddressLine1Displayed = driver.findElement (MEMBERSHIP_ADDRESS_LINE1).getText ();
        setLogs ("Getting Address line 1 Displayed -- " + membershipAddressLine1Displayed);
        return membershipAddressLine1Displayed;
    }

    public String getUserContactEmailId() {
        String memberContactEmailId = driver.findElement (CONTACT_EMAIL_ID_OF_MEMBER).getText ();
        setLogs ("Getting the member contact email id -- " + memberContactEmailId);
        return memberContactEmailId;
    }

    public String getUserAdderssFieldLable() {

        String userAddressFieldLable = driver.findElement (By.xpath (ADDRESS_FIELD_FIELD_LABLE)).getText ();
        setLogs ("Getting the address lable -- " + userAddressFieldLable);
        return userAddressFieldLable;
    }

    public String getUserContactFieldLable() {
        String userContactFieldLable = driver.findElement (By.xpath (CONTACT_FIELD_LABLE)).getText ();
        setLogs ("Getting the contact Details field lable -- " + userContactFieldLable);
        return userContactFieldLable;
    }

    public String getEditDetailsButtonText() {
        String editDetailsButtonText = driver.findElement (By.xpath (EDIT_DETAILS_BUTTON)).getText ();
        setLogs ("Getting the contact field lable -- " + editDetailsButtonText);
        return editDetailsButtonText;
    }

    public String getMemberShipHeaderText() {
        String memberShipPodHeader = driver.findElement (MEMBERSHIP_POD_HEADER_LABLE).getText ();
        setLogs ("Getting the membership pod header lable -- " + memberShipPodHeader);
        return memberShipPodHeader;
    }

    public String getDOBHeaderText() {
        String dOBHeader = driver.findElement (DATE_OF_BIRTH_DETAILS_HEADER_LABEL).getText ();
        setLogs ("Getting the DOB Header Label -- " + dOBHeader);
        return dOBHeader;
    }

    public String getNameHeaderText() {
        String nameHeader = driver.findElement (NAME_HEADER_LABEL).getText ();
        setLogs ("Getting the Name Header Label -- " + nameHeader);
        return nameHeader;
    }

    public String getMemberShipTypeHeaderText() {
        String memberShipTypeHeader = driver.findElement (By.xpath (MEMBERSHIP_TYPE_HEADER_LABLE)).getText ();
        setLogs ("Getting the membership type field lable -- " + memberShipTypeHeader);
        return memberShipTypeHeader;

    }

    public String getMemberShipNumberHeaderText() {
        String memberShipNumberHeader = driver.findElement (MEMBERSHIP_NUMBER_HEADER_LABLE).getText ();
        setLogs ("Getting the membership type field lable -- " + memberShipNumberHeader);
        return memberShipNumberHeader;
    }

    public String getMemberShipRenewalHeaderText() {
        String memberShipRenewalHeader = driver.findElement (MEMBERSHIP_REVEWAL_HEADER_LABLE).getText ();
        setLogs ("Getting the membership type field lable -- " + memberShipRenewalHeader);
        return memberShipRenewalHeader;
    }

    public String getMemberShipTypeDisplayed() {
        String memberShipTypeDisplayed = driver.findElement (MEMBERSHIP_TYPE_DISPLAYED).getText ();
        setLogs ("Getting the membership type displayed -- " + memberShipTypeDisplayed);
        return memberShipTypeDisplayed;
    }

    public String getMemberShipTypeDisplayedChild() {
        String memberShipTypeDisplayed = driver.findElement (MEMBERSHIP_TYPE_DISPLAYED_CHILD).getText ();
        setLogs ("Getting the membership type displayed -- " + memberShipTypeDisplayed);
        return memberShipTypeDisplayed;
    }

    public String getMembershipTypeDisplayedLife() {
        String memberShipTypeDisplayed = driver.findElement (MEMBERSHIP_TYPE_DISPLAYED_LIFE).getText ();
        setLogs ("Getting the membership type displayed -- " + memberShipTypeDisplayed);
        return memberShipTypeDisplayed;
    }

    public String getMembershipTypeDisplayedInd() {
        String memberShipTypeDisplayed = driver.findElement (MEMBERSHIP_TYPE_DISPLAYED).getText ();
        setLogs ("Getting the membership type displayed -- " + memberShipTypeDisplayed);
        return memberShipTypeDisplayed;
    }

    public String getMemberShipNumberDisplayed() {
        String memberShipNumberDisplayed = driver.findElement (MEMBERSHIP_NUMBER_DISPLAYED).getText ();
        setLogs ("Getting the membership number displayed -- " + memberShipNumberDisplayed);
        return memberShipNumberDisplayed;
    }

    public String getMembershipNumberDisplayedChild() {
        String memberShipNumberDisplayed = driver.findElement (MEMBERSHIP_NUMBER_DISPLAYED).getText ();
        setLogs ("Getting the membership number displayed -- " + memberShipNumberDisplayed);
        return memberShipNumberDisplayed;
    }


    public String getMembershipNumberDisplayedLife() {
        String memberShipNumberDisplayed = driver.findElement (MEMBERSHIP_NUMBER_DISPLAYED).getText ();
        setLogs ("Getting the membership number displayed -- " + memberShipNumberDisplayed);
        return memberShipNumberDisplayed;
    }

    public String getMembershipNumberDisplayedInd() {
        String memberShipNumberDisplayed = driver.findElement (MEMBERSHIP_NUMBER_DISPLAYED).getText ();
        setLogs ("Getting the membership number displayed -- " + memberShipNumberDisplayed);
        return memberShipNumberDisplayed;
    }

    public String getMemberShipRenewalDateDisplayed() {

        String memberShipRenewalDateDisplayed = driver.findElement (MEMBERSHIP_RENEWAL_DATE_DISPLAYED).getText ();
        setLogs ("Getting the membership number displayed -- " + memberShipRenewalDateDisplayed);
        return memberShipRenewalDateDisplayed;
    }

    public String getMemberShipRenewalDateDisplayedInd() {
        String memberShipRenewalDateDisplayed = driver.findElement (MEMBERSHIP_RENEWAL_DATE_DISPLAYED_IND).getText ();
        setLogs ("Getting the membership number displayed -- " + memberShipRenewalDateDisplayed);
        return memberShipRenewalDateDisplayed;
    }

    public String getMembershipRenewalDataDisplayedForChild() {
        String memberShipRenewalDateDisplayed = driver.findElement (MEMBERSHIP_RENEWAL_DATE_DISPLAYED_CHILD).getText ();
        setLogs ("Getting the membership number displayed -- " + memberShipRenewalDateDisplayed);
        return memberShipRenewalDateDisplayed;
    }

    public String getsendMeTabHeaderLableText() {
        setLogs ("Getting send me pod header text");
        Helpers.waitForIsDisplayed (driver, SEND_ME_POD_HEADER_LABEL, DEFAULT_TIMEOUT);
        return driver.findElement (SEND_ME_POD_HEADER_LABEL).getText ();
    }

    public String getGettingHereGuideText() {
        return driver.findElement (DOWNLOAD_GETTING_HERE_GUIDE).getText ();
    }

    public String getSendMeCarStickerLinkText() {
        return driver.findElement (SEND_ME_CAR_STICKER_LINK).getText ();
    }

    public String getSendMeReplacementCardLinkText() {
        return driver.findElement (SEND_ME_REPLACE_CARD_LINK).getText ();
    }
    //	public String getSendMeCarStickerPageHeaderText(){
    //		return driver.findElement(REQUEST_CAR_STICKER_HEADER).getText();
    //	}
    //	public String getSendMeReplacementCardHeaderText(){
    //		return driver.findElement(REQUEST_REPLACEMENT_CARD_HEADER).getText();
    //	}
    //	public String getRequestMembershipCardButtonText(){
    //		return driver.findElement(REQUEST_MEMBERSHIP_CARD_BUTTON).getText();
    //	}
    //	public String getRequestCarStickerButtonText(){
    //		return driver.findElement(REQUEST_CAR_STICKER_BUTTON).getText();
    //	}
    //
    //	public String getSendMeReplacementCard(){
    //		return driver.findElement(SEND_ME_REPLACE_CARD_LINK).getText();
    //	}

    public boolean getTheMemberShipPod() {
        boolean memberShipPod = Helpers.isDisplayed (driver, MEMBERSHIP_POD);
        return memberShipPod;
    }



    public boolean getThePersonalDetailsPod() {
        boolean personalDetailsPod = Helpers.isDisplayed (driver, By.xpath (PERSONAL_DETAILS_POD));
        return personalDetailsPod;
    }

	public boolean getTheYourDetailsTab() {
		boolean yourDetailsTab = Helpers.isDisplayed (driver, EDIT_PERSONAL_DETAILS_BUTTON);
		return yourDetailsTab;
	}



    public boolean getTheContactPreferencePod() {
        boolean ContactPreferencePod = Helpers.isDisplayed (driver, CONTACT_PREFERENCE_POD);
        return ContactPreferencePod;
    }

	public boolean getTheShopOrdersPod() {
		boolean ContactPreferencePod = Helpers.isDisplayed (driver, SHOP_ORDERS_POD);
		return ContactPreferencePod;
	}

	public boolean getWelcomeHeader(){
    	boolean welcomeHeader = Helpers.isDisplayed(driver, DASHBOARD_WELCOME_HEADER);
    	return welcomeHeader;
	}

	public boolean getTheMembershipType() {
		boolean memberShipPod = Helpers.isDisplayed (driver, LEAD_MEMBERSHIP_TYPE);
		return memberShipPod;
	}



	public String getShopPodHeaderText() {
		Helpers.waitForElementToAppear(driver, SHOP_POD_HEADER_LABLE, 10, "Shop Pod Header not displayed");
		List<WebElement> shopHeaderText = driver.findElements(SHOP_POD_HEADER_LABLE);
		if (shopHeaderText.size() != 0) {
			String shopHeaderTextDisplayed = driver.findElement(SHOP_POD_HEADER_LABLE).getText();

			return shopHeaderTextDisplayed;
		}
		return null;

	}

	public void clickSeeShopOrdersButton() {
		driver.findElement (SEE_SHOP_ORDERS_BUTTON).click ();
	}

	public void clickImAMemberButton(){driver.findElement(IM_A_MEMBER_BUTTON).click();}

    public String getDirectDebitPaymentsTitleLabel() {
        setLogs ("Getting the direct debit pod title");
        String directDebitLabel = driver.findElement (DIRECT_DEBIT_PAYMENTS_POD_LABEL).getText ();
        return directDebitLabel;

    }

    public String getDirectDebitPaymentsPodDescription() {
        setLogs ("Getting the direct debits payment pod description");
        String directDebitPodDescription = driver.findElement (DIRECT_DEBIT_PAYMENTS_POD_DESCRIPTION).getText ();
        return directDebitPodDescription;
    }

    public String getDirectDebitPaymentsPodManageDirectDebitsLinkLabel() {
		setLogs("Getting the manage direct debits  link label");
		Helpers.waitForElementToAppear(driver, userPage().DIRECT_DEBIT_PAYMENTS_MANAGE_DIRECT_DEBITS_LINK, 10, "no direct debit button displayed");
		List<WebElement> directDebitButton = driver.findElements(DIRECT_DEBIT_PAYMENTS_MANAGE_DIRECT_DEBITS_LINK);
		if (directDebitButton.size() != 0) {
			String manageDirectDebitsLabel = driver.findElement(DIRECT_DEBIT_PAYMENTS_MANAGE_DIRECT_DEBITS_LINK).getText();
			return manageDirectDebitsLabel;
		}
		return null;
	}

	public String getCarStickerRequestConfirmationText() {
		setLogs("Getting the Car sticker Ordered text");
		Helpers.waitForElementToAppear(driver, userPage().SEND_ME_CAR_STICKER_REQUEST_CONFIRM_TEXT, 10, "No car sticker confirmation text displayed");
		List<WebElement> carStickerRequest = driver.findElements(SEND_ME_CAR_STICKER_REQUEST_CONFIRM_TEXT);
		if (carStickerRequest.size() != 0) {
			String carStickerRequestText = driver.findElement(SEND_ME_CAR_STICKER_REQUEST_CONFIRM_TEXT).getText();
			return carStickerRequestText;
		}
		return null;
	}

    public String getDirectDebitPaymentsPodManageDirectDebitsLink() {
        setLogs ("Getting the manage direct debits  link");
        String manageDirectDebitsLink = driver.findElement (DIRECT_DEBIT_PAYMENTS_MANAGE_DIRECT_DEBITS_LINK).getAttribute ("href");
        return manageDirectDebitsLink;
    }

//    public String getDirectDebitUpdateSuccessText() {
//        return driver.findElement (SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT).getText ();
//    }

	public String getDirectDebitUpdateSuccessText() {
		setLogs("Getting the manage direct debits  link label");
		Helpers.waitForElementToAppear(driver, userPage().SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT, 10, "no direct debit success message displayed");
		List<WebElement> directDebitUpdate = driver.findElements(SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT);
		if (directDebitUpdate.size() != 0) {
			String directDebitUpdatetext = driver.findElement(SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT).getText();
			return directDebitUpdatetext;
		}
		return null;
	}

    public String getUpdateAccountSettingsSuccessText() {
        return driver.findElement (SEND_ME_UPDATE_ACCOUNT_SETTINGS_CONFIRM_TEXT).getText ();
    }
    public String getUpdatePersonalDetailsSuccessText (){
        return driver.findElement (SEND_ME_UPDATE_PERSONAL_DETAILS_CONFIRM_TEXT).getText ();
    }
	public String getUpdateContactPreferenceSuccessText (){return driver.findElement(SEND_ME_UPDATE_CONTACT_PREFERENCE_CONFIRM_TEXT).getText();}

    public boolean getDirectDebitPaymentsPod() {
        List<WebElement> directDebitPod = driver.findElements (DIRECT_DEBIT_PAYMENTS_POD);
        setLogs ("Verifying that direct debit payments pod exits");
        if (directDebitPod.size () != 0) {
            return true;
        } else {
            return false;
        }
    }

	public boolean checkIfcarStickerLinkIsDisplayed(){
		setLogs("check if promocode section is displayed.......");
		return Helpers.waitForIsDisplayed(driver,REQUEST_CAR_STICKER_LINK,10);
	}
	public boolean checkIfSeeYourMembershipButtonIsDisplayed(){
		setLogs("check if 'see your Memberships' button is displayed.......");
		return Helpers.waitForIsDisplayed(driver,SEE_YOUR_MEMBERSHIPS_BUTTON,10);
	}

    public void clickDirectDebitPaymentsPodManageDirectDebitsLink() {
        Helpers.waitForIsDisplayed (driver, SEND_ME_NEW_DIRECT_DEBIT_LINK, 15);
        setLogs ("Clicking manage direct debits link");
        driver.findElement (SEND_ME_NEW_DIRECT_DEBIT_LINK).click ();
    }

	public boolean checkNewContactPrefNoticeIsDisplayed() {
		setLogs("check if contact Pref Notice is displayed......");
		return Helpers.waitForIsDisplayed(driver, CONTACT_PREF_UPDATE_NOTICE, 10);
	}

	public void clickUpdatePreferencesButtonOnUpdateNotice() {
		driver.findElement(CONTACT_PREF_UPDATE_NOTICE_BUTTON).click ();
	}

    public boolean getAccountSettingsButton() {
        return false;
    }

	public void clickSeeYourMembershipsButton() {
		driver.findElement(SEE_YOUR_MEMBERSHIPS_BUTTON).click ();
	}

	public void clickSeeYourAccountButton() {
		driver.findElement(SEE_YOUR_ACCOUNT_SETTINGS_PAGE).click ();
	}

	public void clickSideNavDashboardButton() {
		driver.findElement(SIDE_NAV_DASHBOARD_OPTION).click ();
	}

	public void clickStaticSideNavDashboardButton() {
		driver.findElement(STATIC_SIDE_NAV_DASHBOARD).click ();
	}

	public void clickRenewMembershipsButton() {
		driver.findElement(RENEWAL_BUTTON).click ();
	}

	public String getDDMembershipInrenewalText () {
		List<WebElement> ddRenewalText = driver.findElements(DD_MEMBERSHIP_IN_RENEWAL_TEST);
		if (ddRenewalText.size()!=0){
			String ddRenewalTextDisplayed = driver.findElement(DD_MEMBERSHIP_IN_RENEWAL_TEST).getText();
			return ddRenewalTextDisplayed;
		}
		return null;
	}
}
