package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.org.nationaltrust.apis.CreateDonateFormRequest;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by Sowjanya Annepu
 */
public class DonateAddMapPinPage extends CommemorativeMapPinDonateSelectors {

    private WebDriver driver;

    public DonateAddMapPinPage(WebDriver driver){
        this.driver=driver;
        driver.navigate().to(EnvironmentConfiguration.getText("donateMapPinURL"));
        Helpers.waitHandlingException(1000);
    }

    private String placeName="Tyntesfield";

    public DonateMapPinStep1Page clickDedicateADonationButton(){
        Helpers.waitHandlingException(2000);
        Helpers.waitForVisibility(ADD_MAP_PIN_DEDICATE_A_DONATION);
        ADD_MAP_PIN_DEDICATE_A_DONATION.click();
        return new DonateMapPinStep1Page(driver);
    }

    public DonateAddMapPinPage searchPlaceAndAutoSelectMapPin(){
        try {
            Thread.sleep(2000);
            Helpers.clearAndSetText(driver,By.cssSelector("input[type='search']"),placeName);
           // ADD_MAP_PIN_SEARCH_BOX.sendKeys("Ty");
            Thread.sleep(1000);
            ADD_MAP_PIN_TYNTESFIELD_PLACE.click();
            Thread.sleep(1000);
            ADD_MAP_PIN_PLACE_NAME_ON_PIN.getText().contains(placeName);
            ADD_MAP_PIN_PLACE_ON_PROPERTY_CARD.getText().contains(placeName);
            return this;
        }
          catch (Exception e)
        {
            setLogs("exception occured while searching for place");
            return null;
        }
    }

    public DonateAddMapPinPage searchPlace(String placeName){
        Helpers.waitHandlingException(2000);
        Helpers.clearAndSetText(driver,By.cssSelector("input[type='search']"),placeName);
        Helpers.waitHandlingException(1000);
        ADD_MAP_PIN_SEARCH_BOX.sendKeys(Keys.ENTER);
       // ADD_MAP_PIN_SEARCH_BOX(placeName, Keys.ENTER);
        Helpers.waitHandlingException(1000);
        return this;
    }

    public DonateAddMapPinPage defaultTextInRightPanel()
    {
        return this;
    }

    public DonateAddMapPinPage placeholderTextInSearchBar()
    {
        return this;
    }

    public boolean placeNameOnPinMatchesRightHandCard(){
        ADD_MAP_PIN_PLACE_NAME_ON_PIN.getText().contains(placeName);
        return  ADD_MAP_PIN_PLACE_ON_PROPERTY_CARD.getText().contains(placeName);
    }

    public boolean verifyPublishedStories(CreateDonateFormRequest createDonateFormRequest){
        if(ADD_MAP_PIN_MEMORIES_TEXT_ON_PIN.isDisplayed())
            ADD_MAP_PIN_MEMORIES_LINK.click();
        return viewStories(createDonateFormRequest);
    }

    public boolean viewStories(CreateDonateFormRequest createDonateFormRequest) {
        Helpers.waitHandlingException(4000);
        Helpers.waitForVisibility(MAP_PIN_POP_UP_DEDICATE_A_DONATION_BUTTON);
        RHS_CARD_SUBJECT.getText().contains(createDonateFormRequest.getCommemorativeSubject());
        Helpers.waitHandlingException(1000);
        if (createDonateFormRequest.getDonationStory().contains(RHS_CARD_STORY.getText()) &&
                RHS_CARD_RELATION.getText().contains(createDonateFormRequest.getCommemorativeRelation()))
        {
            Helpers.waitHandlingException(2000);
            RHS_CARD_CLOSE_BUTTON.sendKeys(Keys.ENTER);
            Helpers.waitHandlingException(1000);
            ADD_MAP_PIN_DEDICATE_A_DONATION.isDisplayed();
            status=true;
        }

        return status;
    }

    public boolean verifySharingLink(CreateDonateFormRequest createDonateFormRequest, String sharingLink){
        driver.navigate().to(sharingLink);
        return viewStories(createDonateFormRequest);
    }
}