package uk.org.nationaltrust.pages;

import org.openqa.selenium.WebDriver;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by Sowjanya Annepu
 */
public class DonateMapPinStep2Page extends CommemorativeMapPinDonateSelectors{

    protected WebDriver driver;

    public DonateMapPinStep2Page(WebDriver driver){
        this.driver=driver;
    }

    public DonateMapPinStep2Page  navigateToStoryStep2(String url){
        driver.navigate().to(url);
        return this;
    }

    public boolean storyIsMandatory(){
        /* DON-603*/
        Helpers.waitForVisibility(STEP2_ADD_IMAGE_BUTTON);
        STEP2_ADD_IMAGE_BUTTON.click();
        Helpers.waitHandlingException(1000);
        return STEP2_STORY_VALIDATION_ERROR_TEXT.isDisplayed();
    }

    public String storyFieldMaxLength(String story){
        STEP2_STORY_MAX_CHAR_TEXT.getText().contains(storyMaxCharText);
        STEP2_STORY.sendKeys(story);
        Helpers.waitForVisibility(STEP2_STORY_NEGATIVE_COUNTER_TEXT);
        return STEP2_STORY_NEGATIVE_COUNTER_TEXT.getText();
    }

    public String step2PublicPinHelpText (){
    	Helpers.waitForVisibility(STEP2_PUBLIC_PIN_HELP);
    	return STEP2_PUBLIC_PIN_HELP.getText();

	}

	public String step2PrivatePinHelpText (){
		Helpers.waitForVisibility(STEP2_PRIVATE_PIN_HELP);
		return STEP2_PRIVATE_PIN_HELP.getText();

	}

    public DonateMapPinStep2Page enterStoryText(String myStory){
        Helpers.waitForVisibility(STEP2_STORY);
        STEP2_STORY.sendKeys(myStory);
        return this;
    }

    public DonateMapPinStep1Page backBUtton(){
        PREVIOUS_BUTTON.click();
        return new DonateMapPinStep1Page(driver);
    }

    public DonateMapPinAddImagePage clickAddImage(){
        STEP2_ADD_IMAGE_BUTTON.click();
        return new DonateMapPinAddImagePage(driver);
    }
}