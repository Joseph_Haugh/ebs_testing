package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import uk.org.nationaltrust.framework.Helpers;

/**
 * Created by nick.thompson on 08/11/2016.
 */
public class MembershipsPage extends PageBase {

	protected WebDriver driver;
	private By SUPPORTER_POSTCODE = By.cssSelector("#postcode");

	public MembershipsPage(WebDriver dr) {
		this.driver = dr;
	}

	public By SELECT_THE_MEMBERSHIP_HEADING = By.cssSelector("body > main > section > form > div:nth-child(1) > div > fieldset > div > legend");

	public By LEAD_MEMBER_FIELD_LABEL = By.cssSelector("#membershipRadioIdLabel-0 > div.radio-panel--content > div > div:nth-child(1) > dl > dt");
	public By RENEWAL_MONTH_FIELD_LABEL = By.cssSelector("#membershipRadioIdLabel-0 > div.radio-panel--content > div > div.small-12.medium-3.columns.end > dl > dt");
	public By PAYMENT_METHOD_FIELD_LABEL = By.cssSelector("#membershipRadioIdLabel-0 > div.radio-panel--content > div > div:nth-child(3) > dl > dt");

	public By FIRST_MEMBERSHIP_RADIO_BUTTON = By.cssSelector("#membershipRadioIdLabel-0 > div.radio-panel--header");

	public By SECOND_MEMBERSHIP_RADIO_BUTTON = By.cssSelector("#membershipRadioIdLabel-1 > div.radio-panel--header");

	public By PAYMENT_CANCELLED_TEXT = By.cssSelector("body > main > section > form > div:nth-child(1) > div > div > div > p");

	public static By MEMBERSHIP_TYPE = By.cssSelector("#membershipType-0");

	public static By MEMBERSHIP_ALREADY_SUBMITTED_ERROR = By.cssSelector("#mainContent > section > form > div:nth-child(1) > div > div > div > p");
	public static By MEMBERSHIP_MONTHLY_PRICE_DISPLAYED = By.cssSelector("#monthlyPrice-0");
	public static By CHILD_NEED_TO_BUY_MEMBERSHIP_MESSAGE = By.cssSelector("#membershipChangeInfo-0");
	public static By MEMBERSHIP_TYPE_CHANGE = By.id("membershipChangeInfo-0");

	public static By MEMBERSHIP_ANNUAL_PRICE_DISPLAYED = By.id("annualPrice-0");

//	public static By MEMBERSHIP_ANNUAL_PRICE_DISPLAYED = By.cssSelector("#annualDDPaymentOptionDiv > label > span > span.js-membership-price.pymtOptPriceYearly");

	public static By MEMBERSHIP_RENEWAL_DATE = By.cssSelector("#renewalMonth-0");

	public By SUPPORTER_NUMBER = By.cssSelector("#leadMemberNumber-0");

	public By LEAD_MEMBER_NAME = By.cssSelector("#leadMemberName-0");

	public By SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT = By.cssSelector ("#tab-my-memberships > div > div.alert-box.info > div");

	public static By PAYMENT_METHOD = By.cssSelector("#currentPaymentMethod-0");

	public static By SENIOR_DISCOUNT_RENEWAL_TEXT = By.cssSelector("#seniorDiscountMessage-0");

	public static By NO_GIFT_SELECTED_ANCHOR_TEXT = By.linkText("Please choose a gift option to continue");

	public String getMembershipHeaderLabelText (){
		List<WebElement> membershipPageHeader = driver.findElements(SELECT_THE_MEMBERSHIP_HEADING);
		if (membershipPageHeader.size()!=0){
			String membershipHeaderDisplayed = driver.findElement(SELECT_THE_MEMBERSHIP_HEADING).getText();
			setLogs ("Get membership page Header text Displayed");
			return membershipHeaderDisplayed;
		}
		return null;
	}

	public String getLeadMemberFieldLabelText (){
		List<WebElement> leadMemberHeader = driver.findElements(LEAD_MEMBER_FIELD_LABEL);
		if (leadMemberHeader.size()!=0){
			String leadMemberHeaderDisplayed = driver.findElement(LEAD_MEMBER_FIELD_LABEL).getText();
			setLogs("Get Header Text for Lead member displayed");
			return leadMemberHeaderDisplayed;

					}
		return null;
	}
	public String getRenewalMonthFieldLabelText(){
		List<WebElement> renewalMonthHeader = driver.findElements(RENEWAL_MONTH_FIELD_LABEL);
		if (renewalMonthHeader.size() !=0){
			String renewalMonthHeaderDisplayed = driver.findElement(RENEWAL_MONTH_FIELD_LABEL).getText();
			setLogs("Get Renewal Month Header Label Text");
			return renewalMonthHeaderDisplayed;
		}
		return null;
	}
	public String getPaymentMethodFieldLabelText(){
		List<WebElement> paymentMethodHeader = driver.findElements(PAYMENT_METHOD_FIELD_LABEL);
		if (paymentMethodHeader.size() != 0) {
			String paymentMethodHeaderDisplayed =driver.findElement(PAYMENT_METHOD_FIELD_LABEL).getText();
			setLogs("Get Payment MethodHeader Label text");
			return paymentMethodHeaderDisplayed;
		}
		return null;
	}

	public String getRenewalDateDisplayed() {
		List<WebElement> renewalDateField = driver.findElements(MEMBERSHIP_RENEWAL_DATE);
		if (renewalDateField.size() != 0) {
			String renewalDateDisplayed = driver.findElement(MEMBERSHIP_RENEWAL_DATE).getText();
			setLogs("Getting the renewal date displayed");
			return renewalDateDisplayed;

		}
		return null;
	}

	public String getMembershipTypeDisplayed() {
		List<WebElement> membershipTypeText = driver.findElements(MEMBERSHIP_TYPE);
		if (membershipTypeText.size() != 0) {
			String membershipTypeDisplayed = driver.findElement(MEMBERSHIP_TYPE).getText();
			setLogs("Getting the membership Type Displayed");
			return membershipTypeDisplayed;

		}
		return null;
	}

	public String getLeadMemberDisplayed() {
		List<WebElement> leadMemberField = driver.findElements(LEAD_MEMBER_NAME);
		if (leadMemberField.size() != 0) {
			String leadMemberDisplayed = driver.findElement(LEAD_MEMBER_NAME).getText();
			setLogs("Getting the lead member name");
			return leadMemberDisplayed;
		}
		return null;
	}

	public String getPaymentMethodDisplayed() {
		List<WebElement> paymentMethod = driver.findElements(PAYMENT_METHOD);
		if (paymentMethod.size() != 0) {
			String paymentMethodDisplayed = driver.findElement(PAYMENT_METHOD).getText();
			setLogs("Getting the Payment Method displayed");
			return paymentMethodDisplayed;
		}
		return null;
	}

	public String getMonthlyPrice() {
		List<WebElement> monthlyPrice = driver.findElements(MEMBERSHIP_MONTHLY_PRICE_DISPLAYED);
		if (monthlyPrice.size() != 0) {
			String monthlyPriceDisplayed = driver.findElement(MEMBERSHIP_MONTHLY_PRICE_DISPLAYED).getText();
			setLogs("Getting the Monthly Price displayed");
			return monthlyPriceDisplayed;
		}
		return null;
	}

	public String getAnnualPrice() {
		List<WebElement> annualPrice = driver.findElements(MEMBERSHIP_ANNUAL_PRICE_DISPLAYED);
		if (annualPrice.size() != 0) {
			String annualPriceDisplayed = driver.findElement(MEMBERSHIP_ANNUAL_PRICE_DISPLAYED).getText();
			setLogs("Getting the Annual Price displayed");
			return annualPriceDisplayed;
		}
		return null;
	}
	public void selectTheFirstMembershipInView (){
		driver.findElement(FIRST_MEMBERSHIP_RADIO_BUTTON).click();
	}

	public String paymentCancelledText (){

		List<WebElement> cancelledText = driver.findElements(PAYMENT_CANCELLED_TEXT);
		if (cancelledText.size()!=0){

			driver.findElement(PAYMENT_CANCELLED_TEXT).getText();
		}
		return null;
	}
	public String getMembershipPreviouslyRenewedErrorText() {
		List<WebElement> membAlreadyRenewedError = driver.findElements(MEMBERSHIP_ALREADY_SUBMITTED_ERROR);
		if (membAlreadyRenewedError.size() != 0) {
			String membAlreadyRenewedErrorText = driver.findElement(MEMBERSHIP_ALREADY_SUBMITTED_ERROR).getText();
			return membAlreadyRenewedErrorText;
		}
		return null;
	}

	public String getChildNeedsToJoinText() {
		List<WebElement> childNeedsToJoin = driver.findElements(CHILD_NEED_TO_BUY_MEMBERSHIP_MESSAGE);
		if (childNeedsToJoin.size() != 0) {
			String childNeedsToJoinText = driver.findElement(CHILD_NEED_TO_BUY_MEMBERSHIP_MESSAGE).getText();
			return childNeedsToJoinText;
		}
		return null;
	}

	public String getChangeReasonText() {
		List<WebElement> typeChange = driver.findElements(MEMBERSHIP_TYPE_CHANGE);
		if (typeChange.size() != 0) {
			String typeChangeText = driver.findElement(MEMBERSHIP_TYPE_CHANGE).getText();
			return typeChangeText;
		}
		return null;
	}


	public Boolean firstMembershipRadioButtonExists (){
		List<WebElement> secondRadioButton = driver.findElements(FIRST_MEMBERSHIP_RADIO_BUTTON);
		if (secondRadioButton.size()!=0){
			return true;
		} else {
			return false;
		}}
	public Boolean secondMembershipRadioButtonExists (){
		List<WebElement> secondRadioButton = driver.findElements(SECOND_MEMBERSHIP_RADIO_BUTTON);
		if (secondRadioButton.size()!=0){

		return true;
	} else {
		return false;
	}}

	public void selectTheSecondMembershipInView() {driver.findElement(SECOND_MEMBERSHIP_RADIO_BUTTON).click();

	}

	public String getPostcodeDisplayed() {
		List<WebElement> postcodeField = driver.findElements(SUPPORTER_POSTCODE);
		if (postcodeField.size() != 0) {
			String postcodeDisplayed = driver.findElement(SUPPORTER_POSTCODE).getAttribute("value");
			setLogs("Getting the Postcode from the contact details section" + postcodeDisplayed);
			return postcodeDisplayed;
		}
		return null;

	}

	public String getDirectDebitUpdateSuccessText() {
		setLogs("Getting the manage direct debits  link label");
		Helpers.waitForElementToAppear(driver, userPage().SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT, 10, "no direct debit success message displayed");
		List<WebElement> directDebitUpdate = driver.findElements(SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT);
		if (directDebitUpdate.size() != 0) {
			String directDebitUpdatetext = driver.findElement(SEND_ME_PAYMENT_UPDATE_CONFIRM_TEXT).getText();
			return directDebitUpdatetext;
		}
		return null;
	}

	public String getSeniorDiscountTextDisplayed(){
		List<WebElement> seniorDiscount = driver.findElements(SENIOR_DISCOUNT_RENEWAL_TEXT);
		if(seniorDiscount.size()!=0){
			String seniorDiscountText = driver.findElement(SENIOR_DISCOUNT_RENEWAL_TEXT).getText();
			setLogs ("Getting the senior discount text displayed");
			return seniorDiscountText;
		}
		return null;
	}

	public boolean checkIfSeniorDiscountIsDisplayed(){
		setLogs("check if Annual DD Payment Option is displayed.......");
		return Helpers.waitForIsDisplayed(driver,SENIOR_DISCOUNT_RENEWAL_TEXT,10);
	}

	public String getNoGiftSelectedAnchorText() {
		Helpers.waitForElementToAppear(driver, membershipsPage().NO_GIFT_SELECTED_ANCHOR_TEXT, 10, "No Gift selected Anchor Link not appearing");
		List<WebElement> badGiftAnchor = driver.findElements(NO_GIFT_SELECTED_ANCHOR_TEXT);
		if (badGiftAnchor.size() != 0) {
			String badGiftAnchorTextDisplayed = driver.findElement(NO_GIFT_SELECTED_ANCHOR_TEXT).getText();
			return badGiftAnchorTextDisplayed;
		}
		return null;
	}
}









