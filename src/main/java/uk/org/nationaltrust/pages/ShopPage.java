package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Nick.Thompson on 28/03/2018.
 */
public class ShopPage extends PageBase {

	protected WebDriver driver;

	public ShopPage(WebDriver dr) {
		this.driver = dr;
	}

	public By SHOP_CUSTOMER_LOGIN_HEADER = By.cssSelector ("#maincontent > div.page-title-wrapper > h1 > span");

	public String getShopCustomerLoginHeaderDisplayed (){
		List<WebElement> shopLoginHeaderText = driver.findElements(SHOP_CUSTOMER_LOGIN_HEADER);
		if (shopLoginHeaderText.size() !=0){
			String shopLoginHeaderTextDisplayed = driver.findElement(SHOP_CUSTOMER_LOGIN_HEADER).getText();

			return shopLoginHeaderTextDisplayed;
		}
		return null;

	}
}
