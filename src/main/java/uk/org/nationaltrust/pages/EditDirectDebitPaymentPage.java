package uk.org.nationaltrust.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.framework.SupporterServiceDBConnection;

import java.util.List;

/**
 * Created by Pramod.Reguri on 08/07/2016.
 */
public class EditDirectDebitPaymentPage extends PageBase {

	SupporterServiceDBConnection dbConnection = new SupporterServiceDBConnection();

	private static final By MEMBERSHIP_PAYMNENTS_DIRECT_DEBIT_BANK_SORTCODE = By.cssSelector(".details-list>dd>dl>dt:nth-of-type(3)");

	private static final By MEMBER_ACCOUNT_NAME = By.cssSelector("#currentAccountName");

	private static final By MEMBER_ACCOUNT_NUMBER = By.cssSelector("#currentAccountNumber");

	private static final By MEMBER_SORT_CODE = By.cssSelector("#currentSortCode");

	private static final By MANAGE_DIRECT_DEBIT_DETAILS_HEADER = By.cssSelector("#main-content > section > div:nth-child(1) > header > h1");

	private static final By CURRENT_DETAILS_HEADER = By.cssSelector("#current-details-container>h3");

	private static final By DIRECT_DEBIT_DETAILS_HEADER = By.cssSelector("#mainContent > section > div:nth-child(1) > header > h1");

	private static final By ACCOUNT_NAME_LABEL = By.cssSelector("#accountNameLabel");

	private static final By ACCOUNT_NUMBER_LABEL = By.cssSelector("#accountNumberLabel");

	private static final By SORT_CODE_LABEL = By.cssSelector("#formContainer > fieldset > div.row > label");

	private static final By PAYMENT_TERM_LABEL = By.cssSelector("#edit-form-container\\2e paymentFrequencyLabel");

	public By MONTHLY_PAYMENT_TERM_RADIO_BUTTON = By.id("directDebitForms[0].paymentTerm-MONTHLY-label");

	public By ANNUAL_PAYMENT_TERM_RADIO_BUTTON = By.id("directDebitForms[0].paymentTerm-ANNUAL-label");

	public By ANNUAL_PAYMENT_TERM_RADIO_BUTTON_SECOND_MEMBERSHIP = By.cssSelector("#directDebitForms\\5b 1\\5d \\2e paymentTerm-ANNUAL-label");

public By ANNUAL_ONLY_INFO_TEXT = By.id("annualOnlyAdvice");



//	public By ANNUAL_PAYMENT_TERM_RADIO_BUTTON = By.cssSelector("#paymentOptions > div:nth-child(3) > div > div:nth-child(2) > div:nth-child(3) > label");



	private static final By MONTHLY_PAYMENT_TERM_RADIO_BUTTON_SECOND_MEMBERSHIP = By.cssSelector("#directDebitForms\\5b 1\\5d \\2e paymentTerm-MONTHLY");



	private static final By TERMS_AND_CONDITIONS_HEADING = By.cssSelector("#monthlyTermsCopy > fieldset > legend");

	private static final By TERMS_AND_CONDITIONS_ACCEPT_WORDING = By.cssSelector("#monthlyTermsAcceptedLabel");

	public By TERMS_AND_CONDITIONS_CHECKBOX = By.cssSelector("#monthlyTermsAcceptedLabel");


	public By UPDATE_DD_MESSAGE_TEXT = By.cssSelector("#bank-account-added > p:nth-child(1)");

	public By PAYMENT_OPTIONS_1_LINK = By.id("directDebitForms[0].viewPaymentDetails");


	public By PAYMENT_OPTIONS_2_LINK = By.id("directDebitForms1.viewOptions");

	public By EXISTING_DIRECT_DEBIT_DETAILS_DROPDOWN = By.cssSelector("#directDebitForms0\\2e bankAccount");
	public By EXISTING_DIRECT_DEBIT_DETAILS_SECOND_MEMB_DROPDOWN = By.cssSelector("#directDebitForms1\\2e bankAccount");

	public By DIRECT_DEBIT_GUARANTEE_LINK = By.id("directDebitPanelToggle");

	public static By DIRECT_DEBIT_GUARANTEE_INITIAL_INFORMATIVE_TEXT = By.cssSelector ("#directDebitPanel > p:nth-child(3)");

	public static By DIRECT_DEBIT_CANNOT_BE_UPDATED = By.id("membership-0-unableToUpdateDirectDebit");

	public By CHANGE_TO_DD_INFORMATIVE_TEXT = By.id ("annualDDAdvice");

	public By MONTHLY_DD_TO_ANNUAL_DD_INFORMATIVE_TEXT =By.cssSelector("#monthlyToAnnualWarning > p");

	public By ANNUAL_ONLY_DD_INFORMATIVE_TEXT = By.id("annualOnlyAdvice");

	protected WebDriver driver;

	public EditDirectDebitPaymentPage(WebDriver dr) {
		this.driver = dr;
	}

	public By GET_ACCOUNT_NAME_FIELD = By.cssSelector("#accountName");

	public By GET_ACCOUNT_NUMBER_FIELD = By.cssSelector("#accountNumber");

	public By GET_SORT_CODE_ONE_FIELD = By.cssSelector("div.sortCode > div>input[id*=sortCode1]");

	public By GET_SORT_CODE_TWO_FIELD = By.cssSelector("div.sortCode > div>input[id*=sortCode2]");

	public By GET_SORT_CODE_THREE_FIELD = By.cssSelector("div.sortCode > div>input[id*=sortCode3]");

	public By OPEN_MEMBERSHIP_ONE = By.cssSelector("");

	public By OPEN_MEMBERSHIP_TWO = By.cssSelector("");

	public By GET_PAYMENT_TERM_FIELD = By.cssSelector("#paymentFrequency");

	public By GET_ADD_NEW_DIRECT_DEBIT_LINK = By.id("directDebitForms0.addBankAccountDetails");

	public By GET_ADD_NEW_DIRECT_DEBIT_LINK_SECOND = By.id("directDebitForms1.addBankAccountDetails");

	public By GET_PAYMENT_DETAILS_SUBMIT_BUTTON = By.cssSelector("#MyNTUpdatePaymentDetails");

	public By UPDATE_PAYMENT_DETAILS_BUTTON = By.id("submitDirectDebitManagement");

	public By CANCEL_LINK = By.id("cancelDirectDebitManagement");

	public String getBankSortCodeLabelText() {
		setLogs("Getting the bank sort code header label ");
		String sortCodeLabel = Helpers.getText(driver, MEMBERSHIP_PAYMNENTS_DIRECT_DEBIT_BANK_SORTCODE);
		return sortCodeLabel;
	}

	public String getLeadMemberAccountName() {
		return driver.findElement(EditDirectDebitPaymentPage.MEMBER_ACCOUNT_NAME).getText();
	}

	public String getLeadMemberAccounNumber() {
		return driver.findElement(EditDirectDebitPaymentPage.MEMBER_ACCOUNT_NUMBER).getText();
	}

	public String getLeadMemberSortcode() {
		return driver.findElement(EditDirectDebitPaymentPage.MEMBER_SORT_CODE).getText();
	}

	public String getPaymentPageHeaderLabel() {
		setLogs("Getting the manage direct debit payments label");
		return driver.findElement(EditDirectDebitPaymentPage.MANAGE_DIRECT_DEBIT_DETAILS_HEADER).getText();
	}

	public boolean getEditPaymentDetailsPage() {
		setLogs("Verifying that the user is navigated to the edit direct debit payment details page");
		if (driver.findElements(MEMBERSHIP_PAYMNENTS_DIRECT_DEBIT_BANK_SORTCODE).size() != 0) {
			return true;
		} else if (driver.findElements(MEMBERSHIP_PAYMNENTS_DIRECT_DEBIT_BANK_SORTCODE).size() == 0) {
			return false;
		}
		throw new IllegalArgumentException("Navigated to unknown page");
	}

	public String getManageDirectDebitDetailsHeaderLabel() {
		setLogs("Getting the manage direct debit details label");
		return driver.findElement(MANAGE_DIRECT_DEBIT_DETAILS_HEADER).getText();
	}

	public String getCurrentDetailsDetailsHeaderLabel() {
		setLogs("Getting the current details label");
		return driver.findElement(CURRENT_DETAILS_HEADER).getText();
	}

	public String getDirectDebitDetailsHeader() {
		setLogs("Getting the direct debit details label");
		return driver.findElement(DIRECT_DEBIT_DETAILS_HEADER).getText();
	}

	public String getDirectDebitDetailsPageAccountNameLabel() {
		setLogs("Getting the account name label");
		return driver.findElement(ACCOUNT_NAME_LABEL).getText();
	}

	public String getDirectDebitDetailsPageAccountNumberLabel() {
		setLogs("Getting the account number label");
		return driver.findElement(ACCOUNT_NUMBER_LABEL).getText();
	}

	public String getDirectDebitDetailsPageSortCodeLabel() {
		setLogs("Getting the sort code label");
		return driver.findElement(SORT_CODE_LABEL).getText();
	}

	public String getDirectDebitDetailsPagePaymentTermLabel() {
		setLogs("Getting the payment term lable");
		return driver.findElement(PAYMENT_TERM_LABEL).getText();
	}

	public String getDirectDebitChangesMessageText() {
		setLogs("Getting the direct debit changes message");
		return driver.findElement(CHANGE_TO_DD_INFORMATIVE_TEXT).getText();
	}

	public String selectDirectdebitGuaranteeLink (){
		Helpers.waitForIsDisplayed(driver, DIRECT_DEBIT_GUARANTEE_LINK, 50);
		List <WebElement> guaranteeLink = driver.findElements(DIRECT_DEBIT_GUARANTEE_LINK);
		if(guaranteeLink.size()!=0){
			setLogs ("Getting the account Sign out Button");
			driver.findElement(DIRECT_DEBIT_GUARANTEE_LINK).click();
		}
		return null;
	}

	public String getDirectDebitMonthlyToAnnualMessageText() {
		setLogs("Getting the Monthly DD to Annual DD changes message");
		return driver.findElement(MONTHLY_DD_TO_ANNUAL_DD_INFORMATIVE_TEXT).getText();
	}

	public String getDirectDebitGuaranteeInfoText() {
		setLogs("Getting the DD Guarantee Info text");
		Helpers.waitForElementToAppear(driver, editDirectDebitPaymentPage().DIRECT_DEBIT_GUARANTEE_INITIAL_INFORMATIVE_TEXT, 10, "Guarantee not displayed after 10 seconds");
		List<WebElement> guaranteeText = driver.findElements(DIRECT_DEBIT_GUARANTEE_INITIAL_INFORMATIVE_TEXT);
		if (guaranteeText.size() != 0) {
			String guaranteeTextDisplayed = driver.findElement(DIRECT_DEBIT_GUARANTEE_INITIAL_INFORMATIVE_TEXT).getText();
			return guaranteeTextDisplayed;
		}
		return null;
	}

	public String getDirectDebitCannotBeUpdatedInfoText() {
		setLogs("Getting the DD Cannot be updated Info text");
		Helpers.waitForElementToAppear(driver, editDirectDebitPaymentPage().DIRECT_DEBIT_CANNOT_BE_UPDATED, 10, "DD cannot be updated text not displayed after 10 seconds");
		List<WebElement> ddNoUpdateText = driver.findElements(DIRECT_DEBIT_CANNOT_BE_UPDATED);
		if (ddNoUpdateText.size() != 0) {
			String ddNoUpdateTextDisplayed = driver.findElement(DIRECT_DEBIT_CANNOT_BE_UPDATED).getText();
			return ddNoUpdateTextDisplayed;
		}
		return null;
	}



	public String getAnnualOnlyDDMessageText() {
		setLogs("Getting the Annual Only DD changes message");
		return driver.findElement(ANNUAL_ONLY_DD_INFORMATIVE_TEXT).getText();
	}

	public String getTermsAndConditionsHeader() {
		setLogs("Getting the terms and conditions Header label");
		return driver.findElement(TERMS_AND_CONDITIONS_HEADING).getText();
	}
	public String getTermsAndConditionsAcceptanceText() {
		setLogs("Getting the terms and conditions acceptance text displayed");
		return driver.findElement(TERMS_AND_CONDITIONS_ACCEPT_WORDING).getText();
	}

	public String getExistingDirectDebitDetailsFromDropDown() {
		setLogs("Getting the existing DD details from Dropdown");
		return driver.findElement(EXISTING_DIRECT_DEBIT_DETAILS_DROPDOWN).getText();
	}



	public void setAccountName(String accountName) {
		driver.findElement(editDirectDebitPaymentPage().GET_ACCOUNT_NAME_FIELD).sendKeys(accountName);
	}

	public void setAccountNumber(String accountNumber) {
		driver.findElement(editDirectDebitPaymentPage().GET_ACCOUNT_NUMBER_FIELD).sendKeys(accountNumber);
	}

	public void setSortCode(String sortCodeFieldOne, String sortCodeFieldTwo, String sortCodeFieldThree) {
		driver.findElement(editDirectDebitPaymentPage().GET_SORT_CODE_ONE_FIELD).sendKeys(sortCodeFieldOne);
		driver.findElement(editDirectDebitPaymentPage().GET_SORT_CODE_TWO_FIELD).sendKeys(sortCodeFieldTwo);
		driver.findElement(editDirectDebitPaymentPage().GET_SORT_CODE_THREE_FIELD).sendKeys(sortCodeFieldThree);
	}

	public String getDirectDebitUpdatedMessageDisplayed() {
		List<WebElement> updateDD = driver.findElements(UPDATE_DD_MESSAGE_TEXT);
		if (updateDD.size() != 0) {
			String updateDDText = driver.findElement(UPDATE_DD_MESSAGE_TEXT).getText();

			return updateDDText;
		}
		return null;
	}

	public void setPaymentTerm(String paymentTerm) {
		driver.findElement(editDirectDebitPaymentPage().GET_PAYMENT_TERM_FIELD).sendKeys(paymentTerm);
	}

	public void setSupporterForGiftDD (){
		String sqlSta = "UPDATE " + EnvironmentConfiguration.getText("account")+ " set SUPPORTER_NUMBER = '256213582' where" + " ACCOUNT_EMAIL = 'explore@bt.com'" ;
		setLogs(sqlSta);
		dbConnection.setData(sqlSta);
	}
	public void setSupporterForMonthlyToAnnualDD (){
		String sqlSta = "UPDATE " + EnvironmentConfiguration.getText("account")+ " set SUPPORTER_NUMBER = '269950819' where" + " ACCOUNT_EMAIL = 'explore@bt.com'" ;
		setLogs(sqlSta);
		dbConnection.setData(sqlSta);
	}

	public String selectAddNewAccountLink (){
		Helpers.waitForElementToAppear(driver, GET_ADD_NEW_DIRECT_DEBIT_LINK, 10, "Add new DD link missing or incorrect");
		List<WebElement> addDDLink = driver.findElements(GET_ADD_NEW_DIRECT_DEBIT_LINK);
		if(addDDLink.size()!=0){
			driver.findElement(GET_ADD_NEW_DIRECT_DEBIT_LINK).click();
		}
		return null;
	}

	public String selectPaymentOption1Link (){
		Helpers.waitForIsDisplayed(driver, PAYMENT_OPTIONS_1_LINK, 10);
		List <WebElement> paymentOption = driver.findElements(PAYMENT_OPTIONS_1_LINK);
		if(paymentOption.size()!=0){
			setLogs ("Getting the account Sign out Button");
			driver.findElement(PAYMENT_OPTIONS_1_LINK).click();
		}
		return null;
	}

	public String selectPaymentOption2Link (){
		Helpers.waitForIsDisplayed(driver, PAYMENT_OPTIONS_2_LINK, 10);
		List <WebElement> paymentOption = driver.findElements(PAYMENT_OPTIONS_2_LINK);
		if(paymentOption.size()!=0){
			setLogs ("Getting the account Sign out Button");
			driver.findElement(PAYMENT_OPTIONS_2_LINK).click();
		}
		return null;
	}

	public boolean checkIfAddNewDDLinkIsDisplayed(){
		setLogs("check if Add New DD link is displayed.......");
		return Helpers.waitForIsDisplayed(driver,GET_ADD_NEW_DIRECT_DEBIT_LINK,10);

	}

	public boolean checkIfSecondAddNewDDLinkIsDisplayed(){
		setLogs("check if Add New DD link is displayed.......");
		return Helpers.waitForIsDisplayed(driver,GET_ADD_NEW_DIRECT_DEBIT_LINK_SECOND,10);

	}

	public boolean checkIfMembCannotBeUpdatedTextDisplayed(){
		setLogs("check if Memb Cannot Be updated text is displayed.......");
		return Helpers.waitForIsDisplayed(driver,DIRECT_DEBIT_CANNOT_BE_UPDATED,10);
	}

	public boolean annualOnlyInfoText (){
		setLogs ("Check if Annual Only info text displayed.....");
		return Helpers.waitForIsDisplayed(driver, ANNUAL_ONLY_INFO_TEXT, 10);
	}

	public boolean monthlyTermRadioButton (){
		setLogs ("Check if Monthly Term radio button displayed.....");
		return Helpers.waitForIsDisplayed(driver, MONTHLY_PAYMENT_TERM_RADIO_BUTTON, 10);
	}

	public boolean annualTermRadioButton (){
		setLogs ("Check if Annual Term radio button displayed.....");
		return Helpers.waitForIsDisplayed(driver, ANNUAL_PAYMENT_TERM_RADIO_BUTTON, 10);
	}
	public boolean paymentOptionLinkExists (){
		setLogs ("Check if Payment Options link displayed.....");
		return Helpers.waitForIsDisplayed(driver, PAYMENT_OPTIONS_1_LINK, 10);
	}
}
