package uk.org.nationaltrust.pages;

import static org.openqa.selenium.By.xpath;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;

/**
 * @author Pramod
 */
public class JointMembershipPage extends PageBase {

	private static final By PERSONAL_DETAILS_LABEL = By.cssSelector(".active-step>.join-steps-content");

	private static final By INDIVIDUAL_MEMBERSHIP_TEXT = By.cssSelector("h1.nt-form-heading");

	private static final By NAME_LABEL = By.cssSelector("#main-content > section > form > div.group > div:nth-child(1) > div > fieldset > div > legend");

	private static final By YOUR_DETAILS_HEADER_LABEL = By.cssSelector("h2.nt-form-subheading");

	private static final By TITLE_LABEL = By.cssSelector(".large-4>label[for*=title]>span:nth-of-type(1)");

	private static final By TITLE_LABEL_REQUIRED_INDICATOR = By.cssSelector(".large-4>label[for*=title]>span:nth-of-type(2)");

	private static final By FIRST_NAME_REQUIRED_INDICATOR = By.cssSelector("label[for*=firstName] > span[class=required]");

	public By CONTINUE_BUTTON = By.name("_eventId_continue");

	public By PERSONAL_DETAILS_CONTINUE_BUTTON = By.cssSelector("#main-content > section > form > div:nth-child(3) > div > button");

	public By ADDITIONAL_MEMBER_DETIALS_CONTINUE_BUTTON = By.name("_eventId_continue");

	public By GIFT_GIVER_PERRSONAL_DETAILS_CONTINUE_BUTTON = By.cssSelector("#main-content > section > form > div:nth-child(3) > div > button");

	public By GET_CONTACT_YOU_YES_CHECKBOX = By.cssSelector("#main-content > section > form > div.group > div:nth-child(5) > div > fieldset > div > div > div:nth-child(3) > label");

	public By GET_CONTACT_YOU_NO_CHECKBOX = By.cssSelector("#main-content > section > form > div:nth-child(2) > div:nth-child(5) > div > fieldset > div > div > div:nth-child(4) > label");

	public By GET_CONTACT_BY_PHONE_YES_CHECKBOX = By.id("contactPermissions.contactPhoneConsented1-label");

	public By GET_CONTACT_BY_PHONE_NO_CHECKBOX = By.id("contactPermissions.contactPhoneConsented2-label");

	public By GET_CONTACT_BY_EMAIL_YES_CHECKBOX = By.id("contactPermissions.contactEmailConsented1-label");

	public By GET_CONTACT_BY_EMAIL_NO_CHECKBOX = By.id("contactPermissions.contactEmailConsented2-label");

	public By GET_CONTACT_BY_POST_YES_CHECKBOX = By.id("contactPermissions.contactPostConsented1-label");

	public By GET_CONTACT_BY_POST_NO_CHECKBOX = By.id("contactPermissions.contactPostConsented2-label");

	public By GET_GIFT_CONTACT_BY_PHONE_YES_CHECKBOX = By.id("giftGiver.contactPermissions.contactPhoneConsented1-label");

	public By GET_GIFT_CONTACT_BY_PHONE_NO_CHECKBOX = By.id("giftGiver.contactPermissions.contactPhoneConsented2-label");

	public By GET_GIFT_CONTACT_BY_EMAIL_YES_CHECKBOX = By.id("giftGiver.contactPermissions.contactEmailConsented1-label");

	public By GET_GIFT_CONTACT_BY_EMAIL_NO_CHECKBOX = By.id("giftGiver.contactPermissions.contactEmailConsented2-label");

	public By GET_GIFT_CONTACT_BY_POST_YES_CHECKBOX = By.id("giftGiver.contactPermissions.contactPostConsented1-label");

	public By GET_GIFT_CONTACT_BY_POST_NO_CHECKBOX = By.id("giftGiver.contactPermissions.contactPostConsented2-label");

	private static final By GET_NEW_CONTACT_PREF_INFO_TEXT = By.id("js-permissionStatementContainer");

	private static final By GET_CONTACTING_YOU_HEADER_DISPLAYED = By.cssSelector("#permission-statement > legend");

	private static final By GET_NEW_CONTACT_PREAMBLE_INFO_TEXT = By.id("permission-statement-description");

	private static final By GET_PRIVACY_POLICY_TEXT = By.id("fairProcessingNoticeText");

	public By GET_MEMBERSHIP_PACK_TO_ME_CHECKBOX = By.cssSelector("#main-content > section > form > div:nth-child(4) > div:nth-child(1) > div > fieldset > div > div > div:nth-child(1) > label");

	//	#main-content > section > form > div:nth-child(4) > div:nth-child(1) > div > fieldset > div > div > div:nth-child(1) > label

	public By GET_MEMBERSHIP_PACK_TO_RECIPIENT_CHECKBOX = By.cssSelector("label[for='giftPackRecipient-GIFT_GIVER']");

	//	public By GET_TITLE_FIELD = By.cssSelector("#name\\2e title");
	public By GET_TITLE_FIELD = By.id("name.title");

	public By GET_GIFT_GIVER_TITLE_FIELD = By.id("giftGiver.name.title");

	//	public By GET_FIRST_NAME_FIELD = By.cssSelector("#name\\2e firstName");
	public By GET_FIRST_NAME_FIELD = By.id("name.firstName");

	public By GET_GIFT_GIVER_FIRST_NAME_FIELD = By.id("giftGiver.name.firstName");

	//	public By GET_LAST_NAME_FIELD = By.cssSelector("#name\\2e lastName");
	public By GET_LAST_NAME_FIELD = By.id("name.lastName");

	public By GET_GIFT_GIVER_LAST_NAME_FIELD = By.id("giftGiver.name.lastName");

	//	public By GET_DOB_DAY_FIELD = By.cssSelector("#dateOfBirth\\2e day");
	public By GET_DOB_DAY_FIELD = By.id("dateOfBirth.day");

	public By GET_GIFT_GIVER_DOB_DAY_FIELD = By.id("giftGiver.dateOfBirth.day");

	//	public By GET_DOB_MONTH_FIELD = By.cssSelector("#dateOfBirth\\2e month");
	public By GET_DOB_MONTH_FIELD = By.id("dateOfBirth.month");

	public By GET_GIFT_GIVER_DOB_MONTH_FIELD = By.id("giftGiver.dateOfBirth.month");

	//	public By GET_DOB_YEAR_FIELD = By.cssSelector("#dateOfBirth\\2e year");
	public By GET_DOB_YEAR_FIELD = By.id("dateOfBirth.year");

	public By GET_GIFT_GIVER_DOB_YEAR_FIELD = By.id("giftGiver.dateOfBirth.year");

	//	public By GET_ADDRESS_LINE_1_FIELD = By.cssSelector("#address\\2e addressLine1");
	public By GET_ADDRESS_LINE_1_FIELD = By.id("address.addressLine1");

	public By GET_GIFT_GIVER_ADDRESS_LINE_1_FIELD = By.id("giftGiver.address.addressLine1");

	//	public By GET_ADDRESS_LINE_2_FIELD = By.cssSelector("#address\\2e addressLine2");
	public By GET_ADDRESS_LINE_2_FIELD = By.id("address.addressLine2");

	public By GET_GIFT_GIVER_ADDRESS_LINE_2_FIELD = By.id("giftGiver.address.addressLine2");

	//	public By GET_ADDRESS_LINE_3_FIELD = By.cssSelector("#address\\2e addressLine3");
	public By GET_ADDRESS_LINE_3_FIELD = By.id("address.addressLine3");

	public By GET_GIFT_GIVER_ADDRESS_LINE_3_FIELD = By.id("giftGiver.address.addressLine3");

	//	public By GET_ADDRESS_LINE_4_FIELD = By.cssSelector("#address\\2e addressLine4");
	public By GET_ADDRESS_LINE_4_FIELD = By.id("address.addressLine4");

	public By GET_GIFT_GIVER_ADDRESS_LINE_4_FIELD = By.id("giftGiver.address.addressLine4");

	//	public By GET_ADDRESS_CITY_FIELD = By.cssSelector("#address\\2e city");
	public By GET_ADDRESS_CITY_FIELD = By.id("address.city");

	public By GET_GIFT_GIVER_ADDRESS_CITY_FIELD = By.id("giftGiver.address.city");

	//	public By GET_ADDRESS_COUNTY_FIELD = By.cssSelector("#address\\2e county");
	public By GET_ADDRESS_COUNTY_FIELD = By.id("address.county");

	public By GET_GIFT_GIVER_ADDRESS_COUNTY_FIELD = By.id("giftGiver.address.county");

	//	public By GET_ADDRESS_POSTCODE_FIELD = By.cssSelector("#address\\2e postcode");
	public By GET_ADDRESS_POSTCODE_FIELD = By.id("address.postcode");

	public By GET_GIFT_GIVER_POSTCODE_FIELD = By.id("giftGiver.address.postcode");

	//	public By GET_PRIMARY_PHONE_NUMBER_FIELD = By.cssSelector("#contactDetails\\2e preferredTelephoneNumber");
	public By GET_PRIMARY_PHONE_NUMBER_FIELD = By.id("contactDetails.preferredTelephoneNumber");

	public By GET_GIFT_GIVER_PRIMARY_PHONE_NUMBER_FIELD = By.id("giftGiver.contactDetails.preferredTelephoneNumber");

	public By GET_RECIPIENT_PHONE_NUMBER_FIELD = By.id("giftRecipientContactDetails.telephoneNumber");

	//	public By GET_ALTERNATIVE_PHONE_NUMBER_FIELD = By.cssSelector("#contactDetails\\2e alternativeTelephoneNumber");
	public By GET_ALTERNATIVE_PHONE_NUMBER_FIELD = By.id("contactDetails.alternativeTelephoneNumber");

	public By GET_GIFT_GIVER_ALTERNATIVE_PHONE_NUMBER_FIELD = By.id("giftGiver.contactDetails.alternativeTelephoneNumber");

	//	public By GET_EMAIL_ADDRESS_FIELD = By.cssSelector("#contactDetails\\2e emailAddress");
	public By GET_EMAIL_ADDRESS_FIELD = By.id("contactDetails.emailAddress");

	public By GET_GIFT_GIVER_EMAIL_ADDRESS_FIELD = By.id("giftGiver.contactDetails.emailAddress");

	public By GET_GIFT_START_DAY_FIELD = By.id("giftPackStartDate.day");

	public By GET_GIFT_START_MONTH_FIELD = By.id("giftPackStartDate.month");

	public By GET_GIFT_START_YEAR_FIELD = By.id("giftPackStartDate.year");

	public By ADDITIONAL_MEMBER_TITLE = By.id("adultAdditionalMembers0.name.title");

	public By ADDITIONAL_MEMBER_FIRST_NAME = By.id("adultAdditionalMembers0.name.firstName");

	public By ADDITIONAL_MEMBER_LAST_NAME = By.id("adultAdditionalMembers0.name.lastName");

	public By ADDITIONAL_MEMBER_DOB_DAY = By.id("adultAdditionalMembers0.dateOfBirth.day");

	public By ADDITIONAL_MEMBER_DOB_MONTH = By.id("adultAdditionalMembers0.dateOfBirth.month");

	public By ADDITIONAL_MEMBER_DOB_YEAR = By.id("adultAdditionalMembers0.dateOfBirth.year");

	public By DATE_PICKER_OPTION = By.cssSelector("#datePickerEl");

	public By DATE_PICKER_MONTH_FORWARD = By.cssSelector("tr.monthSelector > td:nth-child(3)");

	protected WebDriver driver;

	public JointMembershipPage(WebDriver dr) {
		this.driver = dr;
	}

	public void navigateToJointMemberShipPage() {
		setLogs("Navigating to the Joine membership page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("jointURL"));
	}

	public void navigateToJointGiftMemberShipPage() {
		setLogs("Navigating to the Joint gift membership page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("jointGift"));
	}

	public void navigateToLifeJointGiftMemberShipPage() {
		setLogs("Navigating to the Life Joint gift membership page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("jointLifeGift"));
	}

	public void navigateToLifeJointMemberShipPage() {
		setLogs("Navigating to the life joint membership page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("jointLife"));
	}

	public void navigateToSessionTimeoutPage() {
		setLogs("Navigating to the Individual membership page");
		driver.navigate().to(EnvironmentConfiguration.getBaseURL() + EnvironmentConfiguration.getText("sessionTimeoutURL"));
	}

	public String getPersonalDetailsText() {
		navigateToJointMemberShipPage();
		setLogs("Getting the personal details text");
		return Helpers.getText(driver, PERSONAL_DETAILS_LABEL);
	}

	public String getIndividualMemberShipHeaderText() {
		navigateToJointMemberShipPage();
		setLogs("Getting the personal details text");
		return Helpers.getText(driver, INDIVIDUAL_MEMBERSHIP_TEXT);
	}

	public String getYourDetailsLabelText() {
		navigateToJointMemberShipPage();
		setLogs("Getting the your details header label text");
		return Helpers.getText(driver, YOUR_DETAILS_HEADER_LABEL);
	}

	public String getNameLabelText() {
		navigateToJointMemberShipPage();
		setLogs("Getting the name label text");
		return Helpers.getText(driver, NAME_LABEL);

	}

	public String getTitleLabelText() {
		navigateToJointMemberShipPage();
		setLogs("Getting the title label text");
		return Helpers.getText(driver, TITLE_LABEL);
	}

	public String getTitleLabelRequiredIndicator() {
		navigateToJointMemberShipPage();
		setLogs("Getting the title field required");
		return Helpers.getText(driver, TITLE_LABEL_REQUIRED_INDICATOR);
	}

	public String getFirstNameLabelRequiredIndicator() {
		navigateToJointMemberShipPage();
		setLogs("Getting the first name field required indicator");
		return Helpers.getText(driver, FIRST_NAME_REQUIRED_INDICATOR);
	}

	//	public void clickPersonalDetailsContinueButton() {
	//		driver.findElement(PERSONAL_DETAILS_CONTINUE_BUTTON).click();
	//	}

	public void setTitle(String title) {
		driver.findElement(GET_TITLE_FIELD).sendKeys(title);
	}

	public void setAdditionalMemberTitle(String title) {
		driver.findElement(ADDITIONAL_MEMBER_TITLE).sendKeys(title);
	}

	public void setGiftGiverTitle(String title) {
		driver.findElement(GET_GIFT_GIVER_TITLE_FIELD).sendKeys(title);
	}

	public void setFirstName(String firstName) {
		driver.findElement(GET_FIRST_NAME_FIELD).sendKeys(firstName);
	}

	public void setAdditionalMemberFirstName(String firstName) {
		driver.findElement(ADDITIONAL_MEMBER_FIRST_NAME).sendKeys(firstName);
	}

	public void setGiftGiverFirstName(String firstName) {
		driver.findElement(GET_GIFT_GIVER_FIRST_NAME_FIELD).sendKeys(firstName);
	}

	public void setLastName(String lastName) {
		driver.findElement(GET_LAST_NAME_FIELD).sendKeys(lastName);
	}

	public void setAdditionalMemberLastName(String lastName) {
		driver.findElement(ADDITIONAL_MEMBER_LAST_NAME).sendKeys(lastName);
	}

	public void setGiftGiverLastName(String lastName) {
		driver.findElement(GET_GIFT_GIVER_LAST_NAME_FIELD).sendKeys(lastName);
	}

	public void setDOBDay(String DOBDay) {
		driver.findElement(jointMembershipPage().GET_DOB_DAY_FIELD).sendKeys(DOBDay);
	}

	public void setAdditionalMemberDOBDay(String DOBDay) {
		driver.findElement(ADDITIONAL_MEMBER_DOB_DAY).sendKeys(DOBDay);
	}

	public void setGiftGiverDOBDay(String DOBDay) {
		driver.findElement(GET_GIFT_GIVER_DOB_DAY_FIELD).sendKeys(DOBDay);
	}

	public void setDOBMonth(String DOBMonth) {
		driver.findElement(GET_DOB_MONTH_FIELD).sendKeys(DOBMonth);
	}

	public void setAdditionalMemberDOBMonth(String DOBMonth) {
		driver.findElement(ADDITIONAL_MEMBER_DOB_MONTH).sendKeys(DOBMonth);
	}

	public void setGiftGiverDOBMonth(String DOBMonth) {
		driver.findElement(GET_GIFT_GIVER_DOB_MONTH_FIELD).sendKeys(DOBMonth);
	}

	public void setDOBYear(String DOBYear) {
		driver.findElement(GET_DOB_YEAR_FIELD).sendKeys(DOBYear);
	}

	public void setAdditionalMemberDOBYear(String DOBYear) {
		driver.findElement(ADDITIONAL_MEMBER_DOB_YEAR).sendKeys(DOBYear);
	}

	public void setGiftGiverDOBYear(String DOBYear) {
		driver.findElement(GET_GIFT_GIVER_DOB_YEAR_FIELD).sendKeys(DOBYear);
	}

	public void setAddressLine1(String addressLine1) {
		driver.findElement(GET_ADDRESS_LINE_1_FIELD).sendKeys(addressLine1);
	}

	public void setGiftGiverAddressLine1(String addressLine1) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_LINE_1_FIELD).sendKeys(addressLine1);
	}

	public void setAddressLine2(String addressLine2) {
		driver.findElement(GET_ADDRESS_LINE_2_FIELD).sendKeys(addressLine2);
	}

	public void setGiftGiverAddressLine2(String addressLine2) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_LINE_2_FIELD).sendKeys(addressLine2);
	}

	public void setAddressLine3(String addressLine3) {
		driver.findElement(GET_ADDRESS_LINE_3_FIELD).sendKeys(addressLine3);
	}

	public void setGiftGiverAddressLine3(String addressLine3) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_LINE_3_FIELD).sendKeys(addressLine3);
	}

	public void setAddressLine4(String addressLine4) {
		driver.findElement(GET_ADDRESS_LINE_4_FIELD).sendKeys(addressLine4);
	}

	public void setGiftGiverAddressLine4(String addressLine4) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_LINE_4_FIELD).sendKeys(addressLine4);
	}

	public void setAddressCity(String addressCity) {
		driver.findElement(jointMembershipPage().GET_ADDRESS_CITY_FIELD).sendKeys(addressCity);
	}

	public void setGiftGiverAddressCity(String addressCity) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_CITY_FIELD).sendKeys(addressCity);
	}

	public void setAddressCounty(String addressCounty) {
		driver.findElement(GET_ADDRESS_COUNTY_FIELD).sendKeys(addressCounty);
	}

	public void setGiftGiverAddressCounty(String addressCounty) {
		driver.findElement(GET_GIFT_GIVER_ADDRESS_COUNTY_FIELD).sendKeys(addressCounty);
	}

	public void setAddressPostcode(String addressPostcode) {
		driver.findElement(GET_ADDRESS_POSTCODE_FIELD).sendKeys(addressPostcode);
	}

	public void setGiftGiverAddressPostcode(String addressPostcode) {
		driver.findElement(GET_GIFT_GIVER_POSTCODE_FIELD).sendKeys(addressPostcode);
	}

	public void setPrimaryPhone(String primaryPhone) {
		driver.findElement(GET_PRIMARY_PHONE_NUMBER_FIELD).sendKeys(primaryPhone);
	}

	public void setGiftGiverPrimaryPhone(String primaryPhone) {
		driver.findElement(GET_GIFT_GIVER_PRIMARY_PHONE_NUMBER_FIELD).sendKeys(primaryPhone);
	}

	public void setRecipientPhone(String recipientPhone) {
		driver.findElement(GET_RECIPIENT_PHONE_NUMBER_FIELD).sendKeys(recipientPhone);
	}

	public void setGiftGiverAlternativePhone(String alternativePhone) {
		driver.findElement(GET_GIFT_GIVER_ALTERNATIVE_PHONE_NUMBER_FIELD).sendKeys(alternativePhone);
	}

	public void setAlternativePhone(String alternativePhone) {
		driver.findElement(GET_ALTERNATIVE_PHONE_NUMBER_FIELD).sendKeys(alternativePhone);
	}

	public void setEmailAddress(String emailAddress) {
		driver.findElement(GET_EMAIL_ADDRESS_FIELD).sendKeys(emailAddress);
	}

	public void setGiftGiverEmailAddress(String emailAddress) {
		driver.findElement(GET_GIFT_GIVER_EMAIL_ADDRESS_FIELD).sendKeys(emailAddress);
	}

	public void selectContactYesButton() {
		driver.findElement(GET_CONTACT_YOU_YES_CHECKBOX).click();
	}

	public void selectContactNoButton() {
		driver.findElement(GET_CONTACT_YOU_NO_CHECKBOX).click();
	}

	public void selectSendMeGift() {
		driver.findElement(GET_MEMBERSHIP_PACK_TO_ME_CHECKBOX).click();
	}

	public void selectSendRecipientGift() {
		driver.findElement(GET_MEMBERSHIP_PACK_TO_RECIPIENT_CHECKBOX).click();
	}

	public void setGiftPackDay(String giftDay) {
		driver.findElement(GET_GIFT_START_DAY_FIELD).sendKeys(giftDay);
	}

	public void setGiftPackMonth(String giftMonth) {
		driver.findElement(GET_GIFT_START_MONTH_FIELD).sendKeys(giftMonth);
	}

	public void setGiftPackYear(String giftYear) {
		driver.findElement(GET_GIFT_START_YEAR_FIELD).sendKeys(giftYear);
	}

	public void addPersonalDetailsForLead(String title, String firstName, String lastName, String DOBDay, String DOBMonth, String DOBYear, String addressLine1, String addressLine2,
			String addressLine3, String addressLine4, String addressCity, String addressCounty, String addressPostcode, String primaryPhone, String alternativePhone, String emailAddress) {
		setTitle(title);
		setFirstName(firstName);
		setLastName(lastName);
		setDOBDay(DOBDay);
		setDOBMonth(DOBMonth);
		setDOBYear(DOBYear);
		driver.findElement(individualMembershipPage().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		setAddressLine1(addressLine1);
		setAddressLine2(addressLine2);
		setAddressLine3(addressLine3);
		setAddressLine4(addressLine4);
		setAddressCity(addressCity);
		setAddressCounty(addressCounty);
		setAddressPostcode(addressPostcode);
		setPrimaryPhone(primaryPhone);
		setAlternativePhone(alternativePhone);
		setEmailAddress(emailAddress);
	}

	public void addAdditionalMemberPersonalDetails(String title, String firstName, String lastName, String DOBDay, String DOBMonth, String DOBYear) {
		setAdditionalMemberTitle(title);
		setAdditionalMemberFirstName(firstName);
		setAdditionalMemberLastName(lastName);
		setAdditionalMemberDOBDay(DOBDay);
		setAdditionalMemberDOBMonth(DOBMonth);
		setAdditionalMemberDOBYear(DOBYear);
	}

	public void setRecipientPersonalDetails(String title, String firstName, String lastName, String DOBDay, String DOBMonth, String DOBYear, String addressLine1, String addressLine2,
			String addressLine3, String addressLine4, String addressCity, String addressCounty, String addressPostcode, String recipientPhone) {
		setTitle(title);
		setFirstName(firstName);
		setLastName(lastName);
		setDOBDay(DOBDay);
		setDOBMonth(DOBMonth);
		setDOBYear(DOBYear);
		driver.findElement(individualMembershipPage().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		setAddressLine1(addressLine1);
		setAddressLine2(addressLine2);
		setAddressLine3(addressLine3);
		setAddressLine4(addressLine4);
		setAddressCity(addressCity);
		setAddressCounty(addressCounty);
		setAddressPostcode(addressPostcode);
		setRecipientPhone(recipientPhone);
	}

	public void setGiftGiverPersonalDetails(String title, String firstName, String lastName, String DOBDay, String DOBMonth, String DOBYear, String addressLine1, String addressLine2,
			String addressLine3, String addressLine4, String addressCity, String addressCounty, String addressPostcode, String primaryPhone, String alternativePhone, String emailAddress,
			String giftDay, String giftMonth, String giftYear) {
		setGiftGiverTitle(title);
		setGiftGiverFirstName(firstName);
		setGiftGiverLastName(lastName);
		setGiftGiverDOBDay(DOBDay);
		setGiftGiverDOBMonth(DOBMonth);
		setGiftGiverDOBYear(DOBYear);
		driver.findElement(individualMembershipPage().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		setGiftGiverAddressLine1(addressLine1);
		setGiftGiverAddressLine2(addressLine2);
		setGiftGiverAddressLine3(addressLine3);
		setGiftGiverAddressLine4(addressLine4);
		setGiftGiverAddressCity(addressCity);
		setGiftGiverAddressCounty(addressCounty);
		setGiftGiverAddressPostcode(addressPostcode);
		setGiftGiverPrimaryPhone(primaryPhone);
		setGiftGiverAlternativePhone(alternativePhone);
		setGiftGiverEmailAddress(emailAddress);
		driver.findElement(individualMembershipPage().GET_GIFT_CONTACT_BY_EMAIL_NO_CHECKBOX).click();
		driver.findElement(individualMembershipPage().GET_GIFT_CONTACT_BY_POST_NO_CHECKBOX).click();
		driver.findElement(individualMembershipPage().GET_GIFT_CONTACT_BY_PHONE_NO_CHECKBOX).click();
		selectSendRecipientGift();
	}

	public void setGiftGiverPersonalDetailsNoPrefs(String title, String firstName, String lastName, String DOBDay, String DOBMonth, String DOBYear, String addressLine1, String addressLine2,
			String addressLine3, String addressLine4, String addressCity, String addressCounty, String addressPostcode, String primaryPhone, String alternativePhone, String emailAddress,
			String giftDay, String giftMonth, String giftYear) {
		setGiftGiverTitle(title);
		setGiftGiverFirstName(firstName);
		setGiftGiverLastName(lastName);
		setGiftGiverDOBDay(DOBDay);
		setGiftGiverDOBMonth(DOBMonth);
		setGiftGiverDOBYear(DOBYear);
		driver.findElement(individualMembershipPage().GET_ENTER_ADDRESS_MANUALLY_LINK).click();
		setGiftGiverAddressLine1(addressLine1);
		setGiftGiverAddressLine2(addressLine2);
		setGiftGiverAddressLine3(addressLine3);
		setGiftGiverAddressLine4(addressLine4);
		setGiftGiverAddressCity(addressCity);
		setGiftGiverAddressCounty(addressCounty);
		setGiftGiverAddressPostcode(addressPostcode);
		setGiftGiverPrimaryPhone(primaryPhone);
		setGiftGiverAlternativePhone(alternativePhone);
		setGiftGiverEmailAddress(emailAddress);
		selectSendRecipientGift();

	}

	public boolean checkNewContactPostNoPrefAppears() {
		setLogs("check if contact by Post No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_POST_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPostYesPrefAppears() {
		setLogs("check if contact by Post Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_POST_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailNoPrefAppears() {
		setLogs("check if contact by Email No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_EMAIL_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactEmailYesPrefAppears() {
		setLogs("check if contact by Email Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_EMAIL_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneNoPrefAppears() {
		setLogs("check if contact by Phone No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_NO_CHECKBOX, 10);
	}

	public boolean checkNewContactPhoneYesPrefAppears() {
		setLogs("check if contact by Phone Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_BY_PHONE_YES_CHECKBOX, 10);
	}

	public boolean checkNewContactInfoTextAppears() {
		setLogs("Check ContactPref info text appears.......");
		return Helpers.waitForIsDisplayed(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10);
	}

	public boolean checkOldContactYesOptionAppears() {
		setLogs("Check if Contact Yes option appears");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_YOU_YES_CHECKBOX, 10);
	}

	public boolean checkOldContactNoOptionAppears() {
		setLogs("Check if Contact No option appears");
		return Helpers.waitForIsDisplayed(driver, GET_CONTACT_YOU_NO_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactPostNoPrefAppears() {
		setLogs("check if contact by Post No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_POST_NO_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactPostYesPrefAppears() {
		setLogs("check if contact by Post Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_POST_YES_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactEmailNoPrefAppears() {
		setLogs("check if contact by Email No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_EMAIL_NO_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactEmailYesPrefAppears() {
		setLogs("check if contact by Email Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_EMAIL_YES_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactPhoneNoPrefAppears() {
		setLogs("check if contact by Phone No Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_PHONE_NO_CHECKBOX, 10);
	}

	public boolean checkNewGiftContactPhoneYesPrefAppears() {
		setLogs("check if contact by Phone Yes Checkbox is displayed......");
		return Helpers.waitForIsDisplayed(driver, GET_GIFT_CONTACT_BY_PHONE_YES_CHECKBOX, 10);
	}

	public String getContactPrefContactingYouHeaderDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_CONTACTING_YOU_HEADER_DISPLAYED, 10, "Contact Header text not displayed");
		List<WebElement> contactHeaderText = driver.findElements(GET_CONTACTING_YOU_HEADER_DISPLAYED);
		if (contactHeaderText.size() != 0) {
			String contactHeaderTextDisplayed = driver.findElement(GET_CONTACTING_YOU_HEADER_DISPLAYED).getText();

			return contactHeaderTextDisplayed;
		}
		return null;

	}

	public String getContactPrefPreAmbleTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREAMBLE_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactText = driver.findElements(GET_NEW_CONTACT_PREAMBLE_INFO_TEXT);
		if (contactText.size() != 0) {
			String contactInfoTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREAMBLE_INFO_TEXT).getText();

			return contactInfoTextDisplayed;
		}
		return null;

	}

	public String getContactPrefInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_NEW_CONTACT_PREF_INFO_TEXT, 10, "Contact Pref Info text not displayed");
		List<WebElement> contactInfoText = driver.findElements(GET_NEW_CONTACT_PREF_INFO_TEXT);
		if (contactInfoText.size() != 0) {
			String contactInfoTextDisplayed = driver.findElement(GET_NEW_CONTACT_PREF_INFO_TEXT).getText();

			return contactInfoTextDisplayed;
		}
		return null;

	}

	public String getContactPrivacyPolicyInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver, GET_PRIVACY_POLICY_TEXT, 10, "Privacy policy Info text not displayed");
		List<WebElement> privacyInfoText = driver.findElements(GET_PRIVACY_POLICY_TEXT);
		if (privacyInfoText.size() != 0) {
			String privacyInfoTextDisplayed = driver.findElement(GET_PRIVACY_POLICY_TEXT).getText();

			return privacyInfoTextDisplayed;
		}
		return null;

	}

	public void selectDateOnDatePicker (int day){

		By selectorForDay = xpath("//table[@class = 'calendar']//td[contains(@class,'day')]/span[text() = '"+day+"']");
		driver.findElement(selectorForDay).click();

	}
}