package uk.org.nationaltrust.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.framework.SupporterServiceDBConnection;

/**
 * Created by Nick.Thompson on 21/12/2016.
 */
public class ConfirmationPage extends PageBase {

	protected WebDriver driver;

	public ConfirmationPage(WebDriver dr) {
		this.driver = dr;
	}

	SupporterServiceDBConnection SupporterDBConnection = new SupporterServiceDBConnection();

	public By CONFIRMATION_MAIN_HEADER = By.id("confirmation-page-heading");

	public By CONFIRMATION_MAIN_HEADER_LIFE = By.cssSelector ("#main-content > div > div:nth-child(2) > div > div.panel > h1");

	public static By TWITTER_BUTTON = By.xpath("//*[@id=\"main-content\"]/section[2]/div/div/div/div/ul/li[2]/a/span[1]");//By.cssSelector("#main-content > section:nth-child(3) > div > div > div > div > ul > li:nth-child(1) > a");

	public static By FACEBOOK_BUTTON = By.xpath("//*[@id=\"main-content\"]/section[2]/div/div/div/div/ul/li[1]/a/span[1]");//By.cssSelector("#main-content > section:nth-child(3) > div > div > div > div > ul > li:nth-child(2) > a");

	public static By PINTREST_BUTTON = By.cssSelector("#main-content > section:nth-child(3) > div > div > div > div > ul > li:nth-child(4) > a > span.icon");//By.cssSelector("#main-content > section:nth-child(3) > div > div > div > div > ul > li:nth-child(3) > a");

	public static By GOOGLE_BUTTON = By.xpath("//*[@id=\"main-content\"]/section[2]/div/div/div/div/ul/li[5]/a/span[1]");//By.cssSelector("#main-content > section:nth-child(3) > div > div > div > div > ul > li:nth-child(4) > a");

	public static By EMAIL_BUTTON = By.xpath("//span[contains(text(), 'Email')]");//By.cssSelector("#main-content > section:nth-child(3) > div > div > div > div > ul > li:nth-child(5) > a");

	public static By YOUTUBE_SHARE = By.xpath("//*[@id=\"main-content\"]/section[2]/div/div/div/div/ul/li[6]/a/span[1]");

	public static final By IF_YOU_PAID_BY_DD_NON_MONTHLY_DD_INFO_TEXT = By.cssSelector ("#main > div > div > p:nth-child(2)");

	public By CHISTMAS_CONFRMATION_TEXT = By.cssSelector ("#xmas-card-text > p:nth-child(2)");

	public static final By CONFIRMATION_TEXT = By.cssSelector("#thank-you");

	public static final By CONF_EMAIL_INFO_TEXT = By.id("");

	public static final By TEMPORARY_CARD_CONFIRMATION = By.id("");

	public static final By FREE_ENTRY_HANDBOOK_BULLET_INFO_TEXT = By.cssSelector("#confirmation-membership-pack-description > ul");

	public static final By FREE_PARKING_INFO_TEXT = By.id ("");

	public static final By OTHER_BENEFITS_INFO_TEXT = By.id ("");
	public static final By JUNIOR_GIFT_TWO_EMAILS_TEXT = By.cssSelector("#confirmation-expect-email > p");

	public static final By MEMBER_PACK_21DAY_INFO_TEXT = By.id ("confirmation-membership-pack-description");
	public static final By JUNIOR_MEMBER_PACK_21DAY_INFO_TEXT = By.cssSelector("#confirmation-membership-pack-description > p:nth-child(1)");

	public static final By MEMBER_HANDBOOK_INFO_TEXT = By.id("");

	public static final By CONF_MAIN_TEXT = By.id("confirmation-page-subheading");

	public static final By XMAS_ADULT_PDF = By.cssSelector("#xmas-card-links > div:nth-child(1) > a > div.nt-pdf-banner__image > img");

	public static final By XMAS_YOUNG_PERSON_PDF = By.cssSelector("#xmas-card-links > div:nth-child(3) > a > div.nt-pdf-banner__image > img");

	public static final By XMAS_JUNIOR_PDF = By.cssSelector("#xmas-card-links > div:nth-child(2) > a > div.nt-pdf-banner__image > img");

	private String DIY_ThankYouText="Thank you! Your fundraising kit will arrive in the next seven days, full of ideas and inspiration to help kick-start your activity. We couldn't be happier that you'd like to help protect the places that bring out the best in all of us.";

	public String getConfirmationTextDisplayed (){
		List<WebElement> confirmationText = driver.findElements(CONFIRMATION_TEXT);
		if (confirmationText.size() !=0){
			String confirmationTextDisplayed = driver.findElement(CONFIRMATION_TEXT).getText();
			return confirmationTextDisplayed;
		}
		return null;

	}

	public String getThankYouPageURL(){
		return driver.getCurrentUrl();
	}

	public String getChristmasTextDisplayed() {
		Helpers.waitForElementToAppear(driver,CHISTMAS_CONFRMATION_TEXT , 10, "Christmas text not displayed");
		List<WebElement> xmasText = driver.findElements(CHISTMAS_CONFRMATION_TEXT);
		if (xmasText.size() != 0) {
			String xmasTextDisplayed = driver.findElement(CHISTMAS_CONFRMATION_TEXT).getText();

			return xmasTextDisplayed;
		}
		return null;

	}

	public String getNonMonthlyDDInfoTextDisplayed() {
		Helpers.waitForElementToAppear(driver,IF_YOU_PAID_BY_DD_NON_MONTHLY_DD_INFO_TEXT , 10, "Pay by DD but not Monthly text not displayed");
		List<WebElement> nonMonthlyText = driver.findElements(IF_YOU_PAID_BY_DD_NON_MONTHLY_DD_INFO_TEXT);
		if (nonMonthlyText.size() != 0) {
			String nonMonthlyTesxDisplayed = driver.findElement(IF_YOU_PAID_BY_DD_NON_MONTHLY_DD_INFO_TEXT).getText();

			return nonMonthlyTesxDisplayed;
		}
		return null;

	}

	public boolean checkIfTwitterSocialShareButtonIsDisplayed(){
		setLogs("check if Twitter button is displayed.......");
		return Helpers.waitForIsDisplayed(driver,TWITTER_BUTTON,10);
	}

	public boolean checkIfFacebookSocialShareButtonIsDisplayed(){
		setLogs("check if Facebook button is displayed.......");
		return Helpers.waitForIsDisplayed(driver,FACEBOOK_BUTTON,10);
	}

	public boolean checkIfPintrestSocialShareButtonIsDisplayed(){
		setLogs("check if Pintrest button is displayed.......");
		return Helpers.waitForIsDisplayed(driver,PINTREST_BUTTON,10);
	}

	public boolean checkIfGoogleSocialShareButtonIsDisplayed(){
		setLogs("check if Google button is displayed.......");
		return Helpers.waitForIsDisplayed(driver,GOOGLE_BUTTON,10);
	}

	public boolean checkIfEmailSocialShareButtonIsDisplayed(){
		setLogs("check if Email button is displayed.......");
		return Helpers.waitForIsDisplayed(driver,EMAIL_BUTTON,10);
	}

	public boolean checkIfYoutubeSocialShareButtonIsDisplayed(){
		setLogs("check if You tube button is displayed.......");
		return Helpers.waitForIsDisplayed(driver,YOUTUBE_SHARE,10);
	}

	public boolean checkIfAdultPDFAppears(){
		setLogs("Check Adult PDF appears");
				return Helpers.waitForIsDisplayed(driver, XMAS_ADULT_PDF, 10);
	}

	public boolean checkIfJuniorPDFAppears(){
		setLogs ("Check Junior PDF appears");
		return Helpers.waitForIsDisplayed(driver, XMAS_JUNIOR_PDF, 10);
	}

	public boolean checkIfYoungPersonPDFAppears(){
		setLogs ("Check Young Person PDF appears");
		return Helpers.waitForIsDisplayed(driver, XMAS_YOUNG_PERSON_PDF, 10);
	}

	public String getConfirmationHeaderDisplayed() {
		List<WebElement> confirmationHeaderText = driver.findElements(CONFIRMATION_MAIN_HEADER);
		if (confirmationHeaderText.size() != 0) {
			String confirmationHeader = driver.findElement(CONFIRMATION_MAIN_HEADER).getText();
			return confirmationHeader;
		}
		return null;
	}

	public String getConfirmationLifeHeaderDisplayed() {
		List<WebElement> confirmationHeaderLifeText = driver.findElements(CONFIRMATION_MAIN_HEADER_LIFE);
		if (confirmationHeaderLifeText.size() != 0) {
			String confirmationLifeHeader = driver.findElement(CONFIRMATION_MAIN_HEADER_LIFE).getText();
			return confirmationLifeHeader;
		}
		return null;
	}

	public Map<String, String> getTheJoinDetailsForSingleIndividualMembFromSupporterBD(Set<String> labelNames) {
		String sqlSta = membershipRenewedInDBSql("264810737");
		setLogs(sqlSta);
		return SupporterDBConnection.getDataByLabelSet(sqlSta, labelNames);
		//		setLogs(supporterDetails);
	}

	private String membershipRenewedInDBSql(String supporterNumber) {
		System.out.println("Hello World!");
		return String.format("select PAYMENT_AMOUNT, SUBMISSION_STATUS, INCENTIVE_SOURCE_CODE FROM" + " SUPPORTERAPI_OWNER.PERSON" + " WHERE SUPPORTER_NUMBER = '%s'" + " order by CREATED desc",
				supporterNumber);

	}

	public String getPaymentInstructionConfirmationTextDisplayed() {
		Helpers.waitForElementToAppear(driver,IF_YOU_PAID_BY_DD_NON_MONTHLY_DD_INFO_TEXT , 10, "Payment Instruction text not displayed");
		List<WebElement> paymentInstruction = driver.findElements(IF_YOU_PAID_BY_DD_NON_MONTHLY_DD_INFO_TEXT);
		if (paymentInstruction.size() != 0) {
			String paymentInstructionText = driver.findElement(IF_YOU_PAID_BY_DD_NON_MONTHLY_DD_INFO_TEXT).getText();

			return paymentInstructionText;
		}
		return null;

	}
	public void deleteRenewedRecord(){
		setLogs("Delete record before Renew Supporter submitter sends the transaction to update in CRM so we have some test records in renewal state");
		String sqlSta = "Delete FROM" + " SUPPORTERAPI_OWNER.MEMBERSHIP_RENEWAL where" + " Created >= sysdate -5" ;
		setLogs(sqlSta);
		SupporterDBConnection.setData(sqlSta);
	}

	public String getEmailConfirmationTextDisplayed() {
		List<WebElement> confEmailText = driver.findElements(CONF_EMAIL_INFO_TEXT);
		if (confEmailText.size() != 0) {
			String confEmailTextDisplayed = driver.findElement(CONF_EMAIL_INFO_TEXT).getText();
			return confEmailTextDisplayed;
		}
		return null;
	}

	public String getTempCardConfirmationTextDisplayed() {
		List<WebElement> tempCardConfText = driver.findElements(TEMPORARY_CARD_CONFIRMATION);
		if (tempCardConfText.size() != 0) {
			String tempCardConfTextDisplayed = driver.findElement(TEMPORARY_CARD_CONFIRMATION).getText();
			return tempCardConfTextDisplayed;
		}
		return null;
	}

	public String getMemberInfoBulletTextDisplayed() {
		List<String> message = new ArrayList<String>();
		List<WebElement> welcomePackBulletsMessage = driver.findElements(FREE_ENTRY_HANDBOOK_BULLET_INFO_TEXT);
		for (int i = 0; i <welcomePackBulletsMessage.size(); i++) {
			message.add(welcomePackBulletsMessage.get(i).getText());
		}
		String allMessage = String.join(",", message);
		return allMessage;
	}



	public String getFreeParkingInfoTextDisplayed() {
		List<WebElement> freeParkingText = driver.findElements(FREE_PARKING_INFO_TEXT);
		if (freeParkingText.size() != 0) {
			String freeParkingTextDisplayed = driver.findElement(FREE_PARKING_INFO_TEXT).getText();
			return freeParkingTextDisplayed;
		}
		return null;
	}

	public String getOtherBenefitsInfoTextDisplayed() {
		List<WebElement> otherBenefitsText = driver.findElements(OTHER_BENEFITS_INFO_TEXT);
		if (otherBenefitsText.size() != 0) {
			String otherBenefitsTextDisplayed = driver.findElement(OTHER_BENEFITS_INFO_TEXT).getText();
			return otherBenefitsTextDisplayed;
		}
		return null;
	}


	public String get21DayMemberPackInfoTextDisplayed() {
		List<WebElement> memberPackText = driver.findElements(MEMBER_PACK_21DAY_INFO_TEXT);
		if (memberPackText.size() != 0) {
			String memberPackTextDisplayed = driver.findElement(MEMBER_PACK_21DAY_INFO_TEXT).getText();
			return memberPackTextDisplayed;
		}
		return null;
	}

	public String getJunior21DayMemberPackInfoTextDisplayed() {
		List<WebElement> memberPackText = driver.findElements(JUNIOR_MEMBER_PACK_21DAY_INFO_TEXT);
		if (memberPackText.size() != 0) {
			String memberPackTextDisplayed = driver.findElement(JUNIOR_MEMBER_PACK_21DAY_INFO_TEXT).getText();
			return memberPackTextDisplayed;
		}
		return null;
	}

	public String getJuniorGiftTwoEmailsInfoTextDisplayed() {
		List<WebElement> twoEmailsText = driver.findElements(JUNIOR_GIFT_TWO_EMAILS_TEXT);
		if (twoEmailsText.size() != 0) {
			String twoEmailsTextDisplayed = driver.findElement(JUNIOR_GIFT_TWO_EMAILS_TEXT).getText();
			return twoEmailsTextDisplayed;
		}
		return null;
	}

	public String getMembersHandbookInfoTextDisplayed() {
		List<WebElement> memberHandbookText = driver.findElements(MEMBER_HANDBOOK_INFO_TEXT);
		if (memberHandbookText.size() != 0) {
			String memberHandbookTextDisplayed = driver.findElement(MEMBER_HANDBOOK_INFO_TEXT).getText();
			return memberHandbookTextDisplayed;
		}
		return null;
	}

	public String getGivingMainInfoTextDisplayed() {
		List<WebElement> confMainText = driver.findElements(CONF_MAIN_TEXT);
		if (confMainText.size() != 0) {
			String confMainTextDisplayed = driver.findElement(CONF_MAIN_TEXT).getText();
			return confMainTextDisplayed;
		}
		return null;
	}

	//conf page bullets
	public String getConfPageInformationBullets() {
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.page.member.card.and.handbook.bullets").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);}
	//conf page bullets gift

	public String getConfPageInformationBulletsGift() {
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.gift.page.member.card.and.handbook.bullets").replace("<li>", "").replace("</li>", ",").replaceFirst(".$", "");
		return StringUtils.chop(expectedString);}


		public String getMainGiftConfirmationPageText(){
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.page.main.content.gift.text").replace("<li>", "").replace("</li>", ",").replace("more","more.");
		return StringUtils.chop(expectedString);}


	public String getMainGiftLifeConfirmationPageText(){
		String expectedString = EnvironmentConfiguration.getMessageText("confirmation.page.main.content.gift.life.text").replace("<li>", "").replace("</li>", ",").replace("more","more.");
		return StringUtils.chop(expectedString);}

	public boolean DIYConfirmPage(){
		// Thank you page Header.
		driver.findElement(By.xpath("//title[contains(text(),'Thank you')]"));
		return driver.findElement(By.id("fp-thank-you-description")).getText().contains(DIY_ThankYouText);
	}

}

//	public boolean checkDisplayedText(String optionSelected) {
//		boolean status = false;
//
//		switch (optionSelected) {
//			case "NONE_GIFT_ADULT":
//				//				String getAlreadyHaveAccountWarningText = cpcShopUserPage().getAccountAlreadyExistsInfoTextDisplayed();
//				//				Assert.assertEquals(getAlreadyHaveAccountWarningText, "You already have a MyNT account");
//				if (
//						!checkPasswordQuestionAppears() &&
//								!checkPostcodeQuestionAppears() &&
//								checkNewContactPostYesPrefAppears() &&
//								checkNewContactPostNoPrefAppears() &&
//								checkNewContactEmailNoPrefAppears() &&
//								checkNewContactEmailYesPrefAppears() &&
//								checkNewContactPhoneNoPrefAppears() &&
//								checkNewContactPhoneYesPrefAppears())
//					status = true;
//				break;
//			case "GIFT_ADULT":
//				if (
//						!checkPasswordQuestionAppears() &&
//								!checkPostcodeQuestionAppears() &&
//								!checkNewContactPostYesPrefAppears() &&
//								!checkNewContactPostNoPrefAppears() &&
//								!checkNewContactEmailNoPrefAppears() &&
//								!checkNewContactEmailYesPrefAppears() &&
//								!checkNewContactPhoneNoPrefAppears() &&
//								!checkNewContactPhoneYesPrefAppears())
//					status = true;
//				break;
//			case "GIFT_ADULT_LESS21":
//
//				if (
//						checkPasswordQuestionAppears() &&
//								checkPostcodeQuestionAppears() &&
//								checkNewContactPostYesPrefAppears() &&
//								checkNewContactPostNoPrefAppears() &&
//								checkNewContactEmailNoPrefAppears() &&
//								checkNewContactEmailYesPrefAppears() &&
//								checkNewContactPhoneNoPrefAppears() &&
//								checkNewContactPhoneYesPrefAppears())
//					status = true;
//				break;
//			case "GIFT_ADULT_MORE21":
//
//				if (
//						checkPasswordQuestionAppears() &&
//								checkPostcodeQuestionAppears() &&
//								checkNewContactPostYesPrefAppears() &&
//								checkNewContactPostNoPrefAppears() &&
//								checkNewContactEmailNoPrefAppears() &&
//								checkNewContactEmailYesPrefAppears() &&
//								checkNewContactPhoneNoPrefAppears() &&
//								checkNewContactPhoneYesPrefAppears())
//					status = true;
//				break;
//			case "NON_GIFT_LIFE":
//				//				Assert.assertFalse(checkNewContactPostYesPrefAppears());
//				if (
//						checkPasswordQuestionAppears() &&
//								checkPostcodeQuestionAppears() &&
//								!checkNewContactPostYesPrefAppears() &&
//								!checkNewContactPostNoPrefAppears() &&
//								!checkNewContactEmailNoPrefAppears() &&
//								!checkNewContactEmailYesPrefAppears() &&
//								!checkNewContactPhoneNoPrefAppears() &&
//								!checkNewContactPhoneYesPrefAppears())
//					status = true;
//				break;
//			case "GIFT_LIFE":
//
//				if (
//						checkPasswordQuestionAppears() &&
//								checkPostcodeQuestionAppears() &&
//								checkNewContactPostYesPrefAppears() &&
//								checkNewContactPostNoPrefAppears() &&
//								checkNewContactEmailNoPrefAppears() &&
//								checkNewContactEmailYesPrefAppears() &&
//								checkNewContactPhoneNoPrefAppears() &&
//								checkNewContactPhoneYesPrefAppears())
//					status = true;
//				break;
//
//			case "GIFT_LIFE":
//
//				if (
//						checkPasswordQuestionAppears() &&
//								checkPostcodeQuestionAppears() &&
//								checkNewContactPostYesPrefAppears() &&
//								checkNewContactPostNoPrefAppears() &&
//								checkNewContactEmailNoPrefAppears() &&
//								checkNewContactEmailYesPrefAppears() &&
//								checkNewContactPhoneNoPrefAppears() &&
//								checkNewContactPhoneYesPrefAppears())
//					status = true;
//				break;
//
//			default:
//				break;
//		}
//		return status;
//	}


