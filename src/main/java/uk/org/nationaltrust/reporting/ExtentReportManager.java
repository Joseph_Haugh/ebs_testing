package uk.org.nationaltrust.reporting;

import java.io.File;
import org.testng.ITestContext;
import org.testng.Reporter;
import com.relevantcodes.extentreports.ExtentReports;

/**
 * @author Pramod.Reguri
 */
public class ExtentReportManager {

	private static ExtentReports extent;

	private static ITestContext context;

	public synchronized static ExtentReports getInstance() {
		File extentReportConfig = new File(System.getProperty("user.dir") + "/src/main/resources/extentreport.xml");
		if (extent == null) {
			File outputDirectory = new File(context.getOutputDirectory());
			File resultDirectory = new File(System.getProperty("user.dir") + "/target/htmlReport");
			extent = new ExtentReports(resultDirectory + File.separator + "JoinTestReport.html", true);
			extent.loadConfig(extentReportConfig);
			Reporter.log("Extent Report directory :" + resultDirectory, true);
		}
		return extent;
	}

	public static void setOutputDirectory(ITestContext context) {
		ExtentReportManager.context = context;
	}

}
