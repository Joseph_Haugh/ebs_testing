package uk.org.nationaltrust.reporting;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Reporter;
import uk.org.nationaltrust.framework.TestBase;

/*
@author Pramod.Reguri
 */
public class TestStepLogger extends TestBase {

	public static void log(final String message) {
		Reporter.log(message + "<br>", true);
		ExtentTestManager.getTest().log(LogStatus.PASS, message + "<br>");
	}

}
