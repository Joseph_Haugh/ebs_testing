package uk.org.nationaltrust.resources;

/**
 * Created by Steve Bart on 09/03/16.
 */
public interface JsonPathStrings {

	public static final String JSON_PATH_OPENING_TIMES_NOTE_TEXT = "$.openingTimesNote";
	public static final String JSON_PATH_TODAYS_OPENING_TIMES = "$.openingTimes..";
	public static final String JSON_PATH_BY_CYCLE_TEXT = "$.directions.cycle.htmlDescription";
	public static final String JSON_PATH_POSTAL_ADDRESS = "$.location.postalAddress";
	public static final String JSON_PATH_BY_FERRY_TEXT = "$.directions.ferry.htmlDescription";
	public static final String JSON_PATH_SUSTRANS = "$.directions.cycle.cycleRouteUrl";
	public static final String JSON_PATH_BY_CUSTOM_TEXT = "$.otherDirections[*].htmlDescription";
	public static final String JSON_PATH_BY_ROAD_TEXT = "$.directions.road.htmlDescription";
	public static final String JSON_PATH_BY_ROAD_PARKING_TEXT = "$.directions.road.parking";
	public static final String JSON_PATH_BY_TRAIN_TEXT = "$.directions.train.htmlDescription";
	public static final String JSON_PATH_BY_FOOT_TEXT = "$.directions.foot.htmlDescription";
	public static final String JSON_PATH_BY_BUS_TEXT = "$.directions.bus.htmlDescription";
	public static final String JSON_PATH_BY_UNDERGROUND_TEXT = "$.directions.underground.htmlDescription";
	public static final String JSON_PATH_PLACE_NAME = "$.name";
	public static final String JSON_PATH_EMAIL_ADDRESS = "$.contact.email";
	public static final String JSON_PATH_PHONE_NUMBER = "$.contact.telephone";
	public static final String JSON_PATH_STRAP_LINE = "$.description.strapline";
	public static final String JSON_PATH_ACCESSIBILITY_TEXT = "$.noteCategories[?(@.name=='accessibility')].htmlNotes";
	public static final String JSON_PATH_FAMILY_TEXT = "$.noteCategories[?(@.name=='family')].htmlNotes";
	public static final String JSON_PATH_GENERAL_TEXT = "$.noteCategories[?(@.name=='general')].htmlNotes";
	public static final String JSON_PATH_PRICE_CATEGORY_ONE = "$.categories[0].name";
	public static final String JSON_PATH_PRICE_CATEGORY_TWO = "$.categories[1].name";
	public static final String JSON_PATH_BOOKING_URL = "$.categories[0].bookingUrl";
	public static final String JSON_PATH_ADDITIONAL_FAMILY_TEXT = "$.categories[0].admissionPrices[3].name";
	public static final String JSON_PATH_CAT_ONE_MIN_GROUP_SIZE = "$.categories[0].groupAdmissionPrices..minimum";
	public static final String JSON_PATH_CAT_ONE_FAMILY_TEXT = "$.categories[0].admissionPrices[2].name";
	public static final String JSON_PATH_CAT_ONE_ADULT_TEXT = "$.categories[0].admissionPrices[0].name";
	public static final String JSON_PATH_CAT_ONE_CHILD_TEXT = "$.categories[0].admissionPrices[1].name";
	public static final String JSON_PATH_ADMISSION_PRICES = "$...admissionPrices";
	public static final String JSON_PATH_CAT_ONE_STANDARD_ADULT_PRICE = "$.categories[0].admissionPrices[0].standardAmount.amount";
	public static final String JSON_PATH_CAT_ONE_GIFT_AID_ADULT_PRICE = "$.categories[0].admissionPrices[0].giftAidAmount.amount";
	public static final String JSON_PATH_CAT_ONE_STANDARD_CHILD_PRICE = "$.categories[0].admissionPrices[1].standardAmount.amount";
	public static final String JSON_PATH_CAT_ONE_GIFT_AID_CHILD_PRICE = "$.categories[0].admissionPrices[1].giftAidAmount.amount";
	public static final String JSON_PATH_CAT_ONE_STANDARD_FAMILY_PRICE = "$.categories[0].admissionPrices[2].standardAmount.amount";
	public static final String JSON_PATH_CAT_ONE_GIFT_AID_FAMILY_PRICE = "$.categories[0].admissionPrices[2].giftAidAmount.amount";
	public static final String JSON_PATH_CAT_ONE_GROUP_ADULT_STANDARD_PRICE = "$.categories[0].groupAdmissionPrices[0].admissionPrices[0].standardAmount.amount";
	public static final String JSON_PATH_CAT_ONE_STANDARD_ADDITIONAL_FAMILY_PRICE = "$.categories[0].admissionPrices[2].standardAmount.amount";//"$.categories[0].admissionPrices[3].standardAmount.amount";
	public static final String JSON_PATH_CAT_ONE_GIFT_AID_ADDITIONAL_FAMILY_PRICE = "$.categories[0].admissionPrices[2].giftAidAmount.amount";//"$.categories[0].admissionPrices[3].giftAidAmount.amount";
	public static final String JSON_PATH_CAT_ONE_GROUP_ADULT_GIFT_AID_PRICE = "$.categories[0].groupAdmissionPrices[0].admissionPrices[0].giftAidAmount.amount";//"$.categories[0].groupAdmissionPrices[1].admissionPrices[1].giftAidAmount.amount";
	public static final String JSON_PATH_CAT_ONE_GROUP_CHILD_STANDARD_PRICE = "$.categories[0].groupAdmissionPrices[0].admissionPrices[1].standardAmount.amount";
	public static final String JSON_PATH_CAT_ONE_GROUP_CHILD_GIFT_AID_PRICE = "$.categories[0].groupAdmissionPrices[0].admissionPrices[1].standardAmount.amount";
	public static final String JSON_PATH_MIN_GROUP_SIZE = "$.categories[0].groupAdmissionPrices[0].minimum";
	public static final String JSON_PATHPRICES_NOTE = "$.htmlNote";
	public static final String JSON_PATH_EMERGENCY_NOTICE = "emergencyNotice";
	public static final String JSON_PATH_EVENT_TITLE = "$.title";
	public static final String JSON_PATH_EVENT_SUMMARY = "$.summary";

	public static final String JSON_PATH_EVENT_BOOKING_ADVICE = "$.booking.advice";
	public static final String JSON_PATH_EVENT_BOOKING_PHONENUMBER = "$.booking.phoneNumber";
	public static final String JSON_PATH_EVENT_SUITABILITY_DOGS = "$.dogPolicy";
	public static final String JSON_PATH_EVENT_SUITABILITY_CHILDREN = "$.children";

	public static final String JSON_PATH_EVENT_BOOKING_NOTES = "$.booking";

	public static final String JSON_PATH_EVENT_OTHER_TEXT = "$.otherInfo";
	public static final String JSON_PATH_EVENT_WHATTOWEAR = "$.whatToBringAndWear";
	public static final String JSON_PATH_EVENT_ACCESSIBILITY = "$.accessibility";
	public static final String JSON_PATH_EVENT_NAME_TEXT = "$.name";
	public static final String JSON_PATH_EVENT_EMAIL_TEXT = "$.email";
	public static final String JSON_PATH_EVENT_PHONE_TEXT = "$.phoneNumber";

	public static final String JSON_PATH_EVENT_MEETING_POINT = "$.meetingPoint";

	public static final String JSON_PATH_DAYS_OPEN = "$.openingTimes..daysOpen";
	public static final String JSON_PATH_EVENT_TAGS = "$.tags";

	public static final String JSON_LOCATION_EVENT_TITLE = "$.multiMatch.results[*].title";
	public static final String JSON_LOCATION_EVENT_TAGREF = "$.multiMatch.results[*].tagRefs";
	public static final String JSON_LOCATION_EVENT_DESCRIPTION = "$.multiMatch.results[*].description";
	public static final String JSON_LOCATION_EVENT_PLACE_TITLE = "$.multiMatch.results[*].placeSummary.title";
	public static final String JSON_LOCATION_EVENT_START_DATE_TIME = "$.multiMatch.results[*].nextOccurrenceStartDateTime";
	public static final String JSON_LOCATION_EVENT_END_DATE_TIME = "$.multiMatch.results[*].nextOccurrenceFinishDateTime";
	public static final String JSON_LOCATION_EVENT_OTHER_DATES = "$.multiMatch.results[*].otherDatesInFuture";
	public static final String JSON_LOCATION_EVENT_OTHER_TIMES = "$.multiMatch.results[*].otherTimesCurrentDay";
	public static final String JSON_LOCATION_EVENT_ID = "$.multiMatch.results[*].id";
	public static final String JSON_LOCATION_TAG_GROUP = "&tagGroup=TAG000010";
	public static final String JSON_PATH_PLACE_TAGS = ".tags[*]";
}
