package uk.org.nationaltrust.resources;

import java.io.File;

/**
 * Created by Pramod.Reguri on 11/09/2015.
 */

public class DriverPaths {

	public static final String LOGS_PATH = toFilePath(System.getProperty("user.dir"), "/target/htmlReport/screenshots/");

	// FIXME Path configuration is fragile; also wrong kind of slashies if this is windows
	// FIXME These constants are not of the same kind as the things to either side
	public static final String CHROMEPATH = (System.getProperty("user.dir") + "//vendor//chromedriver.exe");

	public static final String IEPATH = (System.getProperty("user.dir") + "//vendor//IEDriverServer.exe");

	public static final String GECKODRIVERPATH = (System.getProperty("user.dir") + "//vendor//geckodriver.exe");

	public static final String BROWSERSTACKLOCALEXE = (System.getProperty("user.dir") + "//vendor//BrowserStackLocal.exe");

	private static String toFilePath(String... pathElements) {
		StringBuilder path = new StringBuilder();
		for (String elem : pathElements) {
			path.append(elem);
		}
		return path.toString().replace('/', File.separatorChar);
	}
}
