package uk.org.nationaltrust.apis;

import lombok.Builder;
import lombok.Data;

/**
 * Created by Sowjanya Annepu
 */
@Data
@Builder
public class CreateDonateFormRequest {

    private String donationAmount;

    private String donationOtherAmount;

    private String placeID;

    private String donationId;

    private String placeName;

    private String donationStory;

    private String moderationStatus;

    private boolean isDonationPrivateListing;

    private String donationTone;

    private String occasion;

    private String commemorativeSubject;

    private String version;

    private String created;

    private String lastUpdated;

    private String lastUpdatedBy;

    private String moderatorComments;

    private String sharingLink;

    private String donorFullName;

    private String expiryTimeStamp;

    private boolean isCommemorativeDonation;

    private String commemorativeRelation;

    private String isPrivateListing;

    private String donateFormTitle;

    private String donateFormFirstName;

    private String donateFormLastName;

    private String donateFormCountry;

    private String donateFormAddressLine1;

    private String donateFormPostcode;

    private String donateFormEmail;

    private String donateFormIsEmailConsented;

    private String donateFormIsPostConsented;

    private String donateFormIsPhoneConsented;

    private String donateFormIsGiftAidSelected;

    private String otherRelation;

    private String otherOccasion;

    public static CreateDonateFormRequest.CreateDonateFormRequestBuilder createDonateFormRequest(){
        return CreateDonateFormRequest.builder()
                .commemorativeSubject("")
                .isCommemorativeDonation(false)
                .donationOtherAmount("")
                .isPrivateListing("")
                .donateFormTitle("")
                .donateFormFirstName("Test FirstName")
                .donateFormLastName("Test lastName")
                .donateFormCountry("")
                .donateFormAddressLine1("")
                .donateFormPostcode("")
                .donateFormEmail("")
                .otherOccasion("")
                .otherRelation("");
    }


}