package uk.org.nationaltrust.apis;

import java.util.UUID;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateOldCallbackRequest {

	private String eventType;

	private String email;

	private String supporterNumber;

//	private String additionalSupporterNumber;
//
//	private String leadSupporterNumber;

	private String dob;

	private String postcode;

	private String membershipNumber;

	private String trxID;

	public static CreateOldCallbackRequest.CreateOldCallbackRequestBuilder aValidCreateOldCallbackRequestBuilder() {

		return CreateOldCallbackRequest.builder()
				.eventType("CREATED")
				.email((UUID.randomUUID()) + "testActive@NTShop.com")
				.supporterNumber("334121844")
				.dob("1977-09-15")
				.postcode("SN5 7DY")
				.membershipNumber("11111111")
				.trxID("1111111111");

	}


}
