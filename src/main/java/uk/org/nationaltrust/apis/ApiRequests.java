package uk.org.nationaltrust.apis;

import static uk.org.nationaltrust.framework.GenericConfig.REST_SERVICE_URL;

import com.jayway.jsonpath.JsonPath;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by Pramod.Reguri on 22/02/2016.
 *
 // TODO Change this early in 2020
 * add this into the query it was made available in May 2019 so should be usable the following year. It will discount and memberships where there is a change reason
 * and ooha.header_id = oola.header_id
 * and ooha.attribute19 IS NULL  -- Renewal Change Code
 */
public class ApiRequests {

	private static JSONObject json = new JSONObject();

	public int removeSupporterLink(String accountReferenceNumber, String supporterNumber) {
		HttpClient httpClient = HttpClientBuilder.create().build();
		int response = 0;
		try {
			json.put("supporterNumber", supporterNumber);
			json.put("notifyAccountUser", "EMAIL");

			HttpPost request = new HttpPost(EnvironmentConfiguration.getRestServiceUrl() + "/" + accountReferenceNumber + "/unregister-supporter");
			StringEntity params = new StringEntity(json.toString());
			request.addHeader("content-type", "application/json");
			request.setEntity(params);
			HttpResponse httpResponse = httpClient.execute(request);
			response = httpResponse.getStatusLine().getStatusCode();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;

	}

	public static String selfServeCrmHealthEndPoint() {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(EnvironmentConfiguration.getText("selfServeCrmHealthEndPoint"));
		try {
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			String json = EntityUtils.toString(entity);
			return JsonPath.read(json, "crmHealthAtLastCheck");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	//	private String docRefNumber = "";
	//	private String docRefPrefix = "";

	public String createMemberWithSingleStatementTrue(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String price, String dob, String postcode,
			String email) {
		HttpClient client = HttpClientBuilder.create().build();
		try {

			HttpPost request = new HttpPost(EnvironmentConfiguration.getText("wsdlCreateMember"));
			//			request.setHeader("content-type", "application/json");

			//			HttpPost request = new HttpPost(createMembership1(docRefPrefix, docRefNumber));
			StringEntity entity = new StringEntity(createMembershipSingleStatementTrue(firstName, lastName, docRefPrefix, docRefNumber, date, price, dob, postcode, email));
			entity.setContentEncoding("UTF-8");
			entity.setContentType("text/xml");
			entity.setChunked(true);

			request.setEntity(entity);
			request.setHeader("content-type", "text/xml;charset=UTF-8");
			request.setHeader("SOAPAction", "create");

			HttpResponse response = client.execute(request);
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			StringBuilder builder = new StringBuilder();
			String line;
			http:
			//10.247.16.46:7003/soa-infra/services/crm/Membership/Membership

			while ((line = bufReader.readLine()) != null) {
				builder.append(line);
				builder.append(System.lineSeparator());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String createMYNTAccount(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String price, String dob, String postcode, String email) {
		HttpClient client = HttpClientBuilder.create().build();
		try {

			HttpPost request = new HttpPost(EnvironmentConfiguration.getText("wsdlCreateMYNTAccount"));
			//			request.setHeader("content-type", "application/json");

			//			HttpPost request = new HttpPost(createMembership1(docRefPrefix, docRefNumber));
			StringEntity entity = new StringEntity(createMYNTAccountFromShop(firstName, lastName, docRefPrefix, docRefNumber, date, price, dob, postcode, email));
			entity.setContentEncoding("UTF-8");
			entity.setContentType("text/xml");
			entity.setChunked(true);

			request.setEntity(entity);
			request.setHeader("content-type", "text/xml;charset=UTF-8");
			request.setHeader("SOAPAction", "create");

			HttpResponse response = client.execute(request);
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			StringBuilder builder = new StringBuilder();
			String line;
			http:
			//10.247.16.46:7003/soa-infra/services/crm/Membership/Membership

			while ((line = bufReader.readLine()) != null) {
				builder.append(line);
				builder.append(System.lineSeparator());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String createMembershipSingleStatementTrue(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String price, String dob, String postcode,
			String email) {
		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cre=\"http://xmlns.nationaltrust.org.uk/crm/services/1.0/CreateMembership\" xmlns:com=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/common\" xmlns:mem=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/membership\" xmlns:ban=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/banking\" xmlns:per=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/person\" xmlns:add=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/address\" xmlns:con=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPoint\" xmlns:con1=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPref\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <cre:CreateMembershipRequest>\n"
				+ "         <cre:SourceDetails>\n"
				+ "            <com:SourceSystem>SUPPORTERSERVICE</com:SourceSystem>\n"
				+ "         </cre:SourceDetails>\n"
				+ "         <cre:MembershipInfo>\n"
				+ "            <mem:SourceTransactionID>" + docRefPrefix + "" + docRefNumber + "" + date + "</mem:SourceTransactionID>\n"
				+ "            <mem:MembershipType>INDIVIDUAL</mem:MembershipType>\n"
				+ "            <mem:MembershipPeriod>ANNUAL</mem:MembershipPeriod>\n"
				+ "            <mem:MembershipPrice>" + price + "</mem:MembershipPrice>\n"
				+ "            <mem:Channel>WEB</mem:Channel>\n"
				+ "            <mem:PaymentInfo>\n"
				+ "               <mem:PaymentTerm>MONTHLY</mem:PaymentTerm>\n"
				+ "               <mem:PaymentMethod>DIRECT_DEBIT_ATTACHED</mem:PaymentMethod>\n"
				+ "               <mem:DirectDebit>\n"
				+ "                  <ban:BankAccountName>John</ban:BankAccountName>\n"
				+ "                  <ban:BankSortCode>200000</ban:BankSortCode>\n"
				+ "                  <ban:BankAccountNumber>55779911</ban:BankAccountNumber>\n"
				+ "               </mem:DirectDebit>\n"
				+ "            </mem:PaymentInfo>\n"
				+ "            <mem:SourceCode>BINOCS</mem:SourceCode>\n"
				+ "                   </cre:MembershipInfo>\n"
				+ "                 <cre:LeadSupporterInfo>\n"
				+ "            <mem:PersonInfo>\n"
				+ "               <per:Title>Mr.</per:Title>\n"
				+ "               <per:FirstName>" + firstName + "</per:FirstName>\n"
				+ "               <per:LastName>Badger</per:LastName>\n"
				+ "               <per:DateOfBirth>" + dob + "</per:DateOfBirth>\n"
				+ "               </mem:PersonInfo>\n"
				+ "            <mem:AddressInfo>\n"
				+ "               <add:isValidated>false</add:isValidated>\n"
				+ "               <add:AddressLine1>1 badger</add:AddressLine1>\n"
				+ "                 <add:Country>United Kingdom</add:Country>\n"
				+ "               <add:PostCode>" + postcode + "</add:PostCode>\n"
				+ "             </mem:AddressInfo>\n"
				+ "            <mem:TelephonesInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Telephone>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:TelephoneType>GENERAL</con:TelephoneType>\n"
				+ "                  <con:TelephoneNumber>+441189723112</con:TelephoneNumber>\n"
				+ "               </con:Telephone>\n"
				+ "                </mem:TelephonesInfo>\n"
				+ "            <mem:EmailsInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Email>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:EmailAddress>" + email + "</con:EmailAddress>\n"
				+ "               </con:Email>\n"
				+ "                </mem:EmailsInfo>\n"
				+ "            <mem:ContactPrefs>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_SINGLE_STATEMENT</con1:PreferenceType>\n"
				+ "                  <con1:Preference>true</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "               </mem:ContactPrefs>\n"
				+ "             </cre:LeadSupporterInfo>\n"
				+ "         <cre:TransactionTimestamp>2018-02-13T10:46:00-01:00</cre:TransactionTimestamp>\n"
				+ "      </cre:CreateMembershipRequest>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>";
	}

	public String createMemberWithEmailPostAndPhoneTrue(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String price, String dob, String postcode,
			String emailPref, String postPref, String phonePref, String email, String startDate) {
		HttpClient client = HttpClientBuilder.create().build();
		try {

			HttpPost request = new HttpPost(EnvironmentConfiguration.getText("wsdlCreateMember"));
			//			request.setHeader("content-type", "application/json");

			//			HttpPost request = new HttpPost(createMembership1(docRefPrefix, docRefNumber));
			StringEntity entity = new StringEntity(
					createMembershipEmailPostAndPhonePrefsTrue(firstName, lastName, docRefPrefix, docRefNumber, date, price, dob, postcode, emailPref, postPref, phonePref, email, startDate));
			entity.setContentEncoding("UTF-8");
			entity.setContentType("text/xml");
			entity.setChunked(true);

			request.setEntity(entity);
			request.setHeader("content-type", "text/xml;charset=UTF-8");
			request.setHeader("SOAPAction", "create");

			HttpResponse response = client.execute(request);
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			StringBuilder builder = new StringBuilder();
			String line;
			http:
			//10.247.16.46:7003/soa-infra/services/crm/Membership/Membership

			while ((line = bufReader.readLine()) != null) {
				builder.append(line);
				builder.append(System.lineSeparator());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String createMembershipEmailPostAndPhonePrefsTrue(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String price, String dob, String postcode,
			String emailPref, String postPref, String phonePref, String email, String startDate) {
		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cre=\"http://xmlns.nationaltrust.org.uk/crm/services/1.0/CreateMembership\" xmlns:com=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/common\" xmlns:mem=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/membership\" xmlns:ban=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/banking\" xmlns:per=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/person\" xmlns:add=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/address\" xmlns:con=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPoint\" xmlns:con1=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPref\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <cre:CreateMembershipRequest>\n"
				+ "         <cre:SourceDetails>\n"
				+ "            <com:SourceSystem>SUPPORTERSERVICE</com:SourceSystem>\n"
				+ "         </cre:SourceDetails>\n"
				+ "         <cre:MembershipInfo>\n"
				+ "            <mem:SourceTransactionID>"+docRefPrefix+""+docRefNumber+ ""+date+"</mem:SourceTransactionID>\n"
				+ "            <mem:MembershipType>INDIVIDUAL</mem:MembershipType>\n"
				+ "            <mem:MembershipPeriod>ANNUAL</mem:MembershipPeriod>\n"
				+ "            <mem:MembershipPrice>"+price+"</mem:MembershipPrice>\n"
				+ "            <mem:Channel>WEB</mem:Channel>\n"
				+ "            <mem:PaymentInfo>\n"
				+ "               <mem:PaymentTerm>MONTHLY</mem:PaymentTerm>\n"
				+ "               <mem:PaymentMethod>DIRECT_DEBIT_ATTACHED</mem:PaymentMethod>\n"
				+ "               <mem:DirectDebit>\n"
				+ "                  <ban:BankAccountName>John</ban:BankAccountName>\n"
				+ "                  <ban:BankSortCode>200000</ban:BankSortCode>\n"
				+ "                  <ban:BankAccountNumber>55779911</ban:BankAccountNumber>\n"
				+ "               </mem:DirectDebit>\n"
				+ "            </mem:PaymentInfo>\n"
				+ "            <mem:SourceCode>BINOCS</mem:SourceCode>\n"
				+ "                   </cre:MembershipInfo>\n"
				+ "                 <cre:LeadSupporterInfo>\n"
				+ "            <mem:PersonInfo>\n"
				+ "               <per:Title>Mr.</per:Title>\n"
				+ "               <per:FirstName>"+firstName+"</per:FirstName>\n"
				+ "               <per:LastName>"+lastName+"</per:LastName>\n"
				+ "               <per:DateOfBirth>"+dob+"</per:DateOfBirth>\n"
				+ "               </mem:PersonInfo>\n"
				+ "            <mem:AddressInfo>\n"
				+ "               <add:isValidated>false</add:isValidated>\n"
				+ "               <add:AddressLine1>1 badger</add:AddressLine1>\n"
				+ "                 <add:Country>United Kingdom</add:Country>\n"
				+ "               <add:PostCode>"+postcode+"</add:PostCode>\n"
				+ "             </mem:AddressInfo>\n"
				+ "            <mem:TelephonesInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Telephone>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:TelephoneType>GENERAL</con:TelephoneType>\n"
				+ "                  <con:TelephoneNumber>+441189723112</con:TelephoneNumber>\n"
				+ "               </con:Telephone>\n"
				+ "                </mem:TelephonesInfo>\n"
				+ "            <mem:EmailsInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Email>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:EmailAddress>"+email+"</con:EmailAddress>\n"
				+ "               </con:Email>\n"
				+ "                </mem:EmailsInfo>\n"
				+ "            <mem:ContactPrefs>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_EMAIL_YEM</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+emailPref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "					<con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_TELEPHONE_YTE</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+phonePref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "					<con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_POST_YPO</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+postPref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "               </mem:ContactPrefs>\n"
				+ "             </cre:LeadSupporterInfo>\n"
				+ "         <cre:TransactionTimestamp>"+startDate+"T10:46:00-01:00</cre:TransactionTimestamp>\n"
				+ "      </cre:CreateMembershipRequest>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>";
	}

	private String createMembershipEmailPostAndPhonePrefsTrueNEW(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String price, String dob, String postcode,
			String emailPref, String postPref, String phonePref, String email) {
		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cre=\"http://xmlns.nationaltrust.org.uk/crm/services/1.0/CreateMembership\" xmlns:com=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/common\" xmlns:mem=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/membership\" xmlns:ban=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/banking\" xmlns:per=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/person\" xmlns:add=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/address\" xmlns:con=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPoint\" xmlns:con1=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPref\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <cre:CreateMembershipRequest>\n"
				+ "         <cre:SourceDetails>\n"
				+ "            <com:SourceSystem>SUPPORTERSERVICE</com:SourceSystem>\n"
				+ "         </cre:SourceDetails>\n"
				+ "         <cre:MembershipInfo>\n"
				+ "            <mem:SourceTransactionID>"+docRefPrefix+""+docRefNumber+ ""+date+"</mem:SourceTransactionID>\n"
				+ "            <mem:MembershipType>INDIVIDUAL</mem:MembershipType>\n"
				+ "            <mem:MembershipPeriod>ANNUAL</mem:MembershipPeriod>\n"
				+ "            <mem:MembershipPrice>"+price+"</mem:MembershipPrice>\n"
				+ "            <mem:Channel>WEB</mem:Channel>\n"
				+ "            <mem:PaymentInfo>\n"
				+ "               <mem:PaymentTerm>MONTHLY</mem:PaymentTerm>\n"
				+ "               <mem:PaymentMethod>DIRECT_DEBIT_ATTACHED</mem:PaymentMethod>\n"
				+ "               <mem:DirectDebit>\n"
				+ "                  <ban:BankAccountName>John</ban:BankAccountName>\n"
				+ "                  <ban:BankSortCode>200000</ban:BankSortCode>\n"
				+ "                  <ban:BankAccountNumber>55779911</ban:BankAccountNumber>\n"
				+ "               </mem:DirectDebit>\n"
				+ "            </mem:PaymentInfo>\n"
				+ "            <mem:SourceCode>BINOCS</mem:SourceCode>\n"
				+ "                   </cre:MembershipInfo>\n"
				+ "                 <cre:LeadSupporterInfo>\n"
				+ "            <mem:PersonInfo>\n"
				+ "               <per:Title>Mr.</per:Title>\n"
				+ "               <per:FirstName>"+firstName+"</per:FirstName>\n"
				+ "               <per:LastName>"+lastName+"</per:LastName>\n"
				+ "               <per:DateOfBirth>"+dob+"</per:DateOfBirth>\n"
				+ "               </mem:PersonInfo>\n"
				+ "            <mem:AddressInfo>\n"
				+ "               <add:isValidated>false</add:isValidated>\n"
				+ "               <add:AddressLine1>1 badger</add:AddressLine1>\n"
				+ "                 <add:Country>United Kingdom</add:Country>\n"
				+ "               <add:PostCode>"+postcode+"</add:PostCode>\n"
				+ "             </mem:AddressInfo>\n"
				+ "            <mem:TelephonesInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Telephone>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:TelephoneType>GENERAL</con:TelephoneType>\n"
				+ "                  <con:TelephoneNumber>+441189723112</con:TelephoneNumber>\n"
				+ "               </con:Telephone>\n"
				+ "                </mem:TelephonesInfo>\n"
				+ "            <mem:EmailsInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Email>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:EmailAddress>"+email+"</con:EmailAddress>\n"
				+ "               </con:Email>\n"
				+ "                </mem:EmailsInfo>\n"
				+ "            <mem:ContactPrefs>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_EMAIL_YEM</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+emailPref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "					<con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_TELEPHONE_YTE</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+phonePref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "					<con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_POST_YPO</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+postPref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "               </mem:ContactPrefs>\n"
				+ "             </cre:LeadSupporterInfo>\n"
				+ "         <cre:TransactionTimestamp>2017-04-23T10:46:00-01:00</cre:TransactionTimestamp>\n"
				+ "      </cre:CreateMembershipRequest>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>";
	}

	public String createDirectDebitMember(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String date1, String price, String dob, String postcode,
			String emailPref, String postPref, String phonePref, String email, String startDate, String term) {
		HttpClient client = HttpClientBuilder.create().build();
		try {

			HttpPost request = new HttpPost(EnvironmentConfiguration.getText("wsdlCreateMember"));
			//			request.setHeader("content-type", "application/json");

			//			HttpPost request = new HttpPost(createMembership1(docRefPrefix, docRefNumber));
			StringEntity entity = new StringEntity(
					createDirectDebitMembership(firstName, lastName, docRefPrefix, docRefNumber, date, date1, price, dob, postcode, emailPref, postPref, phonePref, email, startDate, term));
			entity.setContentEncoding("UTF-8");
			entity.setContentType("text/xml");
			entity.setChunked(true);

			request.setEntity(entity);
			request.setHeader("content-type", "text/xml;charset=UTF-8");
			request.setHeader("SOAPAction", "create");

			HttpResponse response = client.execute(request);
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			StringBuilder builder = new StringBuilder();
			String line;
			http:
			//10.247.16.46:7003/soa-infra/services/crm/Membership/Membership

			while ((line = bufReader.readLine()) != null) {
				builder.append(line);
				builder.append(System.lineSeparator());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}



	private String createDirectDebitMembership(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String date1, String price, String dob, String postcode,
			String emailPref, String postPref, String phonePref, String email, String startDate, String term) {
		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cre=\"http://xmlns.nationaltrust.org.uk/crm/services/1.0/CreateMembership\" xmlns:com=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/common\" xmlns:mem=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/membership\" xmlns:ban=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/banking\" xmlns:per=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/person\" xmlns:add=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/address\" xmlns:con=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPoint\" xmlns:con1=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPref\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <cre:CreateMembershipRequest>\n"
				+ "         <cre:SourceDetails>\n"
				+ "            <com:SourceSystem>SUPPORTERSERVICE</com:SourceSystem>\n"
				+ "         </cre:SourceDetails>\n"
				+ "         <cre:MembershipInfo>\n"
				+ "            <mem:SourceTransactionID>"+docRefPrefix+""+docRefNumber+""+date+"</mem:SourceTransactionID>\n"
				+ "            <mem:MembershipType>INDIVIDUAL</mem:MembershipType>\n"
				+ "            <mem:MembershipPeriod>ANNUAL</mem:MembershipPeriod>\n"
				+ "            <mem:MembershipPrice>"+price+"</mem:MembershipPrice>\n"
				+ "            <mem:Channel>WEB</mem:Channel>\n"
				+ "            <mem:PaymentInfo>\n"
				+ "               <mem:PaymentTerm>"+term+"</mem:PaymentTerm>\n"
				+ "               <mem:PaymentMethod>DIRECT_DEBIT_ATTACHED</mem:PaymentMethod>\n"
				+ "               <mem:DirectDebit>\n"
				+ "                  <ban:BankAccountName>John</ban:BankAccountName>\n"
				+ "                  <ban:BankSortCode>200000</ban:BankSortCode>\n"
				+ "                  <ban:BankAccountNumber>55779911</ban:BankAccountNumber>\n"
				+ "               </mem:DirectDebit>\n"
				+ "            </mem:PaymentInfo>\n"
				+ "            <mem:SourceCode>BINOCS</mem:SourceCode>\n"
				+ "                   </cre:MembershipInfo>\n"
				+ "                 <cre:LeadSupporterInfo>\n"
				+ "            <mem:PersonInfo>\n"
				+ "               <per:Title>Mr.</per:Title>\n"
				+ "               <per:FirstName>"+firstName+"</per:FirstName>\n"
				+ "               <per:LastName>"+lastName+"</per:LastName>\n"
				+ "               <per:DateOfBirth>"+dob+"</per:DateOfBirth>\n"
				+ "               </mem:PersonInfo>\n"
				+ "            <mem:AddressInfo>\n"
				+ "               <add:isValidated>true</add:isValidated>\n"
				+ "               <add:AddressLine1>"+date1+" Road</add:AddressLine1>\n"
				+ "                 <add:Country>United Kingdom</add:Country>\n"
				+ "               <add:PostCode>"+postcode+"</add:PostCode>\n"
				+ "             </mem:AddressInfo>\n"
				+ "            <mem:TelephonesInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Telephone>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:TelephoneType>GENERAL</con:TelephoneType>\n"
				+ "                  <con:TelephoneNumber>+441189723112</con:TelephoneNumber>\n"
				+ "               </con:Telephone>\n"
				+ "                </mem:TelephonesInfo>\n"
				+ "            <mem:EmailsInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Email>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:EmailAddress>"+email+"</con:EmailAddress>\n"
				+ "               </con:Email>\n"
				+ "                </mem:EmailsInfo>\n"
				+ "            <mem:ContactPrefs>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_EMAIL_YEM</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+emailPref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "					<con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_TELEPHONE_YTE</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+phonePref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "					<con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_POST_YPO</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+postPref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "               </mem:ContactPrefs>\n"
				+ "             </cre:LeadSupporterInfo>\n"
				+ "         <cre:TransactionTimestamp>"+startDate+"T10:46:00-01:00</cre:TransactionTimestamp>\n"
				+ "      </cre:CreateMembershipRequest>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>";
	}


	public String createMembership(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String date1, String price, String dob, String postcode,
			String emailPref, String postPref, String phonePref, String email, String startDate, String term, String phone) {
		HttpClient client = HttpClientBuilder.create().build();
		try {

			HttpPost request = new HttpPost(EnvironmentConfiguration.getText("wsdlCreateMember"));
			//			request.setHeader("content-type", "application/json");

			//			HttpPost request = new HttpPost(createMembership1(docRefPrefix, docRefNumber));
			StringEntity entity = new StringEntity(
					createCustomisableDirectDebitMembership(firstName, lastName, docRefPrefix, docRefNumber, date, date1, price, dob, postcode, emailPref, postPref, phonePref, email, startDate, term, phone));
			entity.setContentEncoding("UTF-8");
			entity.setContentType("text/xml");
			entity.setChunked(true);

			request.setEntity(entity);
			request.setHeader("content-type", "text/xml;charset=UTF-8");
			request.setHeader("SOAPAction", "create");

			HttpResponse response = client.execute(request);
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			StringBuilder builder = new StringBuilder();
			String line;
			http:
			//10.247.16.46:7003/soa-infra/services/crm/Membership/Membership

			while ((line = bufReader.readLine()) != null) {
				builder.append(line);
				builder.append(System.lineSeparator());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String createCustomisableDirectDebitMembership(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String date1, String price, String dob, String postcode,
			String emailPref, String postPref, String phonePref, String email, String startDate, String term, String phone) {
		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cre=\"http://xmlns.nationaltrust.org.uk/crm/services/1.0/CreateMembership\" xmlns:com=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/common\" xmlns:mem=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/membership\" xmlns:ban=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/banking\" xmlns:per=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/person\" xmlns:add=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/address\" xmlns:con=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPoint\" xmlns:con1=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPref\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <cre:CreateMembershipRequest>\n"
				+ "         <cre:SourceDetails>\n"
				+ "            <com:SourceSystem>SUPPORTERSERVICE</com:SourceSystem>\n"
				+ "         </cre:SourceDetails>\n"
				+ "         <cre:MembershipInfo>\n"
				+ "            <mem:SourceTransactionID>"+docRefPrefix+""+docRefNumber+""+date+"</mem:SourceTransactionID>\n"
				+ "            <mem:MembershipType>CHILD</mem:MembershipType>\n"
				+ "            <mem:MembershipPeriod>ANNUAL</mem:MembershipPeriod>\n"
				+ "            <mem:MembershipPrice>"+price+"</mem:MembershipPrice>\n"
				+ "            <mem:Channel>WEB</mem:Channel>\n"
				+ "            <mem:PaymentInfo>\n"
				+ "               <mem:PaymentTerm>"+term+"</mem:PaymentTerm>\n"
				+ "               <mem:PaymentMethod>DIRECT_DEBIT_ATTACHED</mem:PaymentMethod>\n"
				+ "               <mem:DirectDebit>\n"
				+ "                  <ban:BankAccountName>John</ban:BankAccountName>\n"
				+ "                  <ban:BankSortCode>200000</ban:BankSortCode>\n"
				+ "                  <ban:BankAccountNumber>55779911</ban:BankAccountNumber>\n"
				+ "               </mem:DirectDebit>\n"
				+ "            </mem:PaymentInfo>\n"
				+ "            <mem:SourceCode>BINOCS</mem:SourceCode>\n"
				+ "                   </cre:MembershipInfo>\n"
				+ "                 <cre:LeadSupporterInfo>\n"
				+ "            <mem:PersonInfo>\n"
				+ "               <per:Title>Mr.</per:Title>\n"
				+ "               <per:FirstName>"+firstName+"</per:FirstName>\n"
				+ "               <per:LastName>"+lastName+"</per:LastName>\n"
				+ "               <per:DateOfBirth>"+dob+"</per:DateOfBirth>\n"
				+ "               </mem:PersonInfo>\n"
				+ "            <mem:AddressInfo>\n"
				+ "               <add:isValidated>true</add:isValidated>\n"
				+ "               <add:AddressLine1>"+date1+" Road</add:AddressLine1>\n"
				+ "                 <add:Country>United Kingdom</add:Country>\n"
				+ "               <add:PostCode>"+postcode+"</add:PostCode>\n"
				+ "             </mem:AddressInfo>\n"
				+ "            <mem:TelephonesInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Telephone>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:TelephoneType>GENERAL</con:TelephoneType>\n"
				+ "                  <con:TelephoneNumber>"+phone+"</con:TelephoneNumber>\n"
				+ "               </con:Telephone>\n"
				+ "                </mem:TelephonesInfo>\n"
				+ "            <mem:EmailsInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Email>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:EmailAddress>"+email+"</con:EmailAddress>\n"
				+ "               </con:Email>\n"
				+ "                </mem:EmailsInfo>\n"
				+ "            <mem:ContactPrefs>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_EMAIL_YEM</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+emailPref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "					<con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_TELEPHONE_YTE</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+phonePref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "					<con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_POST_YPO</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+postPref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "               </mem:ContactPrefs>\n"
				+ "             </cre:LeadSupporterInfo>\n"
				+ "         <cre:TransactionTimestamp>"+startDate+"T10:46:00-01:00</cre:TransactionTimestamp>\n"
				+ "      </cre:CreateMembershipRequest>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>";
	}

	public String createJuniorMember(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String date1, String price, String dob, String postcode,
			String emailPref, String postPref, String phonePref, String email, String startDate, String term, String phone) {
		HttpClient client = HttpClientBuilder.create().build();
		try {

			HttpPost request = new HttpPost(EnvironmentConfiguration.getText("wsdlCreateMember"));
			//			request.setHeader("content-type", "application/json");

			//			HttpPost request = new HttpPost(createMembership1(docRefPrefix, docRefNumber));
			StringEntity entity = new StringEntity(
					createJuniorMembership(firstName, lastName, docRefPrefix, docRefNumber, date, date1, price, dob, postcode, emailPref, postPref, phonePref, email, startDate, term, phone));
			entity.setContentEncoding("UTF-8");
			entity.setContentType("text/xml");
			entity.setChunked(true);

			request.setEntity(entity);
			request.setHeader("content-type", "text/xml;charset=UTF-8");
			request.setHeader("SOAPAction", "create");

			HttpResponse response = client.execute(request);
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			StringBuilder builder = new StringBuilder();
			String line;
			http:
			//10.247.16.46:7003/soa-infra/services/crm/Membership/Membership

			while ((line = bufReader.readLine()) != null) {
				builder.append(line);
				builder.append(System.lineSeparator());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String createJuniorMembership(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String date1, String price, String dob, String postcode,
			String emailPref, String postPref, String phonePref, String email, String startDate, String term, String phone) {
		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cre=\"http://xmlns.nationaltrust.org.uk/crm/services/1.0/CreateMembership\" xmlns:com=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/common\" xmlns:mem=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/membership\" xmlns:ban=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/banking\" xmlns:per=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/person\" xmlns:add=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/address\" xmlns:con=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPoint\" xmlns:con1=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPref\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <cre:CreateMembershipRequest>\n"
				+ "         <cre:SourceDetails>\n"
				+ "            <com:SourceSystem>SUPPORTERSERVICE</com:SourceSystem>\n"
				+ "         </cre:SourceDetails>\n"
				+ "         <cre:MembershipInfo>\n"
				+ "            <mem:SourceTransactionID>"+docRefPrefix+""+docRefNumber+""+date+"</mem:SourceTransactionID>\n"
				+ "            <mem:MembershipType>CHILD</mem:MembershipType>\n"
				+ "            <mem:MembershipPeriod>ANNUAL</mem:MembershipPeriod>\n"
				+ "            <mem:MembershipPrice>"+price+"</mem:MembershipPrice>\n"
				+ "            <mem:Channel>WEB</mem:Channel>\n"
				+ "            <mem:PaymentInfo>\n"
				+ "               <mem:PaymentTerm>"+term+"</mem:PaymentTerm>\n"
				+ "               <mem:PaymentMethod>DIRECT_DEBIT_ATTACHED</mem:PaymentMethod>\n"
				+ "               <mem:DirectDebit>\n"
				+ "                  <ban:BankAccountName>John</ban:BankAccountName>\n"
				+ "                  <ban:BankSortCode>200000</ban:BankSortCode>\n"
				+ "                  <ban:BankAccountNumber>55779911</ban:BankAccountNumber>\n"
				+ "               </mem:DirectDebit>\n"
				+ "            </mem:PaymentInfo>\n"
				+ "            <mem:SourceCode>BINOCS</mem:SourceCode>\n"
				+ "                   </cre:MembershipInfo>\n"
				+ "                 <cre:LeadSupporterInfo>\n"
				+ "            <mem:PersonInfo>\n"
				+ "               <per:Title>Mr.</per:Title>\n"
				+ "               <per:FirstName>"+firstName+"</per:FirstName>\n"
				+ "               <per:LastName>"+lastName+"</per:LastName>\n"
				+ "               <per:DateOfBirth>"+dob+"</per:DateOfBirth>\n"
				+ "               </mem:PersonInfo>\n"
				+ "            <mem:AddressInfo>\n"
				+ "               <add:isValidated>true</add:isValidated>\n"
				+ "               <add:AddressLine1>"+date1+" Road</add:AddressLine1>\n"
				+ "                 <add:Country>United Kingdom</add:Country>\n"
				+ "               <add:PostCode>"+postcode+"</add:PostCode>\n"
				+ "             </mem:AddressInfo>\n"
				+ "            <mem:TelephonesInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Telephone>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:TelephoneType>GENERAL</con:TelephoneType>\n"
				+ "                  <con:TelephoneNumber>"+phone+"</con:TelephoneNumber>\n"
				+ "               </con:Telephone>\n"
				+ "                </mem:TelephonesInfo>\n"
				+ "            <mem:EmailsInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Email>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:EmailAddress>"+email+"</con:EmailAddress>\n"
				+ "               </con:Email>\n"
				+ "                </mem:EmailsInfo>\n"
				+ "            <mem:ContactPrefs>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_EMAIL_YEM</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+emailPref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "					<con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_TELEPHONE_YTE</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+phonePref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "					<con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_POST_YPO</con1:PreferenceType>\n"
				+ "                  <con1:Preference>"+postPref+"</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "               </mem:ContactPrefs>\n"
				+ "             </cre:LeadSupporterInfo>\n"
				+ "         <cre:TransactionTimestamp>"+startDate+"T10:46:00-01:00</cre:TransactionTimestamp>\n"
				+ "      </cre:CreateMembershipRequest>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>";
	}

	public String createCashPaidMembership(String firstName, String lastName, String docRefNumber, String date, String dob, String postcode, String email, String startDate, String price) {
		HttpClient client = HttpClientBuilder.create().build();
		try {

			HttpPost request = new HttpPost(EnvironmentConfiguration.getText("wsdlCreateCashMember"));
			//			request.setHeader("content-type", "application/json");

			//			HttpPost request = new HttpPost(createMembership1(docRefPrefix, docRefNumber));
			StringEntity entity = new StringEntity(createCashMembership(firstName, lastName, docRefNumber, date, dob, postcode, email, startDate, price));
			entity.setContentEncoding("UTF-8");
			entity.setContentType("text/xml");
			entity.setChunked(true);

			request.setEntity(entity);
			request.setHeader("content-type", "text/xml;charset=UTF-8");
			request.setHeader("SOAPAction", "create");

			HttpResponse response = client.execute(request);
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			StringBuilder builder = new StringBuilder();
			String line;
			http:
			//10.247.16.46:7003/soa-infra/services/crm/Membership/Membership

			while ((line = bufReader.readLine()) != null) {
				builder.append(line);
				builder.append(System.lineSeparator());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String createCashMembership(String firstName, String lastName, String docRefNumber, String date, String dob, String postcode, String email, String startDate, String price) {

		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <ns2:Support xmlns:ns2=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/supportLoadPub\">\n"
				+ "         <DocumentReference>\n"
				+ "            <DocumentReferenceID>"+docRefNumber+""+date+"</DocumentReferenceID>\n"
				+ "            <DocumentOriginator>KofaxDocuments</DocumentOriginator>\n"
				+ "            <DocumentType>Property Enrolment</DocumentType>\n"
				+ "         </DocumentReference>\n"
				+ "         <Supporters>\n"
				+ "            <Supporter>\n"
				+ "               <SupporterNumber/>\n"
				+ "               <LeadMemberFlag>true</LeadMemberFlag>\n"
				+ "               <OrderingPersonFlag>true</OrderingPersonFlag>\n"
				+ "               <MemberFlag>true</MemberFlag>\n"
				+ "               <Title>Mr.</Title>\n"
				+ "               <FirstName>"+firstName+"</FirstName>\n"
				+ "               <Initials/>\n"
				+ "               <MiddleName/>\n"
				+ "               <LastName>"+lastName+"</LastName>\n"
				+ "               <PreviousLastName/>\n"
				+ "               <KnownAs/>\n"
				+ "               <DateOfBirth>"+dob+"</DateOfBirth>\n"
				+ "               <TrustedSourceDateofBirthFlag/>\n"
				+ "               <DateOfDeath/>\n"
				+ "               <TrustedSourceDateofDeathFlag/>\n"
				+ "               <Gender/>\n"
				+ "               <SupporterExpiryDate/>\n"
				+ "               <SupporterCategory>IND</SupporterCategory>\n"
				+ "               <GoneAway/>\n"
				+ "               <TrustedSourceGoneAwayFlag/>\n"
				+ "               <TrustedSourceSupporterNameFlag/>\n"
				+ "               <ContactPoints>\n"
				+ "                  <Contact>\n"
				+ "                     <TelephoneType/>\n"
				+ "                     <TeleponeNumber>+441189723112</TeleponeNumber>\n"
				+ "                     <TelephoneNumberInactive/>\n"
				+ "                     <TrustedSourceTelephoneFlag/>\n"
				+ "                     <EmailAddress>"+email+"</EmailAddress>\n"
				+ "                     <EmailAddressInactive/>\n"
				+ "                     <TrustedSourceEmailFlag/>\n"
				+ "                     <PrimaryContactFlag/>\n"
				+ "                  </Contact>\n"
				+ "               </ContactPoints>\n"
				+ "               <ContactPreferences>YEM,N,YPO,Y,YTE,N</ContactPreferences>\n"
				+ "               <TrustedSourceContactPreferenceFlag/>\n"
				+ "            </Supporter>\n"
				+ "            <SupportAddress>\n"
				+ "               <MemberAddress>true</MemberAddress>\n"
				+ "               <OrderingAddress>true</OrderingAddress>\n"
				+ "               <PrimaryAddressOverrideFlag/>\n"
				+ "               <VanityAddressLine/>\n"
				+ "               <AddressLine1>"+date+" Road</AddressLine1>\n"
				+ "               <AddressLine2></AddressLine2>\n"
				+ "               <AddressLine3/>\n"
				+ "               <AddressLine4/>\n"
				+ "               <City></City>\n"
				+ "               <County></County>\n"
				+ "               <PostCode>"+postcode+"</PostCode>\n"
				+ "               <Country/>\n"
				+ "               <XCoord>-0.329559</XCoord>\n"
				+ "               <YCoord>51.360567</YCoord>\n"
				+ "               <DeliveryPointSuffix/>\n"
				+ "               <ValidStatus>Verified</ValidStatus>\n"
				+ "               <TrustedSourceAddressFlag/>\n"
				+ "            </SupportAddress>\n"
				+ "            <GiftAddress>\n"
				+ "               <MemberAddress>false</MemberAddress>\n"
				+ "               <OrderingAddress>false</OrderingAddress>\n"
				+ "               <PrimaryAddressOverrideFlag/>\n"
				+ "               <VanityAddressLine/>\n"
				+ "               <AddressLine1/>\n"
				+ "               <AddressLine2/>\n"
				+ "               <AddressLine3/>\n"
				+ "               <AddressLine4/>\n"
				+ "               <City/>\n"
				+ "               <County/>\n"
				+ "               <PostCode/>\n"
				+ "               <Country/>\n"
				+ "               <XCoord/>\n"
				+ "               <YCoord/>\n"
				+ "               <DeliveryPointSuffix/>\n"
				+ "               <ValidStatus/>\n"
				+ "               <TrustedSourceAddressFlag/>\n"
				+ "            </GiftAddress>\n"
				+ "         </Supporters>\n"
				+ "         <Membership>\n"
				+ "            <ReceiptNo/>\n"
				+ "            <TripFormID>184307764</TripFormID>\n"
				+ "            <RecruiterID>CMS</RecruiterID>\n"
				+ "            <ReferralMembershipID/>\n"
				+ "            <SourceCode>NTENTRY</SourceCode>\n"
				+ "            <PropertyNo>10732</PropertyNo>\n"
				+ "            <RecruitmentPoint>04</RecruitmentPoint>\n"
				+ "            <MembershipValidFrom>"+startDate+"</MembershipValidFrom>\n"
				+ "            <MembershipValidTo/>\n"
				+ "            <MembershipScheme/>\n"
				+ "            <GroupMemberCount>02</GroupMemberCount>\n"
				+ "            <LiteratureIssued>Y</LiteratureIssued>\n"
				+ "            <ExtraMaterialIssued/>\n"
				+ "            <DeclinePromotion/>\n"
				+ "            <GiftFlag>N</GiftFlag>\n"
				+ "            <GiftType/>\n"
				+ "            <GiftMessage/>\n"
				+ "            <GiftToGiver/>\n"
				+ "            <GiftMembershipStartDate>"+startDate+"</GiftMembershipStartDate>\n"
				+ "            <Channel>Face to Face</Channel>\n"
				+ "            <FirstJoinedDate/>\n"
				+ "            <GiftAidMandatoryQuestion>Yes</GiftAidMandatoryQuestion>\n"
				+ "            <IncludeInPrizeDraw/>\n"
				+ "         </Membership>\n"
				+ "         <Donation>\n"
				+ "            <TripFormID/>\n"
				+ "            <SourceCode/>\n"
				+ "            <FundCode/>\n"
				+ "            <PropertyNo/>\n"
				+ "            <RecruitmentPoint/>\n"
				+ "            <ReceiptNo/>\n"
				+ "            <CreationChannel/>\n"
				+ "            <DonationCategory/>\n"
				+ "            <GiftAidMandatoryQuestion/>\n"
				+ "            <IncludeInPrizeDraw/>\n"
				+ "            <TicketSalevalue/>\n"
				+ "            <NumberOfTickets/>\n"
				+ "            <AcknowledgementReqdFlag/>\n"
				+ "         </Donation>\n"
				+ "         <Gift-Aid>\n"
				+ "            <Signature/>\n"
				+ "            <SignDate/>\n"
				+ "            <EndDate/>\n"
				+ "            <SourceCode>NTENTRY</SourceCode>\n"
				+ "         </Gift-Aid>\n"
				+ "         <PaymentDetails>\n"
				+ "            <PaymentMethod>CC/DC at Property</PaymentMethod>\n"
				+ "            <PaymentFrequency/>\n"
				+ "            <PaymentAmount>"+price+"</PaymentAmount>\n"
				+ "            <PaymentTerm/>\n"
				+ "            <TopupDonationAmount/>\n"
				+ "            <ExistingReceiptID/>\n"
				+ "            <CreditCard>\n"
				+ "               <CardType/>\n"
				+ "               <CardNumber/>\n"
				+ "               <Expiry/>\n"
				+ "               <IssueNo/>\n"
				+ "               <StartDate/>\n"
				+ "               <Signature/>\n"
				+ "               <SignDate/>\n"
				+ "               <Title/>\n"
				+ "               <Initials/>\n"
				+ "               <Surname/>\n"
				+ "               <ReceiptID/>\n"
				+ "            </CreditCard>\n"
				+ "            <DirectDebit>\n"
				+ "               <BankName/>\n"
				+ "               <BankAddress/>\n"
				+ "               <BankPostCode/>\n"
				+ "               <BankSortCode/>\n"
				+ "               <BankAccountNumber/>\n"
				+ "               <BankAccountName/>\n"
				+ "               <BankOriginator>410045</BankOriginator>\n"
				+ "               <DDSignature>N</DDSignature>\n"
				+ "               <DDSignDate>N</DDSignDate>\n"
				+ "               <DDDate/>\n"
				+ "               <ReceiptID/>\n"
				+ "            </DirectDebit>\n"
				+ "         </PaymentDetails>\n"
				+ "      </ns2:Support>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>\n";
	}

	private String createJointCashMembership(String firstName, String lastName, String docRefNumber, String date, String dob, String postcode, String email) {

		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <ns2:Support xmlns:ns2=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/supportLoadPub\">\n"
				+ "         <DocumentReference>\n"
				+ "            <DocumentReferenceID>" + docRefNumber + "" + date + "</DocumentReferenceID>\n"
				+ "            <DocumentOriginator>KofaxDocuments</DocumentOriginator>\n"
				+ "            <DocumentType>Property Enrolment</DocumentType>\n"
				+ "         </DocumentReference>\n"
				+ "         <Supporters>\n"
				+ "            <Supporter>\n"
				+ "               <SupporterNumber/>\n"
				+ "               <LeadMemberFlag>true</LeadMemberFlag>\n"
				+ "               <OrderingPersonFlag>true</OrderingPersonFlag>\n"
				+ "               <MemberFlag>true</MemberFlag>\n"
				+ "               <Title>Mr.</Title>\n"
				+ "               <FirstName>" + firstName + "</FirstName>\n"
				+ "               <Initials/>\n"
				+ "               <MiddleName/>\n"
				+ "               <LastName>" + lastName + "</LastName>\n"
				+ "               <PreviousLastName/>\n"
				+ "               <KnownAs/>\n"
				+ "               <DateOfBirth>" + dob + "</DateOfBirth>\n"
				+ "               <TrustedSourceDateofBirthFlag/>\n"
				+ "               <DateOfDeath/>\n"
				+ "               <TrustedSourceDateofDeathFlag/>\n"
				+ "               <Gender/>\n"
				+ "               <SupporterExpiryDate/>\n"
				+ "               <SupporterCategory>IND</SupporterCategory>\n"
				+ "               <GoneAway/>\n"
				+ "               <TrustedSourceGoneAwayFlag/>\n"
				+ "               <TrustedSourceSupporterNameFlag/>\n"
				+ "               <ContactPoints>\n"
				+ "                  <Contact>\n"
				+ "                     <TelephoneType/>\n"
				+ "                     <TeleponeNumber>01372462594</TeleponeNumber>\n"
				+ "                     <TelephoneNumberInactive/>\n"
				+ "                     <TrustedSourceTelephoneFlag/>\n"
				+ "                     <EmailAddress>" + email + "</EmailAddress>\n"
				+ "                     <EmailAddressInactive/>\n"
				+ "                     <TrustedSourceEmailFlag/>\n"
				+ "                     <PrimaryContactFlag/>\n"
				+ "                  </Contact>\n"
				+ "               </ContactPoints>\n"
				+ "               <ContactPreferences>YEM,N,YPO,Y,YTE,N</ContactPreferences>\n"
				+ "               <TrustedSourceContactPreferenceFlag/>\n"
				+ "            </Supporter>\n"
				+ "            <Supporter>\n"
				+ "               <SupporterNumber/>\n"
				+ "               <LeadMemberFlag>false</LeadMemberFlag>\n"
				+ "               <OrderingPersonFlag>false</OrderingPersonFlag>\n"
				+ "               <MemberFlag>true</MemberFlag>\n"
				+ "               <Title>Mrs.</Title>\n"
				+ "               <FirstName>TestAdd</FirstName>\n"
				+ "               <Initials/>\n"
				+ "               <MiddleName/>\n"
				+ "               <LastName>abcd</LastName>\n"
				+ "               <PreviousLastName/>\n"
				+ "               <KnownAs/>\n"
				+ "               <DateOfBirth>03/10/1951</DateOfBirth>\n"
				+ "               <TrustedSourceDateofBirthFlag/>\n"
				+ "               <DateOfDeath/>\n"
				+ "               <TrustedSourceDateofDeathFlag/>\n"
				+ "               <Gender/>\n"
				+ "               <SupporterExpiryDate/>\n"
				+ "               <SupporterCategory>ADD</SupporterCategory>\n"
				+ "               <GoneAway/>\n"
				+ "               <TrustedSourceGoneAwayFlag/>\n"
				+ "               <TrustedSourceSupporterNameFlag/>\n"
				+ "               <ContactPoints>\n"
				+ "                  <Contact>\n"
				+ "                     <TelephoneType/>\n"
				+ "                     <TeleponeNumber/>\n"
				+ "                     <TelephoneNumberInactive/>\n"
				+ "                     <TrustedSourceTelephoneFlag/>\n"
				+ "                     <EmailAddress/>\n"
				+ "                     <EmailAddressInactive/>\n"
				+ "                     <TrustedSourceEmailFlag/>\n"
				+ "                     <PrimaryContactFlag/>\n"
				+ "                  </Contact>\n"
				+ "               </ContactPoints>\n"
				+ "               <ContactPreferences>YEM,N,YPO,Y,YTE,N</ContactPreferences>\n"
				+ "               <TrustedSourceContactPreferenceFlag/>\n"
				+ "            </Supporter>\n"
				+ "            <SupportAddress>\n"
				+ "               <MemberAddress>true</MemberAddress>\n"
				+ "               <OrderingAddress>true</OrderingAddress>\n"
				+ "               <PrimaryAddressOverrideFlag/>\n"
				+ "               <VanityAddressLine/>\n"
				+ "               <AddressLine1>18 STEVENS LANE</AddressLine1>\n"
				+ "               <AddressLine2>CLAYGATE</AddressLine2>\n"
				+ "               <AddressLine3/>\n"
				+ "               <AddressLine4/>\n"
				+ "               <City>ESHER</City>\n"
				+ "               <County>SURREY</County>\n"
				+ "               <PostCode>" + postcode + "</PostCode>\n"
				+ "               <Country/>\n"
				+ "               <XCoord>-0.329559</XCoord>\n"
				+ "               <YCoord>51.360567</YCoord>\n"
				+ "               <DeliveryPointSuffix/>\n"
				+ "               <ValidStatus>Not Verified</ValidStatus>\n"
				+ "               <TrustedSourceAddressFlag/>\n"
				+ "            </SupportAddress>\n"
				+ "            <GiftAddress>\n"
				+ "               <MemberAddress>false</MemberAddress>\n"
				+ "               <OrderingAddress>false</OrderingAddress>\n"
				+ "               <PrimaryAddressOverrideFlag/>\n"
				+ "               <VanityAddressLine/>\n"
				+ "               <AddressLine1/>\n"
				+ "               <AddressLine2/>\n"
				+ "               <AddressLine3/>\n"
				+ "               <AddressLine4/>\n"
				+ "               <City/>\n"
				+ "               <County/>\n"
				+ "               <PostCode/>\n"
				+ "               <Country/>\n"
				+ "               <XCoord/>\n"
				+ "               <YCoord/>\n"
				+ "               <DeliveryPointSuffix/>\n"
				+ "               <ValidStatus/>\n"
				+ "               <TrustedSourceAddressFlag/>\n"
				+ "            </GiftAddress>\n"
				+ "         </Supporters>\n"
				+ "         <Membership>\n"
				+ "            <ReceiptNo/>\n"
				+ "            <TripFormID>184307764</TripFormID>\n"
				+ "            <RecruiterID>CMS</RecruiterID>\n"
				+ "            <ReferralMembershipID/>\n"
				+ "            <SourceCode>NTENTRY</SourceCode>\n"
				+ "            <PropertyNo>10732</PropertyNo>\n"
				+ "            <RecruitmentPoint>04</RecruitmentPoint>\n"
				+ "            <MembershipValidFrom>17/04/2018</MembershipValidFrom>\n"
				+ "            <MembershipValidTo/>\n"
				+ "            <MembershipScheme/>\n"
				+ "            <GroupMemberCount>02</GroupMemberCount>\n"
				+ "            <LiteratureIssued>Y</LiteratureIssued>\n"
				+ "            <ExtraMaterialIssued/>\n"
				+ "            <DeclinePromotion/>\n"
				+ "            <GiftFlag>N</GiftFlag>\n"
				+ "            <GiftType/>\n"
				+ "            <GiftMessage/>\n"
				+ "            <GiftToGiver/>\n"
				+ "            <GiftMembershipStartDate>17/04/2018</GiftMembershipStartDate>\n"
				+ "            <Channel>Face to Face</Channel>\n"
				+ "            <FirstJoinedDate/>\n"
				+ "            <GiftAidMandatoryQuestion>Yes</GiftAidMandatoryQuestion>\n"
				+ "            <IncludeInPrizeDraw/>\n"
				+ "         </Membership>\n"
				+ "         <Donation>\n"
				+ "            <TripFormID/>\n"
				+ "            <SourceCode/>\n"
				+ "            <FundCode/>\n"
				+ "            <PropertyNo/>\n"
				+ "            <RecruitmentPoint/>\n"
				+ "            <ReceiptNo/>\n"
				+ "            <CreationChannel/>\n"
				+ "            <DonationCategory/>\n"
				+ "            <GiftAidMandatoryQuestion/>\n"
				+ "            <IncludeInPrizeDraw/>\n"
				+ "            <TicketSalevalue/>\n"
				+ "            <NumberOfTickets/>\n"
				+ "            <AcknowledgementReqdFlag/>\n"
				+ "         </Donation>\n"
				+ "         <Gift-Aid>\n"
				+ "            <Signature/>\n"
				+ "            <SignDate/>\n"
				+ "            <EndDate/>\n"
				+ "            <SourceCode>NTENTRY</SourceCode>\n"
				+ "         </Gift-Aid>\n"
				+ "         <PaymentDetails>\n"
				+ "            <PaymentMethod>CC/DC at Property</PaymentMethod>\n"
				+ "            <PaymentFrequency/>\n"
				+ "            <PaymentAmount>114.00</PaymentAmount>\n"
				+ "            <PaymentTerm/>\n"
				+ "            <TopupDonationAmount/>\n"
				+ "            <ExistingReceiptID/>\n"
				+ "            <CreditCard>\n"
				+ "               <CardType/>\n"
				+ "               <CardNumber/>\n"
				+ "               <Expiry/>\n"
				+ "               <IssueNo/>\n"
				+ "               <StartDate/>\n"
				+ "               <Signature/>\n"
				+ "               <SignDate/>\n"
				+ "               <Title/>\n"
				+ "               <Initials/>\n"
				+ "               <Surname/>\n"
				+ "               <ReceiptID/>\n"
				+ "            </CreditCard>\n"
				+ "            <DirectDebit>\n"
				+ "               <BankName/>\n"
				+ "               <BankAddress/>\n"
				+ "               <BankPostCode/>\n"
				+ "               <BankSortCode/>\n"
				+ "               <BankAccountNumber/>\n"
				+ "               <BankAccountName/>\n"
				+ "               <BankOriginator>410045</BankOriginator>\n"
				+ "               <DDSignature>N</DDSignature>\n"
				+ "               <DDSignDate>N</DDSignDate>\n"
				+ "               <DDDate/>\n"
				+ "               <ReceiptID/>\n"
				+ "            </DirectDebit>\n"
				+ "         </PaymentDetails>\n"
				+ "      </ns2:Support>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>\n";
	}

	private String createMembership2(String firstName, String lastName, String addressLine1, String docRefPrefix, String docRefNumber) {
		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope\" xmlns:cre=\"http://xmlns.nationaltrust.org.uk/crm/services/1.0/CreateMembership\" xmlns:com=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/common\" xmlns:mem=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/membership\" xmlns:ban=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/banking\" xmlns:per=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/person\" xmlns:add=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/address\" xmlns:con=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPoint\" xmlns:con1=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPref\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <cre:CreateMembershipRequest xmlns:cre=\"http://xmlns.nationaltrust.org.uk/crm/services/1.0/CreateMembership\" xmlns:com=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/common\" xmlns:mem=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/membership\" xmlns:ban=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/banking\" xmlns:per=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/person\" xmlns:add=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/address\" xmlns:con=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPoint\" xmlns:con1=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPref\">\n"
				+ "         <cre:SourceDetails>\n"
				+ "            <com:SourceSystem>SUPPORTERSERVICE</com:SourceSystem>\n"
				+ "         </cre:SourceDetails>\n"
				+ "         <cre:MembershipInfo>\n"
				+ "            <mem:SourceTransactionID>MOTEST321</mem:SourceTransactionID>\n"
				+ "            <mem:MembershipType>INDIVIDUAL</mem:MembershipType>\n"
				+ "            <mem:MembershipPeriod>ANNUAL</mem:MembershipPeriod>\n"
				+ "            <mem:MembershipPrice>63.00</mem:MembershipPrice>\n"
				+ "            <mem:Channel>WEB</mem:Channel>\n"
				+ "            <mem:PaymentInfo>\n"
				+ "               <mem:PaymentTerm>MONTHLY</mem:PaymentTerm>\n"
				+ "               <mem:PaymentMethod>DIRECT_DEBIT_ATTACHED</mem:PaymentMethod>\n"
				+ "               <mem:DirectDebit>\n"
				+ "                  <ban:BankAccountName>Twirl</ban:BankAccountName>\n"
				+ "                  <ban:BankSortCode>200000</ban:BankSortCode>\n"
				+ "                  <ban:BankAccountNumber>55779911</ban:BankAccountNumber>\n"
				+ "               </mem:DirectDebit>\n"
				+ "            </mem:PaymentInfo>\n"
				+ "            <mem:SourceCode>BINOCS</mem:SourceCode>\n"
				+ "                   </cre:MembershipInfo>\n"
				+ "                 <cre:LeadSupporterInfo>\n"
				+ "            <mem:PersonInfo>\n"
				+ "               <per:Title>Mr.</per:Title>\n"
				+ "               <per:FirstName>Brian</per:FirstName>\n"
				+ "               <per:LastName>Ferry</per:LastName>\n"
				+ "               <per:DateOfBirth>1975-01-31</per:DateOfBirth>\n"
				+ "               </mem:PersonInfo>\n"
				+ "            <mem:AddressInfo>\n"
				+ "               <add:isValidated>false</add:isValidated>\n"
				+ "               <add:AddressLine1>1 Road</add:AddressLine1>\n"
				+ "                 <add:Country>United Kingdom</add:Country>\n"
				+ "               <add:PostCode>SN4 4AX</add:PostCode>\n"
				+ "             </mem:AddressInfo>\n"
				+ "            <mem:TelephonesInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Telephone>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:TelephoneType>GENERAL</con:TelephoneType>\n"
				+ "                  <con:TelephoneNumber>+441189723112</con:TelephoneNumber>\n"
				+ "               </con:Telephone>\n"
				+ "                </mem:TelephonesInfo>\n"
				+ "            <mem:EmailsInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Email>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:EmailAddress>twirl@bt.com</con:EmailAddress>\n"
				+ "               </con:Email>\n"
				+ "                </mem:EmailsInfo>\n"
				+ "            <mem:ContactPrefs>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_SINGLE_STATEMENT</con1:PreferenceType>\n"
				+ "                  <con1:Preference>false</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "               </mem:ContactPrefs>\n"
				+ "             </cre:LeadSupporterInfo>\n"
				+ "         <cre:TransactionTimestamp>2018-01-23T10:46:00-01:00</cre:TransactionTimestamp>\n"
				+ "      </cre:CreateMembershipRequest>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>";
	}

	private String createMYNTAccountFromShop(String firstName, String lastName, String docRefPrefix, String docRefNumber, String date, String price, String dob, String postcode, String email) {
		return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cre=\"http://xmlns.nationaltrust.org.uk/crm/services/1.0/CreateMembership\" xmlns:com=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/common\" xmlns:mem=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/membership\" xmlns:ban=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/banking\" xmlns:per=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/person\" xmlns:add=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/address\" xmlns:con=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPoint\" xmlns:con1=\"http://www.nationaltrust.org.uk/crm/xsd/1.0/contactPref\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <cre:CreateMembershipRequest>\n"
				+ "         <cre:SourceDetails>\n"
				+ "            <com:SourceSystem>SUPPORTERSERVICE</com:SourceSystem>\n"
				+ "         </cre:SourceDetails>\n"
				+ "         <cre:MembershipInfo>\n"
				+ "            <mem:SourceTransactionID>" + docRefPrefix + "" + docRefNumber + "" + date + "</mem:SourceTransactionID>\n"
				+ "            <mem:MembershipType>INDIVIDUAL</mem:MembershipType>\n"
				+ "            <mem:MembershipPeriod>ANNUAL</mem:MembershipPeriod>\n"
				+ "            <mem:MembershipPrice>" + price + "</mem:MembershipPrice>\n"
				+ "            <mem:Channel>WEB</mem:Channel>\n"
				+ "            <mem:PaymentInfo>\n"
				+ "               <mem:PaymentTerm>MONTHLY</mem:PaymentTerm>\n"
				+ "               <mem:PaymentMethod>DIRECT_DEBIT_ATTACHED</mem:PaymentMethod>\n"
				+ "               <mem:DirectDebit>\n"
				+ "                  <ban:BankAccountName>John</ban:BankAccountName>\n"
				+ "                  <ban:BankSortCode>200000</ban:BankSortCode>\n"
				+ "                  <ban:BankAccountNumber>55779911</ban:BankAccountNumber>\n"
				+ "               </mem:DirectDebit>\n"
				+ "            </mem:PaymentInfo>\n"
				+ "            <mem:SourceCode>BINOCS</mem:SourceCode>\n"
				+ "                   </cre:MembershipInfo>\n"
				+ "                 <cre:LeadSupporterInfo>\n"
				+ "            <mem:PersonInfo>\n"
				+ "               <per:Title>Mr.</per:Title>\n"
				+ "               <per:FirstName>" + firstName + "</per:FirstName>\n"
				+ "               <per:LastName>Badger</per:LastName>\n"
				+ "               <per:DateOfBirth>" + dob + "</per:DateOfBirth>\n"
				+ "               </mem:PersonInfo>\n"
				+ "            <mem:AddressInfo>\n"
				+ "               <add:isValidated>false</add:isValidated>\n"
				+ "               <add:AddressLine1>1 badger</add:AddressLine1>\n"
				+ "                 <add:Country>United Kingdom</add:Country>\n"
				+ "               <add:PostCode>" + postcode + "</add:PostCode>\n"
				+ "             </mem:AddressInfo>\n"
				+ "            <mem:TelephonesInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Telephone>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:TelephoneType>GENERAL</con:TelephoneType>\n"
				+ "                  <con:TelephoneNumber>+441189723112</con:TelephoneNumber>\n"
				+ "               </con:Telephone>\n"
				+ "                </mem:TelephonesInfo>\n"
				+ "            <mem:EmailsInfo>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con:Email>\n"
				+ "                  <con:isPrimary>true</con:isPrimary>\n"
				+ "                  <con:EmailAddress>" + email + "</con:EmailAddress>\n"
				+ "               </con:Email>\n"
				+ "                </mem:EmailsInfo>\n"
				+ "            <mem:ContactPrefs>\n"
				+ "               <!--1 or more repetitions:-->\n"
				+ "               <con1:ContactPref>\n"
				+ "                  <con1:PreferenceType>OPT_IN_TO_SINGLE_STATEMENT</con1:PreferenceType>\n"
				+ "                  <con1:Preference>true</con1:Preference>\n"
				+ "               </con1:ContactPref>\n"
				+ "               </mem:ContactPrefs>\n"
				+ "             </cre:LeadSupporterInfo>\n"
				+ "         <cre:TransactionTimestamp>2018-02-13T10:46:00-01:00</cre:TransactionTimestamp>\n"
				+ "      </cre:CreateMembershipRequest>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>";
	}

	//	public Map<String, String> getTheRenewalDetailsForRenewalPrePostMarchFromSupporterBD(String usedSupporterNumber, Set<String> labelNames) {
	//		String sqlSta = membershipRenewedPreMarchInDBSql(usedSupporterNumber);
	//		setLogs(sqlSta);
	//		=return SupporterDBConnection.getData(sqlSta, labelNames);
	//		//		setLogs(supporterDetails);
	//	}
	//
	//
	//	private String membershipRenewedPreMarchInDBSql(String usedSupporterNumber) {
	//		System.out.println( "Hello World!" );
	//		return String.format("select ACCOUNT_REFERENCE FROM" + " ACCOUNTAPI_OWNER.ACCOUNT" + " WHERE SUPPORTER_NUMBER = '%s'" + " order by CREATED desc" ,usedSupporterNumber);
	//
	//	}

	public void setGDPRTrueOnSupporter() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPut request = new HttpPut(EnvironmentConfiguration.getText("supporter.gdpr.true"));
			httpClient.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setGDPRFalseOnSupporter() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPut request = new HttpPut(EnvironmentConfiguration.getText("supporter.gdpr.false"));
			httpClient.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setFutureDate(String date) {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPut request = new HttpPut(date);
			httpClient.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setSystemDate() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPut request = new HttpPut(EnvironmentConfiguration.getText("setSystemDate"));
			httpClient.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setDonateSystemDate() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPut request = new HttpPut(EnvironmentConfiguration.getText("setDonateSystemDate"));
			httpClient.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public void setJoinSystemDate() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPut request = new HttpPut(EnvironmentConfiguration.getText("setJoinSystemDate"));
			httpClient.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void setFutureSupporterAPIDate(String date) {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPut request = new HttpPut (date);
			httpClient.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void setCurrentSupporterAPIDate() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPut request = new HttpPut (EnvironmentConfiguration.getText("setSupporterAPISystemDate"));
			httpClient.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setGDPROn() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPut request = new HttpPut(EnvironmentConfiguration.getText("setGDPROn"));
			httpClient.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setGDPROff() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPut request = new HttpPut(EnvironmentConfiguration.getText("setGDPROff"));
			httpClient.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	private int response = 0;

	public int createCRMMembershipCallBack(CreateCallbackRequest createCalbackRequest) {

		HttpClient httpClient = HttpClientBuilder.create().build();

		try {
			json.put("membershipEventType", createCalbackRequest.getEventType());
			json.put("emailAddress", createCalbackRequest.getEmail());
			json.put("supporterNumber", createCalbackRequest.getSupporterNumber());
			json.put("additionalSupporterNumber", createCalbackRequest.getAdditionalSupporterNumber());
			json.put("leadSupporterNumber", createCalbackRequest.getLeadSupporterNumber());
			json.put("dateOfBirth", createCalbackRequest.getDob());
			json.put("postCode", createCalbackRequest.getPostcode());
			json.put("membershipNumber", createCalbackRequest.getMembershipNumber());
			json.put("sourceTransactionId", createCalbackRequest.getTrxID());


			HttpPost request = new HttpPost(EnvironmentConfiguration.getCreateCallbackURL());
			StringEntity params = new StringEntity(json.toString());
			request.addHeader("content-type", "application/json");
			request.setEntity(params);
			HttpResponse httpResponse = httpClient.execute(request);
			response = httpResponse.getStatusLine().getStatusCode();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}



	public int createOldCRMMembershipCallBack(CreateOldCallbackRequest createOldCallbackRequest) {

		HttpClient httpClient = HttpClientBuilder.create().build();

		try {
			json = new JSONObject();
			json.put("membershipEventType", createOldCallbackRequest.getEventType());
			json.put("emailAddress", createOldCallbackRequest.getEmail());
			json.put("supporterNumber", createOldCallbackRequest.getSupporterNumber());
			json.put("dateOfBirth", createOldCallbackRequest.getDob());
			json.put("postCode", createOldCallbackRequest.getPostcode());
			json.put("membershipNumber", createOldCallbackRequest.getMembershipNumber());
			json.put("sourceTransactionId", createOldCallbackRequest.getTrxID());


			HttpPost request = new HttpPost(EnvironmentConfiguration.getCreateCallbackURL());
			StringEntity params = new StringEntity(json.toString());
			request.addHeader("content-type", "application/json");
			request.setEntity(params);
			HttpResponse httpResponse = httpClient.execute(request);
			response = httpResponse.getStatusLine().getStatusCode();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}


	public String triggerTempCardEmailService() {
		HttpClient client = HttpClientBuilder.create().build();
		try {

			HttpPost request = new HttpPost(EnvironmentConfiguration.getText("wsdlEmailTrigger"));
			//			request.setHeader("content-type", "application/json");

			//			HttpPost request = new HttpPost(createMembership1(docRefPrefix, docRefNumber));

			request.setHeader("content-type", "text/xml;charset=UTF-8");
			request.setHeader("SOAPAction", "create");

			HttpResponse response = client.execute(request);
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			StringBuilder builder = new StringBuilder();
			String line;
			http:
			//10.247.16.46:7003/soa-infra/services/crm/Membership/Membership

			while ((line = bufReader.readLine()) != null) {
				builder.append(line);
				builder.append(System.lineSeparator());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void deleteSupporter(String accountReferenceNumber) {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {

			HttpDelete request = new HttpDelete(REST_SERVICE_URL + "/" + accountReferenceNumber);
			//			StringEntity params = new StringEntity(json.toString());
			request.addHeader("content-type", "application/json");
			//			request.setEntity(params);
			//			httpClient.execute(request);
			HttpResponse httpResponse = httpClient.execute(request);
			response = httpResponse.getStatusLine().getStatusCode();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
