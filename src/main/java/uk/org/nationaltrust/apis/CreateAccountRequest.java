package uk.org.nationaltrust.apis;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.util.UUID;

/**
 * @author Adrian Pillinger.
 */
@Getter
@Builder
public class
CreateAccountRequest {

	private String title;

	private String firstName;

	private String lastName;

	private String emailAdress;

	private String addressLine1;

	private String addressLine2;

	private String addressLine3;

	private String addressLine4;

	private String city;

	private String county;

	private String postCode;

	private String countryCode;

	private String password;

	private String telephone;

	private String telephoneCountyCode;

	private String altTelephone;

	private String altTelephoneCountyCode;

	private boolean emailPref;

	private boolean postPref;

	private boolean phonePref;

	private String activeEmail;

	private String fundraisingDiscoverySource;

	private String fundraisingDiscoverySourceOther;

	private String fundraisingReason;

	private String fundraisingReasonOther;

	private boolean confirmAge;

	public static CreateAccountRequest.CreateAccountRequestBuilder aValidCreateAccountRequestBuilder() {

		return CreateAccountRequest.builder()
				.activeEmail((UUID.randomUUID()) + "testActive@NTShop.com")
				.firstName("testETL")
				.lastName("shopAccount")
				.addressLine1("Nexus Business center")
				.addressLine2("Darby close")
				.addressLine3("")
				.addressLine4("")
				.city("swindon")
				.county("wiltshire")
				.postCode("SN25 2PN")
				.countryCode("GB")
				.password("Passw0rd!")
				.telephone("0179321200")
				.telephoneCountyCode("")
				.altTelephone("")
				.altTelephoneCountyCode("")
				.emailPref(true)
				.postPref(true)
				.phonePref(true)
				.confirmAge(true)
				.fundraisingDiscoverySource("Email")
				.fundraisingReason("I want to help look after special places")
				.fundraisingReasonOther("DIY Testing - reason other")
				.fundraisingDiscoverySourceOther("DIY Testing - Advert other");
	}
}
