package uk.org.nationaltrust.apis;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.TestBase;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.*;
import java.io.IOException;

/**
 * @author by Pramod on 09/03/2016.
 */
public class CRMSoapWebService extends TestBase {

	private final String serverUrl;

	public CRMSoapWebService(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	private String callWebService(String soapAction, String supporterNumber) throws IOException {

		String body = convertSoapMessageToString(generateSoapMessageToGetSupporterDetails(soapAction, serverUrl, supporterNumber));
		StringEntity stringEntity = new StringEntity(body, "text/xml", "UTF-8");
		System.out.println("The soap body --- " + body);
		stringEntity.setChunked(true);

		// Request parameters and other properties.
		setLogs("Request parameters and other properties.");
		HttpPost httpPost = new HttpPost(EnvironmentConfiguration.getText("wsdl"));
		httpPost.setEntity(stringEntity);
		httpPost.addHeader("Accept", "text/xml");
		httpPost.addHeader("SOAPAction", soapAction);

		// Execute and get the response.
		setLogs("Execute and get the response.");
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse response = httpClient.execute(httpPost);
		HttpEntity entity = response.getEntity();

		String strResponse = null;
		if (entity != null) {
			strResponse = EntityUtils.toString(entity);
			return strResponse;

		} else {
			throw new RuntimeException("No response body");
		}

	}

	public String getSupporterDetails(String supporterNumber) throws IOException {
		//return callWebService("get", supporterNumber);
		String postCode = getNodeValue(callWebService("get", supporterNumber));
		return postCode;
	}

	private String convertSoapMessageToString(SOAPMessage message) {
		String result = null;

		if (message != null) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
				message.writeTo(baos);
				result = baos.toString();
			} catch (Exception e) {
			} finally {
				if (baos != null) {
					try {
						baos.close();
					} catch (IOException ioe) {
					}
				}
			}
		}
		return result;
	}

	private SOAPMessage generateSoapMessageToGetSupporterDetails(String prefix, String serverUrl, String supporterNumber) {
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();
			SOAPPart soapPart = soapMessage.getSOAPPart();

			//SOAP Envelope
			SOAPEnvelope envelope = soapPart.getEnvelope();
			envelope.addNamespaceDeclaration(prefix, serverUrl);

			//SOAP Body
			SOAPHeader header = envelope.getHeader();
			SOAPBody soapBody = envelope.getBody();
			header.detachNode();
			SOAPElement soapBodyEle = soapBody.addChildElement("GetSupportDetailsRequest", prefix);
			SOAPElement soapBodyEle1 = soapBodyEle.addChildElement("SupporterNumber", prefix);
			soapBodyEle1.addTextNode(supporterNumber);
			MimeHeaders headers = soapMessage.getMimeHeaders();
			headers.addHeader("SOAPAction", serverUrl + prefix);

			soapMessage.saveChanges();
			return soapMessage;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public String getNodeValue(String xml) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuilder = dbf.newDocumentBuilder();
			Document doc = dbuilder.parse(xml);

			doc.getDocumentElement().normalize();

			NodeList nlist = doc.getElementsByTagName("Address");
			for (int temp = 0; temp < nlist.getLength(); temp++) {
				org.w3c.dom.Node nNode = nlist.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;

					System.out.println(eElement.getAttribute("PostCode"));
					return eElement.getAttribute("PostCode");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
