package uk.org.nationaltrust.apis;

import com.jayway.jsonpath.JsonPath;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;

import java.util.ArrayList;
import java.util.List;

public class RevenueProductsApiRequests {

	private static JSONObject json = new JSONObject();

	private int response = 0;

	public int createAccountWithPersonalDetails(CreateAccountRequest createAccountRequest) {

		HttpClient httpClient = HttpClientBuilder.create().build();

		try {
			json.put("accountEmail", createAccountRequest.getActiveEmail());
			json.put("password", createAccountRequest.getPassword());
			json.put("sendActivationEmail", true);
			json.put("sourceSystem", "SHOP");
//			json.put("title", createAccountRequest.getTitle());
			json.put("firstName", createAccountRequest.getFirstName());
			json.put("lastName", createAccountRequest.getLastName());
			json.put("addressLine1", createAccountRequest.getAddressLine1());
			json.put("addressLine2", createAccountRequest.getAddressLine2());
			json.put("addressLine3", createAccountRequest.getAddressLine3());
			json.put("addressLine4", createAccountRequest.getAddressLine4());
			json.put("city", createAccountRequest.getCity());
			json.put("county", createAccountRequest.getCounty());
			json.put("postcode", createAccountRequest.getPostCode());
			json.put("countryCode", createAccountRequest.getCountryCode());
			json.put("telephoneNumber", createAccountRequest.getTelephone());
			json.put("telephoneNumberCountryCode", createAccountRequest.getTelephoneCountyCode());
			json.put("alternativeTelephoneNumber", createAccountRequest.getAltTelephone());
			json.put("alternativeTelephoneNumberCountryCode", createAccountRequest.getAltTelephoneCountyCode());
			json.put("contactByEmail", createAccountRequest.isEmailPref());
			json.put("contactByPhone", createAccountRequest.isPhonePref());
			json.put("contactByPost", createAccountRequest.isPostPref());

			HttpPost request = new HttpPost(EnvironmentConfiguration.getShopCreateAccountURL());
			StringEntity params = new StringEntity(json.toString());
			request.addHeader("content-type", "application/json");
			request.setEntity(params);
			HttpResponse httpResponse = httpClient.execute(request);
			response = httpResponse.getStatusLine().getStatusCode();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public int getResponseStatusCode(String apiEndPoint) {
		HttpClient httpClient = HttpClientBuilder.create().build();

		try {
			HttpPost	request = new HttpPost(EnvironmentConfiguration.getETLTriggerURL(apiEndPoint));
			HttpResponse httpResponse	= httpClient.execute(request);
			HttpEntity entity = httpResponse.getEntity();
			return httpResponse.getStatusLine().getStatusCode();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public List<String> getETLInfo() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse httpResponse = null;

		try {
			HttpGet request = new HttpGet(EnvironmentConfiguration.getETLInfoURL());
			httpResponse = httpClient.execute(request);
			HttpEntity entity = httpResponse.getEntity();
			String json = EntityUtils.toString(entity);

			List<String> infoList = new ArrayList<>();
			infoList.add(JsonPath.parse(json).read("$.stats.previous-run-started"));
			infoList.add(JsonPath.parse(json).read("$.stats.previous-run-ended"));
			infoList.add(JsonPath.parse(json).read("$.stats.previous-run-record-count"));
			infoList.add(JsonPath.parse(json).read("$.stats.previous-run-record-filename"));
			infoList.add(JsonPath.parse(json).read("$.stats.errors").toString());
			return infoList;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<CreateDonateFormRequest> getCommemorativeDonationResponse(String donationId){
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse httpResponse = null;

		try {
			HttpGet request = new HttpGet(EnvironmentConfiguration.getText("individualDonationStatus")+donationId);
			httpResponse = httpClient.execute(request);
			HttpEntity entity = httpResponse.getEntity();
			String json = EntityUtils.toString(entity);

			List<CreateDonateFormRequest> donateFormRequests = new ArrayList<>();
			donateFormRequests.add(CreateDonateFormRequest.builder()
					.placeID(JsonPath.parse(json).read("$.placeId"))
					.donationId(JsonPath.parse(json).read("$.donationId"))
					.placeName(JsonPath.parse(json).read("$.placeName"))
					.donationStory(JsonPath.parse(json).read("$.story"))
					.moderationStatus(JsonPath.parse(json).read("$.moderationStatus"))
					.isPrivateListing(Boolean.toString(JsonPath.parse(json).read("$.privateListing")))
					.donationTone(JsonPath.parse(json).read("$.tone"))
					.occasion(JsonPath.parse(json).read("$.occasion"))
					.commemorativeSubject(JsonPath.parse(json).read("$.subject"))
					.version(Integer.toString(JsonPath.parse(json).read("$.version")))
					.created(JsonPath.parse(json).read("$.created"))
					.lastUpdated(JsonPath.parse(json).read("$.lastUpdated"))
					.lastUpdatedBy(JsonPath.parse(json).read("$.lastUpdatedBy"))
					.moderatorComments(JsonPath.parse(json).read("$.moderatorComments"))
					.sharingLink(JsonPath.parse(json).read("$.sharingLink"))
					.donateFormEmail(JsonPath.parse(json).read("$.donorEmail"))
					.donorFullName(JsonPath.parse(json).read("$.donorFullName"))
					.expiryTimeStamp(JsonPath.parse(json).read("$.expiryTimestamp")).build());

			return donateFormRequests ;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<CreateDonateFormRequest> getInCelebrationMapPinResponse(String donationId){
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse httpResponse = null;

		try {
			HttpGet request = new HttpGet(EnvironmentConfiguration.getText("donationWithPlaceID")+donationId);
			httpResponse = httpClient.execute(request);
			HttpEntity entity = httpResponse.getEntity();
			String json = EntityUtils.toString(entity);

			List<CreateDonateFormRequest> donateFormRequests = new ArrayList<>();
			donateFormRequests.add(CreateDonateFormRequest.builder()
					.donationStory(JsonPath.parse(json).read("$.story"))
					.donationTone(JsonPath.parse(json).read("$.tone"))
					.occasion(JsonPath.parse(json).read("$.occasion"))
					.commemorativeSubject(JsonPath.parse(json).read("$.subject"))
					.created(JsonPath.parse(json).read("$.created")).build());


			return donateFormRequests ;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public List<CreateDonateFormRequest> getInMemoryMapPinResponse(String donationId){
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse httpResponse = null;

		try {
			HttpGet request = new HttpGet(EnvironmentConfiguration.getText("donationWithPlaceID")+donationId);
			httpResponse = httpClient.execute(request);
			HttpEntity entity = httpResponse.getEntity();
			String json = EntityUtils.toString(entity);

			List<CreateDonateFormRequest> donateFormRequests = new ArrayList<>();
			donateFormRequests.add(CreateDonateFormRequest.builder()
					.donationStory(JsonPath.parse(json).read("$.story"))
					.donationTone(JsonPath.parse(json).read("$.tone"))
					.commemorativeSubject(JsonPath.parse(json).read("$.subject"))
					.created(JsonPath.parse(json).read("$.created")).build());


			return donateFormRequests ;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}



	public String getJsonStringResponse(String APIendpoint ){
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse httpResponse = null;
		try {
			HttpGet request = new HttpGet(EnvironmentConfiguration.getText(APIendpoint));
			httpResponse = httpClient.execute(request);
			HttpEntity entity = httpResponse.getEntity();

			String jsonString = null;
			if(httpResponse.getStatusLine().getStatusCode()==200) {
				jsonString = EntityUtils.toString(entity);
			}
			return jsonString;
		}
		catch(Exception e){
			return null;
		}
	}

	public List<CreateDonateFormRequest> getDonationWithPlaceIDResponse(String donationId, String donationType){
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpResponse httpResponse = null;

		try {
			HttpGet request = new HttpGet(EnvironmentConfiguration.getText("donationWithPlaceID")+donationId);
			httpResponse = httpClient.execute(request);
			HttpEntity entity = httpResponse.getEntity();
			String json = EntityUtils.toString(entity);

			List<CreateDonateFormRequest> donateFormRequests = new ArrayList<>();
			if (donationType.equalsIgnoreCase("in memory")){
				donateFormRequests.add(CreateDonateFormRequest.builder()
						.commemorativeSubject(JsonPath.parse(json).read("$.subject"))
						.donationStory(JsonPath.parse(json).read("$.story"))
						.donationTone(JsonPath.parse(json).read("$.tone"))
						.created(JsonPath.parse(json).read("$.created"))
						.sharingLink(JsonPath.parse(json).read("$.sharingLink")).build());

			}
			if(donationType.equalsIgnoreCase("in celebration")) {
				donateFormRequests.add(CreateDonateFormRequest.builder()
						.commemorativeSubject(JsonPath.parse(json).read("$.subject"))
						.donationStory(JsonPath.parse(json).read("$.story"))
						.donationTone(JsonPath.parse(json).read("$.tone"))
						.occasion(JsonPath.parse(json).read("$.occasion"))
						.created(JsonPath.parse(json).read("$.created"))
						.sharingLink(JsonPath.parse(json).read("$.sharingLink")).build());
			}
			return donateFormRequests ;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
