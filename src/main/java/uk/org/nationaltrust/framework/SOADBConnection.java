package uk.org.nationaltrust.framework;

import org.testng.Assert;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by nick.thompson on 19/02/2018.
 */
public class SOADBConnection extends TestBase {
	private String getSOADBUserName(){
		return EnvironmentConfiguration.getSOADBUserName();

	}
	//	public class CRMDBConnection extends TestBase {
	//		private String getCRMS1TDBUserName(){
	//			return EnvironmentConfiguration.getCRMS1TDBUserName();
	private String getSOADBPassword() {
		return EnvironmentConfiguration.getSOADBPassword();
	}
	//	private String getCRMS1TDBPassword() {
	//		return EnvironmentConfiguration.getCRMS1TDBPassword();
	//	}

	private String getSOADBConnectionString() {
		return EnvironmentConfiguration.getSOADBConnectionString();
	}
	//	private String getCRMS1TDBConnectionString() {
	//		return EnvironmentConfiguration.getCRMS1TDBConnectionString();
	//	}

	private Connection dbConnection() {
		try {
			// Ensure driver is loaded
			Class.forName("oracle.jdbc.driver.OracleDriver");
			// DriverManager.registerDriver(new OracleDriver());
			String connectionString = getSOADBConnectionString();
			log.debug("Connecting to database with connection string " + connectionString);
			// TODO Should use DataSource rather than DriverManager
			Connection conn = DriverManager.getConnection(connectionString, getSOADBUserName(), getSOADBPassword());
			log.debug("Connected.");
			return conn;
		} catch (SQLException e) {
			log.error("Could not connect to database", e);
		} catch (ClassNotFoundException e) {
			log.error("Oracle JDBC driver not accessible - check your classpath", e);
		}
		return null;
	}
	private void closeConnectionQuietly(Connection conn) {
		if (conn == null) {
			return;
		}
		try {
			conn.close();
		} catch (SQLException e) {
			log.error("Failed to close DB connection - weirdness! Things may well go downhill from here...", e);
		}
	}

	public void setData(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			statement.executeQuery(sqlStatement);
//			conn.commit();
		} catch (SQLException e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			closeConnectionQuietly(conn);
		}
	}

	//	public String getData(String sqlStatement, String columnLabel) {
	//		String result = executeSql(sqlStatement, res -> res.getString(columnLabel));
	//		log.info("The  " + columnLabel + " is " + result);
	//		return result;
	//	}

	public Map<String, String> getData(String sqlStatement, Set<String> columnLabels) {
		Map<String, String> resultsByColumnLabel = executeSql(sqlStatement, res -> {
			Map<String, String> results = new HashMap<>();
			for (String columnLabel : columnLabels) {
				String columnValue = res.getString(columnLabel);
				results.put(columnLabel, columnValue);
				log.info("The  " + columnLabel + " is " + columnValue);
			}
			return results;
		});
		return resultsByColumnLabel;
	}

	private <T> T executeSql(String sql, Mapper<ResultSet, T> resultSetConsumer) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing the sql script...");
			ResultSet res = statement.executeQuery(sql);
//			conn.commit();
			res.next();
			return resultSetConsumer.apply(res);
		} catch (SQLException e) {
			log.error("Could not execute instant SQL statement", e);
			Assert.fail();
			return null;
		} finally {
			log.info("Closing the connection");
			closeConnectionQuietly(conn);
		}
	}

	@FunctionalInterface
	private static interface Mapper<S, T> {

		T apply(S s) throws SQLException;
	}

	private Connection CRMDBConnection() {
		try {
			// Ensure driver is loaded
			Class.forName("oracle.jdbc.driver.OracleDriver");
			// DriverManager.registerDriver(new OracleDriver());
			String connectionString = getSOADBConnectionString();
			APPLICATION_LOGS.debug("Connecting to database with connection string " + connectionString);
			// TODO Should use DataSource rather than DriverManager
			Connection conn = DriverManager.getConnection(connectionString, getSOADBUserName(), getSOADBPassword());
			APPLICATION_LOGS.debug("Connected.");
			return conn;
		} catch (SQLException e) {
			APPLICATION_LOGS.error("Could not connect to database", e);
		} catch (ClassNotFoundException e) {
			APPLICATION_LOGS.error("Oracle JDBC driver not accessible - check your classpath", e);
		}
		return null;
	}

	private Connection SOADBConnection() {
		try {
			// Ensure driver is loaded
			Class.forName("oracle.jdbc.driver.OracleDriver");
			// DriverManager.registerDriver(new OracleDriver());
			String connectionString = getSOADBConnectionString();
			APPLICATION_LOGS.debug("Connecting to database with connection string " + connectionString);
			// TODO Should use DataSource rather than DriverManager
			Connection conn = DriverManager.getConnection(connectionString, getSOADBUserName(), getSOADBPassword());
			APPLICATION_LOGS.debug("Connected.");
			return conn;
		} catch (SQLException e) {
			APPLICATION_LOGS.error("Could not connect to database", e);
		} catch (ClassNotFoundException e) {
			APPLICATION_LOGS.error("Oracle JDBC driver not accessible - check your classpath", e);
		}
		return null;
	}

	public String checkCRMStagingTableForNewSupporterPref(String sqlStatement){
		Connection conn = null;
		try{
			conn = SOADBConnection();
			Statement statement = conn.createStatement();
			APPLICATION_LOGS.info("Executing sql to get the data from SOA APPLICATION_LOGS table.........");
			ResultSet res = statement.executeQuery(sqlStatement);
//			conn.commit();
			if(!res.next()){return null;}
			String trx_status = res.getString("trx_status");
			return trx_status;
		}catch (Exception e){
			APPLICATION_LOGS.error("Could not execute instant sql statement", e);
			return "fail";
		} finally {
			APPLICATION_LOGS.info("closing db connection.....");
			closeConnectionQuietly(conn);
		}
	}

	public String checkCRMStagingTableForNewSupporterDDUpdate(String sqlStatement){
		Connection conn = null;
		try{
			conn = SOADBConnection();
			Statement statement = conn.createStatement();
			APPLICATION_LOGS.info("Executing sql to get the data from SOA APPLICATION_LOGS table.........");
			ResultSet res = statement.executeQuery(sqlStatement);
//			conn.commit();
			if(!res.next()){return null;}
			String trx_status = res.getString("trx_status");
			return trx_status;
		}catch (Exception e){
			APPLICATION_LOGS.error("Could not execute instant sql statement", e);
			return "fail";
		} finally {
			APPLICATION_LOGS.info("closing db connection.....");
			closeConnectionQuietly(conn);
		}
	}



	public HashMap<String, String> getContactPreferenceDetailsFromSOADB(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = SOADBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
//			conn.commit();
			res.next();
			HashMap<String, String> preferenceDetailsFromSOA = new HashMap<>();
			preferenceDetailsFromSOA.put("CONTACT_PREFERENCES",res.getString("CONTACT_PREFERENCES"));
			preferenceDetailsFromSOA.put("SUPPORTER_NUMBER",res.getString("SUPPORTER_NUMBER"));

			return preferenceDetailsFromSOA;
		}catch (Exception e){
			log.error("Could not execute instant SQL statement", e);
		}finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getDDDetailsFromSOADB(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = SOADBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
//			conn.commit();
			res.next();
			HashMap<String, String> DDDetailsFromSOA = new HashMap<>();
			DDDetailsFromSOA.put("MEMBERSHIP_NUMBER",res.getString("MEMBERSHIP_NUMBER"));
			DDDetailsFromSOA.put("SUPPORTER_NUMBER",res.getString("SUPPORTER_NUMBER"));
			DDDetailsFromSOA.put("ACCOUNT_NAME",res.getString("ACCOUNT_NAME"));
			DDDetailsFromSOA.put("SORT_CODE",res.getString("SORT_CODE"));
			DDDetailsFromSOA.put("ACCOUNT_NUMBER",res.getString("ACCOUNT_NUMBER"));

			return DDDetailsFromSOA;
		}catch (Exception e){
			log.error("Could not execute instant SQL statement", e);
		}finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getDDDetailsFromCRMDB(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = SOADBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get DD details from CRM DB......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
//			conn.commit();
			res.next();
			HashMap<String, String> DDDetailsFromSOA = new HashMap<>();
			DDDetailsFromSOA.put("bank_account_name",res.getString(""));
			DDDetailsFromSOA.put("BANK_ACCOUNT_NUM",res.getString("BANK_ACCOUNT_NUM"));
			DDDetailsFromSOA.put("BANK_OR_BRANCH_NUMBER",res.getString("BANK_OR_BRANCH_NUMBER"));


			return DDDetailsFromSOA;
		}catch (Exception e){
			log.error("Could not execute instant SQL statement", e);
		}finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public boolean deleteRecord(String email){
		Connection conn = null;
		boolean status = false;
		try{
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			log.info("Connecting to db to delete a record........");
			String query ="delete from ACCOUNTAPI_OWNER where account_email='$'";
			String resultantQuery=query.replace("$",email);
			ResultSet res = statement.executeQuery(resultantQuery);
//			conn.commit();
			status=true;
		}catch (Exception e){
			log.error("Could not delete the record", e);
		} finally {
			log.info("closing db connection.....");
			closeConnectionQuietly(conn);
			return status;
		}
	}
}
