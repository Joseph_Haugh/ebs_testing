package uk.org.nationaltrust.framework.toggle;

/**
 * @author Adrian Pillinger.
 */
public abstract class GdprToggle implements ApplicationFeatureToggle {

	private String REFRESHABLE_GDPR_ENABLED = "refreshable.gdpr.enabled";

	public String getFeatureProperty() {
		return REFRESHABLE_GDPR_ENABLED;
	}
}
