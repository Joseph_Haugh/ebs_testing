package uk.org.nationaltrust.framework.toggle;

/**
 * @author Adrian Pillinger.
 */
public class SelfserveGdprToggle extends GdprToggle {

	private static final String TOGGLE_ENDPOINT = "selfserve.toggle.endpoint";

	private static final String REFRESH_ENDPOINT = "selfserve.refresh.endpoint";

	@Override
	public String getToggleEndpointUrlProperty() {
		return TOGGLE_ENDPOINT;
	}

	@Override
	public boolean isSpringBootApplication() {
		return true;
	}

	@Override
	public String getRefreshEndpointUrlProperty() {
		return REFRESH_ENDPOINT;
	}
}
