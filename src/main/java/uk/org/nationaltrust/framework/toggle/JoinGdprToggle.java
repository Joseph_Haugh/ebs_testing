package uk.org.nationaltrust.framework.toggle;

/**
 * @author Adrian Pillinger.
 */
public class JoinGdprToggle extends GdprToggle {

	private static final String TOGGLE_ENDPOINT = "join.toggle.endpoint";

	@Override
	public String getToggleEndpointUrlProperty() {
		return TOGGLE_ENDPOINT;
	}

	@Override
	public boolean isSpringBootApplication() {
		return false;
	}

	@Override
	public String getRefreshEndpointUrlProperty() {
		return null;
	}
}
