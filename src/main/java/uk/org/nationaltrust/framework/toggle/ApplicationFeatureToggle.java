package uk.org.nationaltrust.framework.toggle;

/**
 * Represents a toggleable feature for a specific application
 *
 * @author Adrian Pillinger.
 */
public interface ApplicationFeatureToggle {

	/**
	 * @return the URL of the feature's toggle endpoint
	 */
	String getToggleEndpointUrlProperty();

	/**
	 * @return true if the application is a spring boot app that uses spring boot's management endpoint for refreshing properties
	 */
	boolean isSpringBootApplication();

	/**
	 * The name of the application's property that toggles the feature on and off
	 *
	 * @return
	 */
	String getFeatureProperty();

	/**
	 * @return the URL of the apps refresh endpoint if a spring boot app
	 */
	String getRefreshEndpointUrlProperty();
}
