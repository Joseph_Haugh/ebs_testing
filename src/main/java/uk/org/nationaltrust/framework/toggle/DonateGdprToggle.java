package uk.org.nationaltrust.framework.toggle;

/**
 * @author Adrian Pillinger.
 */
public class DonateGdprToggle extends GdprToggle {

	private static final String TOGGLE_ENDPOINT = "donate.toggle.endpoint";

	private static final String REFRESH_ENDPOINT = "donate.refresh.endpoint";

	@Override
	public String getToggleEndpointUrlProperty() {
		return TOGGLE_ENDPOINT;
	}

	@Override
	public boolean isSpringBootApplication() {
		return true;
	}

	@Override
	public String getRefreshEndpointUrlProperty() {
		return REFRESH_ENDPOINT;
	}
}
