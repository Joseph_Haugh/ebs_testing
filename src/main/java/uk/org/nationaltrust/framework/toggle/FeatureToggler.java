package uk.org.nationaltrust.framework.toggle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;

/**
 * @author Adrian Pillinger.
 */
public class FeatureToggler {

	public static final boolean ON = true;

	public static final boolean OFF = false;

	private static Logger LOG = LoggerFactory.getLogger(FeatureToggler.class);

	private static CloseableHttpClient httpclient;

	public static void main(String[] args) throws IOException {

		EnvironmentConfiguration.populate("test");
		FeatureToggler.enableFeature(new DonateGdprToggle(), ON);
		FeatureToggler.enableFeature(new SelfserveGdprToggle(), ON);
		FeatureToggler.enableFeature(new JoinGdprToggle(), ON);
		FeatureToggler.enableFeature(new SupporterServiceGdprToggle(), ON);

		//		FeatureToggler.enableFeature(new DonateGdprToggle(), OFF);
		//		FeatureToggler.enableFeature(new SelfserveGdprToggle(), OFF);
		//		FeatureToggler.enableFeature(new JoinGdprToggle(), OFF);
		//		FeatureToggler.enableFeature(new SupporterServiceGdprToggle(), OFF);
	}

	public static void enableFeature(ApplicationFeatureToggle featureToggle, boolean enabled) throws IOException {
		initialiseHttpClient();

		String featureProperty = featureToggle.getFeatureProperty();
		String endpointUrl = EnvironmentConfiguration.getText(featureToggle.getToggleEndpointUrlProperty());

		LOG.info("Enabling feature {}, property {}, via endpoint {}", featureToggle.getClass().getName(), featureProperty, endpointUrl);

		if (featureToggle.isSpringBootApplication()) {
			HttpPost httpPost = new HttpPost(endpointUrl);
			List<NameValuePair> nvps = new ArrayList<>();
			nvps.add(new BasicNameValuePair(featureProperty, Boolean.toString(enabled)));
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
			CloseableHttpResponse response = httpclient.execute(httpPost);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new IllegalStateException("Failed to toggle feature");
			}
			System.out.println(response);

			String refreshUrl = EnvironmentConfiguration.getText(featureToggle.getRefreshEndpointUrlProperty());
			CloseableHttpResponse refreshResponse = httpclient.execute(new HttpPost(refreshUrl));
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new IllegalStateException("Failed to refresh app properties");
			}
			System.out.println(refreshResponse);

		} else {
			HttpPut httpPut = new HttpPut(endpointUrl + "/" + Boolean.toString(enabled));
			CloseableHttpResponse response = httpclient.execute(httpPut);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new IllegalStateException("Failed to toggle feature");
			}
			System.out.println(response);
		}
	}

	private static void initialiseHttpClient() {
		if (httpclient == null) {
			httpclient = HttpClients.createDefault();
		}
	}

}
