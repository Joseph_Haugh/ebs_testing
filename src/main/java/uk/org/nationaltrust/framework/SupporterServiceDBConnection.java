package uk.org.nationaltrust.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.sql.*;
import java.util.*;

/**
 * Created by Sowjanya Annepu
 */
public class SupporterServiceDBConnection extends TestBase {

	private Logger log = LoggerFactory.getLogger(getClass());

	private String getDBUserName() {
		return EnvironmentConfiguration.getDBUserName();
	}

	private String getDBPassword() {
		return EnvironmentConfiguration.getDBPassword();
	}

	private String getConnectionString() {
		return EnvironmentConfiguration.getConnectionString();
	}

	private Connection dbConnection() {
		try {
			// Ensure driver is loaded
			Class.forName("oracle.jdbc.driver.OracleDriver");
			// DriverManager.registerDriver(new OracleDriver());
			String connectionString = getConnectionString();
			log.debug("Connecting to database with connection string " + connectionString);
			// TODO Should use DataSource rather than DriverManager
			Connection conn = DriverManager.getConnection(connectionString, getDBUserName(), getDBPassword());
			log.debug("Connected.");
			return conn;
		} catch (SQLException e) {
			log.error("Could not connect to database", e);
		} catch (ClassNotFoundException e) {
			log.error("Oracle JDBC driver not accessible - check your classpath", e);
		}
		return null;
	}

	private void closeConnectionQuietly(Connection conn) {
		if (conn == null) {
			return;
		}
		try {
			conn.close();
		} catch (SQLException e) {
			log.error("Failed to close DB connection - weirdness! Things may well go downhill from here...", e);
		}
	}

	public void setData(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			statement.executeQuery(sqlStatement);
			//            conn.commit();
		} catch (SQLException e) {
			//            log.error("Could not execute instant SQL statement", e);
			throw new RuntimeException("Could not execute instant SQL statement", e);
		} finally {
			closeConnectionQuietly(conn);
		}
	}

	public String getData(String salStatement, String columnLabel) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing the sql script to get the " + columnLabel);
			ResultSet res = statement.executeQuery(salStatement);

			//            conn.commit();
			res.next();
			String result = res.getString(columnLabel);
			log.info("The  " + columnLabel + " is " + result);
			return result;
		} catch (SQLException e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			log.info("Closing the connection");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public String checkAccountDBTableForNewMYNTAccountDetails(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing sql to get the data from Account table.........");
			ResultSet res = statement.executeQuery(sqlStatement);
			//            conn.commit();
			if (!res.next()) {
				return null;
			}
			String status = res.getString("state");
			return status;
		} catch (Exception e) {
			log.error("Could not execute instant sql statement", e);
			return "fail";
		} finally {
			log.info("closing db connection.....");
			closeConnectionQuietly(conn);
		}
	}

	public HashMap<String, String> getAccountDetailsFromAccountDB(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......" + sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			//            conn.commit();
			res.next();
			HashMap<String, String> accountDetailsFromAccountDB = new HashMap<>();
			accountDetailsFromAccountDB.put("state", res.getString("state"));
			accountDetailsFromAccountDB.put("source_system", res.getString("source_system"));
			accountDetailsFromAccountDB.put("account_reference", res.getString("account_reference"));

			return accountDetailsFromAccountDB;
		} catch (Exception e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getAccountReferenceAccountDB(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......" + sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			conn.setAutoCommit(false);
			//            conn.commit();
			res.next();
			HashMap<String, String> accountReferenceFromAccountDB = new HashMap<>();
			accountReferenceFromAccountDB.put("ACCOUNT_REFERENCE", res.getString("ACCOUNT_REFERENCE"));
			return accountReferenceFromAccountDB;
		} catch (Exception e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;

	}

	public HashMap<String, String> getRegistrationExperienceFromAccountDB(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......" + sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			conn.setAutoCommit(false);
			//            conn.commit();
			res.next();
			HashMap<String, String> registrationExperienceFromAccountDB = new HashMap<>();
			registrationExperienceFromAccountDB.put("EXISTING_SUPPORTER_FLAG", res.getString("EXISTING_SUPPORTER_FLAG"));
			registrationExperienceFromAccountDB.put("ACCOUNT_EMAIL", res.getString("ACCOUNT_EMAIL"));
			return registrationExperienceFromAccountDB;
		} catch (Exception e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getPersonalDetailsFromAccountDB(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get account details from Supporter DB......" + sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			conn.setAutoCommit(false);
			//            conn.commit();
			res.next();
			HashMap<String, String> personalDetailsDetailsFromAccountDB = new HashMap<>();
			personalDetailsDetailsFromAccountDB.put("SUPPORTER_NUMBER", res.getString("SUPPORTER_NUMBER"));
			personalDetailsDetailsFromAccountDB.put("ACCOUNT_EMAIL", res.getString("ACCOUNT_EMAIL"));
			personalDetailsDetailsFromAccountDB.put("SOURCE_SYSTEM", res.getString("SOURCE_SYSTEM"));
			personalDetailsDetailsFromAccountDB.put("REGISTRY_EXPORT_TIMESTAMP", res.getString(("REGISTRY_EXPORT_TIMESTAMP")));
			personalDetailsDetailsFromAccountDB.put("STATE", res.getString("STATE"));
			//            personalDetailsDetailsFromAccountDB.put("CONTACT_BY_EMAIL",res.getString("CONTACT_BY_EMAIL"));
			//            personalDetailsDetailsFromAccountDB.put("CONTACT_BY_PHONE",res.getString("CONTACT_BY_PHONE"));
			//            personalDetailsDetailsFromAccountDB.put("CONTACT_BY_POST",res.getString("CONTACT_BY_POST"));
			//            personalDetailsDetailsFromAccountDB.put("CONTACT_EMAIL",res.getString("CONTACT_EMAIL"));
			////			personalDetailsDetailsFromAccountDB.put("TITLE",res.getString("TITLE"));
			//            personalDetailsDetailsFromAccountDB.put("FIRST_NAME",res.getString("FIRST_NAME"));
			//            personalDetailsDetailsFromAccountDB.put("LAST_NAME",res.getString("LAST_NAME"));
			//            personalDetailsDetailsFromAccountDB.put("ADDRESS_LINE_1",res.getString("ADDRESS_LINE_1"));
			//            personalDetailsDetailsFromAccountDB.put("ADDRESS_LINE_2",res.getString("ADDRESS_LINE_2"));
			//            personalDetailsDetailsFromAccountDB.put("ADDRESS_LINE_3",res.getString("ADDRESS_LINE_3"));
			//            personalDetailsDetailsFromAccountDB.put("ADDRESS_LINE_4",res.getString("ADDRESS_LINE_4"));
			//            personalDetailsDetailsFromAccountDB.put("CITY",res.getString("CITY"));
			//            personalDetailsDetailsFromAccountDB.put("COUNTY",res.getString("COUNTY"));
			//            personalDetailsDetailsFromAccountDB.put("POST_CODE",res.getString("POST_CODE"));
			//            personalDetailsDetailsFromAccountDB.put("TELEPHONE_NUMBER",res.getString("TELEPHONE_NUMBER"));
			//            personalDetailsDetailsFromAccountDB.put("ALTERNATIVE_TELEPHONE_NUMBER",res.getString("ALTERNATIVE_TELEPHONE_NUMBER"));
			return personalDetailsDetailsFromAccountDB;
		} catch (Exception e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	private <T> T executeSql(String sql, SupporterServiceDBConnection.Mapper<ResultSet, T> resultSetConsumer) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing the sql script...");
			ResultSet res = statement.executeQuery(sql);
			//			conn.commit();
			res.next();
			return resultSetConsumer.apply(res);
		} catch (SQLException e) {
			log.error("Could not execute instant SQL statement", e);
			Assert.fail();
			return null;
		} finally {
			log.info("Closing the connection");

			closeConnectionQuietly(conn);
		}
	}

	@FunctionalInterface
	private static interface Mapper<S, T> {

		T apply(S s) throws SQLException;
	}

	public void insertData(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			statement.executeUpdate(sqlStatement);
			conn.commit();
		} catch (SQLException e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			closeConnectionQuietly(conn);
		}
	}

	public HashMap<String, String> getMembershipDetailsFromSupporterDB(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from Supporter DB......" + sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			//            conn.commit();
			res.next();
			HashMap<String, String> membershipDetailsFromSupporterDB = new HashMap<>();
			//			membershipDetailsFromSupporterDB.put("state",res.getString("state"));
			//			membershipDetailsFromSupporterDB.put("source_system",res.getString("source_system"));
			//			membershipDetailsFromSupporterDB.put("account_reference",res.getString("account_reference"));
			membershipDetailsFromSupporterDB.put("email_status", res.getString("email_status"));
			membershipDetailsFromSupporterDB.put("lead_barcode", res.getString("lead_barcode"));
			membershipDetailsFromSupporterDB.put("additional_barcode", res.getString("additional_barcode"));
			membershipDetailsFromSupporterDB.put("paid_ref", res.getString("paid_ref"));

			return membershipDetailsFromSupporterDB;
		} catch (Exception e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getPersonalDetailsFromAccountDBForResolicitAccount(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from Supporter Account DB......" + sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			conn.setAutoCommit(false);
			//			conn.commit();
			res.next();
			HashMap<String, String> personalDetailsDetailsFromAccountDB = new HashMap<>();
			personalDetailsDetailsFromAccountDB.put("SUPPORTER_NUMBER", res.getString("SUPPORTER_NUMBER"));
			personalDetailsDetailsFromAccountDB.put("ACCOUNT_EMAIL", res.getString("ACCOUNT_EMAIL"));
			personalDetailsDetailsFromAccountDB.put("SOURCE_SYSTEM", res.getString("SOURCE_SYSTEM"));
			personalDetailsDetailsFromAccountDB.put("REGISTRY_EXPORT_TIMESTAMP", res.getString(("REGISTRY_EXPORT_TIMESTAMP")));
			personalDetailsDetailsFromAccountDB.put("STATE", res.getString("STATE"));
			return personalDetailsDetailsFromAccountDB;
		} catch (Exception e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getDonationDetailsFromSupporterDB(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get account details from Supporter DB......" + sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			conn.setAutoCommit(false);
			//            conn.commit();
			res.next();
			HashMap<String, String> donationDetailsFromSupporterDB = new HashMap<>();
			donationDetailsFromSupporterDB.put("AMOUNT_PENCE", res.getString("AMOUNT_PENCE"));
			donationDetailsFromSupporterDB.put("FIRST_NAME", res.getString("FIRST_NAME"));
			donationDetailsFromSupporterDB.put("LAST_NAME", res.getString("LAST_NAME"));
			donationDetailsFromSupporterDB.put("EMAIL_ADDRESS", res.getString(("EMAIL_ADDRESS")));
			donationDetailsFromSupporterDB.put("CONTACT_BY_EMAIL", res.getString("CONTACT_BY_EMAIL"));
			donationDetailsFromSupporterDB.put("CONTACT_BY_POST", res.getString("CONTACT_BY_POST"));
			donationDetailsFromSupporterDB.put("CONTACT_BY_PHONE", res.getString("CONTACT_BY_PHONE"));
			donationDetailsFromSupporterDB.put("GIFT_AID_CONSENT", res.getString("GIFT_AID_CONSENT"));
			donationDetailsFromSupporterDB.put("DONATION_TYPE", res.getString("DONATION_TYPE"));
			donationDetailsFromSupporterDB.put("SUBMISSION_STATUS", res.getString("SUBMISSION_STATUS"));
			donationDetailsFromSupporterDB.put("PAYMENT_STATUS", res.getString("PAYMENT_STATUS"));
			donationDetailsFromSupporterDB.put("AMOUNT_PENCE", res.getString("AMOUNT_PENCE"));

			return donationDetailsFromSupporterDB;
		} catch (Exception e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public List<SupporterRecord> getSupporterData(String sql, String type) {
		Connection conn = null;
		ResultSet res = null;
		List<SupporterRecord> supporters = new ArrayList();
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing the sql script...");
			res = statement.executeQuery(sql);
			//            conn.commit();
			res.next();
			switch (type) {
				case "DONATE_commemorative":
					do {
						supporters.add(SupporterRecord.builder()
								.amount(res.getString("amount_pence"))
								.firstName(res.getString("first_name"))
								.lastName(res.getString("last_name"))
								.emailAddress(res.getString("email_address"))
								.emailPref(res.getString("contact_by_email"))
								.postPref(res.getString("contact_by_post"))
								.phonePref(res.getString("contact_by_phone"))
								.donationType(res.getString("donation_type"))
								.relation(res.getString("relation"))
								.subject(res.getString("subject"))
								.occasion(res.getString("occasion"))
								.tone(res.getString("tone"))
								.fundCode(res.getString("appeal_fund_code"))
								.sourceCode(res.getString("campaign_source_code"))
								.crmTransactionID(res.getString("crm_transaction_id"))
								.build());
					} while (res.next());
					break;
				case "JOIN_PromoCode":
					do {
						supporters.add(SupporterRecord.builder()
								.amount(res.getString("paid_amount"))
								.firstName(res.getString("first_name"))
								.lastName(res.getString("last_name"))
								.emailAddress(res.getString("email_id"))
								.giftAidConsent(res.getString("gift_aid_declared"))
								.promocode(res.getString("promocode"))
								.emailPref(res.getString("CONTACT_BY_EMAIL"))
								.postPref(res.getString("CONTACT_BY_POST"))
								.phonePref(res.getString("CONTACT_BY_PHONE"))
								.build());
					} while (res.next());
					break;
				case "DIY_DONATION":
					do {
						supporters.add(SupporterRecord.builder()
								.emailAddress(res.getString("EMAIL_ADDRESS"))
								.firstName(res.getString("first_name"))
								.lastName(res.getString("last_name"))
								.emailPref(res.getString("contact_by_email"))
								.postPref(res.getString("contact_by_post"))
								.phonePref(res.getString("contact_by_phone"))
								.fundraisingDiscoverySource(res.getString("DISCOVERY_CHANNEL"))
								.fundraisingDiscoverySourceOther(res.getString("DISCOVERY_CHANNEL_OTHER"))
								.fundraisingReason(res.getString("REASON"))
								.fundraisingReasonOther(res.getString("REASON_OTHER"))
								.submissionStatus(res.getString("SUBMISSION_STATUS"))
								.build());
					} while (res.next());
					break;
			}
			return supporters;
		} catch (SQLException e) {
			log.error("Could not execute instant SQL statement", e);
			Assert.fail();
			return null;
		} finally {
			log.info("Closing the connection");
			closeConnectionQuietly(conn);
		}
	}

	public Map<String, String> getDataByLabelSet(String sqlStatement, Set<String> columnLabels) {
		Map<String, String> resultsByColumnLabel = executeSql(sqlStatement, res -> {
			Map<String, String> results = new HashMap<>();
			for (String columnLabel : columnLabels) {
				String columnValue = res.getString(columnLabel);
				results.put(columnLabel, columnValue);
				log.info("The  " + columnLabel + " is " + columnValue);
			}
			return results;
		});
		return resultsByColumnLabel;
	}

}