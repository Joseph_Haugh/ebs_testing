package uk.org.nationaltrust.framework;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Function;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import javax.xml.crypto.dsig.spec.ExcC14NParameterSpec;

/**
 * @author Pramod.Reguri on 26/05/2016.
 */
public class Helpers extends TestBase implements GenericConfig{
	private static final DateTimeFormatter VALID_UNTIL_DATE_FORMATTER = new DateTimeFormatterBuilder().parseCaseInsensitive()
			.appendPattern("dd-MMM-yyyy")
			.toFormatter();

	public static void click(WebDriver driver, By locator) {
		find(driver, locator).click();
	}

	public static String getText(WebDriver driver, By locator) {
		return find(driver, locator).getText();

	}

	public static WebElement find(WebDriver driver, By locator) {
		return driver.findElement(locator);
	}

	public boolean isElementPresent(WebDriver driver, Object locator) {
		return getElement(driver, locator) != null;
	}

	public static Boolean isDisplayed(WebDriver driver, By locator) {
		if (driver.findElements(locator).size() != 0) {
			return find(driver, locator).isDisplayed();
		} else {
			return false;
		}

	}

	public static WebElement getElement(WebDriver driver, Object locator, Object... opParams) {
		By by = locator instanceof By ? (By) locator : By.xpath(locator.toString());
		WebDriver wDriver = (WebDriver) (opParams.length > 0 ? opParams[0] : driver);
		WebElement elem = null;
		try {
			elem = wDriver.findElement(by);
		} catch (NoSuchElementException e) {

		}
		return elem;
	}

	public static void pressKeyboardKey(WebDriver driver, String key) {
		Actions action = new Actions(driver);
		action.sendKeys(key).build().perform();

	}

	public static Boolean waitForIsDisplayed(WebDriver driver, By locator, Integer... timeout) {
		try {
			waitFor(driver, ExpectedConditions.visibilityOfElementLocated(locator),
					(timeout.length > 0 ? timeout[0] : null));
		} catch (org.openqa.selenium.TimeoutException exception) {
			//System.out.println("Could not find the expected element");
			log.error("Could not find the expected element");
			return false;
		}
		return true;
	}

	private static void waitFor(WebDriver driver, ExpectedCondition<WebElement> condition, Integer timeout) {
		timeout = timeout != null ? timeout : 5;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(condition);
	}

	public static void scrollToObject(WebDriver driver, By locator) {
		WebElement element = driver.findElement(locator);
		int elementPosition = element.getLocation().getY();

		String js = String.format("window.scroll(0, %s)", elementPosition);

		((JavascriptExecutor) driver).executeScript(js);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	public static void scrollToBottomOfPage(WebDriver driver) {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0,document.body.scrollHeight);");
	}

	public static void scrollToTopOfPage(WebDriver driver){
		((JavascriptExecutor) driver).executeScript("scroll(0,-250);");
	}

	public static boolean waitForElementToAppear(WebDriver driver, By selector, int timeOutInSeconds, String timeOutMessage) {
		try {

			waitFor(driver, ExpectedConditions.visibilityOfElementLocated(selector), timeOutInSeconds);
			return true;

		} catch (TimeoutException e) {

			throw new IllegalArgumentException(timeOutMessage);
		}

	}

	public static Boolean waitForIsClickable(WebDriver driver, By locator, Integer... timeout) {
		try {
			waitFor(driver, ExpectedConditions.elementToBeClickable(locator),
					(timeout.length > 0 ? timeout[0] : null));
		} catch (org.openqa.selenium.TimeoutException exception) {
			return false;
		}
		return true;
	}

	public static void clearAndSetText(WebDriver driver, By locator, String text) {
		waitForIsDisplayed(driver, locator, 10);
		driver.findElement(locator).clear();
		driver.findElement(locator).sendKeys(text);
	}
	public static void clearAndSetTextMYNT(WebDriver driver, By locator, String text) {
		driver.findElement(locator).clear();
		driver.findElement(locator).sendKeys(text);
	}

	public static String getTheCurrentDateAndTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String todayDate = df.format(date).toString();
		String currentTime = sdf.format(cal.getTime()).toString();
		String dateAndCurrenttime = todayDate + "T" + currentTime;
		return dateAndCurrenttime;
	}

	public static String getTheCurrentDateAndTimeX1(int month) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date dt = DateUtils.addMonths(new Date(), month);
		String futureDate = df.format(dt).toString();
		String dateAndCurrenttime = futureDate ;// currentTime
		return dateAndCurrenttime;
	}


	public static LocalDate getTheCurrentDate() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String todayDate = df.format(date).toString();
		String todaysDate = todayDate;
		return LocalDate.parse(todaysDate);
	}

	public static LocalDate getTheCurrentDateX1() {
		DateFormat df = new SimpleDateFormat("dd/MM/yyy");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String todayDate = df.format(date).toString();
		String todaysDate = todayDate;
		return LocalDate.parse(todaysDate);
	}

	public static String getTheCurrentDay() {
		DateFormat df = new SimpleDateFormat("dd");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String todayDate = df.format(date).toString();
		String currentTime = sdf.format(cal.getTime()).toString();
		String dateAndCurrenttime = todayDate;
		return dateAndCurrenttime;
	}

	public static String getTheCurrentMonth() {
		DateFormat df = new SimpleDateFormat("MM");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String todayDate = df.format(date).toString();
		String currentTime = sdf.format(cal.getTime()).toString();
		String dateAndCurrenttime = todayDate;
		return dateAndCurrenttime;
	}
	public static String getTheCurrentYear() {
		DateFormat df = new SimpleDateFormat("yyyy");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String todayDate = df.format(date).toString();
		String currentTime = sdf.format(cal.getTime()).toString();
		String dateAndCurrenttime = todayDate;
		return dateAndCurrenttime;
	}

	public static String getTheCurrentDateForTokenTable()
	{
		DateFormat df = new SimpleDateFormat("dd-MMM-yy");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH.mm.");
		Date date = new Date();
		String todayDate = df.format(date).toString();
		String currentTime = sdf.format(cal.getTime()).toString();
		String dateAndCurrenttime = todayDate + " " + currentTime;
		return dateAndCurrenttime.toUpperCase();
	}

	public static String getFutureDateTime(int month) {
		// TODO Java8 time is your friend
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = DateUtils.addMonths(new Date(), month);
		String futureDate = df.format(dt).toString();
		String dateAndCurrenttime = futureDate + "T11:59";// currentTime
		return dateAndCurrenttime;

	}

	public static String getRenewFutureDateTime(int month) {
		// TODO Java8 time is your friend
		DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		Date dt = DateUtils.addMonths(new Date(), month);
		String futureDate = df.format(dt).toString();
		String dateAndCurrenttime = futureDate ;// currentTime
		return dateAndCurrenttime;
	}

	public static String getFutureDate(int month) {
		// TODO Java8 time is your friend
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = DateUtils.addMonths(new Date(), month);
		String futureDate = df.format(dt).toString();
		String dateAndCurrenttime = futureDate;
		return dateAndCurrenttime;

	}

	public static String getFutureDateMM(int month) {
		// TODO Java8 time is your friend
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = DateUtils.addMonths(new Date(), month);
		String futureDate = df.format(dt).toString();
		String dateAndCurrenttime = futureDate;
		return dateAndCurrenttime;

	}

	public static boolean waitForElementToDisappear(WebDriver driver, By selector, long timeOutInSeconds, String timeOutMessage) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(selector));
			return true;

		} catch (TimeoutException e) {
			throw new IllegalArgumentException(timeOutMessage);
		}

	}

	public static String generateRandomChracters(int length) {
		String randomString = RandomStringUtils.randomAlphanumeric(length);
		return randomString;
	}

    public static void waitForPageLoad(WebDriver driver) {

        Wait<WebDriver> wait = new WebDriverWait(driver, 90);
        wait.until(new Function<WebDriver, Object>() {
            public Boolean apply(WebDriver driver) {
                System.out.println("Current Window State       : "
                        + String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")));
                return String
                        .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
                        .equals("complete");
            }
        });
    }

    public static boolean waitForJSandJQueryToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 1500);
        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Boolean) ((JavascriptExecutor) driver).executeScript("return (window.jQuery != null) && (window.jQuery.active === 0);"));
                } catch (Exception e) {
                    // no jQuery present
                    return true;
                }
            }
        };
        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {

            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };
        try {
            log.debug("Performing AJAX and JQuery wait...");
            Thread.sleep(1500);
        } catch (InterruptedException e) {
        }
        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }

    public static void shortWait() throws Exception{
	    log.debug("short sleep for page to redirect.......");
	    Thread.sleep(20000);;
    }
	public static void shortWait2s() throws Exception{
		log.debug("short sleep for page to redirect.......");
		Thread.sleep(2000);;
	}

	public static void waitHandlingException(int timeInMilliSeconds){
		try {
			Thread.sleep(timeInMilliSeconds);
		}
		catch(Exception e){
			log.debug("short sleep for page to handle exception");
		}
	}

	public static String getApplicationId(WebDriver driver){
		String currentUrl = driver.getCurrentUrl();
		log.debug("Application Id:- "+StringUtils.substringBetween(currentUrl, "=", "&"));
		return StringUtils.substringBetween(currentUrl, "=", "&");
	}

	public static String getDonateApplicationId (WebDriver driver){
		     String currentUrl = driver.getCurrentUrl();
		     log.debug("Application Id:- "+StringUtils.substringBetween( currentUrl, "=", "&"));
	     return StringUtils.substringAfter(currentUrl, "=");
	}

	public static String getCurrentURL(WebDriver driver){
		String currentUrl = driver.getCurrentUrl();
		log.debug("Current URL:- "+StringUtils.substringBetween(currentUrl, "", "join?"));
		return StringUtils.substringBetween(currentUrl, "", "join?");
	}

	public static Date getTime(long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}

	public static String getTimeNow() {
		LocalTime today = LocalTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
		return formatter.format(today);
	}

	public static LocalDate getDateNow() {
		LocalDate today = LocalDate.now();

		return today;
	}

	public static String getDateTimeNow(String pattern){
		LocalDate today = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return formatter.format(today);
	}

	public static String getTheCurrentDateJustNumeric() {
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String todayDate = df.format(date).toString();
		String currentTime = sdf.format(cal.getTime()).toString();
		String dateAndCurrenttime = todayDate + currentTime;;
		return dateAndCurrenttime;
	}

	public static int getTheDayXDaysFromToday(String days) {
		String sdf = LocalDate.now().plusDays(Long.parseLong(days)).format(DateTimeFormatter.ofPattern("dd"));
		String currentDate = sdf.toString().trim();
		String dateXDays = currentDate;
		return Integer.parseInt(dateXDays);
	}

	public static String getTheDateXDaysFromToday(String days) {
		String sdf = LocalDate.now().plusDays(Long.parseLong(days)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String currentDate = sdf.toString().trim();
		String dateXDays = currentDate;
		return dateXDays;
	}

	public static String getEarlierDateTime( String lessMinutes){

		DateFormat df = new SimpleDateFormat ("dd-MMM-yy HH.mm.ss.S");
		Calendar cal = Calendar.getInstance();
		cal.add (Calendar.MINUTE, Integer.parseInt(lessMinutes));
		System.out.println(df.format(cal.getTime()));
		return df.format (cal.getTime());
	}

	public static String getEarlierDateTimeDays( String lessDays){

		DateFormat df = new SimpleDateFormat ("dd-MMM-yy HH.mm.ss.S");
		Calendar cal = Calendar.getInstance();
		cal.add (Calendar.DATE, Integer.parseInt(lessDays));
		System.out.println(df.format(cal.getTime()));
		return df.format (cal.getTime());
	}

	public static String getCurrentEnvironment(WebDriver driver){
		String currentUrl = driver.getCurrentUrl();
		log.debug("Environment:- "+StringUtils.substringBetween(currentUrl, "secure.", ".nationaltrust"));
		return StringUtils.substringBetween(currentUrl, "secure.", ".nationaltrust");
	}

	public static String renewalStartDateConverter(String membershipStartDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss.S");
		DateTimeFormatter renderer = DateTimeFormatter.ofPattern("MMMM yyyy");
		// business end date is the last day of the month which contains the anniversary of the start date. Roundabout derivation to avoid problems with leap years. MembershipEnd Date of the last day of a month should show a renewal month of the following month
		return LocalDate.parse(membershipStartDate,formatter).withDayOfMonth(1).plusYears(1).plusMonths(1).minusDays(1).format(renderer);
	}

	public static void waitForSpecifiedTime(int time) throws Exception {
		log.debug("Wait till for specified time");
		Thread.sleep(time);
	}

	public static String giftAidConverter(String answer) {
		if (answer != "YES") {
			return "NOT_ANSWERED";
		}
		else return "YES";
	}

	public static String giftAidConverterCRM(String answer) {
		if (answer != null) {
			return "YES";
		}
		else return "NOT_ANSWERED";
	}
	public static String getFutureDateMMM(int month) {
		// TODO Java8 time is your friend
		DateFormat df = new SimpleDateFormat("yyyy-MMM-dd");
		Date dt = DateUtils.addMonths(new Date(), month);
		String futureDate = df.format(dt).toString();
		String dateAndCurrenttime = futureDate;
		return dateAndCurrenttime;

	}

	/**
	    * Calculate membership end date: (Application Commenced Date + 1 year) - 1 day . e.g. If today is 12th October 2018, the end date will be 11th October 2019
	    * Calculate valid until date: (Membership end date + 1 day) and then get the last day of that month.
	    *
	    * @param membershipStartDate the start date of the membership
	    * @return the valid until date for the barcode
	 * @param membershipStartDate*/
	public static String calculateBarcodeValidUntilDate(LocalDate membershipStartDate) {
LocalDate membershipEndDate = calculateMembershipEndDate(membershipStartDate);
		LocalDate localDate = calculateValidUntilDate(membershipEndDate);

		return localDate.format(VALID_UNTIL_DATE_FORMATTER).toUpperCase();
}

public static LocalDate calculateMembershipEndDate(LocalDate membershipStartDate) {
return membershipStartDate.plusYears(1).minusDays(1);
}

private static LocalDate calculateValidUntilDate(LocalDate membershipEndDate) {
	return membershipEndDate.plusDays(1).with(lastDayOfMonth());
}

public static void clearMYNTCookieMessage(){
	driver.findElements(By.cssSelector("#cookie-statement-cls-btn > span"))
			.stream()
			.filter(WebElement::isDisplayed)
			.forEach(WebElement::click);
}

	public static void clearWebsiteCookieMessage(){
		driver.findElements(By.cssSelector("body > div.nt-footer-cookie-statement.nt-palette-neutral > div > div > p > a.cls-btn > span"))
				.stream()
				.filter(WebElement::isDisplayed)
				.forEach(WebElement::click);
	}


	public static void waitForVisibility(WebElement element){
		new WebDriverWait(driver, 60)
				.until(ExpectedConditions.visibilityOf(element));
	}


	public static void selectdropdownByVisibleText(WebElement element, String dropDownValue){
		Helpers.waitForVisibility(element);
		Select dropDown= new Select(element);
		dropDown.selectByVisibleText(dropDownValue);

	}

}

//LocalDate.parse(membershipEndDate,formatter).withDayOfMonth(1).plusYears(1).plusMonths(1).minusDays(1).format(renderer);
