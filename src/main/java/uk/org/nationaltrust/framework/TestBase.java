package uk.org.nationaltrust.framework;

import static uk.org.nationaltrust.webdriver.WebDriverConfigBean.aWebDriverConfig;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.browserstack.local.Local;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.SessionId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.common.SauceOnDemandSessionIdProvider;
import com.saucelabs.saucerest.SauceREST;
import com.saucelabs.testng.SauceOnDemandAuthenticationProvider;
//import com.sun.org.apache.regexp.internal.RE;
import uk.org.nationaltrust.apis.ApiRequests;
import uk.org.nationaltrust.reportfactory.ReportFactoy;
import uk.org.nationaltrust.webdriver.SessionIdAccessor;
import uk.org.nationaltrust.webdriver.WebDriverConfigBean;
import uk.org.nationaltrust.webdriver.WebDriverManager;
import uk.org.nationaltrust.reporting.ExtentTestManager;
import uk.org.nationaltrust.reporting.ExtentReportManager;
/**
 * @author Pramod tor on 26/05/2016. */


	public class TestBase implements GenericConfig, SauceOnDemandSessionIdProvider, SauceOnDemandAuthenticationProvider {

		public static ExtentReports extent;
	    protected static WebDriver driver = null;
		protected static int DEFAULT_TIMEOUT = 30000;

		public Local local = null;

		public WebDriverConfigBean webDriverConfig = null;
		public WebDriver secondBrowserInstance = null;

		protected Logger APPLICATION_LOGS = LoggerFactory.getLogger(getClass());

		private SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication();

		protected static Logger log = LoggerFactory.getLogger(TestBase.class);
		public static String testName = "";
		public static String suiteName = "";

		public static String bsLocalIdentifier = EnvironmentConfiguration.getBrowserstackUser()+Helpers.getTheCurrentDateAndTime();
		ApiRequests callApi = new ApiRequests();

		//SupporterServiceDBConnection JoinSupporterDBConnection = new SupporterServiceDBConnection();

		//private ExtentReports reporter = ReportFactoy

		/**
		 * {@inheritDoc}
		 */
		@Override
		public final String getSessionId() {
			if (driver == null) {
				throw new IllegalStateException("WebDriver initialisation failure.");
			}
			if (!(driver instanceof SessionIdAccessor)) {
				throw new IllegalStateException("WebDriver does not implement SessionIdAccessor");
			}
			SessionId sessionId = ((SessionIdAccessor) driver).getSessionId();
			if (sessionId == null) {
				throw new IllegalStateException("WebDriver has no active session");
			} else {
				return sessionId.toString();
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public final SauceOnDemandAuthentication getAuthentication() {
			return authentication;
		}

		public void updateTestStatusInSauceLab(int result, String method, String session) {
			String jobId = getSessionId();
			SauceREST client = new SauceREST(sauceUser, sauceKey);
			Map<String, Object> sauceJob = new HashMap<>();
			// sauceJob.put("name", "Test method: " + method);

			if (result == ITestResult.SUCCESS) {
				client.jobPassed(jobId);
			} else if (result == ITestResult.FAILURE) {
				client.jobFailed(jobId);
			} else if (result == ITestResult.SKIP) {
				client.jobFailed(jobId);
			}

			client.updateJobInfo(jobId, sauceJob);

		}

	@BeforeSuite(alwaysRun = true)
	public void extentSetup(ITestContext context) throws IOException {
		ExtentReportManager.setOutputDirectory(context);
		extent = ExtentReportManager.getInstance();
		suiteName = context.getCurrentXmlTest().getSuite().getName(); // We can append time info if required? --> (+"_"+Helpers.getDateTimeNow("dd-MM-yyyy").replace("-","_");)
	}

	@BeforeSuite(alwaysRun = true)
	@Parameters({"mode","deviceProfile"})
	public void startBrowserStackLocalService(@Optional("optionalMode") String mode, String deviceProfile) {
		if(mode.equalsIgnoreCase("BROWSERSTACK")){
			setLogs("Starting browserstack local service.....");
			local = new Local();
			Map<String, String> options = new HashMap<String, String>();
			options.put("key", EnvironmentConfiguration.getBrowserstackAccessKey());
            options.put("--localIdentifier",bsLocalIdentifier);
			try {
				local.start(options);
			} catch (Exception e) {
				throw new IllegalStateException("error while starting browserstack local service:- " + e.toString());
			}
			setLogs("BrowserStack Local EXE launched.....");
		}
	}

		@BeforeClass (alwaysRun = true)
		@Parameters({ "browser", "env", "mode", "deviceProfile" })
		public void initializeTests(String browser, String env, String mode, String deviceProfile) {
			EnvironmentConfiguration.displayMessage();
			APPLICATION_LOGS.debug("Env:- "+  env);
			EnvironmentConfiguration.populate(env);
			APPLICATION_LOGS.debug("loaded property files....");
			webDriverConfig = aWebDriverConfig()
					.withBrowser(browser)
					.withDeploymentEnvironment(env)
					.withSeleniumMode(mode)
					.withSeleniumDeviceProfile(deviceProfile);
			driver = WebDriverManager.openBrowser(webDriverConfig, getClass());
			APPLICATION_LOGS.debug("running tests on:- "+mode+" browser = "+browser);
			APPLICATION_LOGS.debug("Will use baseURL " + EnvironmentConfiguration.getBaseURL());
			setLogs("Starting the Test in the class  -- " + getClass().getSimpleName());
			APPLICATION_LOGS.debug("*******************************************************************************************");

//			String sqlSta = "Delete FROM" + " SUPPORTERAPI_OWNER.SUPPORTER_INTERACTION where" + " Created >= sysdate -5" ;
//			setLogs(sqlSta);
//			JoinSupporterDBConnection.setData(sqlSta);
			callApi.setSystemDate();
						callApi.setCurrentSupporterAPIDate();
//			callApi.setGDPROn();
//			navigate(EnvironmentConfiguration.getBaseURL());
		}

	 @AfterClass(alwaysRun = true)
	 public void tearDown() {
		try{
			setLogs("quitting driver after class:- " + getClass().getSimpleName());
		driver.quit();
			}catch (Exception e){
		setLogs("Error while quitting driver:- "+e.toString());
		}
		if (secondBrowserInstance != null) {
			secondBrowserInstance.quit();
		}
		}

		@BeforeMethod (alwaysRun = true)
		@Parameters("browser")
		public final void testCaseName(Method method, String browser) {
			testName = method.getName() + "_" + browser;
			APPLICATION_LOGS.debug("\t**** " + testName + " ****\t");
			ExtentTestManager.startTest(testName);
		}

		@AfterMethod(alwaysRun = true)
		@Parameters("mode")
		public final void results(ITestResult testResult, Method method, String mode) throws IOException {
			int result = testResult.getStatus();
			ExtentTestManager.getTest().getTest().setStartedTime(Helpers.getTime(testResult.getStartMillis()));
			ExtentTestManager.getTest().getTest().setEndedTime(Helpers.getTime(testResult.getEndMillis()));
			switch (result) {
				case ITestResult.FAILURE:
					APPLICATION_LOGS.debug("Test Failed due to assertion failure -- Please see the screenshot");
					String screenShotname = method.getName();
					//takeScreenShot(screenShotname);
					//Helpers.getScreenshot(screenShotname);

					//String image = ExtentTestManager.getTest().addScreenCapture("screenshots/" + screenShotname + ".png");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "FAILED");
					ExtentTestManager.getTest().log(LogStatus.FAIL, testResult.getThrowable());
					//ExtentTestManager.getTest().log(LogStatus.INFO, image);
					break;
				case ITestResult.SUCCESS:
					APPLICATION_LOGS.debug("(Pass)");
					ExtentTestManager.getTest().log(LogStatus.PASS, "PASSED");
					break;
				case ITestResult.SKIP:
					APPLICATION_LOGS.debug("(Skipped)");
					ExtentTestManager.getTest().log(LogStatus.SKIP, "SKIPPED");
					break;
				default:
					APPLICATION_LOGS.error("Unexpected test result status code: " + result);
			}
			APPLICATION_LOGS.debug("*******************************************************************************************");
			ExtentTestManager.endTest();
			extent.flush();
		}

		public void navigate(String url) {
			driver.get(url);
		}

		public void takeScreenShot(String methodName) {
			File screenshotFile = new File(LOGS_PATH + methodName + ".png");
			byte[] screenshotData = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			try {
				FileUtils.writeByteArrayToFile(screenshotFile, screenshotData);
			} catch (IOException e) {
				APPLICATION_LOGS.error("Could not write screenshot to " + screenshotFile, e);
			}
		}

		public void setLogs(String message) {
			APPLICATION_LOGS.debug(message);
			ExtentTest testReporter = ReportFactoy.getTest();
			if(testReporter != null){
			testReporter.log(LogStatus.PASS, message);
			}

		}

	@AfterSuite(alwaysRun = true)
	public void closeExtentReport() {
		extent.flush();
		extent.close();
	}

	@AfterSuite(alwaysRun = true)
	@Parameters({"mode"})
	public void StopBrowserStackLocalService(@Optional("optionalMode")String mode){
		if(mode.equalsIgnoreCase("BROWSERSTACK")){
			try {
				if (local != null) {
					setLogs("Stopping browserstack local service.....");
					local.stop();
				}
			}catch (Exception e){
				setLogs("Exception while stopping local service..."+e.toString());
			}
		}
	}


//	@AfterMethod
//	public void clearDown(){
//		String sqlSta = "Delete FROM" + " SUPPORTERAPI_OWNER.SUPPORTER_INTERACTION where" + " Created >= sysdate -5" ;
//		setLogs(sqlSta);
//		JoinSupporterDBConnection.setData(sqlSta);
//
//	}
}
