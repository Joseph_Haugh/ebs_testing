package uk.org.nationaltrust.framework;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author Pramod on 26/05/2016.
 */
public class EnvironmentConfiguration implements GenericConfig {

	private static Properties MY_CONFIG = null;

	private static Properties MESSAGE_CONFIG = null;

	public static void populate(String environmentName) {
		Logger log = LoggerFactory.getLogger(EnvironmentConfiguration.class);
		log.info("Loading environment properties for profile " + environmentName);
		String propsResourceName = "/" + environmentName + "_config.properties";

		try (Reader input = new InputStreamReader(EnvironmentConfiguration.class.getResourceAsStream(propsResourceName), UTF_8)) {
			MY_CONFIG = new Properties();
			MY_CONFIG.load(input);
		} catch (IOException e) {
			log.error("Could not load environment properties - this is going to break...", e);
		}
	}

	public static void displayMessage() {
		Logger log = LoggerFactory.getLogger(EnvironmentConfiguration.class);
		log.info("Loading message properties for displayed messages ");
		String messageResourceName = "/messages.properties";
		try (Reader messageinput = new InputStreamReader(EnvironmentConfiguration.class.getResourceAsStream(messageResourceName), UTF_8)) {
			MESSAGE_CONFIG = new Properties();
			MESSAGE_CONFIG.load(messageinput);
		} catch (IOException e) {
			log.error("Could not load environment properties - this is going to break...", e);

		}
	}

	public static String getHomePageUrl() { return MY_CONFIG.getProperty("homePageUrl"); }

	public static String getWebsiteBaseURL(){return  MY_CONFIG.getProperty("websiteBaseURL");}

	public static String getBaseURL() { return MY_CONFIG.getProperty("baseURL"); }

	public static String getDonateBaseURL() { return MY_CONFIG.getProperty("donateBaseURL"); }

	public static String getSpecificAppealLink() { return MY_CONFIG.getProperty("specificAppealLink"); }

	public static String getRenewBaseURL() { return MY_CONFIG.getProperty("renewBaseURL");
	}

	public static String getBaseURLMYNT() {
		return MY_CONFIG.getProperty("myNTBaseURL");
	}


	public static String getPURL() {
		return MY_CONFIG.getProperty("PURL");
	}

	public static String getErrorPagesBaseURLJoin(){return MY_CONFIG.getProperty("errorPageURLJoin");}
	public static String getErrorPagesBaseURLMYNT(){return MY_CONFIG.getProperty("errorPageURLMYNT");}
	public static String getErrorPagesBaseURLDonate(){return MY_CONFIG.getProperty("errorPageURLJoin");}
	public static String getErrorPagesBaseURLRenew(){return MY_CONFIG.getProperty("errorPageURLJoin");}

	public static String getMaintenancePageJoinURL(){return MY_CONFIG.getProperty("maintenancePageJoin");}
	public static String getMaintenancePageMyNtURL(){return MY_CONFIG.getProperty("maintenancePageMyNt");}
	public static String getMaintenancePageRenewURL(){return MY_CONFIG.getProperty("maintenancePageRenew");}
	public static String getMaintenancePageDonateURL(){return MY_CONFIG.getProperty("maintenancePageDonate");}

	public static String getSauceKey() {
		return sauceKey;
	}

	public static String getSauceUser() {
		return sauceUser;
	}

    public static String getBrowserstackUser(){
        return "sowjanyaannepu1";
    }

	public static String getBrowserstackAccessKey(){
		return "AA41Xx7yu432kbe8L4DU";
	}
	public static String getInternetExplorerDriverPath() {
		return IE_DRIVER_PATH;
	}

	public static String getChromeDriverPath() {
		return CHROME_DRIVE_RPATH;
	}

	public static String getDBUserName() {
		return MY_CONFIG.getProperty("DBUserName");
	}

	public static String getDBPassword() {
		return MY_CONFIG.getProperty("DBPassword");
	}

	public static String getConnectionString() {
		return MY_CONFIG.getProperty("connectionString");
	}

	public static String getPasswordResetUrl() {
		return MY_CONFIG.getProperty("passwordResetUrl");
	}

	public static String toFilePath(String... pathElements) {
		StringBuilder path = new StringBuilder();
		for (String elem : pathElements) {
			path.append(elem);
		}
		return path.toString().replace('/', File.separatorChar);
	}
	public static String getText(String parameter) {
		return MY_CONFIG.getProperty(parameter);
	}

	public static String getSquirellURL(){
		return MY_CONFIG.getProperty("squirrelURL");
	}


	public static String getActivationUrl() {
		return MY_CONFIG.getProperty("activationUrl");
	}

	public static String getMessageText(String messageName) {
		return MESSAGE_CONFIG.getProperty(messageName);
	}

	public static String getShopCreateAccountURL(){
		return MY_CONFIG.getProperty("ShopCreateAccountURL");
	}

	public static String getCreateCallbackURL(){
		return MY_CONFIG.getProperty("wsdlCreateCRMCallback");
	}
	public static String getETLTriggerURL(String etlTriggerURL) {
		return MY_CONFIG.getProperty(etlTriggerURL);
	}
	public static String getETLInfoURL() {

		return MY_CONFIG.getProperty("ETLInfoURL");
	}
	public static String getCSVFileURL(){

		return MY_CONFIG.getProperty("CSVFileURL");
	}
	public static String getCRMPSTDBUserName() {
		return MY_CONFIG.getProperty("CRMDBDBUserName");
	}

	public static String getCRMPSTDBPassword() {
		return MY_CONFIG.getProperty("CRMDBPassword");
	}
	public static String getCRMPSTDBConnectionString() {
		return MY_CONFIG.getProperty("CRMDBConnectionString");
	}
	public static String getMarketingURL() {
		return MY_CONFIG.getProperty("marketingURL");
	}
	public static String getRestServiceUrl() {
		return MY_CONFIG.getProperty("restApiToUnsetSupporterNumber");
	}

	public static String getCPCURL() {
		return MY_CONFIG.getProperty("CPCURL");
	}

	public static String getCPCDMURL() {
		return MY_CONFIG.getProperty("CPCDMURL");
	}
	public static String getCPCShopURL() {
		return MY_CONFIG.getProperty("CPCShopURL");
	}
	public static String getCPCErrorPagesBaseURL() {
		return MY_CONFIG.getProperty("cpcErrorPageURL");

	}

	public static String getCPCMaintenancePageURL() {
		return MY_CONFIG.getProperty("cpcMaintenancePage");

	}
	public static String getSOADBPassword() {
		return MY_CONFIG.getProperty("SOADBPassword");
	}
	public static String getSOADBConnectionString() {
		return MY_CONFIG.getProperty("SOADBConnectionString");
	}

	public static String getSOADBUserName() {
		return MY_CONFIG.getProperty("SOADBUserName");
	}
	public static String getCPCBadTokenURL() {
		return MY_CONFIG.getProperty("CPCBadToken");
	}


}

