package uk.org.nationaltrust.framework;

import org.testng.Assert;

import java.sql.*;
import java.util.*;

/**
 * Created by nick.thompson on 04/04/2017.
 */
public class CRMDBConnection extends TestBase {
	private String getCRMPSTDBUserName(){
		return EnvironmentConfiguration.getCRMPSTDBUserName();

	}
	//	public class CRMDBConnection extends TestBase {
	//		private String getCRMS1TDBUserName(){
	//			return EnvironmentConfiguration.getCRMS1TDBUserName();
	private String getCRMPSTDBPassword() {
		return EnvironmentConfiguration.getCRMPSTDBPassword();
	}
	//	private String getCRMS1TDBPassword() {
	//		return EnvironmentConfiguration.getCRMS1TDBPassword();
	//	}

	private String getCRMPSTDBConnectionString() {
		return EnvironmentConfiguration.getCRMPSTDBConnectionString();
	}
	//	private String getCRMS1TDBConnectionString() {
	//		return EnvironmentConfiguration.getCRMS1TDBConnectionString();
	//	}

	private Connection dbConnection() {
		try {
			// Ensure driver is loaded
			Class.forName("oracle.jdbc.driver.OracleDriver");
			// DriverManager.registerDriver(new OracleDriver());
			String connectionString = getCRMPSTDBConnectionString();
			log.debug("Connecting to database with connection string " + connectionString);
			// TODO Should use DataSource rather than DriverManager
			Connection conn = DriverManager.getConnection(connectionString, getCRMPSTDBUserName(), getCRMPSTDBPassword());
			log.debug("Connected.");
			return conn;
		} catch (SQLException e) {
			log.error("Could not connect to database", e);
		} catch (ClassNotFoundException e) {
			log.error("Oracle JDBC driver not accessible - check your classpath", e);
		}
		return null;
	}
	private void closeConnectionQuietly(Connection conn) {
		if (conn == null) {
			return;
		}
		try {
			conn.close();
		} catch (SQLException e) {
			log.error("Failed to close DB connection - weirdness! Things may well go downhill from here...", e);
		}
	}

	public void setData(String sqlStatement) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			statement.executeQuery(sqlStatement);
			conn.commit();
		} catch (SQLException e) {
			log.error("Could not execute instant SQL statement", e);
		} finally {
			closeConnectionQuietly(conn);
		}
	}

	//	public String getData(String sqlStatement, String columnLabel) {
	//		String result = executeSql(sqlStatement, res -> res.getString(columnLabel));
	//		log.info("The  " + columnLabel + " is " + result);
	//		return result;
	//	}

	public Map<String, String> getData(String sqlStatement, Set<String> columnLabels) {
		Map<String, String> resultsByColumnLabel = executeSql(sqlStatement, res -> {
			Map<String, String> results = new HashMap<>();
			for (String columnLabel : columnLabels) {
				String columnValue = res.getString(columnLabel);
				results.put(columnLabel, columnValue);
				log.info("The  " + columnLabel + " is " + columnValue);
			}
			return results;
		});
		return resultsByColumnLabel;
	}

	private <T> T executeSql(String sql, Mapper<ResultSet, T> resultSetConsumer) {
		Connection conn = null;
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing the sql script...");
			ResultSet res = statement.executeQuery(sql);
			conn.setAutoCommit(false);
			conn.commit();
			res.next();
			return resultSetConsumer.apply(res);
		} catch (SQLException e) {
			log.error("Could not execute instant SQL statement", e);
			Assert.fail();
			return null;
		} finally {
			log.info("Closing the connection");
			closeConnectionQuietly(conn);
		}
	}

	@FunctionalInterface
	private static interface Mapper<S, T> {

		T apply(S s) throws SQLException;
	}

	private Connection CRMDBConnection() {
		try {
			// Ensure driver is loaded
			Class.forName("oracle.jdbc.driver.OracleDriver");
			// DriverManager.registerDriver(new OracleDriver());
			String connectionString = getCRMPSTDBConnectionString();
			APPLICATION_LOGS.debug("Connecting to database with connection string " + connectionString);
			// TODO Should use DataSource rather than DriverManager
			Connection conn = DriverManager.getConnection(connectionString, getCRMPSTDBUserName(), getCRMPSTDBPassword());
			APPLICATION_LOGS.debug("Connected.");
			return conn;
		} catch (SQLException e) {
			APPLICATION_LOGS.error("Could not connect to database", e);
		} catch (ClassNotFoundException e) {
			APPLICATION_LOGS.error("Oracle JDBC driver not accessible - check your classpath", e);
		}
		return null;
	}


	public String checkCRMlogTableForNewMembership(String sqlStatement){
		Connection conn = null;
		try{
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing sql to get the data from CRM log table.........");
			ResultSet res = statement.executeQuery(sqlStatement);
//			conn.commit();
			if(!res.next()){return null;}
			String status = res.getString("status");
			return status;
		}catch (Exception e){
			log.error("Could not execute instant sql statment", e);
			return "fail";
		} finally {
			log.info("closing db connection.....");
			closeConnectionQuietly(conn);
		}
	}

	public HashMap<String, String> getContactPreferenceDetailsFromCrmDB(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
//			conn.commit();
			res.next();
			HashMap<String, String> preferenceDetailsFromCrm = new HashMap<>();
			preferenceDetailsFromCrm.put("'YEM'",res.getString("'YEM'"));
			preferenceDetailsFromCrm.put("'YTE'",res.getString("'YTE'"));
			preferenceDetailsFromCrm.put("'YPO'",res.getString("'YPO'"));
			preferenceDetailsFromCrm.put("'SS'",res.getString("'SS'"));

			return preferenceDetailsFromCrm;
		}catch (Exception e){
			log.error("Could not execute instant SQL statement", e);
		}finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}
	public HashMap<String, String> getSupporterAndEmailDetailsFromCrmDB(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
//			conn.commit();
			res.next();
			HashMap<String, String> supporterAndEmailDetailsFromCrm = new HashMap<>();
			supporterAndEmailDetailsFromCrm.put("EMAIL_ADDRESS",res.getString("EMAIL_ADDRESS"));
			supporterAndEmailDetailsFromCrm.put("PARTY_NUMBER",res.getString("PARTY_NUMBER"));


			return supporterAndEmailDetailsFromCrm;
		}catch (Exception e){
			log.error("Could not execute instant SQL statement", e);
		}finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getSupporterNumberFromCrmDBLogTable(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
//			conn.commit();
			res.next();
			HashMap<String, String> supporterNumberDetailsFromCrmLog = new HashMap<>();
			supporterNumberDetailsFromCrmLog.put("SUPPORTER_NO",res.getString("SUPPORTER_NO"));



			return supporterNumberDetailsFromCrmLog;
		}catch (Exception e){
			log.error("Could not execute instant SQL statement", e);
		}finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getDDDetailsFromCRMDB(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get DD details from CRM DB......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			conn.commit();
			res.next();
			HashMap<String, String> DDDetailsFromCRM = new HashMap<>();
			DDDetailsFromCRM.put("BANK_ACCOUNT_NAME",res.getString("BANK_ACCOUNT_NAME"));
			DDDetailsFromCRM.put("BANK_ACCOUNT_NUM",res.getString("BANK_ACCOUNT_NUM"));
			DDDetailsFromCRM.put("BANK_OR_BRANCH_NUMBER",res.getString("BANK_OR_BRANCH_NUMBER"));


			return DDDetailsFromCRM;
		}catch (Exception e){
			log.error("Could not execute instant SQL statement", e);
		}finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public List<SupporterRecord> getSupporterData(String sql) {
		Connection conn = null;
		ResultSet res=null;
		List<SupporterRecord> supporters=new ArrayList<SupporterRecord>();
		try {
			conn = dbConnection();
			Statement statement = conn.createStatement();
			log.info("Executing the sql script...");
			res = statement.executeQuery(sql);
//			conn.commit();
			res.next();
			do{
				supporters.add(SupporterRecord.builder()
						.supporterNumber(res.getString(1))
						.DOB(res.getString(2))
						.postCode(res.getString(3))
						.startDate(res.getString(6))
						.endDate(res.getString(7))
						.build());
			}while(res.next());
			return supporters;
		} catch (SQLException e) {
			log.error("Could not execute instant SQL statement", e);
			Assert.fail();
			return null;
		} finally {
			log.info("Closing the connection");
			closeConnectionQuietly(conn);
		}
	}

	public String getAllData(String salStatement, String columnLabel, String columnLabel2,  String columnLabel3 ,  String columnLabel4 ,  String columnLabel5 ,  String columnLabel6 ,  String columnLabel7 ,  String columnLabel8 ,  String columnLabel9 ,  String columnLabel10 ) {
		Connection conn = null;
		try {
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			APPLICATION_LOGS.info("Executing the sql script to get the " + columnLabel + columnLabel2 +  columnLabel3 + columnLabel4 + columnLabel5 + columnLabel6 + columnLabel7 + columnLabel8 + columnLabel9 + columnLabel10);
			ResultSet res = statement.executeQuery(salStatement);

			conn.commit();
			res.next();
			String result = res.getString(columnLabel + columnLabel2 + columnLabel3 + columnLabel4 + columnLabel5 + columnLabel6 + columnLabel7 + columnLabel8 + columnLabel9 + columnLabel10);
			APPLICATION_LOGS.info("The  " + columnLabel + columnLabel2 + columnLabel3 + columnLabel4 + columnLabel5 + columnLabel6 + columnLabel7 + columnLabel8 + columnLabel9 + columnLabel10 + " is " + result);
			return result;
		} catch (SQLException e) {
			APPLICATION_LOGS.error("Could not execute instant SQL statement", e);
		} finally {
			APPLICATION_LOGS.info("Closing the connection");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public  HashMap<String,String> getSupporterDetailsFromDB(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			APPLICATION_LOGS.info("Executing sql script to get the supporter data from db......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			conn.commit();
			res.next();
			HashMap<String, String> supporterDataFromCRM = new HashMap<>();
			supporterDataFromCRM.put("supporter_number",res.getString("supporter_number"));
			supporterDataFromCRM.put("title",res.getString("title"));
			supporterDataFromCRM.put("first_name",res.getString("first_name"));
			supporterDataFromCRM.put("last_name",res.getString("last_name"));
			supporterDataFromCRM.put("address1",res.getString("address1"));
			supporterDataFromCRM.put("gift_aid",res.getString("gift_aid"));
			supporterDataFromCRM.put("amount",res.getString("amount"));
			supporterDataFromCRM.put("ss",res.getString("ss"));
			supporterDataFromCRM.put("creation_date",res.getString("creation_date"));
			supporterDataFromCRM.put("email",res.getString("email"));
			return supporterDataFromCRM;
		} catch(Exception e){
			APPLICATION_LOGS.error("Could not execute instant SQL statement", e);
		}finally {
			APPLICATION_LOGS.info("Closing the connection");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public String getData(String salStatement, String columnLabel) {
		Connection conn = null;
		try {
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			APPLICATION_LOGS.info("Executing the sql script to get the " + columnLabel);
			ResultSet res = statement.executeQuery(salStatement);

			conn.commit();
			res.next();
			String result = res.getString(columnLabel);
			APPLICATION_LOGS.info("The  " + columnLabel + " is " + result);
			return result;
		} catch (SQLException e) {
			APPLICATION_LOGS.error("Could not execute instant SQL statement", e);
		} finally {
			APPLICATION_LOGS.info("Closing the connection");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getMembershipDetailsFromCrmDB(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			conn.commit();
			res.next();
			HashMap<String, String> membershipDetailsFromCrm = new HashMap<>();
			membershipDetailsFromCrm.put("title",res.getString("title"));
			membershipDetailsFromCrm.put("first_name",res.getString("first_name"));
			membershipDetailsFromCrm.put("last_name",res.getString("last_name"));
			membershipDetailsFromCrm.put("dob",res.getString("dob"));
			membershipDetailsFromCrm.put("address",res.getString("address"));
			membershipDetailsFromCrm.put("postcode",res.getString("postcode"));
			membershipDetailsFromCrm.put("country",res.getString("country"));
			membershipDetailsFromCrm.put("status",res.getString("status"));
			membershipDetailsFromCrm.put("email",res.getString("email"));
			membershipDetailsFromCrm.put("phone_number",res.getString("phone_number"));
			membershipDetailsFromCrm.put("channel",res.getString("channel"));
			membershipDetailsFromCrm.put("mem_start_date",res.getString("mem_start_date"));
			membershipDetailsFromCrm.put("mem_end_date",res.getString("mem_end_date"));
			membershipDetailsFromCrm.put("mem_first_joined",res.getString("mem_first_joined"));
			membershipDetailsFromCrm.put("payment_freq",res.getString("payment_freq"));
			membershipDetailsFromCrm.put("pay_method",res.getString("pay_method"));
			membershipDetailsFromCrm.put("mem_status",res.getString("mem_status"));
			membershipDetailsFromCrm.put("source",res.getString("source"));
			membershipDetailsFromCrm.put("mem_type_desc",res.getString("mem_type_desc"));
			membershipDetailsFromCrm.put("mem_type",res.getString("mem_type"));
			membershipDetailsFromCrm.put("bank_account_num",res.getString("bank_account_num"));
			membershipDetailsFromCrm.put("sortcode",res.getString("sortcode"));
			return membershipDetailsFromCrm;
		}catch (Exception e){
			log.error("Could not execute instant SQL statement", e);
		}finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getContactPreferencesFromCrmDB(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			conn.commit();
			res.next();
			HashMap<String, String> membershipDetailsFromCrm = new HashMap<>();
			membershipDetailsFromCrm.put("",res.getString("title"));
			membershipDetailsFromCrm.put("first_name",res.getString("first_name"));
			membershipDetailsFromCrm.put("last_name",res.getString("last_name"));
			membershipDetailsFromCrm.put("dob",res.getString("dob"));
			membershipDetailsFromCrm.put("address",res.getString("address"));
			membershipDetailsFromCrm.put("postcode",res.getString("postcode"));
			membershipDetailsFromCrm.put("country",res.getString("country"));
			membershipDetailsFromCrm.put("status",res.getString("status"));
			membershipDetailsFromCrm.put("email",res.getString("email"));
			membershipDetailsFromCrm.put("phone_number",res.getString("phone_number"));
			membershipDetailsFromCrm.put("channel",res.getString("channel"));
			membershipDetailsFromCrm.put("mem_start_date",res.getString("mem_start_date"));
			membershipDetailsFromCrm.put("mem_end_date",res.getString("mem_end_date"));
			membershipDetailsFromCrm.put("mem_first_joined",res.getString("mem_first_joined"));
			membershipDetailsFromCrm.put("payment_freq",res.getString("payment_freq"));
			membershipDetailsFromCrm.put("pay_method",res.getString("pay_method"));
			membershipDetailsFromCrm.put("mem_status",res.getString("mem_status"));
			membershipDetailsFromCrm.put("source",res.getString("source"));
			membershipDetailsFromCrm.put("mem_type_desc",res.getString("mem_type_desc"));
			membershipDetailsFromCrm.put("mem_type",res.getString("mem_type"));
			membershipDetailsFromCrm.put("bank_account_num",res.getString("bank_account_num"));
			membershipDetailsFromCrm.put("sortcode",res.getString("sortcode"));
			return membershipDetailsFromCrm;
		}catch (Exception e){
			log.error("Could not execute instant SQL statement", e);
		}finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

	public HashMap<String, String> getMembershipDetailsFromCrmDBForCardPayments(String sqlStatement)
	{
		Connection conn = null;
		try{
			conn = CRMDBConnection();
			Statement statement = conn.createStatement();
			log.info("Executing SQL to get membership details from CRM DB......"+ sqlStatement);
			ResultSet res = statement.executeQuery(sqlStatement);
			conn.commit();
			res.next();
			HashMap<String, String> membershipDetailsFromCrm = new HashMap<>();
			membershipDetailsFromCrm.put("title",res.getString("title"));
			membershipDetailsFromCrm.put("first_name",res.getString("first_name"));
			membershipDetailsFromCrm.put("last_name",res.getString("last_name"));
			membershipDetailsFromCrm.put("dob",res.getString("dob"));
			membershipDetailsFromCrm.put("address",res.getString("address"));
			membershipDetailsFromCrm.put("postcode",res.getString("postcode"));
			membershipDetailsFromCrm.put("country",res.getString("country"));
			membershipDetailsFromCrm.put("status",res.getString("status"));
			membershipDetailsFromCrm.put("email",res.getString("email"));
			membershipDetailsFromCrm.put("phone_number",res.getString("phone_number"));
			membershipDetailsFromCrm.put("channel",res.getString("channel"));
			membershipDetailsFromCrm.put("mem_start_date",res.getString("mem_start_date"));
			membershipDetailsFromCrm.put("mem_end_date",res.getString("mem_end_date"));
			membershipDetailsFromCrm.put("mem_first_joined",res.getString("mem_first_joined"));
			membershipDetailsFromCrm.put("payment_freq",res.getString("payment_freq"));
			membershipDetailsFromCrm.put("pay_method",res.getString("pay_method"));
			membershipDetailsFromCrm.put("mem_status",res.getString("mem_status"));
			membershipDetailsFromCrm.put("source",res.getString("source"));
			membershipDetailsFromCrm.put("mem_type_desc",res.getString("mem_type_desc"));
			membershipDetailsFromCrm.put("mem_type",res.getString("mem_type"));
			return membershipDetailsFromCrm;
		}catch (Exception e){
			log.error("Could not execute instant SQL statement", e);
		}finally {
			log.info("Closing DB connection...");
			closeConnectionQuietly(conn);
		}
		return null;
	}

//	public HashMap<String, String> getContactPreferenceDetailsFromCrmDB(String sqlStatement)
//	{
//		Connection conn = null;
//		try{
//			conn = CRMDBConnection();
//			Statement statement = conn.createStatement();
//			log.info("Executing SQL to get membership details from CRM DB......"+ sqlStatement);
//			ResultSet res = statement.executeQuery(sqlStatement);
//			conn.commit();
//			res.next();
//			HashMap<String, String> preferenceDetailsFromCrm = new HashMap<>();
//			preferenceDetailsFromCrm.put("YEM",res.getString("YEM"));
//			preferenceDetailsFromCrm.put("YTE",res.getString("YTE"));
//			preferenceDetailsFromCrm.put("YPO",res.getString("YPO"));
//			preferenceDetailsFromCrm.put("SS",res.getString("SS"));
//
//			return preferenceDetailsFromCrm;
//		}catch (Exception e){
//			log.error("Could not execute instant SQL statement", e);
//		}finally {
//			log.info("Closing DB connection...");
//			closeConnectionQuietly(conn);
//		}
//		return null;
//	}


}


