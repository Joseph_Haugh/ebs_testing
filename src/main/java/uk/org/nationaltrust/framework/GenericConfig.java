package uk.org.nationaltrust.framework;

/**
 * @Pramod
 */
public interface GenericConfig {


	static final String sauceUser = "nick_thompson";
	static final String sauceKey = "20b08669-fb20-409c-ace4-975fbfc10313";
	static final String LOGS_PATH = EnvironmentConfiguration.toFilePath(System.getProperty("user.dir"), "/target/screenshots/");
	public static final String CHROME_DRIVE_RPATH = (System.getProperty("user.dir") + "//browserdrivers//chromedriver.exe");
	public static final String IE_DRIVER_PATH = (System.getProperty("user.dir") + "//browserdrivers//IEDriverServer.exe");
	static final String baseUrl = EnvironmentConfiguration.getBaseURL();
	static final String baseUrlMYNT = EnvironmentConfiguration.getBaseURLMYNT();
	static final String CPCUrl = EnvironmentConfiguration.getCPCURL();
	static final String CPCShopUrl = EnvironmentConfiguration.getCPCShopURL();
	static final String CPCDMUrl = EnvironmentConfiguration.getCPCDMURL();
	static final String CPCBadTokenUrl = EnvironmentConfiguration.getCPCBadTokenURL();
	public static final String REST_SERVICE_URL = EnvironmentConfiguration.getRestServiceUrl();



}
