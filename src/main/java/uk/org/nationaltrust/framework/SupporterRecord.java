package uk.org.nationaltrust.framework;

import lombok.Builder;
import lombok.Data;

/**
 * Created by Sowjanya Annepu
 */

@Builder
@Data
public class SupporterRecord {
    // TODO change fields
    private String amount;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String emailPref;
    private String postPref;
    private String phonePref ;
    private String giftAidConsent ;
    private String donationType ;
    private String relation;
    private String subject;
    private String occasion;
    private String fundCode;
    private String sourceCode;
    private String tone;
    private String crmTransactionID;
    private String submissionStatus;
    private String supporterNumber;
    private String DOB;
    private String postCode;
    private String endDate;
    private String startDate;
    private String promocode;
    private String fundraisingDiscoverySource;
    private String fundraisingDiscoverySourceOther;
    private String fundraisingReason;
    private String fundraisingReasonOther;
    private String confirmAge;

}