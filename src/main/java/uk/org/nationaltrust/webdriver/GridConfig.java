package uk.org.nationaltrust.webdriver;

import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * @author Pramod
 */
public class GridConfig {

	private static DesiredCapabilities desktopCapbility = null;

	private static DesiredCapabilities mobileCapbility = null;

	private static Platform platform = null;

	public static DesiredCapabilities getGridCaps(WebDriverConfigBean config) {
		switch (config.getSauceDeviceType()) {
			case "desktop":
				return getDesktopCaps(config.getSauceOS(), config.getBrowserType());
			case "mobile":
				return getMobileCaps(config.getSauceDevice(), config.getSauceDeviceVersion(), config.getSauceDeviceName());
			default:
				throw new IllegalArgumentException("Unknown device type " + config.getSauceDeviceType() +
						" (expected 'desktop' or 'mobile')");
		}
	}

	private static DesiredCapabilities getMobileCaps(String deviceName, String deviceVersion, String platform) {
		return null;
	}

	private static DesiredCapabilities getDesktopCaps(String platform, String browserName) {
		switch (browserName.toUpperCase()) {
			case "CHROME":
				desktopCapbility = DesiredCapabilities.chrome();
				desktopCapbility.setPlatform(getPlatform(platform));
				return desktopCapbility;
			case "FIREFOX":
				desktopCapbility = DesiredCapabilities.firefox();
				desktopCapbility.setPlatform(getPlatform(platform));
				return desktopCapbility;
			case "IE":
				desktopCapbility = DesiredCapabilities.internetExplorer();
				desktopCapbility.setPlatform(getPlatform(platform));
				return desktopCapbility;
			case "SAFARI":
				desktopCapbility = DesiredCapabilities.safari();
				desktopCapbility.setPlatform(getPlatform(platform));
				return desktopCapbility;

		}
		return null;
	}

	private static Platform getPlatform(String platform) {

		switch (platform.toUpperCase()) {
			case "WINDOWS":
				return Platform.WINDOWS;

			case "WINDOWS8":
				return Platform.WIN8;

			case "WINDOWS8_1":
				return Platform.WIN8_1;

			case "WINDOWS10":
				return Platform.WIN10;

			case "LINUX":
				return Platform.LINUX;

			case "MAC":
				return Platform.MAC;

			default:
				throw new IllegalArgumentException("Unsupported Platform type: " + platform +
						"( expected 'windows', 'linux', or 'Mac')");

		}
	}

}
