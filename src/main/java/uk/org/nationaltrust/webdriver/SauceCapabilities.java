package uk.org.nationaltrust.webdriver;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * @author Pramod
 */
public class SauceCapabilities {

	private static DesiredCapabilities sauceMobileCaps = null;

	private static DesiredCapabilities desktopCaps = null;

	private static DesiredCapabilities getMobileCaps(String device, String version, String deviceModel) {
		switch (device.toUpperCase()) {
			case "IPHONE":
				sauceMobileCaps = DesiredCapabilities.iphone();
				sauceMobileCaps.setCapability("appiumVersion", "1.4.13");
				//sauceMobileCaps.setCapability("platform", "OS X 10.10");
				sauceMobileCaps.setCapability("deviceName", "iPhone " + deviceModel);
				sauceMobileCaps.setCapability("platformName", "iOS");
				sauceMobileCaps.setCapability("platformVersion", version);
				sauceMobileCaps.setCapability("browserName", "Safari");
				sauceMobileCaps.setCapability("deviceOrientation", "portrait");
				sauceMobileCaps.setCapability("deviceOrientation", "portrait");
				break;
			case "ANDROID":
				sauceMobileCaps = DesiredCapabilities.android();
				sauceMobileCaps.setCapability("appiumVersion", "1.4.13");
				sauceMobileCaps.setCapability("deviceName", deviceModel);
				sauceMobileCaps.setCapability("deviceOrientation", "portrait");
				sauceMobileCaps.setCapability("browserName", "Browser");
				sauceMobileCaps.setCapability("platformVersion", version);
				sauceMobileCaps.setCapability("platformName", "Android");
				sauceMobileCaps.setCapability("acceptSslCerts", true);
				break;
			default:
				throw new IllegalArgumentException("Unknown device: " + device +
						" expected 'iphone' or 'android')");
		}
		return sauceMobileCaps;
	}

	private static DesiredCapabilities getDesktopCaps(String os, String version, String browserName) {
		desktopCaps = new DesiredCapabilities();
		desktopCaps.setCapability(CapabilityType.BROWSER_NAME, browserName);
		if (version != null) {
			desktopCaps.setCapability(CapabilityType.VERSION, version);
		}
		desktopCaps.setCapability(CapabilityType.PLATFORM, os);
		return desktopCaps;
	}

	public static DesiredCapabilities getSauceCaps(WebDriverConfigBean config) {
		switch (config.getSauceDeviceType()) {
			case "desktop":
				return getDesktopCaps(config.getSauceOS(), config.getSauceDeviceVersion(), config.getBrowserType());
			case "mobile":
				return getMobileCaps(config.getSauceDevice(), config.getSauceDeviceVersion(), config.getSauceDeviceName());
			default:
				throw new IllegalArgumentException("Unknown device type " + config.getSauceDeviceType() +
						" (expected 'desktop' or 'mobile')");
		}
	}

	private SauceCapabilities() {
		// Purely static functionality
	}

}
