package uk.org.nationaltrust.webdriver;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Pramod
 */
public class WebDriverInvoker  implements InvocationHandler {

	private static final String QUIT_METHOD = "quit";

	private static final String CLOSE_METHOD = "close";

	private static String GET_SESSIONID_METHOD = "getSessionId";

	private Logger log = LoggerFactory.getLogger(getClass());

	private ThreadLocal<WebDriver> threadDriver;

	public WebDriverInvoker(ThreadLocal<WebDriver> threadDriver) {
		this.threadDriver = threadDriver;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		String methodName = method.getName();
		Object underlyingDriver = threadDriver.get();

		if (underlyingDriver == null) {
			throw new IllegalStateException("WebDriver has not been initialised (in this thread)");
		}

		if (methodName.equals(GET_SESSIONID_METHOD)) {
			return getSessionIdKludgily(underlyingDriver);
		} else {
			return invokeNormally(method, args, underlyingDriver);
		}
	}

	private Object invokeNormally(Method method, Object[] args, Object underlyingDriver) throws Throwable {
		String methodName = method.getName();

		try {
			log.debug("Invoking webDriver#" + methodName);
			return method.invoke(underlyingDriver, args);
		} finally {
			switch (methodName) {
				case QUIT_METHOD:
					log.debug("WebDriver quit, dropping instance for this thread");
					threadDriver.remove();
					break;
				case CLOSE_METHOD:
					log.error("Window close, I don't know whether to drop the browser or not! So I didn't. Please stop doing that.");
					break;
				default:
					break;
			}
		}
	}

	private Object getSessionIdKludgily(Object underlyingDriver) {
		// RemoteWebDriver doesn't expose this via any useful interface, so we
		// must access it via knowledge of its real type which we shouldn't really have
		SessionId sessionId = ((RemoteWebDriver) underlyingDriver).getSessionId();
		return sessionId;
	}


}
