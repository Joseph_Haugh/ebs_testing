package uk.org.nationaltrust.webdriver;

import org.openqa.selenium.remote.SessionId;

/**
 * Created by Administrator on 26/05/2016.
 */
public interface SessionIdAccessor {

	public SessionId getSessionId();

}
