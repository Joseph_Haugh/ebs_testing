package uk.org.nationaltrust.webdriver;

/**
 * @author Pramod
 */
public enum DeviceProfiles {

	IPHONE_6SPLUS("mobile", "IOS", "iphone", "6 Plus", "9.2"),
	ANDROID_EMULATOR_5_1("mobile", "Android", "android", "Android Emulator", "5.1"),

	//grid desktop profiles
	GRID_MAC("desktop", "MAC", "Mac", "Mac", "9.0"),
	GRID_LINUX_FIREFOX("desktop", "LINUX", "Linux","Linux", " "),
	GRID_LINUX_CHROME("desktop", "LINUX", "Linux","Linux", " "),

	//Sauce desktop profiles
	LINUX_FIREFOX_45("desktop", "Linux", "Dell", "Linux_Redhat", "45.0"),
	LINUX_CHROME_45("desktop", "Linux", "Dell", "Linux_Redhat", "48.0"),
	LINUX_OPERA_12("desktop", "Linux", "Dell", "Linux_Redhat", "12.15"),
	WINDOWS_7_IE10("desktop", "Windows 7", "Lenovo", "Think_Pad", "10.0"),
	WINDOWS_10_CHROME("desktop", "Windows 10", "Lenovo", "Think_Pad", "49.0"),

    //browserstack profiles
    BS_WIN_7_CHROME_59("desktop","Windows","7","Chrome","59.0"),
    BS_WIN_7_FF_54("desktop","Windows","7","Firefox","54.0"),
    BS_WIN_7_IE_11("desktop","Windows","7","IE","11.0"),
    BS_MAC_YOSEMITE_CHROME_59("desktop","OS X","Yosemite","Chrome","59.0"),
	BS_MAC_HIGH_SIERRA_CHROME_74("desktop","OS X","High Sierra","Chrome","76.0");


	private String device;

	private String os;

	private String deviceManufacturer;

	private String deviceModel;

	private String osVerison;

	private String browserVerison;

	private String browser;


    private DeviceProfiles(String deviceType, String os, String osVersion, String browser, String browserVerison){
		this.device = deviceType;
		this.os = os;
		this.osVerison = osVersion;
		this.browser = browser;
		this.browserVerison = browserVerison;
//		this.deviceManufacturer = deviceName;
//
//		this.deviceModel = model;
//		this.osVerison = version;
	}

	public String getOs() {
		return os;
	}

	public String getDeviceManufacturer() {
		return deviceManufacturer;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public String getDeviceType() {
		return device;
	}

	public String getOsVersion() {
		return osVerison;
	}

	public String getBrowserVersion() {
		return browserVerison;
	}
}
