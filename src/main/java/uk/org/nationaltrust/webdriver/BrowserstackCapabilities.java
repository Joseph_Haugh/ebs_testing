package uk.org.nationaltrust.webdriver;

import org.openqa.selenium.Platform;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * @author by Ashok Kesani on 09/08/2017.
 */

public class BrowserstackCapabilities {

	private static Platform desktopPlatform = null;

	private static Platform mobilePlatform = null;

	private static DesiredCapabilities MobileCaps = null;

	private static DesiredCapabilities desktopCaps = null;

	private BrowserstackCapabilities() {
		// Purely static functionality
	}

	private static DesiredCapabilities getMobileCaps(String device, String version, String deviceModel) {
		switch (device.toUpperCase()) {
			case "IPHONE":
				MobileCaps = DesiredCapabilities.iphone();
				MobileCaps.setCapability("appiumVersion", "1.6.3");
				MobileCaps.setCapability("platformName", "iOS");
				MobileCaps.setCapability("browserName", "Safari");
				MobileCaps.setCapability("platformVersion", version);
				MobileCaps.setCapability("deviceName", "iPhone " + deviceModel);
				MobileCaps.setCapability("deviceOrientation", "portrait");
				MobileCaps.setCapability("autoAcceptAlerts", true);
				break;
			case "ANDROID":
				MobileCaps = DesiredCapabilities.android();
				MobileCaps.setCapability("appiumVersion", "1.5.3");
				MobileCaps.setCapability("deviceOrientation", "portrait");
				MobileCaps.setCapability("deviceName", deviceModel);
				MobileCaps.setCapability("browserName", "Browser");
				MobileCaps.setCapability("platformVersion", version);
				MobileCaps.setCapability("platformName", "Android");
				MobileCaps.setCapability("autoAcceptAlerts", true);
				break;

			default:
				throw new IllegalArgumentException("Unknown device: " + device +
						" expected 'iphone' or 'android')");
		}
		return MobileCaps;
	}

	public static Platform getPlatform(String newPlatform) {
		if (newPlatform.equals("desktop")) {
			return desktopPlatform;
		} else if (newPlatform.equals("mobile")) {
			return mobilePlatform;
		}
		throw new RuntimeException("Unknown platform: " + newPlatform + " (expected 'desktop' or 'mobile')");
	}

	private static DesiredCapabilities getDesktopCaps(String os, String osVersion, String browserVersion, String browserName) {
		switch (browserName.toUpperCase()){
			case "IE":
				desktopCaps = DesiredCapabilities.internetExplorer();
				desktopCaps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				break;
			case "FIREFOX":
				desktopCaps = DesiredCapabilities.firefox();
				break;
			case "CHROME":
				desktopCaps = DesiredCapabilities.chrome();
				break;
			case "SAFARI":
				desktopCaps = DesiredCapabilities.safari();
				break;
			case "EDGE":
				desktopCaps = DesiredCapabilities.edge();
				break;
			default:
				throw new IllegalArgumentException("Unknown browser Type: " + browserName +
						" expected 'ie' or 'firefox' or 'chrome' OR 'edge')");
		}
		desktopCaps.setCapability("browser", browserName);
		desktopCaps.setCapability("browser_version", browserVersion);
		desktopCaps.setCapability("os", os);
		desktopCaps.setCapability("os_version", osVersion);
		desktopCaps.setCapability("browserstack.debug", "true");
		desktopCaps.setCapability("browserstack.local","true");
		return desktopCaps;
	}

	public static DesiredCapabilities getCaps(WebDriverConfigBean config) {
		switch (config.getDeviceType()) {
			case "desktop":
				return getDesktopCaps(config.getOS(), config.getOsVersion() ,config.getBrowserVersion(), config.getBrowserType());
			case "mobile":
				return getMobileCaps(config.getDevice(), config.getBrowserVersion(), config.getDeviceName());
			default:
				throw new IllegalArgumentException("Unknown device type " + config.getDeviceType() +
						" (expected 'desktop' or 'mobile')");
		}
	}
}
