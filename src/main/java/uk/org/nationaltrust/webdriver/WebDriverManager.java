package uk.org.nationaltrust.webdriver;

import static java.lang.reflect.Proxy.newProxyInstance;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.internal.FindsByClassName;
import org.openqa.selenium.internal.FindsByCssSelector;
import org.openqa.selenium.internal.FindsById;
import org.openqa.selenium.internal.FindsByLinkText;
import org.openqa.selenium.internal.FindsByName;
import org.openqa.selenium.internal.FindsByTagName;
import org.openqa.selenium.internal.FindsByXPath;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.nationaltrust.framework.EnvironmentConfiguration;
import uk.org.nationaltrust.framework.Helpers;
import uk.org.nationaltrust.framework.TestBase;


/**
 *  @author Pramod
 */
public class WebDriverManager {

	private static final Class<?>[] WEBDRIVER_INTERFACES =
			{
					WebDriver.class, JavascriptExecutor.class,
					FindsById.class, FindsByClassName.class, FindsByLinkText.class, FindsByName.class,
					FindsByCssSelector.class, FindsByTagName.class, FindsByXPath.class,
					HasInputDevices.class, HasCapabilities.class, TakesScreenshot.class,
					SessionIdAccessor.class
			};

	private static Logger log = LoggerFactory.getLogger(WebDriverManager.class);

	private static ThreadLocal<WebDriver> threadDriver = new ThreadLocal<>();

	private static WebDriverManager INSTANCE = new WebDriverManager();

	private String hubUrl = "http://sel01.inf.nt.ad.local:4444/wd/hub";

	public static WebDriver openBrowser(WebDriverConfigBean config, Class<?> invokingClass) {
		return INSTANCE.createBrowser(config, invokingClass);
	}

	private WebDriver createBrowser(WebDriverConfigBean config, Class<?> invokingClass) {
		log.debug("Running the test in " + config.getDeploymentEnvironment() + " environment - opening browser " + config.getBrowserType());
		WebDriver underlyingWebDriver = createUnderlyingBrowser(config, invokingClass);
		threadDriver.set(underlyingWebDriver);
		WebDriver proxy = createProxy();
		log.debug("Created threadsafe web-driver proxy " + proxy.toString());
		return proxy;
	}

	private WebDriver createProxy() {
		return (WebDriver) newProxyInstance(WebDriverManager.class.getClassLoader(),
				WEBDRIVER_INTERFACES,
				new WebDriverInvoker(threadDriver));
	}

	private WebDriver createUnderlyingBrowser(WebDriverConfigBean config, Class<?> invokingClass) {
		WebDriver driver;
		switch (config.getSeleniumMode().toUpperCase()) {
			case "SINGLE":
				driver = openLocalBrowser(config);
			//	driver.manage().window().maximize();
				break;
			case "SAUCE":
				driver = openSauceBrowser(config, invokingClass);
				// Don't maximise if running on a phone
				if (config.isSauceDesktopOS()) {
					driver.manage().window().maximize();
				}
				break;
			case "BROWSERSTACK":
				driver = openBrowserStackBrowser(config,invokingClass);
				//driver.manage().window().maximize();
				break;

			case "GRID":
				driver = initialiseGrid(config, invokingClass);
				if (config.isSauceDesktopOS()) {
					driver.manage().window().maximize();
					// Don't maximise if running on a phone
				}
				break;
			default:
				throw new IllegalArgumentException("Unknown operation mode: " + config.getSeleniumMode());
		}
		return driver;

	}

	private WebDriver openLocalBrowser(WebDriverConfigBean config) {
		try {
			switch (config.getBrowserType().toUpperCase()) {
				case "CHROME":
					System.setProperty("webdriver.chrome.driver", EnvironmentConfiguration.getChromeDriverPath());
					return new ChromeDriver();
				case "FIREFOX":
					return new FirefoxDriver();
				case "IE":
					System.setProperty("webdriver.ie.driver", EnvironmentConfiguration.getInternetExplorerDriverPath());
					return new InternetExplorerDriver();
				default:
					throw new IllegalArgumentException("Unsupported browser type: " + config.getBrowserType() +
							"( expected 'firefox', 'chrome', or 'ie')");
			}
		} catch (Exception e) {
			log.error("Failed to open browser " + config.getBrowserType(), e);
			throw new IllegalStateException(e);
		}

	}

	private WebDriver openSauceBrowser(WebDriverConfigBean config, Class<?> invokingClass) {

		String sauceUserName = EnvironmentConfiguration.getSauceUser();
		String sauceUserPassword = EnvironmentConfiguration.getSauceKey();
		String jobName = config.getSauceDevice() + "_" +
				config.getSauceDeviceVersion() + "_" +
				invokingClass.getSimpleName().toString();
		String sauceUrl = "http://" + sauceUserName + ":" + sauceUserPassword + "@ondemand.saucelabs.com:80/wd/hub";
		try {
			DesiredCapabilities caps = SauceCapabilities.getSauceCaps(config);
			caps.setCapability("tunnelIdentifier", "ntsc2");
			caps.setCapability("name", jobName);
			return new RemoteWebDriver(new URL(sauceUrl), caps);
		} catch (MalformedURLException e) {
			throw new IllegalStateException("Malformed SauceLabs WebDriver URL: " + sauceUrl, e);
		}
	}

	private WebDriver openBrowserStackBrowser(WebDriverConfigBean config, Class<?> invokingClass){
		String browserStackUserName = EnvironmentConfiguration.getBrowserstackUser();
		String browserStackKey = EnvironmentConfiguration.getBrowserstackAccessKey();
		String browserstackUrl = "https://" + browserStackUserName + ":" + browserStackKey + "@hub-cloud.browserstack.com/wd/hub";
		String jobName = invokingClass.getSimpleName().toString() + "_" + config.getBrowserType()+"_"+ Helpers.getDateTimeNow("dd-MM-yyyy").replace("-","_");

		try{
			String browserstackLocalIdentifier = System.getenv("BROWSERSTACK_LOCAL_IDENTIFIER");
			DesiredCapabilities caps = BrowserstackCapabilities.getCaps(config);
//			caps.setCapability("localIdentifier", TestBase.bsLocalIdentifier);
			caps.setCapability("build", jobName);
			caps.setCapability("browserstack.autoWait", "0");
			caps.setCapability("project", TestBase.suiteName);
			caps.setCapability("browserstack.localIdentifier", browserstackLocalIdentifier);
//			caps.setCapability("browserstack.local", "true");
			//caps.setCapability("name", testName);
			return new RemoteWebDriver(new URL(browserstackUrl),caps);
		}catch (MalformedURLException e){
			throw new IllegalStateException("Malformed browserstack WebDriver URL: " + browserstackUrl, e);
		}
	}

	private WebDriver initialiseGrid(WebDriverConfigBean config, Class<?> invokingClass) {

		try {
			DesiredCapabilities caps = GridConfig.getGridCaps(config);
			//			caps.setCapability(CapabilityType.BROWSER_NAME, config.getBrowserType());
			//			caps.setCapability(CapabilityType.PLATFORM, Platform.ANY);

			return new RemoteWebDriver(new URL(hubUrl), caps);
		} catch (MalformedURLException e) {
			throw new IllegalStateException("Malformed Selenium hub URL: " + hubUrl, e);
		}
	}

}
