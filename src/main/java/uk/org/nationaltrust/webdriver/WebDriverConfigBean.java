package uk.org.nationaltrust.webdriver;

/**
 * @author Pramod
 */
public class WebDriverConfigBean {

	private String deploymentEnvironment;

	private String browserType;

	private String seleniumMode;

	private String seleniumServer;

	private String browserVerison;

	private String osVersion;

	private DeviceProfiles deviceProfile;

	public static WebDriverConfigBean aWebDriverConfig() {
		return new WebDriverConfigBean()
				.withSeleniumMode("SINGLE")
				.withSeleniumServer("local");
	}

	private WebDriverConfigBean() {
		// Use the static method, it's cleaner
	}

	public WebDriverConfigBean withBrowser(String browser) {
		this.browserType = browser;
		return this;
	}

	public WebDriverConfigBean withDeploymentEnvironment(String env) {
		this.deploymentEnvironment = env;
		return this;
	}

	public WebDriverConfigBean withSeleniumMode(String mode) {
		this.seleniumMode = mode;
		return this;
	}

	public WebDriverConfigBean withSeleniumServer(String server) {
		this.seleniumServer = server;
		return this;
	}

	public WebDriverConfigBean withSeleniumDeviceProfile(String profile) {
		this.deviceProfile = DeviceProfiles.valueOf(profile);
		return this;
	}

	public String getDeploymentEnvironment() {
		return deploymentEnvironment;
	}

	public String getBrowserType() {
		return browserType;
	}

	public String getSeleniumMode() {
		return seleniumMode;
	}

	public String getSauceOS() {
		String sauceOS = deviceProfile.getOs();
		return sauceOS;
	}

	public String getSauceDeviceType() {
		String sauceDeviceType = deviceProfile.getDeviceType();
		return sauceDeviceType;
	}

	public String getSauceDevice() {
		//return sauceDevice;

		return deviceProfile.getDeviceManufacturer();
	}

	public String getSauceDeviceVersion() {
		String sauceDeviceVersion = deviceProfile.getOsVersion();
		return sauceDeviceVersion;
	}

	public String getSauceDeviceName() {
		return deviceProfile.getDeviceModel();
	}

	public boolean isSauceDesktopOS() {
		switch (getSauceOS().toUpperCase()) {
			case "WINDOWS":
			case "LINUX":
			case "MAC":
				return true;
			// FIXME we ought to enumerate the known non-desktop cases, and default to throwing for unknown OS
			default:
				return false;
		}
	}

    public String getDeviceName() {
        return deviceProfile.getDeviceModel();
    }

	/*browser stack details*/
	public String getOS() {
		String os = deviceProfile.getOs();
		return os;
	}

	public String getOsVersion(){
		String osVersion = deviceProfile.getOsVersion();
		return  osVersion;
	}

	public String getDeviceType() {
		String deviceType = deviceProfile.getDeviceType();
		return deviceType;
	}

	public String getDevice() {
		return deviceProfile.getDeviceManufacturer();
	}

	public String getBrowserVersion() {
		String browserVersion = deviceProfile.getBrowserVersion();
		return browserVersion;
	}




}
